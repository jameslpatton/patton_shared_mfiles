%%% DummyTest: Regression comparisons of learning function approximations
%************** MATLAB "M" function (PATTON)  *************
% Dummy data exploration- diffferent regressions on learning-like-data 
% This compares 4 functions that approximate a learning curve, tests how 
% well it might do on articially constructed data. The data is noisy. It
% assumes 
%   y=p1+p2*exp(-x/p3)
% this is the root program to run to test regression and there user 
% interface; you must change the paramters below to see differences. 
% SEE ALSO:   expRegressionViaLogXform, expDecay
%
% REVISIONS:  8/2011 (patton) initiated
%~~~~~~~~~~~~~~~~~~~~~ Begin: ~~~~~~~~~~~~~~~~~~~~~~~
 
% setup
figure(1); clf
figure(2); clf
msz=5; % markersizze

%% make dummy 
t=(1:400)';
p=[15 100 25] % init the [final SS value, change, and time constant] 
nz=10*randn(size(t)); % noize magnitude
e=p(2).*exp(-t./p(3))+p(1)+nz;  % dummy learning curve of error
eIdeal=p(2).*exp(-t./p(3))+p(1);  % dummy learning curve of error

%% transforms
lt=log(t); % transform t
le=log(e); % transform e
N=length(t); % size of the dots
d=mean(e(N-10:N)); % steady state value
leo=log(e-d); % log of error after subtracting offset

%% intiial plots
figure(1); subplot(2,2,1); 
plot(t,e,'ko','markerSize',msz,'markerfacecolor','k'); hold on; 
xlabel('t'); ylabel('e'); title('Raw Data and each fit'); 
plot(N-5,d,'kd','markerSize',14); % final value

subplot(2,2,2); 
plot(lt,e,'ko','markerSize',msz,'markerfacecolor','k'); hold on; 
xlabel('log t'); ylabel('e'); title('Log-Transform horizontal axis');

subplot(2,2,3); 
plot(t,le,'ko','markerSize',msz,'markerfacecolor','k'); hold on; 
xlabel('t'); ylabel('log e'); title('Log-Transform vertical axis');

subplot(2,2,4); 
plot(t,leo,'ko','markerSize',msz,'markerfacecolor','k'); hold on; 
xlabel('t'); ylabel(['Subtract offset by ' num2str(d) ', then log e']); 
title({'Subtract the final mean value, ';'then Transform vertical axis'});

%% fit the curve using nonlineaar optimization
figure(2);
[p,cost,fcnExp]=expRegression(rand(1,3),t,e,'plotIt'); beep
fn=expDecay(p,t); figure(1); subplot(2,2,1); plot(t,fn,'r','linewidth',2);

%% fit using transformed data and least squares fit
b=regress(e,[lt lt./lt]);
elt=b(1)*lt+ b(2); 
subplot(2,2,2); plot(lt,elt,'g','linewidth',2); % plot transformed
subplot(2,2,1); plot(t,elt,'g','linewidth',2); % plot un-transformed

b=regress(le,[t t./t]);
el=(b(1)*t+ b(2)); 
%subplot(3,1,3); plot([0 max(t)],[b(2) b(2)+max(t)*b(1)],'b','linewidth',2); % plot transformed
subplot(2,2,3); plot(t,el,'b','linewidth',2); % plot transformed
subplot(2,2,1); plot(t,exp(el),'b','linewidth',2); % plot un-transformed

b=regress(leo,[t t./t]);
elo=(b(1)*t+ b(2)); % log of error with offset removed
%subplot(3,1,3); plot([0 max(t)],[b(2) b(2)+max(t)*b(1)],'b','linewidth',2); % plot transformed
subplot(2,2,4); plot(t,elo,'c','linewidth',2); % plot transformed
subplot(2,2,1); plot(t,exp(elo)+d,'c','linewidth',2); % plot un-transformed

plot(t,eIdeal,'y','linewidth',1); % plot ideal
