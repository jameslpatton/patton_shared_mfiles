% function to read the IMI information and make a table
% 8/28/2010 initiated by JIM PATTON

%% Expected questions: (p=0 for negative question) (G=group) (then order)
% p G order:
% 1 2     8          After working at this activity for awhile, I felt pretty competent
% 1 2     9          I am satisfied with my performance at this task
% 1 4           19   I believe doing this activity could be beneficial to me
% 1 4           20   I believe this activity could be of some value to me
% 0 3        14      I didn�t put much energy into this.
% 0 3        15      I didn�t try very hard to do well at this activity.
% 1 1  1             I enjoyed doing this activity very much
% 1 3        16      I put a lot of effort into this
% 1 4     10         I think I am pretty good at this activity
% 1 2     11         I think I did pretty well at this activity, compared to others
% 1 4           21   I think doing this activity could help me to move better
% 1 4           22   I think that doing this activity is useful for moving better
% 1 4           23   I think this is an important activity
% 1 4           24   I think this is important to do because it can make me move better
% 1 1  2             I thought this activity was quite enjoyable
% 0 1  3             I thought this was a boring activity.
% 1 3        17      I tried very hard on this activity
% 1 2     12         I was pretty skilled at this activity
% 1 4           25   I would be willing to do this again because it has some value to me
% 1 1  4             I would describe this activity as very interesting
% 1 3        18      It was important to me to do well at this task
% 0 1  5             This activity did not hold my attention at all.
% 1 1  6             This activity was fun to do
% 0 2     13         This was an activity that I couldn�t do very well.
% 1 1  7             While doing this activity, I was thinking about how much I enjoyed it 
%
% group 1: Interest/enjoyment
% group 2: Perceived competence
% group 3: Percieved motivation/effort
% group 4: Percieved value of the intervention


%% BEGIN:
function IMIchug(filename)
if ~exist('filename','var'), filename='imi.txt'; end
functionName='IMIchug.m';
clc
fprintf('\n\n\n\n ~~ %s ~~',functionName);
%C=[0 0 1]; 
C=[1 0 0; .1 .3 .1; 0 0 1; .2 .2 0];
nQ=25;  % number of questions

SUBJ=[
     1
     2
     3
     4
     5
     6
     7
     8
     9
    10
    11
    12
    13 % 14 excluded because we discovered clinical treatments
    15
    16
    17
    18
    19 
    21 % 20 withdrew before starting
    22
    23
    24
    25
    26
    %27
    ];
nSubj=length(SUBJ); % number of subjects
    
fSz=11;   % 
% C=colorcube(19);
rowIncrement=1/(nQ+1);
%close all;
figure(1); clf; set(gcf,'Color',[1 1 1]); drawnow;

%% setup charateristics of each question
G={ 'Percieved Interest//enjoyment ';                 ...
    'Percieved Competence ';               ...
    'Percieved Motivation//effort ';        ...
    'Percieved Value ' };

orderGroups=[   ...  % space-staggered to indicate colums for ea group
   8       
   9       
         19
         20
      14   
      15   
1          
      16   
   10     
   11      
         21
         22
         23
         24
2          
3          
      17   
   12      
         25
4          
      18   
5          
6          
   13      
7          
];

posQ=[...
 1
 1
 1
 1
 0
 0
 1
 1
 1
 1
 1
 1
 1
 1
 1
 0
 1
 1
 1
 1
 1
 0
 1
 0
 1];

Qgroup=[...
2 
2 
4 
4 
3 
3 
1 
3 
2 
2 
4 
4 
4 
4 
1 
1 
3 
2 
4 
1 
3 
1 
1 
2 
1 ];
[~,Qindex]=sort(orderGroups);
posQ=posQ(Qindex);
Qgroup=Qgroup(Qindex);
D=NaN*ones(nQ,nSubj);

%% loop for ea subject
subjCtr=0; % counter
for subj=SUBJ',
  subjCtr=subjCtr+1;
  %if subj==11||subj==14, subj=subj+1; end
  %if subj==14, subj=subj+1; end
  filename=['answersVREA' num2str(subj) '.txt'];
  
  H=hdrload(filename);
  firstRow=H(1,:); H(1,:)=[]; % clip first line
  h=sortrows(H); % sort alphabetically first
  fprintf('\n subj %d (%d rows) : %s :  ', subj, size(H,1), firstRow   );
  
  %% COMPARE
%   if subj>1, % comparison of this with previous set of quesions
%     size(h), size(hprev)
%     COMPARE=~(h==hprev); 
%     if sum(sum(COMPARE)), 
%       fprintf('question file not equal!\n'); 
%       COMPARE
%     end % end if sum
%   end % end if i
%   hprev=h;
  
  [len,wid]=size(h);
  h=h(Qindex,:);  % sort by question group
  
  %% extract - separate the text and the data
  for i=1:len;   % loop rows (questions)
    for j=1:wid  % loop cols 
      if h(i,j)=='=', break; end
    end
    t{i,1}=h(i,1:j-1);
    d(i,1)=str2num(h(i,j+1:wid));
    if subj>1 && ~strcmp(t{i},tLast{i}),
      fprintf('\n !! lines unequal: ')
      fprintf('\n subj %d LINE:  %s ',subj,t{i})
      fprintf('\n subj %d LINE:  %s ',subj-1,tLast{i})
    end
    %fprintf('\n%s=%f.',t{i-1},d(i-1));
  end
  tLast=t;
  
  %% plot this
  plot([0 1],[1 1]); hold on; axis off;
  text(0,1.01,'Strongly disagree* ','fontSize',fSz, ...
               'fontWeight','bold' ...
               );
  text(1,1.01, 'Strongly agree* ',  'fontSize',fSz, ...
               'horizontalAlignment','right', ...
               'fontWeight','bold' ...
               );
  axis([0 1 0 1])  
  for i=1:len;   % loop rows (questions)
    y=1-i*rowIncrement;
      plot([0 0 1 1],y*[1.01 1 1 1.01]-rowIncrement/2,'color',[.2 .2 .2]);
      if ~posQ(i), 
        t{i}=['(' t{i} ')* '];
        d(i)=1-d(i);
      end
      H=textonplot([t{i} ' '],.005,y);
    set(H,'fontSize',fSz, ...
               'HorizontalAlignment','left', ...
               'VerticalAlignment','middle', ...
               'Color',[.5 .5 .5]                );
            ...%   'Color',C(Qgroup(i),:));
    
    ym=y+rowIncrement*(-1/4+1/nQ/2*subj+1/nQ*2.5*[1 -1 -1 1]);

    patch(d(i)+.002*[-1 -1 1 1],  ym, ...
           C(Qgroup(i),:), 'EdgeColor','none','FaceAlpha',.8);
    D(i,subjCtr)=d(i);
  end  
  title('IMI Questionnaire results','Fontweight','bold',  'fontSize',fSz-1)
end
textonplot('* Answers to negative questions were inverted  ',-.01,.005);

%% STATS 
for i=1:len; 
  y=1-i*rowIncrement;
  ym=y+rowIncrement/2*[1 -1 -1 1]; 
  Dm=mean(cleanNaN(D(i,:)',0)')+confidence(cleanNaN(D(i,:)',0)',.95)*[-1 -1 1 1]; 
  patch(Dm, ym, C(Qgroup(i),:), 'EdgeColor','none','FaceAlpha',.2); 
end

vertGrpPos=[.85 .59 .38 .16];
for i=1:4
  text(-.05,vertGrpPos(i),G{i}, 'HorizontalAlignment','center',    ...
                          'VerticalAlignment','middle',       ...
                          'rotation', 90,                     ...
                          'color', C(i,:),  'fontSize',fSz-1);
end

%% assemble output file
headerTxt='Compiled IMI results for VREA';
%  list of questions
TB=char(9);
for i=1:len; 
  %fprintf('\n%d %s',i,t{i}); 
  headerTxt=str2mat(headerTxt,['Question ' num2str(i) ': ' t{i}]);
end 
labelstr=['Subject']; 
for i=1:len; 
  labelstr=[labelstr, char(9) 'Question' num2str(i)];
end
headerTxt=str2mat(headerTxt,labelstr);
% headerTxt %
% size(SUBJ)
% size(D')
% D'
mat2txt('IMIresults.txt',headerTxt,[SUBJ D']);

%% file for question group means
DD=[mean(D(1:7,:)); mean(D(8:13,:)); mean(D(14:18,:)); mean(D(19:25,:))]
size(DD)
mat2txt('imiQGrp.txt',['Subj' TB G{1} TB G{2} TB G{3} TB G{4}],[SUBJ DD']);

%% PRint fig
fprintf('\n printing eps file..\n'); drawnow
orient tall
print -depsc2 IMIfig.eps

fprintf('\n ~~ END %s ~~\n\n',functionName);
end % end prog
