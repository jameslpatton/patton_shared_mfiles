% jimsSISOplot: plots for SISO tf on right w/4 separate overlapping windows
% MATLAB function
% (patton) created 2014-apr-5
% 

function jimsSISOplot(S);
fcnName='jimsSISOplot';
fprintf('\n ~ %s ~ \n', fcnName)
if ~exist('S')
  fprintf('\nsyntax: jimsSISOplot(S) \n using a default example of S: ')
  fprintf('\n     S=tf([1 2 .001],[4 5 6]) ')
  S=tf([1 2 .001],[4 5 6])
end

put_fig(4,.75, 0,.25,.2); nichols(S); 
put_fig(3,.75,.22,.25,.2); nyquist(S);  
put_fig(2,.75,.44,.25,.2); bode(S); 
put_fig(1,.75,.66,.25,.2); rlocus(S);

fprintf('\n ~ END %s ~ \n', fcnName)

