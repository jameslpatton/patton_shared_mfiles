%%************** MATLAB "M" fcn ************
% batch file to Analyze data from VRROOM
% SYNTAX:     analysis 
% REVISIONS:  2007-06-25 (patton) INITIATED
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~

function eTime=VRROOM_analysis(targFileName,dataFileName,plotSpec,fillColor)

%% SETUP
fcnName='VRROOM_analysis.m';
fprintf('\n\n\n ~ %s ~',fcnName)
% closeFigs
put_fig(100,.2,.01,.3,.96);
put_fig(200,.6,.15,.4,.7);
put_fig(300,.01,.5,.3,.4);

if ~exist('plotSpec','var'), plotSpec='bo'; end
if ~exist('fillColor','var'), fillColor='b'; end
thresholdVelocity=.008;

%% FILE CHECK
if ~exist('targFileName','var'); 
  [targFileName,targFilePathName]=uigetfile('*.txt', 'Choose Target File');
end
if ~exist('dataFileName','var'); 
  [dataFileName,dataFilePathName]=uigetfile('*.txt', 'Choose Data File');
end
fprintf('\n Checking for files..')
filesNeeded={targFileName,dataFileName};
for i=1:length(filesNeeded); 
	if ~exist(filesNeeded{i},'file'), 
    fprintf('Missing: %s',filesNeeded{i}); 
    error('  Halting analysis.  ')
  end
end
fprintf('\n~ All files Found.')

%% LOAD 
fprintf('\n Loading targFile into mem..')
[targHeader, targs] = hdrload(targFileName);
fprintf('DONE. (%d row header, %d movement trials) ', size(targHeader,1), size(targs,1))
figure(200); clf; plotVrroomman; hold on
drawnow; pause(.3)
Phase= textract(targFileName,'Phase');
endX= textract(targFileName,'endX');
endY= textract(targFileName,'endY');
endZ= textract(targFileName,'endZ');
TARG=[endX	endY	endZ];
plot3(endX,endY,endZ,'mo');

fprintf('\n Loading dataFile into mem (takes a while)..')
[dataHeader,data,Ntrials,eTime]=LoadAndRepairData(dataFileName,targs);  % load&ck
fprintf('DONE. (%d row header, %d movement trials, %f minutes.)'...
  , size(dataHeader,1), size(data,1), eTime)

%% PROCESS TRIALS: 
beginTime=data{1}(1,1);
lastTime=beginTime; % init to initial time
lastTrial=0; % init
outData=[];
fprintf('\n Trial:   ')

outData=NaN*ones(Ntrials,15); % init
trajHdr=plot3(0,0,0,'b-');
desiredTrajHdr=plot3(0,0,0,'r-','linewidth',2);
desiredHdr=plot3(0,0,0,'r*','markersize',6);
startHdr=plot3(0,0,0,'b+');
prevTrajHdr=plot3(0,0,0,'c-');
figure(300); vHdr=plot(0,0,'b-'); axis([0 5 0 1])
prevXYZ=[0 0 0];

for trialNum=2:Ntrials
%   pause
  %  fprintf('\nTrial %d, header for this is:  \n',trialNum)
%   dataHeader{trialNum}
  fprintf('%d,',trialNum)
  XYZ=data{trialNum}(:,2:4)
  Start=TARG(trialNum-1,:);
  Targ=TARG(trialNum,:);
  t=data{trialNum}(:,1)-data{trialNum}(1,1);
  dt=diff(t);
  t(1)=[];
  Vel=diff(XYZ)./[dt dt dt];
  speed=sqrt(Vel(:,1).^2+Vel(:,3).^2+Vel(:,3).^2);
  set(vHdr,'xdata',t,'ydata',speed);
  
  figure(200); 
  %plot3(XYZ(:,1),XYZ(:,2),XYZ(:,3),'b-')
  set(desiredTrajHdr, 'XData', [Start(1) Targ(1)], ... % update
                      'YData', [Start(2) Targ(2)], ...
                      'ZData', [Start(3) Targ(3)]); 
  set(desiredHdr, 'XData', Start(1), ... % update
                  'YData', Start(2), ...
                  'ZData', Start(3)); 
  set(trajHdr,'XData',XYZ(:,1),'YData',XYZ(:,2),'ZData',XYZ(:,3)); % update
  set(prevTrajHdr,'XData',prevXYZ(:,1),'YData',prevXYZ(:,2),'ZData',prevXYZ(:,3)); % update
  set(startHdr,'XData',XYZ(1,1),'YData',XYZ(1,2),'ZData',XYZ(1,3)); % update
  pause(.002); drawnow; 
  
  measNum=0; % count up the values
  
  measNum=measNum+1; measures(measNum).name='Elapsed Time';
  startTime=data{trialNum}(1,1);
  endTime=data{trialNum}(size(data{trialNum},1),1);
  if trialNum>1
    measures(measNum).values(trialNum)=endTime-startTime;
  else measures(measNum).values(trialNum)=0;
  end

%   measNum=measNum+1; measures(measNum).name='Cumulative Experiment Time';
%   measures(measNum).values(trialNum)=endTime-beginTime;

%   measNum=measNum+1; measures(measNum).name='rateTargetsPermin';  
%   measures(measNum).values(trialNum)=1/measures(measNum-2).values(trialNum)/60;

%   measNum=measNum+1; measures(measNum).name='#Targets Per 2 Minute Intervals';
%   timeSince=endTime-lastTime;
%   if timeSince>(2*60)
%     measures(measNum).values(trialNum)=trialNum-lastTrial;
%     lastTrial=trialNum;
%     lastTime=endTime; %fprintf('tic.')
%   elseif trialNum==1
%     measures(measNum).values(1)=0;
%   else
%     measures(measNum).values(trialNum)=measures(measNum).values(trialNum-1);
%   end

%   measNum=measNum+1; measures(measNum).name='cumulativeTime';  
%   measures(measNum).values(trialNum)=data{trialNum}(size(data{trialNum},1),1)-data{1}(1,1);

  measNum=measNum+1; measures(measNum).name='Length Ratio';
  if trialNum>1
    measures(measNum).values(trialNum)=straightness(XYZ);
  else measures(measNum).values(trialNum)=0;
  end

%   measNum=measNum+1; measures(measNum).name='Time Stationary (s)';  
%   measures(measNum).values(trialNum)=timeStationary(data{trialNum},thresholdVelocity);
  
  measNum=measNum+1; measures(measNum).name='Max PD (m)';   
  if trialNum>1
    measures(measNum).values(trialNum)=MaxPerpDistTraj2Line(XYZ,Start,Targ,0);
  else measures(measNum).values(trialNum)=0;
  end
 
  nMeasures=measNum; 

  % plot measures 
  figure(100)
  for measNum=1:nMeasures    
    subplot(nMeasures,1,measNum); 
    plot(trialNum,measures(measNum).values(trialNum),plotSpec,...
      'markersize',3, 'MarkerFaceColor',fillColor); 
    ylabel(measures(measNum).name);
    hold on; 
  end
  xlabel('Trials')
  
  %% assemble output data matrix
  dataRow=NaN*ones(1,nMeasures); % init
  for measNum=1:nMeasures
    dataRow(measNum)=measures(measNum).values(trialNum); 
  end
  outData(trialNum,1:nMeasures)=dataRow; 
  
  if round(trialNum/30)==trialNum/30, fprintf('\n'); drawnow; end % perodic <CR
  prevXYZ=XYZ;
  
  pause(.2)
end % END for trialNum

%% assemble output file
headerRow=''; % init
for measNum=1:nMeasures, headerRow=[headerRow measures(measNum).name char(9)]; end
mat2txt([dataFileName 'PerformanceMeasures.txd'],headerRow,[outData])

fprintf('\n\n~ END %s ~\n\n',fcnName)
end


%%************** MATLAB fcn ************
%% LoadAndRepairData: load & error-trap mistaken line(s) in file; calc eTime
function [dataHeader,data,Ntrials,eTime]=LoadAndRepairData(dataFileName,targs)

[dataHeader, data, Ntrials] = HDRLOADplus(dataFileName); % load
if size(targs,1)~=size(data,1)
  fprintf('\nNon-equal # targets in input and output: ')
  i=1;
  while i<size(data,1)
    i=i+1;
    if size(data{i},1)<3
      fprintf(' Record %d is being removed. ', i)
      data={data{1:i-1,:} data{i+1:size(data,1),:}}'; % this cuts out the record
      dataHeader={dataHeader{1:i-1,:} dataHeader{i+1:size(dataHeader,1),:}}'; % this cuts out the record
      Ntrials=Ntrials-1;
    end
  end
  fprintf('\nDONE. (%d row header, %d movement trials) ', size(dataHeader,1), size(data,1))
end
eTime=(data{Ntrials}(size(data{Ntrials},1),1)-data{1}(1,1))/60;
end

%%************** MATLAB fcn ************
%% straightness: ratio of a cumulative path length to straight line.
function lengthRatio=straightness(X)
  
  [M,N]=size(X);

  % sum up path lenght for the movment
  pathLength=0; % init  cd 
  for row=2:M % loop for each time step
    SS=0;
    for col=1:N  % loop for each column
      SS=SS+( X(row,col)-X(row-1,col) )^2;
    end    
    pathLength=pathLength+sqrt(SS);
  end
  
  % find ideal length (dist from start to end point)
  SS=0;
  for col=1:N
    SS=SS+( X(M,col)-X(1,col) )^2;
  end
  idealLength=sqrt(SS);
  
  lengthRatio=pathLength/idealLength;
end

%%************** MATLAB fcn ************
%% timeStationary: calc amount of time in a trial below threshold velocity 
function ts=timeStationary(D,threshVelocity)

  if ~exist('threshVelocity','var'), threshVelocity=.07; end % if~passed
  
  M     =size(D,1);                % find size
  t     = D(:,1);                 % extract
  xyz   = D(:,2:4);               % extract 
  Hz    = 50;                     % resampling freq
  cutoff= 8;                      % filter cutoff freq
  filtOrder=5;                    % filter order
  tU=(t(1):1/Hz:t(M))';           % uniform time 50 hz
  xyzU=interp1(t,xyz,tU);         % resample
  nSamples=length(tU);            % 
  if nSamples>3*filtOrder;        % is enough samples
    xyzFilt=butterx(xyzU,Hz ...   % filter
      ,filtOrder,cutoff);         % 
  else                            % otherwise
    xyzFilt=xyzU;                 % just pass it through
  end
  [vel,acc]=dbl_diff(xyzFilt,Hz,0);% double differentiate (well)

  % differentiate:
  %dt=diff(t);  xyzDot=diff(xyz)./[dt dt dt];
  %velocity=[xyzDot(1,:); xyzDot];% stack extra 2 equate overall length
  speed = NaN*tU;                 % initialize
  for row=1:nSamples              % loop for each time step
   speed(row)=norm(vel(row,:));   % speed calc
  end
  
%   size(tU),   size(vel),    size(acc),  size(speed)
  
  I=speed<threshVelocity;         % find which ones are below threshold
  I(1)=[];                        % clip out to match the diff
  ts=sum(I)/Hz;                   % add up all time steps (or divide by Hz)  
  
  % plot:
%   figure(2); plot(tU-tU(1),speed);%  
%   hold on; drawnow; figure(1);
end



