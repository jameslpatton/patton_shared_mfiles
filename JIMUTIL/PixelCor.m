
%% analyzes pixel similarity of two series in 2 d using pixels
% SYNTAX: Cor=pixelCor(n,x1,x2,window,plotIt)
%   - the final 2 args are optional
%     n is the number of grid elements (the density of the pixel grid)
%     x1 and x2 are the datasets, each an n-by-2 array of x,y coordinates
%
% Example: 
%  x1=[(1:.08:5)'-.2  sin(2*(1:.08:5)')];     % dummy data 1
%  x2=[(1:.05:5)'     sin(2*(1:.05:5)')+.1];  % dummy data 2
%  PixelCor(10,x1,x2)
%
% Example that looks at density:
%  x1=[(1:.08:5)'-.2  sin(2*(1:.08:5)')];     % dummy data
%  x2=[(1:.05:5)'     sin(2*(1:.05:5)')+.1];
%  figure(1); for density=1:50, C(density)=PixelCor(density,x1,x2); end
%  figure(2); plot(C); xlabel('grid density'); ylabel('Correlation')

function Cor=PixelCor(n,x1,x2,window,plotIt)

%%
if ~exist('plotIt','var'), plotIt='yes'; end; % if not given
if ~exist('window','var'), 
  X=[x1;x2];% STACK 
  window=[min(X(:,1)) max(X(:,1)) min(X(:,2)) max(X(:,2))];
end; % if not given

W=window(2)-window(1); w=W/n; % width and height
H=window(4)-window(3); h=H/n;
Bx=[0 0 w w 0]; By=[0 h h 0 0];   % how to draw a grid box

if plotIt,
  clf
  plot(x1(:,1),x1(:,2),'b.', x2(:,1),x2(:,2), 'r.'); hold on; box % data
  drawnow;
  % plot grid
  for i=1:n
    line(window(1)+w*i*[1 1], window(3)+[0 H],'color',[.8 .8 .8]); % x grid line
    line(window(1)+[0 W], window(3)+h*i*[1 1],'color',[.8 .8 .8])  % y grid line
  end
end

% check if inside box
M1=zeros(n); M2=M1; R=M1;
for i=1:n % rows
  for j=1:n % columns
    xMin=window(1)+w*(i-1); xMax=xMin+w; % this box
    yMin=window(3)+h*(j-1); yMax=yMin+h;
    
    for q=1:size(x1,1) % each point in x1
      if (x1(q,1)>xMin&&x1(q,1)<xMax&&x1(q,2)>yMin&&x1(q,2)<yMax),
        M1(i,j)=1;
        if plotIt,
          plot(xMin+.9*Bx,yMin+.9*By,'b'); 
        end
      end
    end
    
    for q=1:length(x2) % each point in x1
      if (x2(q,1)>xMin&&x2(q,1)<xMax&&x2(q,2)>yMin&&x2(q,2)<yMax),
        M2(i,j)=1;
        if plotIt, 
          plot(xMin+.1*w+.9*Bx,yMin+.1*h+.9*By,'r'); 
        end
      end
    end
    R(i,j)=M1(i,j)&&M2(i,j); % if the same (either nothing or somethign both
    if plotIt,
      if R(i,j)
        patch(xMin+Bx,yMin+By,'y','FaceAlpha',.5);  
      end
    end
    
  end
end
if plotIt, title('yellow marks pixels where both were present or both not presnt'); drawnow; end
Cor=sum(sum(R))/n^2;

end