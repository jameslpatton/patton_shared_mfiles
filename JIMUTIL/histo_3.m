

%************** MATLAB "M" function  *************
% (Patton) 3 dimensional histogram using N bins in each dimension
% syntax: [f,ctrs]=histo_n(x,N)
%          x is datapoint observations, with each column a dimension 
%          N is number of bins in each dimension 
%          bounds (optional) are the minimums (row 1) & manimums (row 2) 
%                 for each dim. Default is to take the maxs&mins of datapts 
%          plotIt (optional) set to 1 to plot. Default is 1 (to plot histo)
%          f is the frequency (count) of observations in each bin
%          ctrs is each bin's centerpoint coordinates for each dimension
% Note this is a reduced and simplified (faster) ersion of histo_n.m
% EXAMPLES:   histo_3(randn(1000,3),10);
%               Creates bubble plot histogram for a 3D random normal data.
% REVISIONS:  2013-Jan-23 (patton) altered from histo_n
% SEE ALSO: histo_n
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~~~~~~~~~~

function [f,ctrs]=histo_3(x,N,bounds,plotIt)

%% ____ SETUP ____
prog_name='histo_3.m';                              % name of this program
fprintf(' ~%s~ ', prog_name);        % MSG
if ~exist('N','var'), N=10; end;                    % default if not passed
if ~exist('bounds','var')||isempty(bounds),
  bounds=[min(x);max(x)];
end% default if not passed
if ~exist('plotIt','var'), plotIt=1; end;           % default if not passed
nD=size(x,2);                                       % number of Dimentions
nX=size(x,1);                                       % numeber of values
if nD~=3, error('x does not have 3 colums'); end;   % 
fprintf('(%dvalues, %dbins)',nX,N);                 % MSG
f=zeros(N,N,N);                                     % intit output count 
divs=zeros(N+1,3);                                  % intit bin divisions 
ctrs=zeros(N+1,3);                                  % intit bin centers 

%% create grid 
R=bounds(2,:)-bounds(1,:);                          % all ranges
for d=1:nD,                                         % loopp for ea dimen.
  divs(:,d)=(bounds(1,d):R(d)/N:bounds(2,d))';      % N bin edge divisions         
  ctrs(:,d)=divs(:,d)-R(d)/(2*N);                   % coordnates, each bin
end
ctrs(1,:)=[];                                       % clip first one off 

%% 
fprintf(' Sorting..')      
for i=1:nX,
  for b1=1:N,
    for b2=1:N,
      for b3=1:N,
        if(x(i,1)>=divs(b1,1)&&x(i,1)<divs(b1+1,1) ...
         && x(i,2)>=divs(b2,2)&&x(i,2)<divs(b2+1,2) ...
         && x(i,3)>=divs(b3,3)&&x(i,3)<divs(b3+1,3)),
          f(b1,b2,b3)=f(b1,b2,b3)+1;
        end;
      end;
    end;
  end; 
end;                          
fprintf('Done. ');                                  

%% PLOT if ...
if plotIt==1,                                       % 
  fprintf(' Plotting...');                            % message
  for b1=1:N, for b2=1:N, for b3=1:N,               % 3 FORs for dimensions
        mkrSz=(f(b1,b2,b3))*50/max(max(max(f)))+1;  % cal marker size
        plot3(ctrs(b1,1),ctrs(b2,2),ctrs(b3,3)   ...%
          ,'o','markersize',mkrSz,...               %
          'MarkerEdgeColor',[1 1 1], ...            %
          'MarkerFaceColor',[0 0 1] ); hold on      %
  end; end; end;                                    %
  axis equal; grid on                               % 3 ENDs for dimensions
end

%fprintf(' ~END %s~ ', prog_name);                 % MSG
end % end fuctnion
