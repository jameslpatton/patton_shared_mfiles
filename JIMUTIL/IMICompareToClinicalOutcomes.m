% function to compare the IMI information in correlations
% 2012/05/14 initiated by JIM PATTON

function IMICompareToClinicalOutcomes()
functionName='IMICompareToClinicalOutcomes';
fprintf('\n\n\n\n ~~ %s ~~\n',functionName);

fsz=8;

%% load
[IMIh,IMId]=hdrload('imiQGrp.txt');
size(IMId)
IMIcolLables=parse(IMIh(size(IMIh,1),:))
[COh,COd]=hdrload('ClinicalOutcomes.txt');
size(COd)
COcolLables=parse(COh(size(COh,1),:))

clf
ctr=0
for i=1:4
  for j=1:6
    ctr=ctr+1;
    subplot(4,6,ctr),
    IMId
    COd
    plot(IMId(:,i+1),COd(:,j+1),'o', 'markersize',2); 
    grid on
    set(gca, 'fontSize',fsz-1);
    rho=corr([IMId(:,i+1),COd(:,j+1)]); r(i,j)=rho(2);
    title(['r=' num2str(r(i,j))], 'fontSize',fsz)
    xlabel(IMIcolLables(i+1,:), 'fontSize',fsz)
    ylabel(COcolLables(j+1,:), 'fontSize',fsz)
  end
end

orient landscape
suptitle('Correlations Between IMI scores & Clinical Outcomes')
print -depsc2 correlationsBetweenIMI&ClinicalOutcomes

fprintf('\n ~~ END %s ~~\n\n',functionName);
end % end prog
