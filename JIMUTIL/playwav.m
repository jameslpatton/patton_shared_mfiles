% ************** MATLAB "M" function (jim Patton) *************
% play a wav sound file
% SYNTAX:     playwav(filename);
% INPUTS:     filename     file name including extension
% OUTPUTS:    sound to sound device
% CALLS:      wavread.m, sound.m
% VERSIONS:   8/11/99 patton initated
%~~~~~~~~~~~~~~~~~~~~~~ Begin Program: ~~~~~~~~~~~~~~~~~~~~~~~~

function playwav(filename);

[soundData,soundHz]=wavread(filename);  
sound(soundData,soundHz);

return