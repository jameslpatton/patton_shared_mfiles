%______*** MATLAB "M" file (jim Patton) ***_______
% This sets defaults for:
%
%                              (m) mass m
%                              /
%                             / lenth l
%                            /     
%                           / theta
%                       _ /o\ -  -  -  -     
%                   __=/_=_=_|          b
%       ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
%                          -a-		
%       lf = foot length;   b = ankle height
%	a = horizontal ankle to heel distance
%
% INPUTS : 		none
% OUTPUTS : 		see below
% CALLS : 		-
% INITIATED:	 	9/18/95 by jim patton 
%______________________________________________________

fprintf(' ~ defaults.m MATLAB M-file (jim Patton) ~ ... '); 

% _____________ SET GEOMETRIC (& other) PARAMETRS :____________
global theta_f;                             % readable by constraint fcn
torad=pi/180;                               % conversion factor to radians from deg
g=9.80;                                     % gravity (m/s/s)
count=1;                                    % init iteration counter
theta=[];                                   % init to null
constraint=9;                               % init to 'conflicct' status

H=1.78;                                     % body height
mass=80;                                    % (80 kg --Typical for a male.)

mf    = 2*.0145*mass;                       % mass of 2 feet
m     = mass-mf;                            % pendulum mass
l     = .575 * H;                           % pendulum length 
r     = .575 * H;                           % pendulum length 
lf    = 0.152 * H;                          % foot length
a     = 0.19*lf;                            % horiz. ankle to heel dist.
b     = 0.039 * H;                          % ankle height
c     = .5*lf-a;                            % horiz. ankle to CM
d     = lf-a;                               % DEFAULT "desired" COP at toe

tau_physio_p=-0.0931*2*m*H*9.81;            % max physiol. plantarflexion torque
tau_physio_d= 0.0489*2*m*H*9.81;            % max physiol. dorsiflexion torque

theta_i=40/180*pi;                          % really low starting point!
tf=.6;                                      % seconds (init guess,end of simulation)
tol=5e-7;                                   % meters (error tol.for convergence)
minstep=5e-6;                               % seconds (min integration step size)
maxstep=.01;                                % seconds (max integration step size)
options=[tol,minstep,maxstep];              % group these into one array

if ~exist('miu'), miu_old=0; miu=1; end;    % if no miu specified

fprintf('- end defaults.m -\n');
