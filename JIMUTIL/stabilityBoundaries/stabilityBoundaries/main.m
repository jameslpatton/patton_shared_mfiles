% ************ MATLAB "M" file (jim Patton) ***************
% This MATLAB m-file generates the simple inverted pendulum feasable space
% based on the body, foot, and environment, and strength parameters. This 
% is the root program to run.
% REVISIONS:  4/1/96    INITIATED from fndfeas1.m (patton)
%             1/19/2000 renamed main, and used to determine the feasible
%                       states from readme.txt file params.
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

fprintf('\n ~ main.m for determining the feasible staates (P a t t o n) ~\n')

% _________ SETUP ___________
global theta_f;                               % readable by constraint fcn
torad=pi/180;                                 % conversion factor to radians from deg 
g=9.80;                                       % gravity (m/s/s)  
theta=[];                                     % init to null 
constraint=9;                                 % init to 'conflicct' status 
figure(1);clf;figure(2);clf;drawnow;pause(.1);% update scrn
fprintf('\n CURRENT DIRECTORY: %s',cd);       % message
paramName='paramters.txt';

% _________ GET PARAMETRS ___________
if 1 % ~exist('H'), 
  fprintf('\n Getting params from %s..',...
    paramName);   % message 
  if ~exist(paramName); 
    error('NO "%s" FILE!',paramName);
  end
  female=findInTxt('paramters.txt','female=');% find gender
  if female==-8888|isempty(female),gender='w';% if sex not specified
  elseif female==1, gender='f';  
  else  gender='m';                                                              
  end                                         % END if gender
  gender
  female=findInTxt('paramters.txt','female=');% find gender
  H     =findInTxt(paramName,'height(m)=')    % body height
  mass  =findInTxt(paramName,'mass(kg)=')     % body mass	
  lf    =findInTxt(paramName,             ... % foot length
          'foot length (lf)=')                
  a     =findInTxt(paramName,             ... % horiz ankle to heel 
          'horiz dist ankle to heel (a)=')    
  b     =findInTxt(paramName,             ... % vert ankle height	
          'vert dist ank to sole (b)=')       
  wf     =findInTxt(paramName,            ... % foot width	
                'foot width (wf)=')           % ...	
  mf    = 2*.0145*mass                        % mass of 2 feet
  m     = mass-mf                             % pendulum mass
  l     = .575 * H                            % pendulum length 
  r     = .575 * H                            % pendulum length 
  c     = .5*lf-a                             % horiz. ankle to CM
  d     = lf-a                                % DEFAULT "desired" COP at toe
  
  planes=['sagittal';'frontal '];
  bounds=['fast';'slow'];                     % name the 2 bounds

  tau_physio_p=-0.0931*2*m*H*9.81;            % max physiol. plantarflexion torque
  tau_physio_d= 0.0489*2*m*H*9.81;            % max physiol. dorsiflexion torque

  theta_i=40/180*pi;                          % really low starting point!
  tf=.6;                                      % seconds (init guess,end of simulation)
  tol=5e-7;                                   % meters (error tol.for convergence)
  minstep=5e-6;                               % seconds (min integration step size)
  maxstep=.01;                                % seconds (max integration step size)
  options=[tol,minstep,maxstep];              % group these into one array

  if ~exist('miu'), miu_old=0; miu=1; end;    % if no miu specified
end

% _________ DISPLAY PARAMETRS ___________
fprintf('\nGeometric (& other) Parameters:\n');
fprintf('m=%3.3f  H=%3.3f  l=r=%3.3f   lf=%3.3f\n', m, H,l,lf)
fprintf('a=%3.3f  b=%3.3f  c=%3.3f    miu=%3.3f\n',a,b,c,miu)
fprintf('Initial theta of %2.0f degrees (%.3f rad)\n',theta_i/pi*180,theta_i);

for direction=1:2
  if direction==2, lf=wf; a=wf/2; end
    
%___ borders and strength limits ___
figure(1);                                    % 
do_bdr;                                       % find inetersection
theta_fs=[pi-acos((lf-a)/r) acos(a/r)];       % heel toe locations
theta_strength(1)=asin(-tau_physio_p/(m*g*r))+pi/2;		% how far can lean T
theta_strength(2)=asin(-tau_physio_d/(m*g*r))+pi/2; 		% how far can lean H
if theta_strength(1)<theta_fs(1),				     % if too weak 
  theta_fs(1)=theta_strength(1);				     % feasible theta_f 
  fprintf('Too weak! theta_f=%f (%fpct),'... 	% display	
    ,theta_fs(1), (1/r*sin(theta_f-pi/2)+a)/lf ); 		% display	
end                                           % END if theta_strength 
if theta_strength(2)>theta_fs(2),             % if too weak 
  theta_fs(2)=theta_strength(2);              % feasible theta_f 
  fprintf('Too weak! theta_f=%f (%fpct),' ... % display	
   ,theta_fs(1),(1/r*sin(theta_f-pi/2)+a)/lf )% 	
end                                           % END if theta_strength

%______ BOUNDARIES ______
for bound=1:2,                                % each boundary
  fprintf('\n BOUNDARY: %s',bounds(bound,:));           
  fprintf('\n___Performing Iterations:___')
  count=1;                                    % init iteration counter

  %______ SET THIS boundary's PARAMETERS ______
  tf=.6;                                      % end sim guess(sec) 
  ok=0;                                       % set to not OK
  error_pos=9999; error_vel=9999;             % init with high val
  error_pos_goal=.007;  error_vel_goal=.02;   % minimum val needed
  Dtheta_i_LO=min(Dtheta_FRIC);               % lowest possible
  Dtheta_i_HI=max(Dtheta_FRIC);               % highest possible
  theta_f=theta_fs(bound);                        % final heel or toe

  %______________ LOOP for OPTO _______________
  while(error_pos>error_pos_goal          ... % while not solved
       |error_vel>error_vel_goal)             %
    Dtheta_i=(Dtheta_i_HI+Dtheta_i_LO)/2;     % choose avg value
    theta_i=interp1(Dtheta_FRIC,theta_COP,Dtheta_i);		% PT ON BORDER
    fprintf(	                            ... % message
     '\n%d] Dtheta_i:<%1.6f,%1.6f,%1.6f>',count,Dtheta_i_LO,Dtheta_i,Dtheta_i_HI);% ...

    %============= SIMULATION =============
    fprintf('SIM..'); pause(.01);             % display
    %x0= [theta_i; Dtheta_i];                 % initial values
    %rk45('model_7',tf,x0,options);           % SIMULATION *****
    sim('model_7',tf);                        % SIMULATION *****
    fprintf('DONE@%1.3fs.',max(time));        % display
    if max(constraint)==9,                    % if contraint encountered,
      fprintf('CONFLICT');                    % ALARM MSG
    end 	

    %______________ PLOT _______________
    figure(1);							% show here
    plot(theta,   Dtheta,         'k.'	   ... % plot trajectory
        ,theta_f,   0,            'bo'    ... % plot target 
        ,theta_COP, Dtheta_COP,   'm'     ... % plot COP border
        ,theta_COP, Dtheta_FRIC,  'c'     ... % plot frict border
        ,theta_F_C, Dtheta_F_C,   'r' );      % plot COP-frict bdr 
    %axis([.5 2 0 3]);	
    drawnow;                                   

    %_OPTIMIZATION CHANGES FOR THIS ITERATION:_
    error_pos=abs(theta(length(theta))-theta_f);% x  error 
    error_vel=abs(Dtheta(length(Dtheta)));    % Dx error 
    fprintf('{%d}<%g,%g>',min(constraint),... % display 
      error_pos,error_vel);                   %  ...
    pause(.01);                               % to update disply
    if(error_pos<error_pos_goal           ... % if near sln
      &error_vel<error_vel_goal)              %
    elseif min(abs(theta-theta_f))==      ... % if the final state
      abs(theta(length(theta))-theta_f)   ... % is headed to
     &Dtheta(length(theta))>0,				       % target but short
      tf=tf+.05;                              % increase sim time
      fprintf('=>incr.tf.');                  % message
    else                                      % otherwise
      tf=tf-.005;fprintf('=>decr.tf, adjust');% decrease sim time
      if min(Dtheta)>=error_vel_goal,         % if overshoot
        fprintf(' HI.'); Dtheta_i_HI=Dtheta_i;% init vel too hi
      else                                    % otherwise..
        fprintf(' LO.'); Dtheta_i_LO=Dtheta_i;% init vel too low
      end                                     % END if Dtheta 
    end                                       % END if error
    count=count+1;                            % icrease counter
  end                                         % END while
  if bound==1,                                % if toe
    FAST=[time tau theta Dtheta constraint];  % mtrx of sln
  else                                        %	
    SLOW=[time tau theta Dtheta constraint];  % mtrx of sln
  end	                                       % END if heel
end                                           % END for i (boundary)

%_ CONSTRUCT POLYGON FROM BORDERS & TRAJ'S _
figure(1+direction);                          % plot results here
[POpolygon,NCpolygon]=do_feas3(SLOW,FAST, ... % make polygons from 
    [theta_COP' Dtheta_FRIC'],lf,a,r,H);      % 2 traj & border
mak_ptch(NCpolygon,lf,a,r,H,.7);              % plot the polygon
ax=axis; plot([-.5 ax(2)],[ 0 0],'k');        % horizontal line
title([deblank(planes(direction,:))       ... % plot title
    ' boundaries (' cd ')']);         

polygon=[ NCpolygon(:,1)*lf               ... % de-normalize
          NCpolygon(:,2)*H                ... %
          NCpolygon(:,3:4)];                  %

%_ SAVE _
h=str2mat(['Feasible states for '         ...
  deblank(planes(direction,:))            ...
  ' plane movement.'],                    ...
 ['FDA project, Jim Patton, created '     ...
   whenis(clock)],                        ...
 ['This file is best viewed in Excel '    ...
 'or loaded into MATLAB using hdrload.m'],...
 'Data begins below:',                    ...
 '____');
 
hn=str2mat(h,                             ...
   ['position normalized to foot legth'   ...
 ' (0=toe, 1=heel)' setstr(9)             ...
    'velocity normalized to body height'  ...
 ' (1/sec)' setstr(9)                     ...
    'torque (N)' setstr(9)                ...
    'code for what boundary is what: '    ...
 '(0=fast, 1=slow, -1=upper)']);
h=str2mat(h,                              ...
   ['position (m) (0=toe)' setstr(9)      ...
    'velocity (m/sec)' setstr(9)          ...
    'torque (N)' setstr(9)                ...
    'code for what boundary is what: '    ...
 '(0=fast, 1=slow, -1=upper)']);

ouput_filename=[                          ... % construct a save command
    deblank(planes(direction,:))          ...
    '_bound.dat'];
mat2txt(ouput_filename,h,polygon);

ouput_filename=[                          ... % construct a save command
    deblank(planes(direction,:))          ...
    '_norm_bound.dat'];
mat2txt(ouput_filename,hn,NCpolygon);

%_ PRINT _
cmd=['print -depsc2 '                     ... % construct print-to-file 
 deblank(planes(direction,:)) '_normFS' ];    %   command
disp(cmd); eval(cmd);

end % END for direction
fprintf('\n ~ END main.m ~ \n\n')


