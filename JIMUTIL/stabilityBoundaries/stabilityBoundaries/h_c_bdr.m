%______*** MATLAB "M" function (jim Patton) ***_______
%
% COP_bdr()
% The following takes COP constriant HI (heel) and
% friction constriant LO (toe) and looks at them
% for the inverted pendulum and calculates the difference
% (absolute value) between the torques.  This is used 
% in scalar optimization for the border of constriants.
%
% INPUTS : For optimization :
%		Dtheta
%          Parameters:
% 		theta, g, m, mf, r, miu, b, c, COP1, COP2
%
% OUTPUTS :	distance  (from the 2 constriant torques)
%
% FUNCTION INITIATED 1 SEPT 1994  by Jim Patton
%______________________________________________________

function distance = h_c_bdr(Dtheta, theta, g, m, mf, r, miu, b, c, COP1, COP2)
%disp('*** h_c_bdr.m (function) ***');

%______________________________________________________
% (#3): friction LO -- posterior shear (slip forwards)
LF_tau	= 	m*g*r*cos(theta) + 						...
		( ( miu*sin(theta) - cos(theta) )*m*r*r*Dtheta*Dtheta - 	...
		miu*r*g*(mf+m) ) / ( miu*cos(theta) + sin(theta) );

%______________________________________________________
% (#6): COP2 (at heel) 
HC_tau  = 	( (b*sin(theta)  + COP2*cos(theta) )*m*g*cos(theta) +	...
		( COP2*sin(theta) - b*cos(theta) )*m*r*Dtheta*Dtheta - 	...
		COP2*(mf+m)*g + c*mf*g ) / 					...
		( COP2/r*cos(theta) + b/r*sin(theta) + 1 );

distance = abs(LF_tau-HC_tau);

%______*** MATLAB "M" function (jim Patton) ***_______
