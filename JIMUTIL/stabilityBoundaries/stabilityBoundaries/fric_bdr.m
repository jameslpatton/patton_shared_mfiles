%______*** MATLAB "M" function (jim Patton) ***_______
%
% COP_bdr()
% The following takes 2 COP constriants (heel and toe)
% for the inverted pendulum and calculates the difference
% (absolute value) between the torques.  This is used 
% in scalar optimization for the border of constriants.
%
% INPUTS : For optimization :
%		Dtheta
%          Parameters:
% 		theta, g, m, mf, r, miu, b, c, COP1, COP2
%
% OUTPUTS :	distance  (from the 2 FRIC constriant torques)
%
% FUNCTION INITIATED 23 AUG 1994  by Jim Patton
%______________________________________________________

function distance = fric_bdr(Dtheta, theta, g, m, mf, r, miu)
%disp('*** FRIC_bdr (function) ***');

%______________________________________________________
% (#3): friction LO -- posterior shear
LF_tau	= 	m*g*r*cos(theta) + 						...
		( ( miu*sin(theta) - cos(theta) )*m*r*r*Dtheta*Dtheta - 	...
		miu*r*g*(mf+m) ) / ( miu*cos(theta) + sin(theta) );

%______________________________________________________
% (#4): friction HI -- anterior shear
HF_tau	= 	m*g*r*cos(theta) + 						...
		( ( -miu*sin(theta) - cos(theta) )*m*r*r*Dtheta*Dtheta + 	...
		miu*r*g*(mf+m) ) / ( -miu*cos(theta) + sin(theta) ); 

distance = abs(LF_tau-HF_tau);

%______*** MATLAB "M" function (jim Patton) ***_______
