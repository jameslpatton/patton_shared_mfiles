%______*** MATLAB "M" function (jim Patton) ***_______
% this renders the shaded area of the feasible phase plane figure
% SYNTAX:	[POpolygon,NCpolygon]=do_feas3(heel,toe,border,lf,a,r,H)
% CALLED BY: 	feedbk#.m	see thes files for more info
% INPUTS : 	heel		time,torque,theta,Dtheta,etc. (lo vel time recs)
%		toe		time,torque,theta,Dtheta,etc. (hi vel time recs)
%		border		theta,Dtheta,etc. (constriant intersect borders)
%		lf		foot length
%		a		horiz. ankle to heel dist.
%		r		pendulum length 
%		H		BODY HEIGHT
% OUTPUTS : 	POpolygon 	for plotting (x and y data in columns)
% INITIATED:	8/29/95 by Jim Patton from "do_surf4.m" 
%		6/18/97 patton. clean up
%		6/18/97 patton. add the normalizaton job inside this function
%_________________ FUNCTION STATEMENT: _________________

function [POpolygon,NCpolygon]=do_feas3(heel,toe,border,lf,a,r,H)
fprintf('\n___ do_feas3.m  function (P a t t o n) __\n')

%______ SET LOCAL VARIABLES : _________
g		= 9.80;						% gravity (m/s/s)
torad		= pi/180;					% conversion factor 
 								% to radians 

%_______ FIND INTERSECTION OF BOUNDS: _______
fprintf('Finding intersections of lines'); 			% message
pause(.01);							% update display
near=[1 1 1 1];						% init the index 
for i=1:length(heel(:,1))					% loop for heel
  for j=1:length(border(:,1)) 				% loop for border 
    if(heel(i,3)-border(j,1))^2 +		...
     (heel(i,4)-border(j,2))^2 < 		...
     (heel(near(1),3)-border(near(2),1))^2+	...
     (heel(near(1),4)-border(near(2),2))^2,	 
      near(1:2)= [i,j];
    end %if
  end % for j
end % for i

for i=1:length(toe(:,1)) 					% loop for toe
  for j=1:length(border(:,1)) 				% loop for border 
    if(toe(i,3)-border(j,1))^2 +		...
     (toe(i,4)-border(j,2))^2 < 		...
     (toe(near(3),3)-border(near(4),1))^2 +	...
     (toe(near(3),4)-border(near(4),2))^2, 
      near(3:4)= [i,j];
    end %if
  end % for j
end % for i

%_____________ render polygon: ________________
x = [	 toe(near(3):length(toe(:,1)),3); 			... % pos
	 pi-acos((lf-a)/r);					...
	 acos(a/r);	  					...
	 flipud(heel(near(1):length(heel(:,1)),3)); 		...
	 border(near(2):near(4),1) 	];
y = [	 toe(near(3):length(toe(:,1)),4);			... % vel
	 0;	  						...
	 0;							...
	 flipud(heel(near(1):length(heel(:,1)),4)); 		...
	 border(near(2):near(4),2) 	];
z = [	 toe(near(3):length(toe(:,1)),2);			... % torque?
	 0;	  						...
	 0;							...
	 flipud(heel(near(1):length(heel(:,1)),2)); 		...
	 zeros(near(4)-near(2)+1,1)	];
type = [zeros(length(toe(:,1))- near(3)+1,1);		... % curve seg code 
	 1; 							...
	 1;	 						...
	 ones(length(heel(:,1))-near(1)+1,1);			...
	 -1*ones(near(4)-near(2)+1,1) 	];
POpolygon=[x y z type];

NCpolygon=normpoly(POpolygon,lf,a,r,H); 			% normalzd cartesian

fprintf('\n_______ END do_feas3.m _______\n');	return;	

