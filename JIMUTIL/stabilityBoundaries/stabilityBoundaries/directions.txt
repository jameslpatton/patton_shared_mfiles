Instruction for creating stability boundaries (feasible state boundaries) for a subject: 
(Each subject should have his or her own subdirectory)
 
0.  Extract these MFILES to a folder
1.  If you have not made a parameters.txt file for the subject yet: 
      a.  Copy this file from another directory or use the sample paramters.txt from the folder containinig the MFILES.
      b.  Edit this file in a text editor
2.  Run MATLAB.
3.  Change to subdirectory in MATLAB where the MFILES are located 
4.  Type "setpath" to add these programs to the search path
5.  Change to the subject's subdirectory in MATLAB
6.  Type "main" and watch it go. 
7.  It will create data files, which are readable from a text editor, spreadsheet program, or loaded into MATLAB using hdrload.m:
      frontal_norm_bound.dat    
      frontal_bound.dat
      sagittal_bound.dat
      sagittal_norm_bound.dat

8.  It will also create several printable, encapsulated post-script figures:
      frontal_normFS.eps
      sagittal_normFS.eps
