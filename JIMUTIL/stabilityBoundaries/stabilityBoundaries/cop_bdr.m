%______*** MATLAB "M" function (jim Patton) ***_______
%
% COP_bdr()
% The following takes 2 COP constriants (heel and toe)
% for the inverted pendulum and calculates the difference
% (absolute value) between the torques.  This is used 
% in scalar optimization for the border of constriants.
%
% INPUTS : For optimization :
%		Dtheta
%          Parameters:
% 		theta, g, m, mf, r, b, c, COP1, COP2
%
% OUTPUTS :	distance 	(from the 2 COP constriant torques)
%
% FUNCTION INITIATED 23 AUG 1994  by Jim Patton
%______________________________________________________

function distance = cop_bdr(Dtheta, theta, g, m, mf, r, b, c, COP1, COP2)
%disp('*** COP_bdr (function) ***');

%______________________________________________________
% (#5): COP1 (at toe) 
LC_tau  = 	( (b*sin(theta)  + COP1*cos(theta) )*m*g*cos(theta) +	...
		( COP1*sin(theta) - b*cos(theta) )*m*r*Dtheta*Dtheta - 	...
		COP1*(mf+m)*g + c*mf*g ) / 					...
		( COP1/r*cos(theta) + b/r*sin(theta) + 1 );

%______________________________________________________
% (#6): COP2 (at heel) 
HC_tau  = 	( (b*sin(theta)  + COP2*cos(theta) )*m*g*cos(theta) +	...
		( COP2*sin(theta) - b*cos(theta) )*m*r*Dtheta*Dtheta - 	...
		COP2*(mf+m)*g + c*mf*g ) / 					...
		( COP2/r*cos(theta) + b/r*sin(theta) + 1 );

distance = abs(LC_tau-HC_tau);


%______*** MATLAB "M" function (jim Patton) ***_______
