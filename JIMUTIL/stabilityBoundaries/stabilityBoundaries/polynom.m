2%______*** MATLAB "M" file (jim Patton) ***_______
% The following SIMM-generated, position-dependent strength 
% data, and fits it to a polynomial, for use in cnstrnt7.m M-function.
% INPUTS: 	strength.dat		(text-data file:angle,plantar,dorsi)
% OUTPUTS:	p_p			()	
%		p_d
%		plot of fits
% INITIATED:	4/1/96 by Jim Patton%
% SEE: 		cd c:\jim\pai\stop_mov\devel
% SYNTAX:	strength
%______________________begin:__________________________

fprintf('\n____________ tau_poly.m  (J i m   P a t t o n) ______________\n')

%__________
%order=menu2('select polynomial order:','1','2','3','4','5','6','7','8','9')
load strength.dat				% make sure in c:\jim\pai\stop_mov\data
clg;

for order=1:9,
  subplot(3,3,order),
  plot(strength(:,1),strength(:,2),'bo',  strength(:,1),strength(:,3),'ro'); hold;
  est_strength=[];
  p_p=[];
  p_d=[];
  p_p=polyfit(strength(:,1),strength(:,2), order);
  p_d=polyfit(strength(:,1),strength(:,3), order);
  est_strength(:,1)=(.6:.01:2.2)';
  est_strength(:,2)=polyval(p_p, est_strength(:,1));
  est_strength(:,3)=polyval(p_d, est_strength(:,1));
  plot(est_strength(:,1),est_strength(:,2),'b', est_strength(:,1),est_strength(:,3),'r');
  title(['order ' num2str(order)])
end %for order
%______*** END MATLAB "M" function (jim Patton) ***_______
