%______*** MATLAB "M" function (jim Patton) ***_______
% This MATLAB M-File plots the phase plane boundary
% SYNTAX:	function y=mak_ptch(polygon,lf,a,r,H,C)
% CALLED BY: 	feedbk#.m	see thes files for more info
% INPUTS : 	heel		time,torque,theta,Dtheta,etc. 
%				 (lo velocity time records)
%		toe		time,torque,theta,Dtheta,etc. 
%				 (hi velocity time records)
%		border		theta,Dtheta,etc. 
%				 (constriant intersection borders)
%		lf		foot length
%		a		horiz. ankle to heel dist.
%		r		pendulum length 
%		H		BODY HEIGHT
%		C		color vector or gray scalar
% REVISIONS: 	9/6/95 patton INITIATED
%		6/20/97 patton allowed for color patches and grey
% ~~~~~~~~~~~~~~~~~~~~~~~~~ begin ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function y=mak_ptch(polygon,lf,a,r,H,C)
fprintf(' ~ mak_ptch.m (P a t t o n) ~ ')

% ________ SETUP: ________
load lrunshoe.dat;	% outline of a foot
if length(C)==1, C=[C C C]; end;

% ________ PLOT: ________
%plot(	[0 (lf-a)/lf	1],		[0 0 0], 	'k',	...
%	[0 (lf-a)/lf	1],		[0 0 0], 	'm*'	);
%ax=axis;  hold on;  
patch(polygon(:,1), polygon(:,2),C); hold on;
patch(1-lrunshoe(:,1),lrunshoe(:,2)/2.5-.2,[.3 .3 .3]);

% ________ PLOT TEXT: ________
%title('Anterior/Posterior Phase Plane');
%text(.5,.4,'A'); text(.5,.15,'B'); text(1.2,.65,'C'); 
%text(1.3,.35,'D'); text(1.5,.05,'E');
xlabel('Position normalized to foot length');
ylabel('Anterior velocity normalized to body height (s-1)');

ax=axis;
plot(	[0 0],[ax(3) 	ax(4)],		'k', 		...
	[1 1],[ax(3) 	ax(4)],		'k', 		...
	(lf-a)/lf,	0,		'mo'	);

set(gca,'fontsize',7);
hold on;

fprintf(' ~ END mak_ptch.m  ~ ')
