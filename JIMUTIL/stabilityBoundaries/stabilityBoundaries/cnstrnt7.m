%______*** MATLAB "M" function (jim Patton) ***_______
% This is a matlab state-control feedback algorithm.
% The following takes a 2 element state vector for the inverted pendulum
% and calculates the appropriate ankle plantar flexion torque based on 
%    * STRENGTH (max physiological torque)
%    * GRAVITY (no flying off the floor)
%    * FRICTION (no slipping)
%    * COP (COP must remain under the foot)
% Note that HI refers to HIGH torque bound (dorsiflexion) and LO (plantarflex)
% INPUTS: 	[states:]
%		theta  		= 	u(1)
%		Dtheta 		= 	u(2)
% 	   	[parameters,  starting with u(3):]
% 		g 		= 	u(3)
% 		m 		= 	u(4)
% 		mf 		=	u(5)
% 		r 		= 	u(6)
% 		miu 		=	u(7)
% 		a 		= 	u(8)
% 		b 		= 	u(9)
% 		c 		= 	u(10)
% 		d 		= 	u(11)
% 		tau_physio_p	= 	u(12)
% 		tau_physio_d	= 	u(13)
% OUTPUTS:	tau (torque)		= out(1)
%		constriant type	= out(2) ==> defined as follows:
%					0 ....... max physiological plantarflex
%					1 ....... max physiological dorsiflex
%					2 ....... gravity (should never be active)
%					3 ....... friction posterior shear 
%					4 ....... friction anterior shear
%					5 ....... COP at toe
%					6 ....... COP at heel
%					7 ....... <reserved for later>
%					8 ....... <reserved for later>
%					9 ....... impossible situation -- no sln
%		status			= out(3) (nonzero=stop simulation) 
% INITIATED:	4/1/96 from cnstrnt2.m by Jim Patton
% SEE: 		cd c:\jim\pai\stop_mov\devel% SYNTAX:	sensifig
%______________________________________________________

function out=cnstrnt7(u)
global theta_f;
%disp('*** cnstrnt7 (function) ***'); fprintf('m=%f',u(4)); %fprintf('(a=%0.3f)',u(8));

%_ define max&min 7th order polynom strength coeff.(see polynom.m) scaled to max __
p_p=1.0e+006*[	 0.01013860281429	-0.09967767402294  	 0.41323392433571  	...
		-0.93568283743572 	 1.24935090756151  	-0.98399009009533   	...
		 0.42348863101991  	-0.07689798218661]./( -0.0931*u(4)*u(6)/.575*9.81);
p_d=1.0e+005*[	 0.01673738474932  	-0.15123570535529   	 0.56979328128020  	...
		-1.15765240776887   	1.36762493339202  	-0.93941733313825   	...
		 0.34794164293589  	-0.05307846290949]./(  0.0489*u(4)*u(6)/.575*9.81);

%______________ preset bounds using max physio constraints _______________
LO_out = u(12)*polyval(p_p,u(1));	% begin by using tau_phsio and work up
LO_con = 0;				% 
HI_out = u(13)*polyval(p_d,u(1));	% begin by using tau_phsio and work down 
HI_con = 1;				% 
out(3) = 0;				% begin with OK status

%__________________ % (#2):gravity ________________
grav_factor = .1;
G_tau	= 	u(4)*u(3)*u(6)*cos(u(1)) + 						...
		u(4)*u(6)*u(6)*u(2)*u(2)*tan(u(1)) + 					...
		(u(5)+u(4))*(grav_factor - u(3))*u(6)/cos(u(1));
%if G_tau > LO_out 
%    if 90-u(1)/pi*180 > 10
%	LO_out = G_tau;		% winner in LO category
%	LO_con = 2;			% set constriant type indicator
%	%fprintf('- cnstrnt1 gravity:  tau = %4.1f___________________\n', out(1))
%    end 	%if
%end 	%if

%________________ % (#3): friction LO -- anterior shear _______________
LF_tau	= 	u(4)*u(3)*u(6)*cos(u(1)) + 						...
		( ( u(7)*sin(u(1)) - cos(u(1)) )*u(4)*u(6)*u(6)*u(2)*u(2) - 	...
		u(7)*u(6)*u(3)*(u(5)+u(4)) ) / ( u(7)*cos(u(1)) + sin(u(1)) );
if LF_tau > LO_out 
    LO_out =  LF_tau; 		% winner in LO category
    LO_con = 3; 		% set constriant type indicator 
    %fprintf('- cnstrnt1 friction LO:  tau = %4.1f\n', LO_out );
end 	%if

%___________________ % (#4): friction HI -- posterior shear ___________________
HF_tau	= 	u(4)*u(3)*u(6)*cos(u(1)) + 						...
		( ( -u(7)*sin(u(1)) - cos(u(1)) )*u(4)*u(6)*u(6)*u(2)*u(2) + 	...
		u(7)*u(6)*u(3)*(u(5)+u(4)) ) / ( -u(7)*cos(u(1)) + sin(u(1)) ); 
if HF_tau < HI_out 
    HI_out =  HF_tau; 		% winner in HI category
    HI_con = 4; 		% set constriant type indicator 
    %fprintf('- cnstrnt1 friction HI:  tau = %4.1f\n', HI_out );
end 	%if

%_________________ % (#5): COP LO (at toe) _________________
LC_tau  = 	( ( u(9)*sin(u(1))  + u(11)*cos(u(1)) )*u(4)*u(3)*cos(u(1)) +	...
		( u(11)*sin(u(1)) - u(9)*cos(u(1)) )*u(4)*u(6)*u(2)*u(2) - 		...
		u(11)*(u(5)+u(4))*u(3) + u(10)*u(5)*u(3) ) / 			...
		( u(11)/u(6)*cos(u(1)) + u(9)/u(6)*sin(u(1)) + 1 );
if LC_tau > LO_out 
    LO_out = LC_tau; 		% winner in LO category
    LO_con = 5; 		% set constriant type indicator 
    %fprintf('- cnstrnt1 LO COP:  tau = %4.1f\n', out(1));
end 	%if

%_______________ % (#6): COP HI (at heel) _________________
HC_tau  = 	( ( u(9)*sin(u(1)) - u(8)*cos(u(1)) )*u(4)*u(3)*cos(u(1)) +		...
		 ( -u(8)*sin(u(1)) - u(9)*cos(u(1)) )*u(4)*u(6)*u(2)*u(2) + 	...
		    u(8)*(u(5)+u(4))*u(3) + u(10)*u(5)*u(3) ) / 			...
		 ( -u(8)/u(6)*cos(u(1)) + u(9)/u(6)*sin(u(1)) + 1 );
if HC_tau < HI_out 
    HI_out = HC_tau; 		% winner in HI category
    HI_con = 6; 		% set constriant type indicator 
    %fprintf('- cnstrnt1 HI COP:  tau = %4.1f\n', out(1));
end 	%if

%__________________ % Assemble the final tau: ____________________
if HI_out < LO_out, 
  if theta_f>90*pi/180, 	out(1)= LC_tau;   out(2)=9;   	out(3)=0;
  else				out(1)= HC_tau;   out(2)=9;   	out(3)=0;	end;
else
  if theta_f>90*pi/180,	out(1)=LO_out;	   out(2)=LO_con;	out(3)=0;
  else				out(1)=HI_out;	   out(2)=HI_con;	out(3)=0; 	end;
end 	% if

%__________________ % Detect if we've missed the goal state: ______________
if 	u(1)>140*pi/180,   		out(3) = 1;   	%disp('too much')
elseif 	u(1)<10*pi/180,   		out(3) = 1;   	%disp('too much')
elseif 	u(2) < -5			out(3) = 1;	%disp('not enough')
end %if

return;

%______*** END MATLAB "M" function (jim Patton) ***_______
