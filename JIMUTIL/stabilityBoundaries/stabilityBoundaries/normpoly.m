%______*** MATLAB "M" function (jim Patton) ***_______
% this renders the shaded area of the feasible phase plane figure
%
% CALLED BY: 	feedbk#.m	see thes files for more info
% INPUTS : 	heel		time,torque,theta,Dtheta,etc. (lo velocity time records)
%		toe		time,torque,theta,Dtheta,etc. (hi velocity time records)
%		border		theta,Dtheta,etc. (constriant intersection borders)
%		lf		foot length
%		a		horiz. ankle to heel dist.
%		r		pendulum length 
%		H		BODY HEIGHT
%		
% OUTPUTS : 	polygon for plotting (x and y data in columns)
% INITIATED:	8/29/95 by Jim Patton from "do_surf4.m" 
%_________________ FUNCTION STATEMENT: _________________
%	function cartesian_polygon=normpoly(heel,toe,border,lf,a,r,H)

function cartesian_polygon=normpoly(polygon,lf,a,r,H)
fprintf('\n___ normpoly.m  M-function (J i m   P a t t o n) __\n')

cartesian_polygon(:,1) = (lf-a+r*cos(polygon(:,1)))/lf; 		% trans. to cartesian
cartesian_polygon(:,2) = r*polygon(:,2).*sin(polygon(:,1)) /H; 	% trans. to cartesian
cartesian_polygon(:,3) = polygon(:,3);
cartesian_polygon(:,4) = polygon(:,4);

fprintf('\n_______ END normpoly.m _______\n');	return;	

