%~~~~~~~~~~~~~~~~ MATLAB "M" file  (jim Patton) ~~~~~~~~~~~~~~~~~~~~~~~~
% Generate the state-borders to feasible dynamics based on constraints.
% The borders are the limits beyond which ANY state combintation is 
% feasible. This is a state-projection of the the edges of the football.
% Takes 2 COP constriants (heel and toe) for the inverted pendulum and 
% calculates the difference (absolute value) between the torques. This 
% is used in scalar optimization for borders of constriants.
% INPUTS : 	g,m,mf,r,miu,b,c 	needed in environment
% OUTPUTS :	theta_COP, 	(vector for plotting boundariaes)
%		Dtheta_COP 	(vector for plotting boundariaes)
%		Dtheta_FRIC	(vector for plotting boundariaes)
%		theta_F_C	(vector for plotting boundariaes)
%		Dtheta_F_C	(vector for plotting boundariaes)
% REVISIONS:	INITIATED 23 AUG 1994  by Jim Patton
%		6/17/97 cleaned up and made the domain larger
%~~~~~~~~~~~~~~~~~~~~ BEGIN: ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

disp('*** do_bdr.m (calculate where upper & lower bounds meet ) ***	 ...')
drawnow;

%_____ DONT DO THE WORK IF IT WAS DONE ALREADY _____
% TO MAKE MORE OF THESE (BELOW), USE:
% save FILENAME theta_COP Dtheta_COP Dtheta_FRIC theta_F_C Dtheta_F_C
%if miu==1,
%  fprintf('LOADING: brdrs_d '); load brdrs_d; fprintf(' Done. '); return;
%elseif miu==.05,
%  fprintf('LOADING: brdrs_p5 '); load brdrsp05; fprintf(' Done. ');  return;
%elseif miu_old==miu, fprintf(' no need to update borders! '); return; 
%end % if miu

%________ FIND THE CRITICAL COP CONSTRAINT BOUNDS ________
i = 0;
COP1=lf-a;	COP2=-a;				% TOE & HEEL constraints
theta_COP = 35/180*pi:1/180*pi:70/180*pi;		% domain for COP brdr
theta_F_C = 35/180*pi:.2/180*pi:90/180*pi; 		% domain for COP brdr 
fprintf('.'); pause(.05);				% tick mark

%___________ COP ____________
if ~(exist('Dtheta_COP')),
  for i = 1:length(theta_COP),
    Dtheta_COP(i)=fminbnd('cop_bdr',0/180*pi,500/180*pi, ...
       [], theta_COP(i), g, m, mf, r, b, c, COP1, COP2); 
  end %for theta
end %if
fprintf('.');

%__________ FRICTION ____________
if ~(exist('Dtheta_FRIC')),
  for i = 1:length(theta_COP),
    Dtheta_FRIC(i)=fminbnd('fric_bdr',0/180*pi,500/180*pi, ...
        [], theta_COP(i), g, m, mf, r, miu);
  end % for theta
end % if ~exist
fprintf('.');

%________ FRICTION (LOW)  & COP (HEEL)_________
for i = 1:length(theta_F_C),
  Dtheta_F_C(i)=fminbnd('h_c_bdr',0/180*pi,500/180*pi,	...
    [], theta_F_C(i), g, m, mf, r, miu, b, c, COP1, COP2);
end % for theta

%________ PLOT _______
clf;
plot(	theta_COP*180/pi,Dtheta_COP*180/pi, 'm',				...
	theta_F_C*180/pi,Dtheta_F_C*180/pi, 'b.',				...
	theta_COP*180/pi,Dtheta_FRIC*180/pi, 'g');
text(theta_COP(3)*180/pi,Dtheta_COP(3)*180/pi,'COP boundaries meet along this line');
text(theta_COP(3)*180/pi,Dtheta_FRIC(3)*180/pi,'FRICT. boundaries meet along this line');

miu_old=miu;

%______*** MATLAB "M" file do_bdr.m (jim Patton) ***_______

