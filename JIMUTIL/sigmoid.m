% SIGMOID: calc output of a sigmoidal unit based on activation 
% SYNTAX:   y = sigmoid(z,q,g, offset);
% INPUTS : 	z       activation
%           q       factor to scale z by before calc.
%           g       linear gain of final value
%           offset	linear offset of final value
% OUTPUTS:  y        
% INITIATED:	19 OCT 1994 by Jim Patton from "regres5.m" code
%             2014-Feb-21 tweak structure of code (no calculations) 
%             2019-Mar-26 tweak comments (no calculations) 
%____________ begin:______________

function y = sigmoid(z,q,g, offset);
%disp('*** sigmoid.m (function) ***'); 
y = g/(1+exp(-q*z)) + offset;
%disp('*** END sigmoid.m (function) ***'); 
return;

