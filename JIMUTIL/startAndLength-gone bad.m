%************** MATLAB "M" function  *************
% find startframe of velocity traj. based on threshold & adjust length 
% SYNTAX:     [St,len,speed]=startAndLength(v,maxLen,speedThresh);
% INPUTS:     v             n-by2 velocity vector (in R-2)
%             maxLen        maximum allowable-shorten v if too long
%             speedThresh   threshold of v for detecting movement onset
% OUTPUTS:    St            row # that start of movement was detected
%             len           trimmed length of v if v is longer than maxLen
%             speed         
% REVISIONS:  10-25-2000    init
%             07-15-2006    revised to have maxLen have more roust inputs
%                           and general formula for 2 or 3 dimensions
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~~~~~~~~~~


function [St,len,speed]=startAndLength(v,maxLen,speedThresh);

if ~exist('speedThresh'), speedThresh=.1; end               % if not passed
if ~exist('maxLen')|isempty(maxLen), maxLen=size(v,1); end  % if not passed
St=NaN;

% scan for start frame # (St)
for i=3:length(v(:,1)),                            % Loop:each v time step
  speed=sqrt(sum(v(i,:).^2))                       % speed calc
  if speed>speedThresh, St=i; end;                 % find where mvmt begins
end

% trim length if desired by the optional input maxLen
if St+maxLen>length(v(:,1))-2,                      % if exceeds maxLength
  len=length(v(:,1))-St-2;                          % reduce length(#frames)
else
  len=maxLen;
end



