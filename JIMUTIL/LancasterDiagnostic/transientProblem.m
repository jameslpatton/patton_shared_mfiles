function [xyHMat, xyDMat, xyHCell, xyDCell] =...
    transientProblem(xyHIn, xyDIn, vectorLength)
% transientProblem takes in raw healthy movement data and resamples it in
% order to demonstrate the resampling problem

xyHCell = cell(1,6);
xyDCell = cell(1,6);
[xyHMat, xyDMat] = deal([]);
for i = 1:6
    xyHCell{i} = resample(xyHIn{i},vectorLength,...
        length(xyHIn{i}),60,15);
    xyDCell{i} = resample(xyDIn{i},vectorLength,...
        length(xyDIn{i}),60,15);
%     xyHCell{i} = xyHCell{i}(1:(vectorLength/2),:);
%     xyDCell{i} = xyDCell{i}(1:(vectorLength/2),:);
    xyHMat = vertcat(xyHMat,xyHCell{i});
    xyDMat = vertcat(xyDMat,xyDCell{i});
end
end