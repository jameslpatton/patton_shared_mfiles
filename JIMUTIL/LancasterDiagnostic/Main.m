%Main.m 
%Demonstrates the transient problem

load('Sample_Healthy_Data.mat');

[xyHMat, xyDMat, xyHCell, xyDCell] = transientProblem(xyHIn, xyDIn, vectorLength);

figure(1)
for i = 1:6
    subplot(6,1,i)
    plot(xyHIn{i})
    if i == 1
        title('Raw Movement Data');
    end
end

figure(2)
for i = 1:6
    subplot(6,1,i)
    plot(xyHCell{i})
    if i == 1
        title('Resampled Movement Data');
    end
end

figure(3)
plot(xyHMat);
title('All Movements, Resampled')
xlabel('Time Step')
ylabel('Cartesian Position, m')