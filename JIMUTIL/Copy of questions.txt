q1 I enjoyed doing this activity very much
q2 This activity was fun to do
q3 I thought this was a boring activity.
q4 This activity did not hold my attention at all.
q5 I would describe this activity as very interesting
q6 I thought this activity was quite enjoyable
q7 While doing this activity, I was thought about how I enjoyed 8 it
q9 I think I am pretty good at this activity
q10 I think I did pretty well at this activity, compared to others
q11 After working at this activity for awhile, I felt pretty competent
q12 I am satisfied with my performance at this task
q13 I was pretty skilled at this activity
q14 This was an activity that I couldn�t do very well.
I put a lot of effort into this
I didn�t try very hard to do well at this activity.
I tried very hard on this activity
It was important to me to do well at this task
I didn�t put much energy into this.
I believe this activity could be of some value to me
I think that doing this activity is useful for moving better
I think this is important to do because it can make me move better
I would be willing to do this again because it has some value to me
I think doing this activity could help me to move better
I believe doing this activity could be beneficial to me
I think this is an important activity
-9999
