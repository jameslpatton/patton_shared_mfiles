

%************** MATLAB "M" function  *************
% (Patton) Mutidimensional histogram using N bins in each dimension
% syntax: [f,ctrs]=histo_n(x,N)
%          x is datapoint observations, with each column a dimension 
%          N is number of bins in each dimension 
%          bounds (optional) are the minimums (row 1) & manimums (row 2) 
%                 for each dim. Default is to take the maxs&mins of datapts 
%          plotIt (optional) set to 1 to plot. Default is 1 (to plot histo)
%          f is the frequency of observations in each bin
%          ctrs is each bin's centerpoint coordinates for each dimension
% This is crazy and may take lookign at the evernote picture for this
% EXAMPLES:   histo_n(randn(1000,2),30); 
%               Creates surface histogram for a 2D random normal data.
%             histo_n(randn(1000,3),10);
%               Creates bubble plot histogram for a 3D random normal data.
% REVISIONS:  2012-Sep-10 (patton) INITIATED
%             2013-Jan-3 (patton) altered figures,  cleaned up
% SEE ALSO: histo_3 (a faster fcn if you have 3d)
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~~~~~~~~~~

function [f,ctrs]=histo_n(x,N,bounds,plotIt)

%% ____ SETUP ____
prog_name='histo_n.m';                              % name of this program
fprintf(' ~ %s ~ ', prog_name);        % MSG
if ~exist('N','var'), N=10; end;                    % default if not passed
if ~exist('bounds','var')||isempty(bounds),
  bounds=[min(x);max(x)];
end% default if not passed
if ~exist('plotIt','var'), plotIt=1; end;           % default if not passed
nD=size(x,2);                                       % number of Dimentions
nX=size(x,1);                                       % numeber of values
fprintf('(%dvalues, %ddimensions, %dbins)',nX,nD,N);% MSG
divs=zeros(N+1,nD);                                  % intit bin divisions 
ctrs=zeros(N+1,nD);                                  % intit bin centers 


%% create grid 
R=bounds(2,:)-bounds(1,:);                          % all ranges
for d=1:nD,                                         % loopp for ea dimen.
  divs(:,d)=(bounds(1,d):R(d)/N:bounds(2,d))';      % N bin edge divisions           
  ctrs(:,d)=divs(:,d)-R(d)/(2*N);                   % coordnates, each bin
end
ctrs(1,:)=[];                                       % clip first one off 

%% dynamic code creation
% Three factors need to be looped:                   
% bin, dimension, and datapoint number. We can nest  
% the final and most tricky one, the dimension 
% (because it changes depending on the 
% input dataset) needs to be left for  
% last. if the data point is inside the many limits 
% of the bin, then mark The text cT is dynamic code
% for detecting data is in a hypervoxel.
cT='';                                              % init line codeText  
fT='f=zeros(';                                      % init initialization 
for d=1:nD                                          % each dimension
  if d~=1, fT=[fT ',']; end; fT=[fT 'N'];           % stack this dim
  cT=[cT 'for b' num2str(d) '=1:N, '];              % for loop for each dim
end                                                 % END dimensions
fT=[fT ');'];                                       % add closing stuff 
fprintf('\n Evaluating >> %s   ',fT)                % show this code
eval(fT);                                           % execute init f 

cT=[cT '  if('];                                    % start detection part
for d=1:nD                                          % each dimension
  dT=num2str(d);                                    % string dimension#
  if d~=1, cT=[cT ' & ']; end                       % if not first, put &
  cT=[cT ['x(i,' dT ')>=divs(b' dT ','   dT ')' ... % larger than low mark
         '&x(i,' dT ')<divs(b'  dT '+1,' dT ')']];  % &smaller than hi mark
end                                                 % end loop for each dim
cT=[cT '), '];                                      % cap if & add a comma
qT='f(';                                            % init var forUse later
for d=1:nD                                          % each dimension
  if d~=1, qT=[qT ',']; end;                        % if not first, put , 
  qT=[qT 'b' num2str(d)];                           % stack this dim
end                                                 % END for dimensio
qT=[qT ')'];                                        % add closing stuff 
cT=[cT qT '=' qT '+1; '];                           % incrrease count
cT=[cT 'end;   '];                                  % add an END
for d=1:nD, cT=[cT ' end;']; end                    % put 'end' for ea. dim
cT=[cT ' % dynamically created code'];              % cap it off

fprintf('\n Evaluating >> %s \n Sorting..',cT)      % show this code
for i=1:nX, eval(cT); end;                          % *** !! execute
fprintf('Done. ');                                  % message

%% PLOT if ...
fprintf(' Plotting...');                            % message
if plotIt==1,
  if nD==2, surf(f);                                % surface: 2D data              
  elseif nD==3,                                     % bubbleplot: 3D data
    %plot3(x(:,1),x(:,2),x(:,3),'r.','markersize',1); % plot raw data
    hold on; 
    for b1=1:N, for b2=1:N, for b3=1:N,             %
      mkrSz=(f(b1,b2,b3))*50/max(max(max(f)))+1;    % cal marker size
      plot3(ctrs(b1,1),ctrs(b2,2),ctrs(b3,3),'o',...% 
            'markersize',mkrSz,...          %
            'MarkerEdgeColor',[1 1 1], ...          %
            'MarkerFaceColor',[0 0 1] ); hold on    %
    end; end; end; 
    axis equal; grid on
  else bar(f);                                      % barplot: for other-D 
  end 
end 

fprintf(' ~ END %s ~ ', prog_name);                 % MSG
end % end fuctnion
