% ************** MATLAB "M" script (jim Patton) *************
% Updates the index.htm on  \\Sulu\C\Inetpub\wwwroot\~smpp_pub webpage
% SYNTAX:    updateSmppPub(remote)
% INPUTS:    remote   nonzero for VPN access from outside firewall 
% OUTPUTS:   index.htm file 
% VERSIONS:  6/13/03 added  input arg 'remote'
%            10/3/2011 altered directories
%~~~~~~~~~~~~~~~~~~~~~~ Begin Program: ~~~~~~~~~~~~~~~~~~~~~~~~

function updateSmppPub(remote)

% __ SETUP __
fcnName='updateSmppPub.m';
fprintf('\n\n\n~ %s SCRIPT ~\n',fcnName)             % title message
if ~exist('remote'), remote=0; end                   % if not passed

workingDir='C:\Users\James\Dropbox\personal\HTML\SMPPwebsite\~smpp_pub';
cd(workingDir);
cd

doWebPage(['.htm ';'.html'],[],[],[],0)

cd
fclose all

outFile=['/Users/James/Dropbox/personal/HTML/JimWebCopy/pubs.htm'];
IsolateWebPubs({'patton'},outFile)

outFile='/Users/James/Dropbox/personal/HTML/RobotLabPubs.htm';
matchList={'mussa-ivaldi'; 'patton'}
IsolateWebPubs(matchList,outFile)

return