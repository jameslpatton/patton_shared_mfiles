% main:    Main  script for startup of running an experiment
%************** MATLAB "M" SCRIPT ************
% This launces  A lauches xpcTarget for manipulandum robot 
% INPUTS:     Target file called by "transformtarget" function
% SYNTAX:     analysis 
% REVISIONS:  INITIATED by S. Acosta 
%             written by Y. Wei
%             modified by J. Patton 
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~

%% setup parameters
clc                                                 % Clear Command Window
clear all                                           % Clear all the vars
close all                                           % Close all figures,etc
clear global tg                                     % clear this global var
global tg
global disaxes;                                     % display information
global figaxes;                                     % figure information
global ExpParams;                                   % experiment paramters
trialstosv=20;                                      % #trials saved b4store
zeroforce=[0,0];                                    % no forces
%calib=load('C:\users\Manipulandum\CalibData.mat')  % not used yet
defaultDataDir=['D:\Patton_Shared_Data\sfd'];       % 

%% Startup options & dialog window
ExpDlg.SaveDir    = { {['uigetdir(''' ...           % directory
                         defaultDataDir ''')']} };  %
% ExpDlg.NewSubject = { {'{0}','1'} };                %
%ExpDlg.Debug      = { {'{0}','1'} ,'Debug mode'};  %
ExpDlg.SubjectNum = { 0 'Subject number' [1 1000]}; %
% ExpDlg.Compile    = { {'0','{1}'} , ...             %
%                      'Recompile mex and rtw files'};%
ExpDlg.startTrial = 1;                              %
ExpDlg.targetsFile = { {'{targ_sfd.txd}' ...        %
                        'targ_sfd_noforce.txd' ...  %
                        'testSFDtarg_sfd.txd'} };   %
disp(' Opening GUI Window for Paramaters...')       % 
ExpParams=StructDlg(ExpDlg,'Startup options')       % dialogWindow-params
disp(' Done getting Experimental Paramaters.')      % 
if isempty(ExpParams), disp(' Exit '); return; end  %

%% load sounds
fprintf('\n Loading Sounds..'); pause(.01);         % message
Snd_tooslow=wavread('too_slow.wav');                % too slow sound
Snd_tooslow(1:2500,:)=[];                           % clip this
Snd_toofast=wavread('too_fast.wav');                % too fast sound
Snd_toofast(1:900,:)=[];                            % clip this

%% load targets
transformtarget(ExpParams.targetsFile);             % data in weiXpcFormat
[List_ExpH,List_Exp]=hdrload('listtarget.m');       % load

%% compile
if(~exist('ExpParams.Compile') | ExpParams.Compile)
  fprintf('\n Compiling..'); pause(.01);            % message
  sp=0.01;                                          % sampling interval?
  mex forces.c                                      % build the .dll file
  mex intarget_trialtime1.c                         % build the .dll file
  rtwbuild('sfd_target');                           % realtime wkshop build
  tg.stoptime=inf;                                  % stop time infinite
  fprintf('\n\n Done Compiling. \n\n'); pause(.01); % message
end                                                 % END if ExpDlg.Compile

%% Start target and host
[figaxes,disaxes]=create_display;                   % init display on host
fprintf('\n Open Host..'); pause(.01); sfd_host;    % open host window
fprintf('\n Start Target..'); pause(.01); +tg;      % start the target
tg.status                                           % check if running
fprintf('\n Start Host..'); pause(.01);             % 
% Typing the following command twice makes the system mysteriously work!
set_param('sfd_host','SimulationCommand','start');  % initate host
set_param('sfd_host','SimulationCommand','start');  % initate host
fprintf('\n Host Started.\n\n\n'); drawnow;         % 

return 

