

function bio=sfd_targetbio(flag)
bio = [];
if nargin==0
  bio = [];
                    bio(1).blkName='Clock';
                    bio(1).sigName='NULL';
                    bio(1).portIdx=0;
                    bio(1).dim=[1,1];
                    bio(1).sigWidth=1;
                    bio(1).sigAddress='&sfd_target_B.Clock';

                    bio(2).blkName='E Gain';
                    bio(2).sigName='NULL';
                    bio(2).portIdx=0;
                    bio(2).dim=[1,1];
                    bio(2).sigWidth=1;
                    bio(2).sigAddress='&sfd_target_B.EGain';

                    bio(3).blkName='S Gain';
                    bio(3).sigName='NULL';
                    bio(3).portIdx=0;
                    bio(3).dim=[1,1];
                    bio(3).sigWidth=1;
                    bio(3).sigAddress='&sfd_target_B.SGain';

                    bio(4).blkName='Pack1';
                    bio(4).sigName='NULL';
                    bio(4).portIdx=0;
                    bio(4).dim=[40,1];
                    bio(4).sigWidth=40;
                    bio(4).sigAddress='&sfd_target_B.Pack1[0]';

                    bio(5).blkName='Receive/p1';
                    bio(5).sigName='NULL';
                    bio(5).portIdx=0;
                    bio(5).dim=[72,1];
                    bio(5).sigWidth=72;
                    bio(5).sigAddress='&sfd_target_B.Receive_o1[0]';

                    bio(6).blkName='Receive/p2';
                    bio(6).sigName='NULL';
                    bio(6).portIdx=1;
                    bio(6).dim=[1,1];
                    bio(6).sigWidth=1;
                    bio(6).sigAddress='&sfd_target_B.Receive_o2';

                    bio(7).blkName='S-Function1';
                    bio(7).sigName='NULL';
                    bio(7).portIdx=0;
                    bio(7).dim=[4,1];
                    bio(7).sigWidth=4;
                    bio(7).sigAddress='&sfd_target_B.SFunction1[0]';

                    bio(8).blkName='S-target conditions';
                    bio(8).sigName='NULL';
                    bio(8).portIdx=0;
                    bio(8).dim=[5,1];
                    bio(8).sigWidth=5;
                    bio(8).sigAddress='&sfd_target_B.Stargetconditions[0]';

                    bio(9).blkName='Unpack2/p1';
                    bio(9).sigName='NULL';
                    bio(9).portIdx=0;
                    bio(9).dim=[1,1];
                    bio(9).sigWidth=1;
                    bio(9).sigAddress='&sfd_target_B.Unpack2_o1';

                    bio(10).blkName='Unpack2/p2';
                    bio(10).sigName='NULL';
                    bio(10).portIdx=1;
                    bio(10).dim=[1,1];
                    bio(10).sigWidth=1;
                    bio(10).sigAddress='&sfd_target_B.Unpack2_o2';

                    bio(11).blkName='Unpack2/p3';
                    bio(11).sigName='NULL';
                    bio(11).portIdx=2;
                    bio(11).dim=[1,1];
                    bio(11).sigWidth=1;
                    bio(11).sigAddress='&sfd_target_B.Unpack2_o3';

                    bio(12).blkName='Unpack2/p4';
                    bio(12).sigName='NULL';
                    bio(12).portIdx=3;
                    bio(12).dim=[1,1];
                    bio(12).sigWidth=1;
                    bio(12).sigAddress='&sfd_target_B.Unpack2_o4';

                    bio(13).blkName='Unpack2/p5';
                    bio(13).sigName='NULL';
                    bio(13).portIdx=4;
                    bio(13).dim=[1,1];
                    bio(13).sigWidth=1;
                    bio(13).sigAddress='&sfd_target_B.Unpack2_o5';

                    bio(14).blkName='Unpack2/p6';
                    bio(14).sigName='NULL';
                    bio(14).portIdx=5;
                    bio(14).dim=[1,1];
                    bio(14).sigWidth=1;
                    bio(14).sigAddress='&sfd_target_B.Unpack2_o6';

                    bio(15).blkName='Unpack2/p7';
                    bio(15).sigName='NULL';
                    bio(15).portIdx=6;
                    bio(15).dim=[1,1];
                    bio(15).sigWidth=1;
                    bio(15).sigAddress='&sfd_target_B.Unpack2_o7';

                    bio(16).blkName='Unpack2/p8';
                    bio(16).sigName='NULL';
                    bio(16).portIdx=7;
                    bio(16).dim=[1,1];
                    bio(16).sigWidth=1;
                    bio(16).sigAddress='&sfd_target_B.Unpack2_o8';

                    bio(17).blkName='Unpack2/p9';
                    bio(17).sigName='NULL';
                    bio(17).portIdx=8;
                    bio(17).dim=[1,1];
                    bio(17).sigWidth=1;
                    bio(17).sigAddress='&sfd_target_B.Unpack2_o9';

                    bio(18).blkName='Subsystem/cos_th2';
                    bio(18).sigName='NULL';
                    bio(18).portIdx=0;
                    bio(18).dim=[1,1];
                    bio(18).sigWidth=1;
                    bio(18).sigAddress='&sfd_target_B.cos_th2';

                    bio(19).blkName='Subsystem/sin_th2';
                    bio(19).sigName='NULL';
                    bio(19).portIdx=0;
                    bio(19).dim=[1,1];
                    bio(19).sigWidth=1;
                    bio(19).sigAddress='&sfd_target_B.sin_th2';

                    bio(20).blkName='Subsystem/Xy';
                    bio(20).sigName='NULL';
                    bio(20).portIdx=0;
                    bio(20).dim=[1,1];
                    bio(20).sigWidth=1;
                    bio(20).sigAddress='&sfd_target_B.Xy';

                    bio(21).blkName='Subsystem/Xy2';
                    bio(21).sigName='NULL';
                    bio(21).portIdx=0;
                    bio(21).dim=[1,1];
                    bio(21).sigWidth=1;
                    bio(21).sigAddress='&sfd_target_B.Xy2';

                    bio(22).blkName='Subsystem/f';
                    bio(22).sigName='NULL';
                    bio(22).portIdx=0;
                    bio(22).dim=[1,1];
                    bio(22).sigWidth=1;
                    bio(22).sigAddress='&sfd_target_B.f';

                    bio(23).blkName='Subsystem/f1';
                    bio(23).sigName='NULL';
                    bio(23).portIdx=0;
                    bio(23).dim=[1,1];
                    bio(23).sigWidth=1;
                    bio(23).sigAddress='&sfd_target_B.f1';

                    bio(24).blkName='Subsystem/Fxcos(th2)';
                    bio(24).sigName='NULL';
                    bio(24).portIdx=0;
                    bio(24).dim=[1,1];
                    bio(24).sigWidth=1;
                    bio(24).sigAddress='&sfd_target_B.Fxcosth2';

                    bio(25).blkName='Subsystem/Fxsin(th2)';
                    bio(25).sigName='NULL';
                    bio(25).portIdx=0;
                    bio(25).dim=[1,1];
                    bio(25).sigWidth=1;
                    bio(25).sigAddress='&sfd_target_B.Fxsinth2';

                    bio(26).blkName='Subsystem/Fycos(th2)';
                    bio(26).sigName='NULL';
                    bio(26).portIdx=0;
                    bio(26).dim=[1,1];
                    bio(26).sigWidth=1;
                    bio(26).sigAddress='&sfd_target_B.Fycosth2';

                    bio(27).blkName='Subsystem/Fysin(th2)';
                    bio(27).sigName='NULL';
                    bio(27).portIdx=0;
                    bio(27).dim=[1,1];
                    bio(27).sigWidth=1;
                    bio(27).sigAddress='&sfd_target_B.Fysinth2';

                    bio(28).blkName='Subsystem/PCI-6031E/p1';
                    bio(28).sigName='NULL';
                    bio(28).portIdx=0;
                    bio(28).dim=[1,1];
                    bio(28).sigWidth=1;
                    bio(28).sigAddress='&sfd_target_B.PCI6031E_o1';

                    bio(29).blkName='Subsystem/PCI-6031E/p2';
                    bio(29).sigName='NULL';
                    bio(29).portIdx=1;
                    bio(29).dim=[1,1];
                    bio(29).sigWidth=1;
                    bio(29).sigAddress='&sfd_target_B.PCI6031E_o2';

                    bio(30).blkName='Subsystem/PCI-6031E/p3';
                    bio(30).sigName='NULL';
                    bio(30).portIdx=2;
                    bio(30).dim=[1,1];
                    bio(30).sigWidth=1;
                    bio(30).sigAddress='&sfd_target_B.PCI6031E_o3';

                    bio(31).blkName='Subsystem/PCI-6031E/p4';
                    bio(31).sigName='NULL';
                    bio(31).portIdx=3;
                    bio(31).dim=[1,1];
                    bio(31).sigWidth=1;
                    bio(31).sigAddress='&sfd_target_B.PCI6031E_o4';

                    bio(32).blkName='Subsystem/PCI-6031E/p5';
                    bio(32).sigName='NULL';
                    bio(32).portIdx=4;
                    bio(32).dim=[1,1];
                    bio(32).sigWidth=1;
                    bio(32).sigAddress='&sfd_target_B.PCI6031E_o5';

                    bio(33).blkName='Subsystem/PCI-6031E/p6';
                    bio(33).sigName='NULL';
                    bio(33).portIdx=5;
                    bio(33).dim=[1,1];
                    bio(33).sigWidth=1;
                    bio(33).sigAddress='&sfd_target_B.PCI6031E_o6';

                    bio(34).blkName='Subsystem/Sum';
                    bio(34).sigName='NULL';
                    bio(34).portIdx=0;
                    bio(34).dim=[1,1];
                    bio(34).sigWidth=1;
                    bio(34).sigAddress='&sfd_target_B.Sum_e';

                    bio(35).blkName='Subsystem/Sum1';
                    bio(35).sigName='NULL';
                    bio(35).portIdx=0;
                    bio(35).dim=[1,1];
                    bio(35).sigWidth=1;
                    bio(35).sigAddress='&sfd_target_B.Sum1_h';

                    bio(36).blkName='Subsystem/Elbow Angle/Pulse Generator1';
                    bio(36).sigName='NULL';
                    bio(36).portIdx=0;
                    bio(36).dim=[1,1];
                    bio(36).sigWidth=1;
                    bio(36).sigAddress='&sfd_target_B.PulseGenerator1';

                    bio(37).blkName='Subsystem/Elbow Angle/Encoder Radians';
                    bio(37).sigName='NULL';
                    bio(37).portIdx=0;
                    bio(37).dim=[1,1];
                    bio(37).sigWidth=1;
                    bio(37).sigAddress='&sfd_target_B.EncoderRadians';

                    bio(38).blkName='Subsystem/Elbow Angle/Offset in Radians';
                    bio(38).sigName='NULL';
                    bio(38).portIdx=0;
                    bio(38).dim=[1,1];
                    bio(38).sigWidth=1;
                    bio(38).sigAddress='&sfd_target_B.OffsetinRadians_h';

                    bio(39).blkName='Subsystem/Elbow Angle/Bit1/p1';
                    bio(39).sigName='NULL';
                    bio(39).portIdx=0;
                    bio(39).dim=[1,1];
                    bio(39).sigWidth=1;
                    bio(39).sigAddress='&sfd_target_B.Bit1_o1';

                    bio(40).blkName='Subsystem/Elbow Angle/Bit1/p2';
                    bio(40).sigName='NULL';
                    bio(40).portIdx=1;
                    bio(40).dim=[1,1];
                    bio(40).sigWidth=1;
                    bio(40).sigAddress='&sfd_target_B.Bit1_o2';

                    bio(41).blkName='Subsystem/Elbow Angle/Bits1-8 1/p1';
                    bio(41).sigName='NULL';
                    bio(41).portIdx=0;
                    bio(41).dim=[1,1];
                    bio(41).sigWidth=1;
                    bio(41).sigAddress='&sfd_target_B.Bits181_o1';

                    bio(42).blkName='Subsystem/Elbow Angle/Bits1-8 1/p2';
                    bio(42).sigName='NULL';
                    bio(42).portIdx=1;
                    bio(42).dim=[1,1];
                    bio(42).sigWidth=1;
                    bio(42).sigAddress='&sfd_target_B.Bits181_o2';

                    bio(43).blkName='Subsystem/Elbow Angle/Bits1-8 1/p3';
                    bio(43).sigName='NULL';
                    bio(43).portIdx=2;
                    bio(43).dim=[1,1];
                    bio(43).sigWidth=1;
                    bio(43).sigAddress='&sfd_target_B.Bits181_o3';

                    bio(44).blkName='Subsystem/Elbow Angle/Bits1-8 1/p4';
                    bio(44).sigName='NULL';
                    bio(44).portIdx=3;
                    bio(44).dim=[1,1];
                    bio(44).sigWidth=1;
                    bio(44).sigAddress='&sfd_target_B.Bits181_o4';

                    bio(45).blkName='Subsystem/Elbow Angle/Bits1-8 1/p5';
                    bio(45).sigName='NULL';
                    bio(45).portIdx=4;
                    bio(45).dim=[1,1];
                    bio(45).sigWidth=1;
                    bio(45).sigAddress='&sfd_target_B.Bits181_o5';

                    bio(46).blkName='Subsystem/Elbow Angle/Bits1-8 1/p6';
                    bio(46).sigName='NULL';
                    bio(46).portIdx=5;
                    bio(46).dim=[1,1];
                    bio(46).sigWidth=1;
                    bio(46).sigAddress='&sfd_target_B.Bits181_o6';

                    bio(47).blkName='Subsystem/Elbow Angle/Bits1-8 1/p7';
                    bio(47).sigName='NULL';
                    bio(47).portIdx=6;
                    bio(47).dim=[1,1];
                    bio(47).sigWidth=1;
                    bio(47).sigAddress='&sfd_target_B.Bits181_o7';

                    bio(48).blkName='Subsystem/Elbow Angle/Bits1-8 1/p8';
                    bio(48).sigName='NULL';
                    bio(48).portIdx=7;
                    bio(48).dim=[1,1];
                    bio(48).sigWidth=1;
                    bio(48).sigAddress='&sfd_target_B.Bits181_o8';

                    bio(49).blkName='Subsystem/Elbow Angle/Bits9-1/p1';
                    bio(49).sigName='NULL';
                    bio(49).portIdx=0;
                    bio(49).dim=[1,1];
                    bio(49).sigWidth=1;
                    bio(49).sigAddress='&sfd_target_B.Bits91_o1';

                    bio(50).blkName='Subsystem/Elbow Angle/Bits9-1/p2';
                    bio(50).sigName='NULL';
                    bio(50).portIdx=1;
                    bio(50).dim=[1,1];
                    bio(50).sigWidth=1;
                    bio(50).sigAddress='&sfd_target_B.Bits91_o2';

                    bio(51).blkName='Subsystem/Elbow Angle/Bits9-1/p3';
                    bio(51).sigName='NULL';
                    bio(51).portIdx=2;
                    bio(51).dim=[1,1];
                    bio(51).sigWidth=1;
                    bio(51).sigAddress='&sfd_target_B.Bits91_o3';

                    bio(52).blkName='Subsystem/Elbow Angle/Bits9-1/p4';
                    bio(52).sigName='NULL';
                    bio(52).portIdx=3;
                    bio(52).dim=[1,1];
                    bio(52).sigWidth=1;
                    bio(52).sigAddress='&sfd_target_B.Bits91_o4';

                    bio(53).blkName='Subsystem/Elbow Angle/Bits9-1/p5';
                    bio(53).sigName='NULL';
                    bio(53).portIdx=4;
                    bio(53).dim=[1,1];
                    bio(53).sigWidth=1;
                    bio(53).sigAddress='&sfd_target_B.Bits91_o5';

                    bio(54).blkName='Subsystem/Elbow Angle/Bits9-1/p6';
                    bio(54).sigName='NULL';
                    bio(54).portIdx=5;
                    bio(54).dim=[1,1];
                    bio(54).sigWidth=1;
                    bio(54).sigAddress='&sfd_target_B.Bits91_o6';

                    bio(55).blkName='Subsystem/Elbow Angle/Bits9-1/p7';
                    bio(55).sigName='NULL';
                    bio(55).portIdx=6;
                    bio(55).dim=[1,1];
                    bio(55).sigWidth=1;
                    bio(55).sigAddress='&sfd_target_B.Bits91_o7';

                    bio(56).blkName='Subsystem/Elbow Angle/Bits9-1/p8';
                    bio(56).sigName='NULL';
                    bio(56).portIdx=7;
                    bio(56).dim=[1,1];
                    bio(56).sigWidth=1;
                    bio(56).sigAddress='&sfd_target_B.Bits91_o8';

                    bio(57).blkName='Subsystem/Elbow Angle/Encoder Decimal';
                    bio(57).sigName='NULL';
                    bio(57).portIdx=0;
                    bio(57).dim=[1,1];
                    bio(57).sigWidth=1;
                    bio(57).sigAddress='&sfd_target_B.EncoderDecimal';

                    bio(58).blkName='Subsystem/Elbow Angle/Sum';
                    bio(58).sigName='NULL';
                    bio(58).portIdx=0;
                    bio(58).dim=[1,1];
                    bio(58).sigWidth=1;
                    bio(58).sigAddress='&sfd_target_B.Sum_c';

                    bio(59).blkName='Subsystem/Elbow Angle/Unit Delay';
                    bio(59).sigName='NULL';
                    bio(59).portIdx=0;
                    bio(59).dim=[17,1];
                    bio(59).sigWidth=17;
                    bio(59).sigAddress='&sfd_target_B.UnitDelay_b[0]';

                    bio(60).blkName='Subsystem/Jacobian-Cartesian Transformation/J00';
                    bio(60).sigName='NULL';
                    bio(60).portIdx=0;
                    bio(60).dim=[1,1];
                    bio(60).sigWidth=1;
                    bio(60).sigAddress='&sfd_target_B.J00';

                    bio(61).blkName='Subsystem/Jacobian-Cartesian Transformation/J01';
                    bio(61).sigName='NULL';
                    bio(61).portIdx=0;
                    bio(61).dim=[1,1];
                    bio(61).sigWidth=1;
                    bio(61).sigAddress='&sfd_target_B.J01';

                    bio(62).blkName='Subsystem/Jacobian-Cartesian Transformation/J10';
                    bio(62).sigName='NULL';
                    bio(62).portIdx=0;
                    bio(62).dim=[1,1];
                    bio(62).sigWidth=1;
                    bio(62).sigAddress='&sfd_target_B.J10';

                    bio(63).blkName='Subsystem/Jacobian-Cartesian Transformation/J11';
                    bio(63).sigName='NULL';
                    bio(63).portIdx=0;
                    bio(63).dim=[1,1];
                    bio(63).sigWidth=1;
                    bio(63).sigAddress='&sfd_target_B.J11';

                    bio(64).blkName='Subsystem/Jacobian-Cartesian Transformation/Sum';
                    bio(64).sigName='NULL';
                    bio(64).portIdx=0;
                    bio(64).dim=[1,1];
                    bio(64).sigWidth=1;
                    bio(64).sigAddress='&sfd_target_B.Sum_a';

                    bio(65).blkName='Subsystem/Jacobian-Cartesian Transformation/Sum1';
                    bio(65).sigName='NULL';
                    bio(65).portIdx=0;
                    bio(65).dim=[1,1];
                    bio(65).sigWidth=1;
                    bio(65).sigAddress='&sfd_target_B.Sum1';

                    bio(66).blkName='Subsystem/Shoulder Angle/Pulse Generator';
                    bio(66).sigName='NULL';
                    bio(66).portIdx=0;
                    bio(66).dim=[1,1];
                    bio(66).sigWidth=1;
                    bio(66).sigAddress='&sfd_target_B.PulseGenerator';

                    bio(67).blkName='Subsystem/Shoulder Angle/Offset in Radians';
                    bio(67).sigName='NULL';
                    bio(67).portIdx=0;
                    bio(67).dim=[1,1];
                    bio(67).sigWidth=1;
                    bio(67).sigAddress='&sfd_target_B.OffsetinRadians';

                    bio(68).blkName='Subsystem/Shoulder Angle/Product1';
                    bio(68).sigName='NULL';
                    bio(68).portIdx=0;
                    bio(68).dim=[1,1];
                    bio(68).sigWidth=1;
                    bio(68).sigAddress='&sfd_target_B.Product1';

                    bio(69).blkName='Subsystem/Shoulder Angle/Bit17/p1';
                    bio(69).sigName='NULL';
                    bio(69).portIdx=0;
                    bio(69).dim=[1,1];
                    bio(69).sigWidth=1;
                    bio(69).sigAddress='&sfd_target_B.Bit17_o1';

                    bio(70).blkName='Subsystem/Shoulder Angle/Bit17/p2';
                    bio(70).sigName='NULL';
                    bio(70).portIdx=1;
                    bio(70).dim=[1,1];
                    bio(70).sigWidth=1;
                    bio(70).sigAddress='&sfd_target_B.Bit17_o2';

                    bio(71).blkName='Subsystem/Shoulder Angle/Bits1-8/p1';
                    bio(71).sigName='NULL';
                    bio(71).portIdx=0;
                    bio(71).dim=[1,1];
                    bio(71).sigWidth=1;
                    bio(71).sigAddress='&sfd_target_B.Bits18_o1';

                    bio(72).blkName='Subsystem/Shoulder Angle/Bits1-8/p2';
                    bio(72).sigName='NULL';
                    bio(72).portIdx=1;
                    bio(72).dim=[1,1];
                    bio(72).sigWidth=1;
                    bio(72).sigAddress='&sfd_target_B.Bits18_o2';

                    bio(73).blkName='Subsystem/Shoulder Angle/Bits1-8/p3';
                    bio(73).sigName='NULL';
                    bio(73).portIdx=2;
                    bio(73).dim=[1,1];
                    bio(73).sigWidth=1;
                    bio(73).sigAddress='&sfd_target_B.Bits18_o3';

                    bio(74).blkName='Subsystem/Shoulder Angle/Bits1-8/p4';
                    bio(74).sigName='NULL';
                    bio(74).portIdx=3;
                    bio(74).dim=[1,1];
                    bio(74).sigWidth=1;
                    bio(74).sigAddress='&sfd_target_B.Bits18_o4';

                    bio(75).blkName='Subsystem/Shoulder Angle/Bits1-8/p5';
                    bio(75).sigName='NULL';
                    bio(75).portIdx=4;
                    bio(75).dim=[1,1];
                    bio(75).sigWidth=1;
                    bio(75).sigAddress='&sfd_target_B.Bits18_o5';

                    bio(76).blkName='Subsystem/Shoulder Angle/Bits1-8/p6';
                    bio(76).sigName='NULL';
                    bio(76).portIdx=5;
                    bio(76).dim=[1,1];
                    bio(76).sigWidth=1;
                    bio(76).sigAddress='&sfd_target_B.Bits18_o6';

                    bio(77).blkName='Subsystem/Shoulder Angle/Bits1-8/p7';
                    bio(77).sigName='NULL';
                    bio(77).portIdx=6;
                    bio(77).dim=[1,1];
                    bio(77).sigWidth=1;
                    bio(77).sigAddress='&sfd_target_B.Bits18_o7';

                    bio(78).blkName='Subsystem/Shoulder Angle/Bits1-8/p8';
                    bio(78).sigName='NULL';
                    bio(78).portIdx=7;
                    bio(78).dim=[1,1];
                    bio(78).sigWidth=1;
                    bio(78).sigAddress='&sfd_target_B.Bits18_o8';

                    bio(79).blkName='Subsystem/Shoulder Angle/Bits9-16/p1';
                    bio(79).sigName='NULL';
                    bio(79).portIdx=0;
                    bio(79).dim=[1,1];
                    bio(79).sigWidth=1;
                    bio(79).sigAddress='&sfd_target_B.Bits916_o1';

                    bio(80).blkName='Subsystem/Shoulder Angle/Bits9-16/p2';
                    bio(80).sigName='NULL';
                    bio(80).portIdx=1;
                    bio(80).dim=[1,1];
                    bio(80).sigWidth=1;
                    bio(80).sigAddress='&sfd_target_B.Bits916_o2';

                    bio(81).blkName='Subsystem/Shoulder Angle/Bits9-16/p3';
                    bio(81).sigName='NULL';
                    bio(81).portIdx=2;
                    bio(81).dim=[1,1];
                    bio(81).sigWidth=1;
                    bio(81).sigAddress='&sfd_target_B.Bits916_o3';

                    bio(82).blkName='Subsystem/Shoulder Angle/Bits9-16/p4';
                    bio(82).sigName='NULL';
                    bio(82).portIdx=3;
                    bio(82).dim=[1,1];
                    bio(82).sigWidth=1;
                    bio(82).sigAddress='&sfd_target_B.Bits916_o4';

                    bio(83).blkName='Subsystem/Shoulder Angle/Bits9-16/p5';
                    bio(83).sigName='NULL';
                    bio(83).portIdx=4;
                    bio(83).dim=[1,1];
                    bio(83).sigWidth=1;
                    bio(83).sigAddress='&sfd_target_B.Bits916_o5';

                    bio(84).blkName='Subsystem/Shoulder Angle/Bits9-16/p6';
                    bio(84).sigName='NULL';
                    bio(84).portIdx=5;
                    bio(84).dim=[1,1];
                    bio(84).sigWidth=1;
                    bio(84).sigAddress='&sfd_target_B.Bits916_o6';

                    bio(85).blkName='Subsystem/Shoulder Angle/Bits9-16/p7';
                    bio(85).sigName='NULL';
                    bio(85).portIdx=6;
                    bio(85).dim=[1,1];
                    bio(85).sigWidth=1;
                    bio(85).sigAddress='&sfd_target_B.Bits916_o7';

                    bio(86).blkName='Subsystem/Shoulder Angle/Bits9-16/p8';
                    bio(86).sigName='NULL';
                    bio(86).portIdx=7;
                    bio(86).dim=[1,1];
                    bio(86).sigWidth=1;
                    bio(86).sigAddress='&sfd_target_B.Bits916_o8';

                    bio(87).blkName='Subsystem/Shoulder Angle/Dot Product';
                    bio(87).sigName='NULL';
                    bio(87).portIdx=0;
                    bio(87).dim=[1,1];
                    bio(87).sigWidth=1;
                    bio(87).sigAddress='&sfd_target_B.DotProduct';

                    bio(88).blkName='Subsystem/Shoulder Angle/Sum';
                    bio(88).sigName='NULL';
                    bio(88).portIdx=0;
                    bio(88).dim=[1,1];
                    bio(88).sigWidth=1;
                    bio(88).sigAddress='&sfd_target_B.Sum';

                    bio(89).blkName='Subsystem/Shoulder Angle/Unit Delay';
                    bio(89).sigName='NULL';
                    bio(89).portIdx=0;
                    bio(89).dim=[17,1];
                    bio(89).sigWidth=17;
                    bio(89).sigAddress='&sfd_target_B.UnitDelay[0]';

                    bio(90).blkName='Subsystem/Elbow Angle/Parity/Math Function';
                    bio(90).sigName='NULL';
                    bio(90).portIdx=0;
                    bio(90).dim=[1,1];
                    bio(90).sigWidth=1;
                    bio(90).sigAddress='&sfd_target_B.MathFunction';

                    bio(91).blkName='Subsystem/Elbow Angle/Parity/Sum';
                    bio(91).sigName='NULL';
                    bio(91).portIdx=0;
                    bio(91).dim=[1,1];
                    bio(91).sigWidth=1;
                    bio(91).sigAddress='&sfd_target_B.Sum_f';

                    bio(92).blkName='Subsystem/Shoulder Angle/Parity/Math Function';
                    bio(92).sigName='NULL';
                    bio(92).portIdx=0;
                    bio(92).dim=[1,1];
                    bio(92).sigWidth=1;
                    bio(92).sigAddress='&sfd_target_B.MathFunction_f';

                    bio(93).blkName='Subsystem/Shoulder Angle/Parity/Sum';
                    bio(93).sigName='NULL';
                    bio(93).portIdx=0;
                    bio(93).dim=[1,1];
                    bio(93).sigWidth=1;
                    bio(93).sigAddress='&sfd_target_B.Sum_k';





else
   bio= 242;
end
