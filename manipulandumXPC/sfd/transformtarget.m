% transformTarget:  change dos system targets file & convert to XPC format    
%********************* MATLAB "M" Function *******************
% This file is to change Jim's style target file to a target file that XPC
% code can call from.  Baisc operation includes: 
% 1) Specify the movement type, either center-out or random-walk movement;
% 2) Transform the target to new visually seen target position if there is visual distortion;
% 3) Form a list of movement wbased on the order of the 
%      N_trial N_movement X_start Y_start X_target Y_target Field_Type
%      Direction Phase  Optional_e
% INPUTS:     
% SYNTAX:      
% REVISIONS:  Development by Yejun Wei, Auguet 14, 2003.
%             modified by J. Patton 1-2007
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function transformtarget(inName)

%% setup
outName='listtarget.m';
fid=fopen(outName,'w');   % open output file for writing

if ~exist('inName','var'),
  %inName=input('Input Protocol File Name: [targ_sfd.txd] ','s');
  %if isempty(inName), inName = 'targ_sa.txd'; end
  addforce=input('Turn on forces? [Y]','s');
  if addforce == 'n'    inName = 'targ_sfd_noforce.txd';
  else                  inName = 'targ_sfd.txd';
  end
end % END if ~exist

fprintf('\n~ transformtarget.m ~ transforming "%s" to "%s" ...', inName,outName);pause(.01);    % message 
[h,d]=hdrload(inName);
offset =0;
UGain=1;
[dRow,dCol]=size(d);

y=[0   1   d(1,2)*UGain  d(1,3)*UGain-offset  0 0.45   d(1,6)  d(1,7)  d(1,9)  d(1,10) 0    0 0 0];
fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);

if dCol == 10
    for i=1:length(d)
      y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)  d(i,10) 0    0 0 0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
 
      y=[d(i,1)   2   d(i,4)*UGain  d(i,5)*UGain-offset  d(i,2)*UGain    d(i,3)*UGain-offset    d(i,6)*0  d(i,7)  d(i,9)  d(i,10) 0   0   0   0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
   
  end
  
%   i=length(d);
%   y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)   d(i,10)    0 0 0 0];
%   fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
%   
%   
end


if dCol == 9 
    for i=1:length(d)
      y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)  0 0    0 0 0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
   
      y=[d(i,1)   2   d(i,4)*UGain  d(i,5)*UGain-offset  d(i+1,2)*UGain    d(i+1,3)*UGain-offset    d(i,6)*0  d(i,7)  d(i,9)  0 0   0   0   0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
  end
  
  i=length(d);
  y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)   0    0 0 0 0];
  fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
end

fclose(fid);
fprintf(' Done tranforming. \n');pause(.01);    % message 


    