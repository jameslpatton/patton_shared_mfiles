% create_display:    create feedback display for the experiment
%************** MATLAB "M" SCRIPT ************
% INPUTS:     
% SYNTAX:      
% REVISIONS:  written by Y. Wei
%             1/2007 modified by J. Patton 
%~~~~~~~~~~~~~~~~~~~~~ Begin : ~~~~~~~~~~~~~~~

function [f,h]=create_display()
f=figure;

fprintf('\n Seting up the Host Display..'); pause(.01);          % 
zoomt=1.2;
scnsize = get(0,'ScreenSize');
hAxes=axes;
set(gcf,'Color','k')
set(hAxes,'Color','k','Position', [0.00 0.00 .99 .99]);
set(gca,'YDir','reverse');
set(gcf,'Position',[scnsize(1) scnsize(2)-100 scnsize(3) scnsize(4)+20],'MenuBar','none');

set(hAxes,'XLim',[scnsize(3)/2-200,scnsize(3)/2+200],'YLim',[scnsize(4)/2-200,scnsize(4)/2+200])
axis equal manual
hLine=line('Parent',hAxes,'Xdata',0,'Ydata',0,'Marker','o','Color','g','LineWidth',2); % marker is bigger (to increase the attention of the patient
hLine=line('Parent',hAxes,'Xdata',100,'Ydata',0,'Marker','o','Color','r','LineWidth',2);
hLine=line('Parent',hAxes,'Xdata',0,'Ydata',100,'Marker','o','Color','y','LineWidth',2);

set(gcf,'DoubleBuffer','On');
set(gcf,'BackingStore','off')

h=gca;
pause(1); % NECESSARY FOR BELOW TO WORK (DO NOT KNOW WHY...)
put_fig(gcf,-.8,.251,.802,.7495); drawnow; % this makes it appear full on the left screen
fprintf(' Done.\n'); pause(.01);          % 


