%______*** MATLAB "M" script (by jim Patton) ***_______
% The sets up JIM PATTON'S directroies to search for in matlab.
% CALLS :      path.m	(matlab file)
% INITIATED:   10-12-98 by jim patton 
%______________________________________________________

  disp('____________________ startup.m ___________________________ ');
  fprintf('Setting up for JIM"s "Patton_shares_MFILES" mfiles library \n-- see: ');
  which jim
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES\CONTRIB
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES\CONTRIB\EZtools
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES\JIMUTIL\digitize
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES\JIMUTIL
  addpath \\R2D2\C\USER\robotLab\patton_shares_MFILES\robotStuff

  disp('Directories have been are added to the path. Type PATH to see'); 

  global DEBUGIT 
  DEBUGIT=0;		% default is for no debugging to be done
  disp('variable DEBUGIT has been set to global.');
  
disp('________________ END startup.m __________________ ');
fprintf('\7');
% load ('train');	sound(y,Fs*6); clear y; clear Fs

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

