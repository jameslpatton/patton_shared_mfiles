
/* 
 *  File     : intarget.c  'S-Function in C for XPC target'
 *  Abstract : Evaluates the conditions for deciding if a target position has been reached.
 *			   The conditions change depending on the falg feedback type.
 *			   If feedback type is 0 the posiotion of the endpoint its velocity and the time
 *			   spent in the target are evaluated. If it is 1 the position of the endpoint projection
 *			   ,on the a line going from the origin to the target, is evaluated.
 *			 
 *      
 *      Input: Robot's endpoint position (X,Y), target position Xt,Yt 
 *			   feedback type:0 normal 1 if the proyection is in target.
 *      
 *
 *    Output :  1 if experimental conditions on position velocity and time are met 
 *				according to feedback type.
 *              0 otherwise. 
 *      
 *  Santiago Acosta 02-12-03
 */


#define S_FUNCTION_NAME intarget_trialtime1
#define S_FUNCTION_LEVEL 2
#define PI 3.14159
#define NDIRECTION 6

#include "simstruc.h"


#define U(element) (*uPtrs[element])  /* Pointer to Input Port0 */


#define INF_TARGET_SIZE 10000  /* 'Infinite target size'  in mm */
#define INF_DISPLAY 100000 /* out of plotting area */



/* PARAMETERS */

	/* 
	 *  Target size :'TS' 
	 */
#define TS_IDX 0
#define TS_PARAM(S) ssGetSFcnParam(S,TS_IDX)
 	/* 
	 *  Hold Interval :'HI' 
	 */
#define HI_IDX 1
#define HI_PARAM(S) ssGetSFcnParam(S,HI_IDX)
 	/* 
	 *  Velocity Threshhold :'VT' 
	 */
#define VT_IDX 2
#define VT_PARAM(S) ssGetSFcnParam(S,VT_IDX)
 
#define NPARAMS 3


real_T x_h[3]= { 0.0,0.0,0.0 };
real_T y_h[3]= { 0.0,0.0,0.0 };

real_T stime = 0.0; /* sampling time */
real_T samples_in_target=0.0; /* number of samples correspondig to parameter duration in target */ 
real_T target_size= 0.0;
real_T vel_thresh=0.0;

static int_T cnt=1;
static real_T countSamples;
static int_T update = 0;
static real_T maxVel = 0;
static real_T LastTrialmaxVel=0;
static real_T TimePerTrial=0;
static real_T Twaiting=1;

/*====================*
 * S-function methods *
 *====================*/

#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
  /* Function: mdlCheckParameters =============================================
   * Abstract:
   *    Validate our parameters to verify they are okay.
   */
  static void mdlCheckParameters(SimStruct *S)
  {
      /* Check 1st parameter: target size */
      {
          if (!mxIsDouble(TS_PARAM(S)) ||
              mxGetNumberOfElements(TS_PARAM(S)) != 1) {
              ssSetErrorStatus(S,"1st parameter to S-function must be a "
                               "scalar \"lower bound\" which limits "
                               "the size of the target in m");
              return;
          }
      }
 
      /* Check 2nd parameter: hold time*/
      {
          if (!mxIsDouble(HI_PARAM(S)) ||
              mxGetNumberOfElements(HI_PARAM(S)) != 1) {
              ssSetErrorStatus(S,"2nd parameter to S-function must be a "
                               "scalar \"upper bound\" which defines "
                               "the time the subject must be in the target in sec.");
              return;
          }
      }
 
      /* Check 3nd parameter: velocity threshold */
      {
          if (!mxIsDouble(VT_PARAM(S)) ||
              mxGetNumberOfElements(VT_PARAM(S)) != 1) {
              ssSetErrorStatus(S,"3rd parameter to S-function must be a "
                               "scalar \"initial value\" which defines "
                               "maximum velocity allowed inside the target in m/sec.");
              return;
          }
      }
  }
#endif /* MDL_CHECK_PARAMETERS */
 


/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{
  
    ssSetNumSFcnParams(S, NPARAMS);  /* Number of expected parameters */
     
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 3); /* discrete states xdot, ydot, velocity (sqrt(xdot^2 + ydot^2)) */ 

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, 8);  /* Five Inputs: X, Y, Xt, Yt,feedback type 0 or 1)  */
    ssSetInputPortDirectFeedThrough(S, 0, 1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, 5);  /*  target reached flag and position or false position */

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 0);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}



/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME); 
    ssSetOffsetTime(S, 0, 0.0);
}



#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ========================================
 * Abstract:
 *    Initialize discrete states .
 */
static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);
    int_T i;
 
		x0[0]=0;    /* xdot = 0 */
        x0[1]=0;    /* ydot = 0 */
		x0[2]=0;    /* velocity */

	stime = ssGetSampleTime(S,0); /* Get the sampling period */	

/*
 * Convert parameters to global variables so that you only call callback functions once
 * at the begining of the simulation. This is intended for Dialog Parameter only.
 * Care should be taken if the parameters are changed during simulation (runtime parameters) 
 * in which case a different procedure needs to be implemented.
 * (see runtime parameters in the 'S-functions' documentation.
 */
	 
	 samples_in_target= *mxGetPr(HI_PARAM(S))/stime;
	 target_size      = *mxGetPr(TS_PARAM(S));
	 vel_thresh       = *mxGetPr(VT_PARAM(S));
										/* try this:  *mxGetPr(HI_PARAM(S)) */
}



/* Function: mdlOutputs =======================================================
 * Abstract:
 *      
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
	real_T            tempx ;
	real_T			  tempy ;
	int_T vftype;
	real_T expTargetSize;
    int_T   i, j;
    real_T toCenter=0;
	int_T   LeftStart=0;

	UNUSED_ARG(tid); /* not used in single tasking mode */

    vftype=(int)U(4);  /* convert the flag to integer for switch */	
      
    if (vftype < 4000 || vftype > 5000)
		vftype = 0;
    
    if (cnt>40) { /*DEBUG wait for derivatives to settle ?? */
 
     switch(vftype)
		{

		/*
		 * Normal visual feedback 
		 */
		case 0:

			expTargetSize = target_size;

			tempx = U(0);
			tempy = U(1);

		break;	

    	default:
    	   expTargetSize = target_size;

			tempx = U(0);
			tempy = U(1);
             break;  
	} /* end switch */
   
   if (sqrt(pow(tempx-U(5),2) + pow(tempy-U(6),2)) > expTargetSize)
	   LeftStart=1;

   if (LeftStart)
       TimePerTrial++;
   
   toCenter = sqrt(pow(0-U(2),2) + pow(0.45-U(3),2)); 
   if (TimePerTrial>500 && toCenter>0.06) 
   {
     y[0]=1;
	 TimePerTrial = 0;
	 Twaiting = 0;
	 LeftStart = 0;
     countSamples = 0;
   }

	
	/*
	* To determine if the 'in target' conditions are met, first check position and velocity and then time inside the 
	* the target area 
	*/
	if (Twaiting==1)
	{	
        if (( sqrt(pow(tempx-U(2),2) + pow(tempy-U(3),2)) <= 1*expTargetSize)   && (x[2] <= vel_thresh) ) 
			{
			   countSamples ++;
			   if (countSamples >= samples_in_target)
				{
					 /*
					  * Set intarget flag to 1 
					  */
					 y[0]=1;
					 TimePerTrial=0; 
					 LeftStart=0;
                     
					  /* Do this only once every time the target is reached */					
					  if(update)
					  { 		  
						/*
						* Update the maximun velocity for the trial
						*/
							LastTrialmaxVel=maxVel;
							maxVel=0;
						/*
					
							/* data has been updated */
							update =0;
					  }	
					}
				 else
				 {
					 y[0]=0;
				 }
			}
		 else
		 {
			y[0]=0;
			countSamples =0;
			update=1;
			if ( maxVel < x[2])
			{
				maxVel = x[2];
			}
		 }
	}
 
 Twaiting=1;

 
		
/* QUICK Fix when there is no visual feedback send a value that is outside the "graphic axes limits" */
/* could be handled better on the display end of things ....*/
		
	if(vftype==10000)
	{	
		y[1]=INF_DISPLAY;
		y[2]=INF_DISPLAY;
	}
	else
	{
		y[1]=tempx;
		y[2]=tempy;
	}

   	//y[3]=LastTrialmaxVel;	 /* update maximun velocity */ 
 //    y[3]=x[2];
	 y[3] = LastTrialmaxVel;
    //  y[3] = stime;
}
 
 
 if (cnt<=40)
	{
        y[1]=INF_DISPLAY;
		y[2]=INF_DISPLAY;
  }

 cnt++;
}


#define MDL_UPDATE
/* Function: mdlUpdate ======================================================
 * Abstract:
 *      xdot = 
 */
static void mdlUpdate(SimStruct *S, int_T tid)
{  
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);
	int_T lp;
    UNUSED_ARG(tid); /* not used in single tasking mode */
   
    
/* speeds are updated every other cycle to wait for valid data from the encoders */
/* the flg is use to alternate cycles */
	
	if (cnt==1)
	{
		for (lp=2;lp>=0;lp--) {  
 				x_h[lp]=U(0); /* = X */
				y_h[lp]=U(1); /* = Y */
		}
	}

	x_h[0]=U(0);
	y_h[0]=U(1);

   /* Note on coeficient 0.0005 in following equations:
	* 0.001 to convert from mm to m, the time between positions (two sampres appart is 2*stime [0.0005=0.001/(2*stime)] 
	*/
	x[0] = (x_h[0] - x_h[2])*0.5/stime;  /* m/s */
	x[1] = (y_h[0] - y_h[2])*0.5/stime;

	x[2] = sqrt(x[0]*x[0] + x[1]*x[1]); /* velocity */

	for (lp=2;lp>0;lp--) {  /* start derivative vector */
 		x_h[lp]=x_h[lp-1];
		y_h[lp]=y_h[lp-1];
	}
 
}



/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
    
     /*UNUSED_ARG(S); /* unused input argument */
    
	

}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
