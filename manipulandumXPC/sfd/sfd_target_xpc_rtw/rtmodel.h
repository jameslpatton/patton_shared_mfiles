/*
 *  rtmodel.h:
 *
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */
#ifndef _RTW_HEADER_rtmodel_h_
#define _RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "sfd_target.h"

#endif                                  /* _RTW_HEADER_rtmodel_h_ */
