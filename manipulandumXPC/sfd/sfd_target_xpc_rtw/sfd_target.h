/*
 * sfd_target.h
 *
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */
#ifndef _RTW_HEADER_sfd_target_h_
#define _RTW_HEADER_sfd_target_h_

#include <limits.h>
#include <string.h>
#include <math.h>
#include <float.h>
#include "rtwtypes.h"
#include "simstruc.h"
#include "fixedpoint.h"
#include "rt_logging.h"
#include "rt_nonfinite.h"
#include "rtlibsrc.h"
#include "dt_info.h"
#include "ext_work.h"

#include "sfd_target_types.h"

/* Macros for accessing real-time model data structure  */
#ifndef rtmGetBlkStateChangeFlag
# define rtmGetBlkStateChangeFlag(rtm) ((rtm)->ModelData.blkStateChange)
#endif

#ifndef rtmSetBlkStateChangeFlag
# define rtmSetBlkStateChangeFlag(rtm, val) ((rtm)->ModelData.blkStateChange = (val))
#endif

#ifndef rtmGetBlockIO
# define rtmGetBlockIO(rtm) ((rtm)->ModelData.blockIO)
#endif

#ifndef rtmSetBlockIO
# define rtmSetBlockIO(rtm, val) ((rtm)->ModelData.blockIO = (val))
#endif

#ifndef rtmGetChecksums
# define rtmGetChecksums(rtm) ((rtm)->Sizes.checksums)
#endif

#ifndef rtmSetChecksums
# define rtmSetChecksums(rtm, val) ((rtm)->Sizes.checksums = (val))
#endif

#ifndef rtmGetConstBlockIO
# define rtmGetConstBlockIO(rtm) ((rtm)->ModelData.constBlockIO)
#endif

#ifndef rtmSetConstBlockIO
# define rtmSetConstBlockIO(rtm, val) ((rtm)->ModelData.constBlockIO = (val))
#endif

#ifndef rtmGetContStateDisabled
# define rtmGetContStateDisabled(rtm) ((rtm)->ModelData.contStateDisabled)
#endif

#ifndef rtmSetContStateDisabled
# define rtmSetContStateDisabled(rtm, val) ((rtm)->ModelData.contStateDisabled = (val))
#endif

#ifndef rtmGetContStates
# define rtmGetContStates(rtm) ((rtm)->ModelData.contStates)
#endif

#ifndef rtmSetContStates
# define rtmSetContStates(rtm, val) ((rtm)->ModelData.contStates = (val))
#endif

#ifndef rtmGetDataMapInfo
# define rtmGetDataMapInfo(rtm) ((rtm)->DataMapInfo)
#endif

#ifndef rtmSetDataMapInfo
# define rtmSetDataMapInfo(rtm, val) ((rtm)->DataMapInfo = (val))
#endif

#ifndef rtmGetDefaultParam
# define rtmGetDefaultParam(rtm) ((rtm)->ModelData.defaultParam)
#endif

#ifndef rtmSetDefaultParam
# define rtmSetDefaultParam(rtm, val) ((rtm)->ModelData.defaultParam = (val))
#endif

#ifndef rtmGetDerivCacheNeedsReset
# define rtmGetDerivCacheNeedsReset(rtm) ((rtm)->ModelData.derivCacheNeedsReset)
#endif

#ifndef rtmSetDerivCacheNeedsReset
# define rtmSetDerivCacheNeedsReset(rtm, val) ((rtm)->ModelData.derivCacheNeedsReset = (val))
#endif

#ifndef rtmGetDirectFeedThrough
# define rtmGetDirectFeedThrough(rtm) ((rtm)->Sizes.sysDirFeedThru)
#endif

#ifndef rtmSetDirectFeedThrough
# define rtmSetDirectFeedThrough(rtm, val) ((rtm)->Sizes.sysDirFeedThru = (val))
#endif

#ifndef rtmGetDiscStates
# define rtmGetDiscStates(rtm) ((rtm)->ModelData.discStates)
#endif

#ifndef rtmSetDiscStates
# define rtmSetDiscStates(rtm, val) ((rtm)->ModelData.discStates = (val))
#endif

#ifndef rtmGetErrorStatusFlag
# define rtmGetErrorStatusFlag(rtm) ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatusFlag
# define rtmSetErrorStatusFlag(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm) ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetFinalTime
# define rtmSetFinalTime(rtm, val) ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetFirstInitCondFlag
# define rtmGetFirstInitCondFlag(rtm) ((rtm)->Timing.firstInitCondFlag)
#endif

#ifndef rtmSetFirstInitCondFlag
# define rtmSetFirstInitCondFlag(rtm, val) ((rtm)->Timing.firstInitCondFlag = (val))
#endif

#ifndef rtmGetIntgData
# define rtmGetIntgData(rtm) ((rtm)->ModelData.intgData)
#endif

#ifndef rtmSetIntgData
# define rtmSetIntgData(rtm, val) ((rtm)->ModelData.intgData = (val))
#endif

#ifndef rtmGetMdlRefGlobalTID
# define rtmGetMdlRefGlobalTID(rtm) ((rtm)->Timing.mdlref_GlobalTID)
#endif

#ifndef rtmSetMdlRefGlobalTID
# define rtmSetMdlRefGlobalTID(rtm, val) ((rtm)->Timing.mdlref_GlobalTID = (val))
#endif

#ifndef rtmGetModelMappingInfo
# define rtmGetModelMappingInfo(rtm) ((rtm)->SpecialInfo.mappingInfo)
#endif

#ifndef rtmSetModelMappingInfo
# define rtmSetModelMappingInfo(rtm, val) ((rtm)->SpecialInfo.mappingInfo = (val))
#endif

#ifndef rtmGetModelName
# define rtmGetModelName(rtm) ((rtm)->modelName)
#endif

#ifndef rtmSetModelName
# define rtmSetModelName(rtm, val) ((rtm)->modelName = (val))
#endif

#ifndef rtmGetNonInlinedSFcns
# define rtmGetNonInlinedSFcns(rtm) ((rtm)->NonInlinedSFcns)
#endif

#ifndef rtmSetNonInlinedSFcns
# define rtmSetNonInlinedSFcns(rtm, val) ((rtm)->NonInlinedSFcns = (val))
#endif

#ifndef rtmGetNonsampledZCs
# define rtmGetNonsampledZCs(rtm) ((rtm)->ModelData.nonsampledZCs)
#endif

#ifndef rtmSetNonsampledZCs
# define rtmSetNonsampledZCs(rtm, val) ((rtm)->ModelData.nonsampledZCs = (val))
#endif

#ifndef rtmGetNumBlockIO
# define rtmGetNumBlockIO(rtm) ((rtm)->Sizes.numBlockIO)
#endif

#ifndef rtmSetNumBlockIO
# define rtmSetNumBlockIO(rtm, val) ((rtm)->Sizes.numBlockIO = (val))
#endif

#ifndef rtmGetNumBlockParams
# define rtmGetNumBlockParams(rtm) ((rtm)->Sizes.numBlockPrms)
#endif

#ifndef rtmSetNumBlockParams
# define rtmSetNumBlockParams(rtm, val) ((rtm)->Sizes.numBlockPrms = (val))
#endif

#ifndef rtmGetNumBlocks
# define rtmGetNumBlocks(rtm) ((rtm)->Sizes.numBlocks)
#endif

#ifndef rtmSetNumBlocks
# define rtmSetNumBlocks(rtm, val) ((rtm)->Sizes.numBlocks = (val))
#endif

#ifndef rtmGetNumContStates
# define rtmGetNumContStates(rtm) ((rtm)->Sizes.numContStates)
#endif

#ifndef rtmSetNumContStates
# define rtmSetNumContStates(rtm, val) ((rtm)->Sizes.numContStates = (val))
#endif

#ifndef rtmGetNumDWork
# define rtmGetNumDWork(rtm) ((rtm)->Sizes.numDwork)
#endif

#ifndef rtmSetNumDWork
# define rtmSetNumDWork(rtm, val) ((rtm)->Sizes.numDwork = (val))
#endif

#ifndef rtmGetNumInputPorts
# define rtmGetNumInputPorts(rtm) ((rtm)->Sizes.numIports)
#endif

#ifndef rtmSetNumInputPorts
# define rtmSetNumInputPorts(rtm, val) ((rtm)->Sizes.numIports = (val))
#endif

#ifndef rtmGetNumNonSampledZCs
# define rtmGetNumNonSampledZCs(rtm) ((rtm)->Sizes.numNonSampZCs)
#endif

#ifndef rtmSetNumNonSampledZCs
# define rtmSetNumNonSampledZCs(rtm, val) ((rtm)->Sizes.numNonSampZCs = (val))
#endif

#ifndef rtmGetNumOutputPorts
# define rtmGetNumOutputPorts(rtm) ((rtm)->Sizes.numOports)
#endif

#ifndef rtmSetNumOutputPorts
# define rtmSetNumOutputPorts(rtm, val) ((rtm)->Sizes.numOports = (val))
#endif

#ifndef rtmGetNumSFcnParams
# define rtmGetNumSFcnParams(rtm) ((rtm)->Sizes.numSFcnPrms)
#endif

#ifndef rtmSetNumSFcnParams
# define rtmSetNumSFcnParams(rtm, val) ((rtm)->Sizes.numSFcnPrms = (val))
#endif

#ifndef rtmGetNumSFunctions
# define rtmGetNumSFunctions(rtm) ((rtm)->Sizes.numSFcns)
#endif

#ifndef rtmSetNumSFunctions
# define rtmSetNumSFunctions(rtm, val) ((rtm)->Sizes.numSFcns = (val))
#endif

#ifndef rtmGetNumSampleTimes
# define rtmGetNumSampleTimes(rtm) ((rtm)->Sizes.numSampTimes)
#endif

#ifndef rtmSetNumSampleTimes
# define rtmSetNumSampleTimes(rtm, val) ((rtm)->Sizes.numSampTimes = (val))
#endif

#ifndef rtmGetNumU
# define rtmGetNumU(rtm) ((rtm)->Sizes.numU)
#endif

#ifndef rtmSetNumU
# define rtmSetNumU(rtm, val) ((rtm)->Sizes.numU = (val))
#endif

#ifndef rtmGetNumY
# define rtmGetNumY(rtm) ((rtm)->Sizes.numY)
#endif

#ifndef rtmSetNumY
# define rtmSetNumY(rtm, val) ((rtm)->Sizes.numY = (val))
#endif

#ifndef rtmGetOdeF
# define rtmGetOdeF(rtm) ((rtm)->ModelData.odeF)
#endif

#ifndef rtmSetOdeF
# define rtmSetOdeF(rtm, val) ((rtm)->ModelData.odeF = (val))
#endif

#ifndef rtmGetOdeY
# define rtmGetOdeY(rtm) ((rtm)->ModelData.odeY)
#endif

#ifndef rtmSetOdeY
# define rtmSetOdeY(rtm, val) ((rtm)->ModelData.odeY = (val))
#endif

#ifndef rtmGetOffsetTimeArray
# define rtmGetOffsetTimeArray(rtm) ((rtm)->Timing.offsetTimesArray)
#endif

#ifndef rtmSetOffsetTimeArray
# define rtmSetOffsetTimeArray(rtm, val) ((rtm)->Timing.offsetTimesArray = (val))
#endif

#ifndef rtmGetOffsetTimePtr
# define rtmGetOffsetTimePtr(rtm) ((rtm)->Timing.offsetTimes)
#endif

#ifndef rtmSetOffsetTimePtr
# define rtmSetOffsetTimePtr(rtm, val) ((rtm)->Timing.offsetTimes = (val))
#endif

#ifndef rtmGetOptions
# define rtmGetOptions(rtm) ((rtm)->Sizes.options)
#endif

#ifndef rtmSetOptions
# define rtmSetOptions(rtm, val) ((rtm)->Sizes.options = (val))
#endif

#ifndef rtmGetPath
# define rtmGetPath(rtm) ((rtm)->path)
#endif

#ifndef rtmSetPath
# define rtmSetPath(rtm, val) ((rtm)->path = (val))
#endif

#ifndef rtmGetPerTaskSampleHits
# define rtmGetPerTaskSampleHits(rtm) ((rtm)->Timing.RateInteraction)
#endif

#ifndef rtmSetPerTaskSampleHits
# define rtmSetPerTaskSampleHits(rtm, val) ((rtm)->Timing.RateInteraction = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsArray
# define rtmGetPerTaskSampleHitsArray(rtm) ((rtm)->Timing.perTaskSampleHitsArray)
#endif

#ifndef rtmSetPerTaskSampleHitsArray
# define rtmSetPerTaskSampleHitsArray(rtm, val) ((rtm)->Timing.perTaskSampleHitsArray = (val))
#endif

#ifndef rtmGetPerTaskSampleHitsPtr
# define rtmGetPerTaskSampleHitsPtr(rtm) ((rtm)->Timing.perTaskSampleHits)
#endif

#ifndef rtmSetPerTaskSampleHitsPtr
# define rtmSetPerTaskSampleHitsPtr(rtm, val) ((rtm)->Timing.perTaskSampleHits = (val))
#endif

#ifndef rtmGetPrevZCSigState
# define rtmGetPrevZCSigState(rtm) ((rtm)->ModelData.prevZCSigState)
#endif

#ifndef rtmSetPrevZCSigState
# define rtmSetPrevZCSigState(rtm, val) ((rtm)->ModelData.prevZCSigState = (val))
#endif

#ifndef rtmGetRTWExtModeInfo
# define rtmGetRTWExtModeInfo(rtm) ((rtm)->extModeInfo)
#endif

#ifndef rtmSetRTWExtModeInfo
# define rtmSetRTWExtModeInfo(rtm, val) ((rtm)->extModeInfo = (val))
#endif

#ifndef rtmGetRTWGeneratedSFcn
# define rtmGetRTWGeneratedSFcn(rtm) ((rtm)->Sizes.rtwGenSfcn)
#endif

#ifndef rtmSetRTWGeneratedSFcn
# define rtmSetRTWGeneratedSFcn(rtm, val) ((rtm)->Sizes.rtwGenSfcn = (val))
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm) ((rtm)->rtwLogInfo)
#endif

#ifndef rtmSetRTWLogInfo
# define rtmSetRTWLogInfo(rtm, val) ((rtm)->rtwLogInfo = (val))
#endif

#ifndef rtmGetRTWRTModelMethodsInfo
# define rtmGetRTWRTModelMethodsInfo(rtm) ((rtm)->modelMethodsInfo)
#endif

#ifndef rtmSetRTWRTModelMethodsInfo
# define rtmSetRTWRTModelMethodsInfo(rtm, val) ((rtm)->modelMethodsInfo = (val))
#endif

#ifndef rtmGetRTWSfcnInfo
# define rtmGetRTWSfcnInfo(rtm) ((rtm)->sfcnInfo)
#endif

#ifndef rtmSetRTWSfcnInfo
# define rtmSetRTWSfcnInfo(rtm, val) ((rtm)->sfcnInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfo
# define rtmGetRTWSolverInfo(rtm) ((rtm)->solverInfo)
#endif

#ifndef rtmSetRTWSolverInfo
# define rtmSetRTWSolverInfo(rtm, val) ((rtm)->solverInfo = (val))
#endif

#ifndef rtmGetRTWSolverInfoPtr
# define rtmGetRTWSolverInfoPtr(rtm) ((rtm)->solverInfoPtr)
#endif

#ifndef rtmSetRTWSolverInfoPtr
# define rtmSetRTWSolverInfoPtr(rtm, val) ((rtm)->solverInfoPtr = (val))
#endif

#ifndef rtmGetReservedForXPC
# define rtmGetReservedForXPC(rtm) ((rtm)->SpecialInfo.xpcData)
#endif

#ifndef rtmSetReservedForXPC
# define rtmSetReservedForXPC(rtm, val) ((rtm)->SpecialInfo.xpcData = (val))
#endif

#ifndef rtmGetRootDWork
# define rtmGetRootDWork(rtm) ((rtm)->Work.dwork)
#endif

#ifndef rtmSetRootDWork
# define rtmSetRootDWork(rtm, val) ((rtm)->Work.dwork = (val))
#endif

#ifndef rtmGetSFunctions
# define rtmGetSFunctions(rtm) ((rtm)->childSfunctions)
#endif

#ifndef rtmSetSFunctions
# define rtmSetSFunctions(rtm, val) ((rtm)->childSfunctions = (val))
#endif

#ifndef rtmGetSampleHitArray
# define rtmGetSampleHitArray(rtm) ((rtm)->Timing.sampleHitArray)
#endif

#ifndef rtmSetSampleHitArray
# define rtmSetSampleHitArray(rtm, val) ((rtm)->Timing.sampleHitArray = (val))
#endif

#ifndef rtmGetSampleHitPtr
# define rtmGetSampleHitPtr(rtm) ((rtm)->Timing.sampleHits)
#endif

#ifndef rtmSetSampleHitPtr
# define rtmSetSampleHitPtr(rtm, val) ((rtm)->Timing.sampleHits = (val))
#endif

#ifndef rtmGetSampleTimeArray
# define rtmGetSampleTimeArray(rtm) ((rtm)->Timing.sampleTimesArray)
#endif

#ifndef rtmSetSampleTimeArray
# define rtmSetSampleTimeArray(rtm, val) ((rtm)->Timing.sampleTimesArray = (val))
#endif

#ifndef rtmGetSampleTimePtr
# define rtmGetSampleTimePtr(rtm) ((rtm)->Timing.sampleTimes)
#endif

#ifndef rtmSetSampleTimePtr
# define rtmSetSampleTimePtr(rtm, val) ((rtm)->Timing.sampleTimes = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDArray
# define rtmGetSampleTimeTaskIDArray(rtm) ((rtm)->Timing.sampleTimeTaskIDArray)
#endif

#ifndef rtmSetSampleTimeTaskIDArray
# define rtmSetSampleTimeTaskIDArray(rtm, val) ((rtm)->Timing.sampleTimeTaskIDArray = (val))
#endif

#ifndef rtmGetSampleTimeTaskIDPtr
# define rtmGetSampleTimeTaskIDPtr(rtm) ((rtm)->Timing.sampleTimeTaskIDPtr)
#endif

#ifndef rtmSetSampleTimeTaskIDPtr
# define rtmSetSampleTimeTaskIDPtr(rtm, val) ((rtm)->Timing.sampleTimeTaskIDPtr = (val))
#endif

#ifndef rtmGetSimMode
# define rtmGetSimMode(rtm) ((rtm)->simMode)
#endif

#ifndef rtmSetSimMode
# define rtmSetSimMode(rtm, val) ((rtm)->simMode = (val))
#endif

#ifndef rtmGetSimTimeStep
# define rtmGetSimTimeStep(rtm) ((rtm)->Timing.simTimeStep)
#endif

#ifndef rtmSetSimTimeStep
# define rtmSetSimTimeStep(rtm, val) ((rtm)->Timing.simTimeStep = (val))
#endif

#ifndef rtmGetStartTime
# define rtmGetStartTime(rtm) ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetStartTime
# define rtmSetStartTime(rtm, val) ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetStepSize
# define rtmGetStepSize(rtm) ((rtm)->Timing.stepSize)
#endif

#ifndef rtmSetStepSize
# define rtmSetStepSize(rtm, val) ((rtm)->Timing.stepSize = (val))
#endif

#ifndef rtmGetStopRequestedFlag
# define rtmGetStopRequestedFlag(rtm) ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequestedFlag
# define rtmSetStopRequestedFlag(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetTaskCounters
# define rtmGetTaskCounters(rtm) ((rtm)->Timing.TaskCounters)
#endif

#ifndef rtmSetTaskCounters
# define rtmSetTaskCounters(rtm, val) ((rtm)->Timing.TaskCounters = (val))
#endif

#ifndef rtmGetTaskTimeArray
# define rtmGetTaskTimeArray(rtm) ((rtm)->Timing.tArray)
#endif

#ifndef rtmSetTaskTimeArray
# define rtmSetTaskTimeArray(rtm, val) ((rtm)->Timing.tArray = (val))
#endif

#ifndef rtmGetTimePtr
# define rtmGetTimePtr(rtm) ((rtm)->Timing.t)
#endif

#ifndef rtmSetTimePtr
# define rtmSetTimePtr(rtm, val) ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTimingData
# define rtmGetTimingData(rtm) ((rtm)->Timing.timingData)
#endif

#ifndef rtmSetTimingData
# define rtmSetTimingData(rtm, val) ((rtm)->Timing.timingData = (val))
#endif

#ifndef rtmGetU
# define rtmGetU(rtm) ((rtm)->ModelData.inputs)
#endif

#ifndef rtmSetU
# define rtmSetU(rtm, val) ((rtm)->ModelData.inputs = (val))
#endif

#ifndef rtmGetVarNextHitTimesListPtr
# define rtmGetVarNextHitTimesListPtr(rtm) ((rtm)->Timing.varNextHitTimesList)
#endif

#ifndef rtmSetVarNextHitTimesListPtr
# define rtmSetVarNextHitTimesListPtr(rtm, val) ((rtm)->Timing.varNextHitTimesList = (val))
#endif

#ifndef rtmGetY
# define rtmGetY(rtm) ((rtm)->ModelData.outputs)
#endif

#ifndef rtmSetY
# define rtmSetY(rtm, val) ((rtm)->ModelData.outputs = (val))
#endif

#ifndef rtmGetZCCacheNeedsReset
# define rtmGetZCCacheNeedsReset(rtm) ((rtm)->ModelData.zCCacheNeedsReset)
#endif

#ifndef rtmSetZCCacheNeedsReset
# define rtmSetZCCacheNeedsReset(rtm, val) ((rtm)->ModelData.zCCacheNeedsReset = (val))
#endif

#ifndef rtmGet_TimeOfLastOutput
# define rtmGet_TimeOfLastOutput(rtm) ((rtm)->Timing.timeOfLastOutput)
#endif

#ifndef rtmSet_TimeOfLastOutput
# define rtmSet_TimeOfLastOutput(rtm, val) ((rtm)->Timing.timeOfLastOutput = (val))
#endif

#ifndef rtmGetdX
# define rtmGetdX(rtm) ((rtm)->ModelData.derivs)
#endif

#ifndef rtmSetdX
# define rtmSetdX(rtm, val) ((rtm)->ModelData.derivs = (val))
#endif

#ifndef rtmGetChecksumVal
# define rtmGetChecksumVal(rtm, idx) ((rtm)->Sizes.checksums[idx])
#endif

#ifndef rtmSetChecksumVal
# define rtmSetChecksumVal(rtm, idx, val) ((rtm)->Sizes.checksums[idx] = (val))
#endif

#ifndef rtmGetDWork
# define rtmGetDWork(rtm, idx) ((rtm)->Work.dwork[idx])
#endif

#ifndef rtmSetDWork
# define rtmSetDWork(rtm, idx, val) ((rtm)->Work.dwork[idx] = (val))
#endif

#ifndef rtmGetOffsetTime
# define rtmGetOffsetTime(rtm, idx) ((rtm)->Timing.offsetTimes[idx])
#endif

#ifndef rtmSetOffsetTime
# define rtmSetOffsetTime(rtm, idx, val) ((rtm)->Timing.offsetTimes[idx] = (val))
#endif

#ifndef rtmGetSFunction
# define rtmGetSFunction(rtm, idx) ((rtm)->childSfunctions[idx])
#endif

#ifndef rtmSetSFunction
# define rtmSetSFunction(rtm, idx, val) ((rtm)->childSfunctions[idx] = (val))
#endif

#ifndef rtmGetSampleTime
# define rtmGetSampleTime(rtm, idx) ((rtm)->Timing.sampleTimes[idx])
#endif

#ifndef rtmSetSampleTime
# define rtmSetSampleTime(rtm, idx, val) ((rtm)->Timing.sampleTimes[idx] = (val))
#endif

#ifndef rtmGetSampleTimeTaskID
# define rtmGetSampleTimeTaskID(rtm, idx) ((rtm)->Timing.sampleTimeTaskIDPtr[idx])
#endif

#ifndef rtmSetSampleTimeTaskID
# define rtmSetSampleTimeTaskID(rtm, idx, val) ((rtm)->Timing.sampleTimeTaskIDPtr[idx] = (val))
#endif

#ifndef rtmGetVarNextHitTime
# define rtmGetVarNextHitTime(rtm, idx) ((rtm)->Timing.varNextHitTimesList[idx])
#endif

#ifndef rtmSetVarNextHitTime
# define rtmSetVarNextHitTime(rtm, idx, val) ((rtm)->Timing.varNextHitTimesList[idx] = (val))
#endif

#ifndef rtmIsContinuousTask
# define rtmIsContinuousTask(rtm, tid) (tid) == 0
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm) ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val) ((rtm)->errorStatus = (val))
#endif

#ifndef rtmIsMajorTimeStep
# define rtmIsMajorTimeStep(rtm) ((rtm)->Timing.simTimeStep) == MAJOR_TIME_STEP
#endif

#ifndef rtmIsSampleHit
# define rtmIsSampleHit(rtm, sti, tid) (rtmIsMajorTimeStep((rtm)) && (rtm)->Timing.sampleHits[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm) ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm) &((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmGetT
# define rtmGetT(rtm) (rtmGetTPtr((rtm))[0])
#endif

#ifndef rtmSetT
# define rtmSetT(rtm, val) (rtmSetErrorStatus((rtm), NULL)) /* Not in use*/
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm) ((rtm)->Timing.tFinal)
#endif

#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val) ((rtm)->Timing.tFinal = (val))
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm) ((rtm)->Timing.t)
#endif

#ifndef rtmSetTPtr
# define rtmSetTPtr(rtm, val) ((rtm)->Timing.t = (val))
#endif

#ifndef rtmGetTStart
# define rtmGetTStart(rtm) ((rtm)->Timing.tStart)
#endif

#ifndef rtmSetTStart
# define rtmSetTStart(rtm, val) ((rtm)->Timing.tStart = (val))
#endif

#ifndef rtmGetTaskTime
# define rtmGetTaskTime(rtm, sti) (rtmGetTPtr((rtm))[(rtm)->Timing.sampleTimeTaskIDPtr[sti]])
#endif

#ifndef rtmSetTaskTime
# define rtmSetTaskTime(rtm, sti, val) (rtmGetTPtr((rtm))[sti] = (val))
#endif

#ifndef rtmGetTimeOfLastOutput
# define rtmGetTimeOfLastOutput(rtm) ((rtm)->Timing.timeOfLastOutput)
#endif

#ifdef rtmGetRTWSolverInfo
#undef rtmGetRTWSolverInfo
#endif
#define rtmGetRTWSolverInfo(rtm) &((rtm)->solverInfo)

/* Definition for use in the target main file */
#define sfd_target_rtModel              rtModel_sfd_target

/* Block signals (auto storage) */
typedef struct _BlockIO_sfd_target {
  real_T OffsetinRadians;               /* '<S5>/Offset in Radians' */
  real_T UnitDelay[17];                 /* '<S5>/Unit Delay' */
  real_T DotProduct;                    /* '<S5>/Dot Product' */
  real_T Product1;                      /* '<S5>/Product1' */
  real_T Sum;                           /* '<S5>/Sum' */
  real_T J10;                           /* '<S4>/J10' */
  real_T OffsetinRadians_h;             /* '<S3>/Offset in Radians' */
  real_T UnitDelay_b[17];               /* '<S3>/Unit Delay' */
  real_T EncoderDecimal;                /* '<S3>/Encoder Decimal' */
  real_T EncoderRadians;                /* '<S3>/Encoder Radians' */
  real_T Sum_c;                         /* '<S3>/Sum' */
  real_T J11;                           /* '<S4>/J11' */
  real_T Sum_a;                         /* '<S4>/Sum' */
  real_T J00;                           /* '<S4>/J00' */
  real_T J01;                           /* '<S4>/J01' */
  real_T Sum1;                          /* '<S4>/Sum1' */
  real_T Receive_o2;                    /* '<Root>/Receive' */
  real_T Unpack2_o1;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o2;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o3;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o4;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o5;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o6;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o7;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o8;                    /* '<Root>/Unpack2' */
  real_T Unpack2_o9;                    /* '<Root>/Unpack2' */
  real_T Stargetconditions[5];          /* '<Root>/S-target conditions' */
  real_T SFunction1[4];                 /* '<Root>/S-Function1' */
  real_T EGain;                         /* '<Root>/E Gain' */
  real_T SGain;                         /* '<Root>/S Gain' */
  real_T Bit1_o1;                       /* '<S3>/Bit1' */
  real_T Bit1_o2;                       /* '<S3>/Bit1' */
  real_T Bits181_o1;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o2;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o3;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o4;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o5;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o6;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o7;                    /* '<S3>/Bits1-8 1' */
  real_T Bits181_o8;                    /* '<S3>/Bits1-8 1' */
  real_T Bits91_o1;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o2;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o3;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o4;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o5;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o6;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o7;                     /* '<S3>/Bits9-1' */
  real_T Bits91_o8;                     /* '<S3>/Bits9-1' */
  real_T PulseGenerator1;               /* '<S3>/Pulse Generator1' */
  real_T Sum_f;                         /* '<S6>/Sum' */
  real_T MathFunction;                  /* '<S6>/Math Function' */
  real_T PCI6031E_o1;                   /* '<S2>/PCI-6031E ' */
  real_T PCI6031E_o2;                   /* '<S2>/PCI-6031E ' */
  real_T PCI6031E_o3;                   /* '<S2>/PCI-6031E ' */
  real_T PCI6031E_o4;                   /* '<S2>/PCI-6031E ' */
  real_T PCI6031E_o5;                   /* '<S2>/PCI-6031E ' */
  real_T PCI6031E_o6;                   /* '<S2>/PCI-6031E ' */
  real_T Bit17_o1;                      /* '<S5>/Bit17' */
  real_T Bit17_o2;                      /* '<S5>/Bit17' */
  real_T Bits18_o1;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o2;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o3;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o4;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o5;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o6;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o7;                     /* '<S5>/Bits1-8 ' */
  real_T Bits18_o8;                     /* '<S5>/Bits1-8 ' */
  real_T Bits916_o1;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o2;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o3;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o4;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o5;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o6;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o7;                    /* '<S5>/Bits9-16' */
  real_T Bits916_o8;                    /* '<S5>/Bits9-16' */
  real_T PulseGenerator;                /* '<S5>/Pulse Generator' */
  real_T Sum_k;                         /* '<S7>/Sum' */
  real_T MathFunction_f;                /* '<S7>/Math Function' */
  real_T cos_th2;                       /* '<S2>/cos_th2' */
  real_T f;                             /* '<S2>/f' */
  real_T Fxcosth2;                      /* '<S2>/Fxcos(th2)' */
  real_T sin_th2;                       /* '<S2>/sin_th2' */
  real_T Fxsinth2;                      /* '<S2>/Fxsin(th2)' */
  real_T Xy;                            /* '<S2>/Xy' */
  real_T Fycosth2;                      /* '<S2>/Fycos(th2)' */
  real_T Fysinth2;                      /* '<S2>/Fysin(th2)' */
  real_T Sum_e;                         /* '<S2>/Sum' */
  real_T Sum1_h;                        /* '<S2>/Sum1' */
  real_T Xy2;                           /* '<S2>/Xy2' */
  real_T f1;                            /* '<S2>/f1' */
  real_T Clock;                         /* '<Root>/Clock' */
  real_T ComplexCheck_o1;               /* 'synthesized block' */
  real_T ComplexCheck_o2;               /* 'synthesized block' */
  real_T MatrixConcatenation[2];        /* 'synthesized block' */
  real_T DataTypeConversion[2];         /* 'synthesized block' */
  real_T ComplexCheck_o1_p;             /* 'synthesized block' */
  real_T ComplexCheck_o2_a;             /* 'synthesized block' */
  real_T MatrixConcatenation_b[2];      /* 'synthesized block' */
  real_T DataTypeConversion_e[2];       /* 'synthesized block' */
  real_T ComplexCheck_o1_pu;            /* 'synthesized block' */
  real_T ComplexCheck_o2_d;             /* 'synthesized block' */
  real_T MatrixConcatenation_o[2];      /* 'synthesized block' */
  real_T DataTypeConversion_g[2];       /* 'synthesized block' */
  real_T ComplexCheck_o1_b;             /* 'synthesized block' */
  real_T ComplexCheck_o2_p;             /* 'synthesized block' */
  real_T MatrixConcatenation_l[2];      /* 'synthesized block' */
  real_T DataTypeConversion_l[2];       /* 'synthesized block' */
  real_T ComplexCheck_o1_i;             /* 'synthesized block' */
  real_T ComplexCheck_o2_e;             /* 'synthesized block' */
  real_T MatrixConcatenation_lx[2];     /* 'synthesized block' */
  real_T DataTypeConversion_l2[2];      /* 'synthesized block' */
  real_T ComplexCheck_o1_m;             /* 'synthesized block' */
  real_T ComplexCheck_o2_h;             /* 'synthesized block' */
  real_T MatrixConcatenation_oh[2];     /* 'synthesized block' */
  real_T DataTypeConversion_h[2];       /* 'synthesized block' */
  real_T ComplexCheck_o1_k;             /* 'synthesized block' */
  real_T ComplexCheck_o2_dd;            /* 'synthesized block' */
  real_T MatrixConcatenation_ln[2];     /* 'synthesized block' */
  real_T DataTypeConversion_ev[2];      /* 'synthesized block' */
  real_T ComplexCheck_o1_h;             /* 'synthesized block' */
  real_T ComplexCheck_o2_c;             /* 'synthesized block' */
  real_T MatrixConcatenation_a[2];      /* 'synthesized block' */
  real_T DataTypeConversion_m[2];       /* 'synthesized block' */
  real_T ComplexCheck_o1_e;             /* 'synthesized block' */
  real_T ComplexCheck_o2_g;             /* 'synthesized block' */
  real_T MatrixConcatenation_j[2];      /* 'synthesized block' */
  real_T DataTypeConversion_c[2];       /* 'synthesized block' */
  uint8_T Receive_o1[72];               /* '<Root>/Receive' */
  uint8_T Pack1[40];                    /* '<Root>/Pack1' */
} BlockIO_sfd_target;

/* Block states (auto storage) for system: '<Root>' */
typedef struct D_Work_sfd_target_tag {
  real_T UnitDelay_DSTATE[17];          /* '<S5>/Unit Delay' */
  real_T UnitDelay_DSTATE_c[17];        /* '<S3>/Unit Delay' */
  real_T Stargetconditions_DSTATE[3];   /* '<Root>/S-target conditions' */
  real_T SFunction1_DSTATE[4];          /* '<Root>/S-Function1' */
  real_T SFunction1_RWORK[301];         /* '<Root>/S-Function1' */
  real_T PCI6031E_RWORK[6];             /* '<Root>/PCI-6031E ' */
  real_T PCI6031E_RWORK_d[64];          /* '<S2>/PCI-6031E ' */
  void *Receive_PWORK;                  /* '<Root>/Receive' */
  void *Send_PWORK;                     /* '<Root>/Send' */
  int32_T clockTickCounter;             /* '<S3>/Pulse Generator1' */
  int32_T clockTickCounter_g;           /* '<S5>/Pulse Generator' */
  int_T Receive_IWORK[2];               /* '<Root>/Receive' */
  int_T PCI6031E_IWORK[2];              /* '<Root>/PCI-6031E ' */
  struct {
    int_T AcquireOK;
  } SFunction_IWORK;                    /* '<S1>/S-Function' */
  int_T Send_IWORK[2];                  /* '<Root>/Send' */
  int_T Bit1_IWORK[2];                  /* '<S3>/Bit1' */
  int_T Bits181_IWORK[2];               /* '<S3>/Bits1-8 1' */
  int_T Bits91_IWORK[2];                /* '<S3>/Bits9-1' */
  int_T PCIDIO961_IWORK[2];             /* '<S3>/PCI-DIO-96 1' */
  int_T PCI6031E_IWORK_j[66];           /* '<S2>/PCI-6031E ' */
  int_T Bit17_IWORK[2];                 /* '<S5>/Bit17' */
  int_T Bits18_IWORK[2];                /* '<S5>/Bits1-8 ' */
  int_T Bits916_IWORK[2];               /* '<S5>/Bits9-16' */
  int_T IntOE_IWORK[2];                 /* '<S5>/Int & OE' */
} D_Work_sfd_target;

/* Backward compatible GRT Identifiers */
#define rtB                             sfd_target_B
#define BlockIO                         BlockIO_sfd_target
#define rtXdot                          sfd_target_Xdot
#define StateDerivatives                StateDerivatives_sfd_target
#define tXdis                           sfd_target_Xdis
#define StateDisabled                   StateDisabled_sfd_target
#define rtP                             sfd_target_P
#define Parameters                      Parameters_sfd_target
#define rtDWork                         sfd_target_DWork
#define D_Work                          D_Work_sfd_target

/* Parameters (auto storage) */
struct _Parameters_sfd_target {
  real_T OffsetShoulderOFFSETSH;        /* Expression: 59430
                                         * '<S5>/Offset Shoulder (OFFSETSH) '
                                         */
  real_T BitsRadianTRIG_SCALE_Va;       /* Expression: 0.0000479368996214262884295
                                         * '<S5>/Bits//Radian (TRIG_SCALE)'
                                         */
  real_T Vector217_Value[17];           /* Expression: [1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536]
                                         * '<S5>/Vector 2^17'
                                         */
  real_T UnitDelay_X0;                  /* Expression: 0
                                         * '<S5>/Unit Delay'
                                         */
  real_T OffsetElbowOFFSETEL_Va;        /* Expression: 75060
                                         * '<S3>/Offset Elbow (OFFSETEL) '
                                         */
  real_T BitsRadianTRIG_SCALE__d;       /* Expression: 0.0000479368996214262884295
                                         * '<S3>/Bits//Radian (TRIG_SCALE)'
                                         */
  real_T Const_Value;                   /* Expression: pi/2
                                         * '<S3>/Const'
                                         */
  real_T Constant2_Value[17];           /* Expression: [1 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536]
                                         * '<S3>/Constant2'
                                         */
  real_T UnitDelay_X0_e;                /* Expression: 0
                                         * '<S3>/Unit Delay'
                                         */
  real_T ALPHA_Value;                   /* Expression: 0.11783577990381
                                         * '<S3>/(ALPHA)'
                                         */
  real_T Receive_P1_Size[2];            /* Computed Parameter: P1Size
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P1[11];                /* Expression: ipAddress
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P2_Size[2];            /* Computed Parameter: P2Size
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P2;                    /* Expression: ipPort
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P3_Size[2];            /* Computed Parameter: P3Size
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P3;                    /* Expression: width
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P4_Size[2];            /* Computed Parameter: P4Size
                                         * '<Root>/Receive'
                                         */
  real_T Receive_P4;                    /* Expression: sampletime
                                         * '<Root>/Receive'
                                         */
  real_T Stargetconditions_P1_Size[2]; /* Computed Parameter: P1Size
                                        * '<Root>/S-target conditions'
                                        */
  real_T Stargetconditions_P1;          /* Expression: 0.01
                                         * '<Root>/S-target conditions'
                                         */
  real_T Stargetconditions_P2_Size[2]; /* Computed Parameter: P2Size
                                        * '<Root>/S-target conditions'
                                        */
  real_T Stargetconditions_P2;          /* Expression: 1
                                         * '<Root>/S-target conditions'
                                         */
  real_T Stargetconditions_P3_Size[2]; /* Computed Parameter: P3Size
                                        * '<Root>/S-target conditions'
                                        */
  real_T Stargetconditions_P3;          /* Expression: 0.1
                                         * '<Root>/S-target conditions'
                                         */
  real_T SFunction1_P1_Size[2];         /* Computed Parameter: P1Size
                                         * '<Root>/S-Function1'
                                         */
  real_T SFunction1_P1;                 /* Expression: 1
                                         * '<Root>/S-Function1'
                                         */
  real_T SFunction1_P2_Size[2];         /* Computed Parameter: P2Size
                                         * '<Root>/S-Function1'
                                         */
  real_T SFunction1_P2;                 /* Expression: 50
                                         * '<Root>/S-Function1'
                                         */
  real_T EGain_Gain;                    /* Expression: 0.875569  % from PCIO.H: [Nm] per Volt for the motor
                                         * '<Root>/E Gain'
                                         */
  real_T SGain_Gain;                    /* Expression: -0.88343  % from PCIO.H: [Nm] per Volt for the motor
                                         * '<Root>/S Gain'
                                         */
  real_T PCI6031E_P1_Size[2];           /* Computed Parameter: P1Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P1[2];                /* Expression: channel
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P2_Size[2];           /* Computed Parameter: P2Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P2[2];                /* Expression: range
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P3_Size[2];           /* Computed Parameter: P3Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P3[2];                /* Expression: reset
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P4_Size[2];           /* Computed Parameter: P4Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P4[2];                /* Expression: initValue
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P5_Size[2];           /* Computed Parameter: P5Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P5;                   /* Expression: sampletime
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P6_Size[2];           /* Computed Parameter: P6Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P6;                   /* Expression: slot
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P7_Size[2];           /* Computed Parameter: P7Size
                                         * '<Root>/PCI-6031E '
                                         */
  real_T PCI6031E_P7;                   /* Expression: boardType
                                         * '<Root>/PCI-6031E '
                                         */
  real_T Send_P1_Size[2];               /* Computed Parameter: P1Size
                                         * '<Root>/Send'
                                         */
  real_T Send_P1[11];                   /* Expression: ipAddress
                                         * '<Root>/Send'
                                         */
  real_T Send_P2_Size[2];               /* Computed Parameter: P2Size
                                         * '<Root>/Send'
                                         */
  real_T Send_P2;                       /* Expression: ipPort
                                         * '<Root>/Send'
                                         */
  real_T Send_P3_Size[2];               /* Computed Parameter: P3Size
                                         * '<Root>/Send'
                                         */
  real_T Send_P3;                       /* Expression: localPort
                                         * '<Root>/Send'
                                         */
  real_T Send_P4_Size[2];               /* Computed Parameter: P4Size
                                         * '<Root>/Send'
                                         */
  real_T Send_P4;                       /* Expression: sampletime
                                         * '<Root>/Send'
                                         */
  real_T Bit1_P1_Size[2];               /* Computed Parameter: P1Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P1[2];                    /* Expression: channel
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P2_Size[2];               /* Computed Parameter: P2Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P2;                       /* Expression: port
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P3_Size[2];               /* Computed Parameter: P3Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P3;                       /* Expression: chip
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P4_Size[2];               /* Computed Parameter: P4Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P4;                       /* Expression: sampletime
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P5_Size[2];               /* Computed Parameter: P5Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P5;                       /* Expression: slot
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P6_Size[2];               /* Computed Parameter: P6Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P6;                       /* Expression: control
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P7_Size[2];               /* Computed Parameter: P7Size
                                         * '<S3>/Bit1'
                                         */
  real_T Bit1_P7;                       /* Expression: boardType
                                         * '<S3>/Bit1'
                                         */
  real_T Bits181_P1_Size[2];            /* Computed Parameter: P1Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P1[8];                 /* Expression: channel
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P2_Size[2];            /* Computed Parameter: P2Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P2;                    /* Expression: port
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P3_Size[2];            /* Computed Parameter: P3Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P3;                    /* Expression: chip
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P4_Size[2];            /* Computed Parameter: P4Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P4;                    /* Expression: sampletime
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P5_Size[2];            /* Computed Parameter: P5Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P5;                    /* Expression: slot
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P6_Size[2];            /* Computed Parameter: P6Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P6;                    /* Expression: control
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P7_Size[2];            /* Computed Parameter: P7Size
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits181_P7;                    /* Expression: boardType
                                         * '<S3>/Bits1-8 1'
                                         */
  real_T Bits91_P1_Size[2];             /* Computed Parameter: P1Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P1[8];                  /* Expression: channel
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P2_Size[2];             /* Computed Parameter: P2Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P2;                     /* Expression: port
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P3_Size[2];             /* Computed Parameter: P3Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P3;                     /* Expression: chip
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P4_Size[2];             /* Computed Parameter: P4Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P4;                     /* Expression: sampletime
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P5_Size[2];             /* Computed Parameter: P5Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P5;                     /* Expression: slot
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P6_Size[2];             /* Computed Parameter: P6Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P6;                     /* Expression: control
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P7_Size[2];             /* Computed Parameter: P7Size
                                         * '<S3>/Bits9-1'
                                         */
  real_T Bits91_P7;                     /* Expression: boardType
                                         * '<S3>/Bits9-1'
                                         */
  real_T PulseGenerator1_Amp;           /* Expression: 1
                                         * '<S3>/Pulse Generator1'
                                         */
  real_T PulseGenerator1_Period;        /* Expression: 2
                                         * '<S3>/Pulse Generator1'
                                         */
  real_T PulseGenerator1_Duty;          /* Expression: 1
                                         * '<S3>/Pulse Generator1'
                                         */
  real_T Constant3_Value;               /* Expression: 0
                                         * '<S3>/Constant3'
                                         */
  real_T PCIDIO961_P1_Size[2];          /* Computed Parameter: P1Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P1[2];               /* Expression: channel
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P2_Size[2];          /* Computed Parameter: P2Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P2;                  /* Expression: port
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P3_Size[2];          /* Computed Parameter: P3Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P3[2];               /* Expression: reset
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P4_Size[2];          /* Computed Parameter: P4Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P4[2];               /* Expression: initValue
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P5_Size[2];          /* Computed Parameter: P5Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P5;                  /* Expression: chip
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P6_Size[2];          /* Computed Parameter: P6Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P6;                  /* Expression: sampletime
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P7_Size[2];          /* Computed Parameter: P7Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P7;                  /* Expression: slot
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P8_Size[2];          /* Computed Parameter: P8Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P8;                  /* Expression: control
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P9_Size[2];          /* Computed Parameter: P9Size
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T PCIDIO961_P9;                  /* Expression: boardType
                                         * '<S3>/PCI-DIO-96 1'
                                         */
  real_T Constant_Value;                /* Expression: 2
                                         * '<S6>/Constant'
                                         */
  real_T PCI6031E_P1_Size_g[2];         /* Computed Parameter: P1Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P1_d[6];              /* Expression: channel
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P2_Size_d[2];         /* Computed Parameter: P2Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P2_o[6];              /* Expression: range
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P3_Size_b[2];         /* Computed Parameter: P3Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P3_c[6];              /* Expression: coupling
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P4_Size_h[2];         /* Computed Parameter: P4Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P4_c;                 /* Expression: sampletime
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P5_Size_d[2];         /* Computed Parameter: P5Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P5_e;                 /* Expression: slot
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P6_Size_f[2];         /* Computed Parameter: P6Size
                                         * '<S2>/PCI-6031E '
                                         */
  real_T PCI6031E_P6_d;                 /* Expression: boardType
                                         * '<S2>/PCI-6031E '
                                         */
  real_T Bit17_P1_Size[2];              /* Computed Parameter: P1Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P1[2];                   /* Expression: channel
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P2_Size[2];              /* Computed Parameter: P2Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P2;                      /* Expression: port
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P3_Size[2];              /* Computed Parameter: P3Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P3;                      /* Expression: chip
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P4_Size[2];              /* Computed Parameter: P4Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P4;                      /* Expression: sampletime
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P5_Size[2];              /* Computed Parameter: P5Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P5;                      /* Expression: slot
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P6_Size[2];              /* Computed Parameter: P6Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P6;                      /* Expression: control
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P7_Size[2];              /* Computed Parameter: P7Size
                                         * '<S5>/Bit17'
                                         */
  real_T Bit17_P7;                      /* Expression: boardType
                                         * '<S5>/Bit17'
                                         */
  real_T Bits18_P1_Size[2];             /* Computed Parameter: P1Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P1[8];                  /* Expression: channel
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P2_Size[2];             /* Computed Parameter: P2Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P2;                     /* Expression: port
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P3_Size[2];             /* Computed Parameter: P3Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P3;                     /* Expression: chip
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P4_Size[2];             /* Computed Parameter: P4Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P4;                     /* Expression: sampletime
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P5_Size[2];             /* Computed Parameter: P5Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P5;                     /* Expression: slot
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P6_Size[2];             /* Computed Parameter: P6Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P6;                     /* Expression: control
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P7_Size[2];             /* Computed Parameter: P7Size
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits18_P7;                     /* Expression: boardType
                                         * '<S5>/Bits1-8 '
                                         */
  real_T Bits916_P1_Size[2];            /* Computed Parameter: P1Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P1[8];                 /* Expression: channel
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P2_Size[2];            /* Computed Parameter: P2Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P2;                    /* Expression: port
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P3_Size[2];            /* Computed Parameter: P3Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P3;                    /* Expression: chip
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P4_Size[2];            /* Computed Parameter: P4Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P4;                    /* Expression: sampletime
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P5_Size[2];            /* Computed Parameter: P5Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P5;                    /* Expression: slot
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P6_Size[2];            /* Computed Parameter: P6Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P6;                    /* Expression: control
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P7_Size[2];            /* Computed Parameter: P7Size
                                         * '<S5>/Bits9-16'
                                         */
  real_T Bits916_P7;                    /* Expression: boardType
                                         * '<S5>/Bits9-16'
                                         */
  real_T PulseGenerator_Amp;            /* Expression: 1
                                         * '<S5>/Pulse Generator'
                                         */
  real_T PulseGenerator_Period;         /* Expression: 2
                                         * '<S5>/Pulse Generator'
                                         */
  real_T PulseGenerator_Duty;           /* Expression: 1
                                         * '<S5>/Pulse Generator'
                                         */
  real_T Constant1_Value;               /* Expression: 0
                                         * '<S5>/Constant1'
                                         */
  real_T IntOE_P1_Size[2];              /* Computed Parameter: P1Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P1[2];                   /* Expression: channel
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P2_Size[2];              /* Computed Parameter: P2Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P2;                      /* Expression: port
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P3_Size[2];              /* Computed Parameter: P3Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P3[2];                   /* Expression: reset
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P4_Size[2];              /* Computed Parameter: P4Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P4[2];                   /* Expression: initValue
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P5_Size[2];              /* Computed Parameter: P5Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P5;                      /* Expression: chip
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P6_Size[2];              /* Computed Parameter: P6Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P6;                      /* Expression: sampletime
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P7_Size[2];              /* Computed Parameter: P7Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P7;                      /* Expression: slot
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P8_Size[2];              /* Computed Parameter: P8Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P8;                      /* Expression: control
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P9_Size[2];              /* Computed Parameter: P9Size
                                         * '<S5>/Int & OE'
                                         */
  real_T IntOE_P9;                      /* Expression: boardType
                                         * '<S5>/Int & OE'
                                         */
  real_T Constant_Value_m;              /* Expression: 2
                                         * '<S7>/Constant'
                                         */
  real_T f_Gain;                        /* Expression: 14.5
                                         * '<S2>/f'
                                         */
  real_T Xy_Gain;                       /* Expression: 14.5
                                         * '<S2>/Xy'
                                         */
  real_T Xy2_Gain;                      /* Expression: 2.2597
                                         * '<S2>/Xy2'
                                         */
  real_T f1_Gain;                       /* Expression: 2.2597
                                         * '<S2>/f1'
                                         */
};

/* Real-time Model Data Structure */
struct _rtModel_sfd_target_Tag {
  const char *path;
  const char *modelName;
  struct SimStruct_tag * *childSfunctions;
  const char *errorStatus;
  SS_SimMode simMode;
  RTWLogInfo *rtwLogInfo;
  RTWExtModeInfo *extModeInfo;
  RTWSolverInfo solverInfo;
  RTWSolverInfo *solverInfoPtr;
  void *sfcnInfo;

  /*
   * NonInlinedSFcns:
   * The following substructure contains information regarding
   * non-inlined s-functions used in the model.
   */
  struct {
    RTWSfcnInfo sfcnInfo;
    SimStruct childSFunctions[14];
    SimStruct *childSFunctionPtrs[14];
    struct _ssBlkInfo2 blkInfo2[14];
    struct _ssSFcnModelMethods2 methods2[14];
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[2];
      uint_T attribs[4];
      mxArray *params[4];
      struct _ssDWorkRecord dWork[2];
    } Sfcn0;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[1];
      real_T const *UPtrs0[8];
      struct _ssPortOutputs outputPortInfo[1];
      uint_T attribs[3];
      mxArray *params[3];
      struct _ssDWorkRecord dWork[1];
    } Sfcn1;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[1];
      real_T const *UPtrs0[17];
      struct _ssPortOutputs outputPortInfo[1];
      uint_T attribs[2];
      mxArray *params[2];
      struct _ssDWorkRecord dWork[2];
    } Sfcn2;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[2];
      real_T const *UPtrs0[1];
      real_T const *UPtrs1[1];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[2];
    } Sfcn3;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[1];
      uint_T attribs[4];
      mxArray *params[4];
      struct _ssDWorkRecord dWork[2];
    } Sfcn4;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[2];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn5;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[8];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn6;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[8];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn7;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[2];
      real_T const *UPtrs0[1];
      real_T const *UPtrs1[1];
      uint_T attribs[9];
      mxArray *params[9];
      struct _ssDWorkRecord dWork[1];
    } Sfcn8;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[6];
      uint_T attribs[6];
      mxArray *params[6];
      struct _ssDWorkRecord dWork[2];
    } Sfcn9;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[2];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn10;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[8];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn11;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortOutputs outputPortInfo[8];
      uint_T attribs[7];
      mxArray *params[7];
      struct _ssDWorkRecord dWork[1];
    } Sfcn12;
    struct {
      time_T sfcnPeriod[1];
      time_T sfcnOffset[1];
      int_T sfcnTsMap[1];
      struct _ssPortInputs inputPortInfo[2];
      real_T const *UPtrs0[1];
      real_T const *UPtrs1[1];
      uint_T attribs[9];
      mxArray *params[9];
      struct _ssDWorkRecord dWork[1];
    } Sfcn13;
  } NonInlinedSFcns;

  /*
   * ModelData:
   * The following substructure contains information regarding
   * the data used in the model.
   */
  struct {
    void *blockIO;
    const void *constBlockIO;
    real_T *defaultParam;
    ZCSigState *prevZCSigState;
    real_T *contStates;
    real_T *discStates;
    real_T *derivs;
    real_T *nonsampledZCs;
    void *inputs;
    void *outputs;
    boolean_T *contStateDisabled;
    boolean_T zCCacheNeedsReset;
    boolean_T derivCacheNeedsReset;
    boolean_T blkStateChange;
  } ModelData;

  /*
   * Sizes:
   * The following substructure contains sizes information
   * for many of the model attributes such as inputs, outputs,
   * dwork, sample times, etc.
   */
  struct {
    uint32_T checksums[4];
    uint32_T options;
    int_T numContStates;
    int_T numU;
    int_T numY;
    int_T numSampTimes;
    int_T numBlocks;
    int_T numBlockIO;
    int_T numBlockPrms;
    int_T numDwork;
    int_T numSFcnPrms;
    int_T numSFcns;
    int_T numIports;
    int_T numOports;
    int_T numNonSampZCs;
    int_T sysDirFeedThru;
    int_T rtwGenSfcn;
  } Sizes;

  /*
   * SpecialInfo:
   * The following substructure contains special information
   * related to other components that are dependent on RTW.
   */
  struct {
    const void *mappingInfo;
    void *xpcData;
  } SpecialInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T stepSize;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    uint32_T clockTick1;
    uint32_T clockTickH1;
    time_T stepSize1;
    time_T tStart;
    time_T tFinal;
    time_T timeOfLastOutput;
    void *timingData;
    real_T *varNextHitTimesList;
    SimTimeStep simTimeStep;
    boolean_T stopRequestedFlag;
    time_T *sampleTimes;
    time_T *offsetTimes;
    int_T *sampleTimeTaskIDPtr;
    int_T *sampleHits;
    int_T *perTaskSampleHits;
    time_T *t;
    time_T sampleTimesArray[2];
    time_T offsetTimesArray[2];
    int_T sampleTimeTaskIDArray[2];
    int_T sampleHitArray[2];
    int_T perTaskSampleHitsArray[4];
    time_T tArray[2];
  } Timing;

  /*
   * Work:
   * The following substructure contains information regarding
   * the work vectors in the model.
   */
  struct {
    struct _ssDWorkRecord *dwork;
  } Work;
};

/* Block parameters (auto storage) */
extern Parameters_sfd_target sfd_target_P;

/* Block signals (auto storage) */
extern BlockIO_sfd_target sfd_target_B;

/* Block states (auto storage) */
extern D_Work_sfd_target sfd_target_DWork;

/* Model entry point functions */

extern void sfd_target_initialize(boolean_T firstTime);
extern void sfd_target_output(int_T tid);
extern void sfd_target_update(int_T tid);
extern void sfd_target_terminate(void);

/* Real-time Model object */
extern rtModel_sfd_target *sfd_target_M;

/* 
 * The generated code includes comments that allow you to trace directly 
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : sfd_target
 * '<S1>'   : sfd_target/Scope (xPC) 
 * '<S2>'   : sfd_target/Subsystem
 * '<S3>'   : sfd_target/Subsystem/Elbow Angle
 * '<S4>'   : sfd_target/Subsystem/Jacobian-Cartesian Transformation
 * '<S5>'   : sfd_target/Subsystem/Shoulder Angle
 * '<S6>'   : sfd_target/Subsystem/Elbow Angle/Parity
 * '<S7>'   : sfd_target/Subsystem/Shoulder Angle/Parity
 */

#endif                                  /* _RTW_HEADER_sfd_target_h_ */
