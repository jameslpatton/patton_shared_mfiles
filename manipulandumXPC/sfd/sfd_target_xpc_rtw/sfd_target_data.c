/*
 * sfd_target_data.c
 *
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */

#include "sfd_target.h"
#include "sfd_target_private.h"

/* Block parameters (auto storage) */
Parameters_sfd_target sfd_target_P = {
  59430.0 ,                             /* OffsetShoulderOFFSETSH : '<S5>/Offset Shoulder (OFFSETSH) ' */
  4.7936899621426287E-005 ,             /* BitsRadianTRIG_SCALE_Va : '<S5>/Bits//Radian (TRIG_SCALE)' */
  /*  Vector217_Value : '<S5>/Vector 2^17' */
  { 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0, 512.0, 1024.0, 2048.0,
    4096.0, 8192.0, 16384.0, 32768.0, 65536.0 } ,
  0.0 ,                                 /* UnitDelay_X0 : '<S5>/Unit Delay' */
  75060.0 ,                             /* OffsetElbowOFFSETEL_Va : '<S3>/Offset Elbow (OFFSETEL) ' */
  4.7936899621426287E-005 ,             /* BitsRadianTRIG_SCALE__d : '<S3>/Bits//Radian (TRIG_SCALE)' */
  1.5707963267948966E+000 ,             /* Const_Value : '<S3>/Const' */
  /*  Constant2_Value : '<S3>/Constant2' */
  { 1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0, 512.0, 1024.0, 2048.0,
    4096.0, 8192.0, 16384.0, 32768.0, 65536.0 } ,
  0.0 ,                                 /* UnitDelay_X0_e : '<S3>/Unit Delay' */
  1.1783577990381000E-001 ,             /* ALPHA_Value : '<S3>/(ALPHA)' */
  /*  Receive_P1_Size : '<Root>/Receive' */
  { 1.0, 11.0 } ,
  /*  Receive_P1 : '<Root>/Receive' */
  { 49.0, 57.0, 50.0, 46.0, 49.0, 54.0, 56.0, 46.0, 48.0, 46.0, 49.0 } ,
  /*  Receive_P2_Size : '<Root>/Receive' */
  { 1.0, 1.0 } ,
  25000.0 ,                             /* Receive_P2 : '<Root>/Receive' */
  /*  Receive_P3_Size : '<Root>/Receive' */
  { 1.0, 1.0 } ,
  72.0 ,                                /* Receive_P3 : '<Root>/Receive' */
  /*  Receive_P4_Size : '<Root>/Receive' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Receive_P4 : '<Root>/Receive' */
  /*  Stargetconditions_P1_Size : '<Root>/S-target conditions' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Stargetconditions_P1 : '<Root>/S-target conditions' */
  /*  Stargetconditions_P2_Size : '<Root>/S-target conditions' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Stargetconditions_P2 : '<Root>/S-target conditions' */
  /*  Stargetconditions_P3_Size : '<Root>/S-target conditions' */
  { 1.0, 1.0 } ,
  0.1 ,                                 /* Stargetconditions_P3 : '<Root>/S-target conditions' */
  /*  SFunction1_P1_Size : '<Root>/S-Function1' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* SFunction1_P1 : '<Root>/S-Function1' */
  /*  SFunction1_P2_Size : '<Root>/S-Function1' */
  { 1.0, 1.0 } ,
  50.0 ,                                /* SFunction1_P2 : '<Root>/S-Function1' */
  0.875569 ,                            /* EGain_Gain : '<Root>/E Gain' */
  -0.88343 ,                            /* SGain_Gain : '<Root>/S Gain' */
  /*  PCI6031E_P1_Size : '<Root>/PCI-6031E ' */
  { 1.0, 2.0 } ,
  /*  PCI6031E_P1 : '<Root>/PCI-6031E ' */
  { 1.0, 2.0 } ,
  /*  PCI6031E_P2_Size : '<Root>/PCI-6031E ' */
  { 1.0, 2.0 } ,
  /*  PCI6031E_P2 : '<Root>/PCI-6031E ' */
  { -10.0, -10.0 } ,
  /*  PCI6031E_P3_Size : '<Root>/PCI-6031E ' */
  { 1.0, 2.0 } ,
  /*  PCI6031E_P3 : '<Root>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  /*  PCI6031E_P4_Size : '<Root>/PCI-6031E ' */
  { 1.0, 2.0 } ,
  /*  PCI6031E_P4 : '<Root>/PCI-6031E ' */
  { 0.0, 0.0 } ,
  /*  PCI6031E_P5_Size : '<Root>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* PCI6031E_P5 : '<Root>/PCI-6031E ' */
  /*  PCI6031E_P6_Size : '<Root>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* PCI6031E_P6 : '<Root>/PCI-6031E ' */
  /*  PCI6031E_P7_Size : '<Root>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  11.0 ,                                /* PCI6031E_P7 : '<Root>/PCI-6031E ' */
  /*  Send_P1_Size : '<Root>/Send' */
  { 1.0, 11.0 } ,
  /*  Send_P1 : '<Root>/Send' */
  { 49.0, 57.0, 50.0, 46.0, 49.0, 54.0, 56.0, 46.0, 48.0, 46.0, 49.0 } ,
  /*  Send_P2_Size : '<Root>/Send' */
  { 1.0, 1.0 } ,
  25000.0 ,                             /* Send_P2 : '<Root>/Send' */
  /*  Send_P3_Size : '<Root>/Send' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Send_P3 : '<Root>/Send' */
  /*  Send_P4_Size : '<Root>/Send' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Send_P4 : '<Root>/Send' */
  /*  Bit1_P1_Size : '<S3>/Bit1' */
  { 1.0, 2.0 } ,
  /*  Bit1_P1 : '<S3>/Bit1' */
  { 1.0, 4.0 } ,
  /*  Bit1_P2_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  3.0 ,                                 /* Bit1_P2 : '<S3>/Bit1' */
  /*  Bit1_P3_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  3.0 ,                                 /* Bit1_P3 : '<S3>/Bit1' */
  /*  Bit1_P4_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bit1_P4 : '<S3>/Bit1' */
  /*  Bit1_P5_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bit1_P5 : '<S3>/Bit1' */
  /*  Bit1_P6_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bit1_P6 : '<S3>/Bit1' */
  /*  Bit1_P7_Size : '<S3>/Bit1' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bit1_P7 : '<S3>/Bit1' */
  /*  Bits181_P1_Size : '<S3>/Bits1-8 1' */
  { 1.0, 8.0 } ,
  /*  Bits181_P1 : '<S3>/Bits1-8 1' */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 } ,
  /*  Bits181_P2_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Bits181_P2 : '<S3>/Bits1-8 1' */
  /*  Bits181_P3_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  3.0 ,                                 /* Bits181_P3 : '<S3>/Bits1-8 1' */
  /*  Bits181_P4_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bits181_P4 : '<S3>/Bits1-8 1' */
  /*  Bits181_P5_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bits181_P5 : '<S3>/Bits1-8 1' */
  /*  Bits181_P6_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bits181_P6 : '<S3>/Bits1-8 1' */
  /*  Bits181_P7_Size : '<S3>/Bits1-8 1' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bits181_P7 : '<S3>/Bits1-8 1' */
  /*  Bits91_P1_Size : '<S3>/Bits9-1' */
  { 1.0, 8.0 } ,
  /*  Bits91_P1 : '<S3>/Bits9-1' */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 } ,
  /*  Bits91_P2_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  2.0 ,                                 /* Bits91_P2 : '<S3>/Bits9-1' */
  /*  Bits91_P3_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  3.0 ,                                 /* Bits91_P3 : '<S3>/Bits9-1' */
  /*  Bits91_P4_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bits91_P4 : '<S3>/Bits9-1' */
  /*  Bits91_P5_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bits91_P5 : '<S3>/Bits9-1' */
  /*  Bits91_P6_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bits91_P6 : '<S3>/Bits9-1' */
  /*  Bits91_P7_Size : '<S3>/Bits9-1' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bits91_P7 : '<S3>/Bits9-1' */
  1.0 ,                                 /* PulseGenerator1_Amp : '<S3>/Pulse Generator1' */
  2.0 ,                                 /* PulseGenerator1_Period : '<S3>/Pulse Generator1' */
  1.0 ,                                 /* PulseGenerator1_Duty : '<S3>/Pulse Generator1' */
  0.0 ,                                 /* Constant3_Value : '<S3>/Constant3' */
  /*  PCIDIO961_P1_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 2.0 } ,
  /*  PCIDIO961_P1 : '<S3>/PCI-DIO-96 1' */
  { 1.0, 2.0 } ,
  /*  PCIDIO961_P2_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* PCIDIO961_P2 : '<S3>/PCI-DIO-96 1' */
  /*  PCIDIO961_P3_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 2.0 } ,
  /*  PCIDIO961_P3 : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  /*  PCIDIO961_P4_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 2.0 } ,
  /*  PCIDIO961_P4 : '<S3>/PCI-DIO-96 1' */
  { 0.0, 0.0 } ,
  /*  PCIDIO961_P5_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  4.0 ,                                 /* PCIDIO961_P5 : '<S3>/PCI-DIO-96 1' */
  /*  PCIDIO961_P6_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* PCIDIO961_P6 : '<S3>/PCI-DIO-96 1' */
  /*  PCIDIO961_P7_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* PCIDIO961_P7 : '<S3>/PCI-DIO-96 1' */
  /*  PCIDIO961_P8_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  139.0 ,                               /* PCIDIO961_P8 : '<S3>/PCI-DIO-96 1' */
  /*  PCIDIO961_P9_Size : '<S3>/PCI-DIO-96 1' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* PCIDIO961_P9 : '<S3>/PCI-DIO-96 1' */
  2.0 ,                                 /* Constant_Value : '<S6>/Constant' */
  /*  PCI6031E_P1_Size_g : '<S2>/PCI-6031E ' */
  { 1.0, 6.0 } ,
  /*  PCI6031E_P1_d : '<S2>/PCI-6031E ' */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0 } ,
  /*  PCI6031E_P2_Size_d : '<S2>/PCI-6031E ' */
  { 1.0, 6.0 } ,
  /*  PCI6031E_P2_o : '<S2>/PCI-6031E ' */
  { -5.0, -5.0, -5.0, -5.0, -5.0, -5.0 } ,
  /*  PCI6031E_P3_Size_b : '<S2>/PCI-6031E ' */
  { 1.0, 6.0 } ,
  /*  PCI6031E_P3_c : '<S2>/PCI-6031E ' */
  { 2.0, 2.0, 2.0, 2.0, 2.0, 2.0 } ,
  /*  PCI6031E_P4_Size_h : '<S2>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* PCI6031E_P4_c : '<S2>/PCI-6031E ' */
  /*  PCI6031E_P5_Size_d : '<S2>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* PCI6031E_P5_e : '<S2>/PCI-6031E ' */
  /*  PCI6031E_P6_Size_f : '<S2>/PCI-6031E ' */
  { 1.0, 1.0 } ,
  11.0 ,                                /* PCI6031E_P6_d : '<S2>/PCI-6031E ' */
  /*  Bit17_P1_Size : '<S5>/Bit17' */
  { 1.0, 2.0 } ,
  /*  Bit17_P1 : '<S5>/Bit17' */
  { 1.0, 4.0 } ,
  /*  Bit17_P2_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  3.0 ,                                 /* Bit17_P2 : '<S5>/Bit17' */
  /*  Bit17_P3_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Bit17_P3 : '<S5>/Bit17' */
  /*  Bit17_P4_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bit17_P4 : '<S5>/Bit17' */
  /*  Bit17_P5_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bit17_P5 : '<S5>/Bit17' */
  /*  Bit17_P6_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bit17_P6 : '<S5>/Bit17' */
  /*  Bit17_P7_Size : '<S5>/Bit17' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bit17_P7 : '<S5>/Bit17' */
  /*  Bits18_P1_Size : '<S5>/Bits1-8 ' */
  { 1.0, 8.0 } ,
  /*  Bits18_P1 : '<S5>/Bits1-8 ' */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 } ,
  /*  Bits18_P2_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Bits18_P2 : '<S5>/Bits1-8 ' */
  /*  Bits18_P3_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Bits18_P3 : '<S5>/Bits1-8 ' */
  /*  Bits18_P4_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bits18_P4 : '<S5>/Bits1-8 ' */
  /*  Bits18_P5_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bits18_P5 : '<S5>/Bits1-8 ' */
  /*  Bits18_P6_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bits18_P6 : '<S5>/Bits1-8 ' */
  /*  Bits18_P7_Size : '<S5>/Bits1-8 ' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bits18_P7 : '<S5>/Bits1-8 ' */
  /*  Bits916_P1_Size : '<S5>/Bits9-16' */
  { 1.0, 8.0 } ,
  /*  Bits916_P1 : '<S5>/Bits9-16' */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0 } ,
  /*  Bits916_P2_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  2.0 ,                                 /* Bits916_P2 : '<S5>/Bits9-16' */
  /*  Bits916_P3_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* Bits916_P3 : '<S5>/Bits9-16' */
  /*  Bits916_P4_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  0.01 ,                                /* Bits916_P4 : '<S5>/Bits9-16' */
  /*  Bits916_P5_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* Bits916_P5 : '<S5>/Bits9-16' */
  /*  Bits916_P6_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  155.0 ,                               /* Bits916_P6 : '<S5>/Bits9-16' */
  /*  Bits916_P7_Size : '<S5>/Bits9-16' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* Bits916_P7 : '<S5>/Bits9-16' */
  1.0 ,                                 /* PulseGenerator_Amp : '<S5>/Pulse Generator' */
  2.0 ,                                 /* PulseGenerator_Period : '<S5>/Pulse Generator' */
  1.0 ,                                 /* PulseGenerator_Duty : '<S5>/Pulse Generator' */
  0.0 ,                                 /* Constant1_Value : '<S5>/Constant1' */
  /*  IntOE_P1_Size : '<S5>/Int & OE' */
  { 1.0, 2.0 } ,
  /*  IntOE_P1 : '<S5>/Int & OE' */
  { 1.0, 2.0 } ,
  /*  IntOE_P2_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  1.0 ,                                 /* IntOE_P2 : '<S5>/Int & OE' */
  /*  IntOE_P3_Size : '<S5>/Int & OE' */
  { 1.0, 2.0 } ,
  /*  IntOE_P3 : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  /*  IntOE_P4_Size : '<S5>/Int & OE' */
  { 1.0, 2.0 } ,
  /*  IntOE_P4 : '<S5>/Int & OE' */
  { 0.0, 0.0 } ,
  /*  IntOE_P5_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  2.0 ,                                 /* IntOE_P5 : '<S5>/Int & OE' */
  /*  IntOE_P6_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* IntOE_P6 : '<S5>/Int & OE' */
  /*  IntOE_P7_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  -1.0 ,                                /* IntOE_P7 : '<S5>/Int & OE' */
  /*  IntOE_P8_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  139.0 ,                               /* IntOE_P8 : '<S5>/Int & OE' */
  /*  IntOE_P9_Size : '<S5>/Int & OE' */
  { 1.0, 1.0 } ,
  13.0 ,                                /* IntOE_P9 : '<S5>/Int & OE' */
  2.0 ,                                 /* Constant_Value_m : '<S7>/Constant' */
  14.5 ,                                /* f_Gain : '<S2>/f' */
  14.5 ,                                /* Xy_Gain : '<S2>/Xy' */
  2.2597 ,                              /* Xy2_Gain : '<S2>/Xy2' */
  2.2597                                /* f1_Gain : '<S2>/f1' */
};

