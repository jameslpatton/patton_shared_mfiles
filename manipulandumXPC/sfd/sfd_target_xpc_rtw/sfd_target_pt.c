/*
 * sfd_target_pt.c
 *
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */

#ifndef _PT_INFO_sfd_target_
#define _PT_INFO_sfd_target_

#include "pt_info.h"

/* Tunable block parameters */

static const BlockTuning rtBlockTuning[] = {

  /* blockName, parameterName,
   * class, nRows, nCols, nDims, dimsOffset, source, dataType, numInstances,
   * mapOffset
   */
  /* Gain */
  {"E Gain", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 0}
  },

  /* Gain */
  {"S Gain", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 1}
  },

  /* S-Function */
  {"PCI-6031E", "P1",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 2}
  },

  /* S-Function */
  {"PCI-6031E", "P2",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 3}
  },

  /* S-Function */
  {"PCI-6031E", "P3",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 4}
  },

  /* S-Function */
  {"PCI-6031E", "P4",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 5}
  },

  /* S-Function */
  {"PCI-6031E", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 6}
  },

  /* S-Function */
  {"PCI-6031E", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 7}
  },

  /* S-Function */
  {"PCI-6031E", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 8}
  },

  /* S-Function */
  {"Receive", "P1",
    {rt_VECTOR, 1, 11, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 9}
  },

  /* S-Function */
  {"Receive", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 10}
  },

  /* S-Function */
  {"Receive", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 11}
  },

  /* S-Function */
  {"Receive", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 12}
  },

  /* S-Function */
  {"S-Function1", "P1",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 13}
  },

  /* S-Function */
  {"S-Function1", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 14}
  },

  /* S-Function */
  {"S-target conditions", "P1",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 15}
  },

  /* S-Function */
  {"S-target conditions", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 16}
  },

  /* S-Function */
  {"S-target conditions", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 17}
  },

  /* S-Function */
  {"Send", "P1",
    {rt_VECTOR, 1, 11, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 18}
  },

  /* S-Function */
  {"Send", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 19}
  },

  /* S-Function */
  {"Send", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 20}
  },

  /* S-Function */
  {"Send", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 21}
  },

  /* Gain */
  {"Subsystem/Xy", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 22}
  },

  /* Gain */
  {"Subsystem/Xy2", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 23}
  },

  /* Gain */
  {"Subsystem/f", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 24}
  },

  /* Gain */
  {"Subsystem/f1", "Gain",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 25}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P1",
    {rt_VECTOR, 1, 6, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 26}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P2",
    {rt_VECTOR, 1, 6, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 27}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P3",
    {rt_VECTOR, 1, 6, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 28}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 29}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 30}
  },

  /* S-Function */
  {"Subsystem/PCI-6031E", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 31}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/(ALPHA)", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 32}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Bits//Radian (TRIG_SCALE)", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 33}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Const", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 34}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Constant2", "Value",
    {rt_VECTOR, 1, 17, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 35}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Constant3", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 36}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Offset Elbow (OFFSETEL)", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 37}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Elbow Angle/Pulse Generator1", "Amplitude",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 38}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Elbow Angle/Pulse Generator1", "Period",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 39}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Elbow Angle/Pulse Generator1", "PulseWidth",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 40}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P1",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 41}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 42}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 43}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 44}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 45}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 46}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bit1", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 47}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P1",
    {rt_VECTOR, 1, 8, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 48}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 49}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 50}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 51}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 52}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 53}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits1-8 1", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 54}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P1",
    {rt_VECTOR, 1, 8, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 55}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 56}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 57}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 58}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 59}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 60}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/Bits9-1", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 61}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P1",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 62}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 63}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P3",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 64}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P4",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 65}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 66}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 67}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 68}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P8",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 69}
  },

  /* S-Function */
  {"Subsystem/Elbow Angle/PCI-DIO-96 1", "P9",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 70}
  },

  /* UnitDelay */
  {"Subsystem/Elbow Angle/Unit Delay", "X0",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 71}
  },

  /* Constant */
  {"Subsystem/Shoulder Angle/Bits//Radian (TRIG_SCALE)", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 72}
  },

  /* Constant */
  {"Subsystem/Shoulder Angle/Constant1", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 73}
  },

  /* Constant */
  {"Subsystem/Shoulder Angle/Offset Shoulder (OFFSETSH)", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 74}
  },

  /* Constant */
  {"Subsystem/Shoulder Angle/Vector 2^17", "Value",
    {rt_VECTOR, 1, 17, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 75}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Shoulder Angle/Pulse Generator", "Amplitude",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 76}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Shoulder Angle/Pulse Generator", "Period",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 77}
  },

  /* DiscretePulseGenerator */
  {"Subsystem/Shoulder Angle/Pulse Generator", "PulseWidth",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 78}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P1",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 79}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 80}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 81}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 82}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 83}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 84}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bit17", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 85}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P1",
    {rt_VECTOR, 1, 8, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 86}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 87}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 88}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 89}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 90}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 91}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits1-8", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 92}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P1",
    {rt_VECTOR, 1, 8, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 93}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 94}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P3",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 95}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P4",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 96}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 97}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 98}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Bits9-16", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 99}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P1",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 100}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P2",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 101}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P3",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 102}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P4",
    {rt_VECTOR, 1, 2, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 103}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P5",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 104}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P6",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 105}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P7",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 106}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P8",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 107}
  },

  /* S-Function */
  {"Subsystem/Shoulder Angle/Int & OE", "P9",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 108}
  },

  /* UnitDelay */
  {"Subsystem/Shoulder Angle/Unit Delay", "X0",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 109}
  },

  /* Constant */
  {"Subsystem/Elbow Angle/Parity/Constant", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 110}
  },

  /* Constant */
  {"Subsystem/Shoulder Angle/Parity/Constant", "Value",
    {rt_SCALAR, 1, 1, 2, -1, rt_SL_PARAM, SS_DOUBLE, 1, 111}
  },

  {NULL, NULL,
    {0, 0, 0, 0, 0, 0, 0, 0, 0}
  }
};

/* Tunable variable parameters */

static const VariableTuning rtVariableTuning[] = {

  /* variableName,
   * class, nRows, nCols, nDims, dimsOffset, source, dataType, numInstances,
   * mapOffset
   */

  {NULL,
    {0, 0, 0, 0, 0, 0, 0, 0, 0}
  }
};

static void * const rtParametersMap[] = {
  &sfd_target_P.EGain_Gain,             /* 0 */
  &sfd_target_P.SGain_Gain,             /* 1 */
  &sfd_target_P.PCI6031E_P1[0],         /* 2 */
  &sfd_target_P.PCI6031E_P2[0],         /* 3 */
  &sfd_target_P.PCI6031E_P3[0],         /* 4 */
  &sfd_target_P.PCI6031E_P4[0],         /* 5 */
  &sfd_target_P.PCI6031E_P5,            /* 6 */
  &sfd_target_P.PCI6031E_P6,            /* 7 */
  &sfd_target_P.PCI6031E_P7,            /* 8 */
  &sfd_target_P.Receive_P1[0],          /* 9 */
  &sfd_target_P.Receive_P2,             /* 10 */
  &sfd_target_P.Receive_P3,             /* 11 */
  &sfd_target_P.Receive_P4,             /* 12 */
  &sfd_target_P.SFunction1_P1,          /* 13 */
  &sfd_target_P.SFunction1_P2,          /* 14 */
  &sfd_target_P.Stargetconditions_P1,   /* 15 */
  &sfd_target_P.Stargetconditions_P2,   /* 16 */
  &sfd_target_P.Stargetconditions_P3,   /* 17 */
  &sfd_target_P.Send_P1[0],             /* 18 */
  &sfd_target_P.Send_P2,                /* 19 */
  &sfd_target_P.Send_P3,                /* 20 */
  &sfd_target_P.Send_P4,                /* 21 */
  &sfd_target_P.Xy_Gain,                /* 22 */
  &sfd_target_P.Xy2_Gain,               /* 23 */
  &sfd_target_P.f_Gain,                 /* 24 */
  &sfd_target_P.f1_Gain,                /* 25 */
  &sfd_target_P.PCI6031E_P1_d[0],       /* 26 */
  &sfd_target_P.PCI6031E_P2_o[0],       /* 27 */
  &sfd_target_P.PCI6031E_P3_c[0],       /* 28 */
  &sfd_target_P.PCI6031E_P4_c,          /* 29 */
  &sfd_target_P.PCI6031E_P5_e,          /* 30 */
  &sfd_target_P.PCI6031E_P6_d,          /* 31 */
  &sfd_target_P.ALPHA_Value,            /* 32 */
  &sfd_target_P.BitsRadianTRIG_SCALE__d, /* 33 */
  &sfd_target_P.Const_Value,            /* 34 */
  &sfd_target_P.Constant2_Value[0],     /* 35 */
  &sfd_target_P.Constant3_Value,        /* 36 */
  &sfd_target_P.OffsetElbowOFFSETEL_Va, /* 37 */
  &sfd_target_P.PulseGenerator1_Amp,    /* 38 */
  &sfd_target_P.PulseGenerator1_Period, /* 39 */
  &sfd_target_P.PulseGenerator1_Duty,   /* 40 */
  &sfd_target_P.Bit1_P1[0],             /* 41 */
  &sfd_target_P.Bit1_P2,                /* 42 */
  &sfd_target_P.Bit1_P3,                /* 43 */
  &sfd_target_P.Bit1_P4,                /* 44 */
  &sfd_target_P.Bit1_P5,                /* 45 */
  &sfd_target_P.Bit1_P6,                /* 46 */
  &sfd_target_P.Bit1_P7,                /* 47 */
  &sfd_target_P.Bits181_P1[0],          /* 48 */
  &sfd_target_P.Bits181_P2,             /* 49 */
  &sfd_target_P.Bits181_P3,             /* 50 */
  &sfd_target_P.Bits181_P4,             /* 51 */
  &sfd_target_P.Bits181_P5,             /* 52 */
  &sfd_target_P.Bits181_P6,             /* 53 */
  &sfd_target_P.Bits181_P7,             /* 54 */
  &sfd_target_P.Bits91_P1[0],           /* 55 */
  &sfd_target_P.Bits91_P2,              /* 56 */
  &sfd_target_P.Bits91_P3,              /* 57 */
  &sfd_target_P.Bits91_P4,              /* 58 */
  &sfd_target_P.Bits91_P5,              /* 59 */
  &sfd_target_P.Bits91_P6,              /* 60 */
  &sfd_target_P.Bits91_P7,              /* 61 */
  &sfd_target_P.PCIDIO961_P1[0],        /* 62 */
  &sfd_target_P.PCIDIO961_P2,           /* 63 */
  &sfd_target_P.PCIDIO961_P3[0],        /* 64 */
  &sfd_target_P.PCIDIO961_P4[0],        /* 65 */
  &sfd_target_P.PCIDIO961_P5,           /* 66 */
  &sfd_target_P.PCIDIO961_P6,           /* 67 */
  &sfd_target_P.PCIDIO961_P7,           /* 68 */
  &sfd_target_P.PCIDIO961_P8,           /* 69 */
  &sfd_target_P.PCIDIO961_P9,           /* 70 */
  &sfd_target_P.UnitDelay_X0_e,         /* 71 */
  &sfd_target_P.BitsRadianTRIG_SCALE_Va, /* 72 */
  &sfd_target_P.Constant1_Value,        /* 73 */
  &sfd_target_P.OffsetShoulderOFFSETSH, /* 74 */
  &sfd_target_P.Vector217_Value[0],     /* 75 */
  &sfd_target_P.PulseGenerator_Amp,     /* 76 */
  &sfd_target_P.PulseGenerator_Period, /* 77 */
  &sfd_target_P.PulseGenerator_Duty,    /* 78 */
  &sfd_target_P.Bit17_P1[0],            /* 79 */
  &sfd_target_P.Bit17_P2,               /* 80 */
  &sfd_target_P.Bit17_P3,               /* 81 */
  &sfd_target_P.Bit17_P4,               /* 82 */
  &sfd_target_P.Bit17_P5,               /* 83 */
  &sfd_target_P.Bit17_P6,               /* 84 */
  &sfd_target_P.Bit17_P7,               /* 85 */
  &sfd_target_P.Bits18_P1[0],           /* 86 */
  &sfd_target_P.Bits18_P2,              /* 87 */
  &sfd_target_P.Bits18_P3,              /* 88 */
  &sfd_target_P.Bits18_P4,              /* 89 */
  &sfd_target_P.Bits18_P5,              /* 90 */
  &sfd_target_P.Bits18_P6,              /* 91 */
  &sfd_target_P.Bits18_P7,              /* 92 */
  &sfd_target_P.Bits916_P1[0],          /* 93 */
  &sfd_target_P.Bits916_P2,             /* 94 */
  &sfd_target_P.Bits916_P3,             /* 95 */
  &sfd_target_P.Bits916_P4,             /* 96 */
  &sfd_target_P.Bits916_P5,             /* 97 */
  &sfd_target_P.Bits916_P6,             /* 98 */
  &sfd_target_P.Bits916_P7,             /* 99 */
  &sfd_target_P.IntOE_P1[0],            /* 100 */
  &sfd_target_P.IntOE_P2,               /* 101 */
  &sfd_target_P.IntOE_P3[0],            /* 102 */
  &sfd_target_P.IntOE_P4[0],            /* 103 */
  &sfd_target_P.IntOE_P5,               /* 104 */
  &sfd_target_P.IntOE_P6,               /* 105 */
  &sfd_target_P.IntOE_P7,               /* 106 */
  &sfd_target_P.IntOE_P8,               /* 107 */
  &sfd_target_P.IntOE_P9,               /* 108 */
  &sfd_target_P.UnitDelay_X0,           /* 109 */
  &sfd_target_P.Constant_Value,         /* 110 */
  &sfd_target_P.Constant_Value_m,       /* 111 */
};

static uint_T const rtDimensionsMap[] = {
  0                                     /* Dummy */
};

#endif                                  /* _PT_INFO_sfd_target_ */
