  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 1;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (sfd_target_P)
    ;%
      section.nData     = 198;
      section.data(198)  = dumData; %prealloc
      
	  ;% sfd_target_P.OffsetShoulderOFFSETSH
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_P.BitsRadianTRIG_SCALE_Va
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% sfd_target_P.Vector217_Value
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 2;
	
	  ;% sfd_target_P.UnitDelay_X0
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 19;
	
	  ;% sfd_target_P.OffsetElbowOFFSETEL_Va
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 20;
	
	  ;% sfd_target_P.BitsRadianTRIG_SCALE__d
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 21;
	
	  ;% sfd_target_P.Const_Value
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 22;
	
	  ;% sfd_target_P.Constant2_Value
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 23;
	
	  ;% sfd_target_P.UnitDelay_X0_e
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 40;
	
	  ;% sfd_target_P.ALPHA_Value
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 41;
	
	  ;% sfd_target_P.Receive_P1_Size
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 42;
	
	  ;% sfd_target_P.Receive_P1
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 44;
	
	  ;% sfd_target_P.Receive_P2_Size
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 55;
	
	  ;% sfd_target_P.Receive_P2
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 57;
	
	  ;% sfd_target_P.Receive_P3_Size
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 58;
	
	  ;% sfd_target_P.Receive_P3
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 60;
	
	  ;% sfd_target_P.Receive_P4_Size
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 61;
	
	  ;% sfd_target_P.Receive_P4
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 63;
	
	  ;% sfd_target_P.Stargetconditions_P1_Size
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 64;
	
	  ;% sfd_target_P.Stargetconditions_P1
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 66;
	
	  ;% sfd_target_P.Stargetconditions_P2_Size
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 67;
	
	  ;% sfd_target_P.Stargetconditions_P2
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 69;
	
	  ;% sfd_target_P.Stargetconditions_P3_Size
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 70;
	
	  ;% sfd_target_P.Stargetconditions_P3
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 72;
	
	  ;% sfd_target_P.SFunction1_P1_Size
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 73;
	
	  ;% sfd_target_P.SFunction1_P1
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 75;
	
	  ;% sfd_target_P.SFunction1_P2_Size
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 76;
	
	  ;% sfd_target_P.SFunction1_P2
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 78;
	
	  ;% sfd_target_P.EGain_Gain
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 79;
	
	  ;% sfd_target_P.SGain_Gain
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 80;
	
	  ;% sfd_target_P.PCI6031E_P1_Size
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 81;
	
	  ;% sfd_target_P.PCI6031E_P1
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 83;
	
	  ;% sfd_target_P.PCI6031E_P2_Size
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 85;
	
	  ;% sfd_target_P.PCI6031E_P2
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 87;
	
	  ;% sfd_target_P.PCI6031E_P3_Size
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 89;
	
	  ;% sfd_target_P.PCI6031E_P3
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 91;
	
	  ;% sfd_target_P.PCI6031E_P4_Size
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 93;
	
	  ;% sfd_target_P.PCI6031E_P4
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 95;
	
	  ;% sfd_target_P.PCI6031E_P5_Size
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 97;
	
	  ;% sfd_target_P.PCI6031E_P5
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 99;
	
	  ;% sfd_target_P.PCI6031E_P6_Size
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 100;
	
	  ;% sfd_target_P.PCI6031E_P6
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 102;
	
	  ;% sfd_target_P.PCI6031E_P7_Size
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 103;
	
	  ;% sfd_target_P.PCI6031E_P7
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 105;
	
	  ;% sfd_target_P.Send_P1_Size
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 106;
	
	  ;% sfd_target_P.Send_P1
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 108;
	
	  ;% sfd_target_P.Send_P2_Size
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 119;
	
	  ;% sfd_target_P.Send_P2
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 121;
	
	  ;% sfd_target_P.Send_P3_Size
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 122;
	
	  ;% sfd_target_P.Send_P3
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 124;
	
	  ;% sfd_target_P.Send_P4_Size
	  section.data(51).logicalSrcIdx = 50;
	  section.data(51).dtTransOffset = 125;
	
	  ;% sfd_target_P.Send_P4
	  section.data(52).logicalSrcIdx = 51;
	  section.data(52).dtTransOffset = 127;
	
	  ;% sfd_target_P.Bit1_P1_Size
	  section.data(53).logicalSrcIdx = 52;
	  section.data(53).dtTransOffset = 128;
	
	  ;% sfd_target_P.Bit1_P1
	  section.data(54).logicalSrcIdx = 53;
	  section.data(54).dtTransOffset = 130;
	
	  ;% sfd_target_P.Bit1_P2_Size
	  section.data(55).logicalSrcIdx = 54;
	  section.data(55).dtTransOffset = 132;
	
	  ;% sfd_target_P.Bit1_P2
	  section.data(56).logicalSrcIdx = 55;
	  section.data(56).dtTransOffset = 134;
	
	  ;% sfd_target_P.Bit1_P3_Size
	  section.data(57).logicalSrcIdx = 56;
	  section.data(57).dtTransOffset = 135;
	
	  ;% sfd_target_P.Bit1_P3
	  section.data(58).logicalSrcIdx = 57;
	  section.data(58).dtTransOffset = 137;
	
	  ;% sfd_target_P.Bit1_P4_Size
	  section.data(59).logicalSrcIdx = 58;
	  section.data(59).dtTransOffset = 138;
	
	  ;% sfd_target_P.Bit1_P4
	  section.data(60).logicalSrcIdx = 59;
	  section.data(60).dtTransOffset = 140;
	
	  ;% sfd_target_P.Bit1_P5_Size
	  section.data(61).logicalSrcIdx = 60;
	  section.data(61).dtTransOffset = 141;
	
	  ;% sfd_target_P.Bit1_P5
	  section.data(62).logicalSrcIdx = 61;
	  section.data(62).dtTransOffset = 143;
	
	  ;% sfd_target_P.Bit1_P6_Size
	  section.data(63).logicalSrcIdx = 62;
	  section.data(63).dtTransOffset = 144;
	
	  ;% sfd_target_P.Bit1_P6
	  section.data(64).logicalSrcIdx = 63;
	  section.data(64).dtTransOffset = 146;
	
	  ;% sfd_target_P.Bit1_P7_Size
	  section.data(65).logicalSrcIdx = 64;
	  section.data(65).dtTransOffset = 147;
	
	  ;% sfd_target_P.Bit1_P7
	  section.data(66).logicalSrcIdx = 65;
	  section.data(66).dtTransOffset = 149;
	
	  ;% sfd_target_P.Bits181_P1_Size
	  section.data(67).logicalSrcIdx = 66;
	  section.data(67).dtTransOffset = 150;
	
	  ;% sfd_target_P.Bits181_P1
	  section.data(68).logicalSrcIdx = 67;
	  section.data(68).dtTransOffset = 152;
	
	  ;% sfd_target_P.Bits181_P2_Size
	  section.data(69).logicalSrcIdx = 68;
	  section.data(69).dtTransOffset = 160;
	
	  ;% sfd_target_P.Bits181_P2
	  section.data(70).logicalSrcIdx = 69;
	  section.data(70).dtTransOffset = 162;
	
	  ;% sfd_target_P.Bits181_P3_Size
	  section.data(71).logicalSrcIdx = 70;
	  section.data(71).dtTransOffset = 163;
	
	  ;% sfd_target_P.Bits181_P3
	  section.data(72).logicalSrcIdx = 71;
	  section.data(72).dtTransOffset = 165;
	
	  ;% sfd_target_P.Bits181_P4_Size
	  section.data(73).logicalSrcIdx = 72;
	  section.data(73).dtTransOffset = 166;
	
	  ;% sfd_target_P.Bits181_P4
	  section.data(74).logicalSrcIdx = 73;
	  section.data(74).dtTransOffset = 168;
	
	  ;% sfd_target_P.Bits181_P5_Size
	  section.data(75).logicalSrcIdx = 74;
	  section.data(75).dtTransOffset = 169;
	
	  ;% sfd_target_P.Bits181_P5
	  section.data(76).logicalSrcIdx = 75;
	  section.data(76).dtTransOffset = 171;
	
	  ;% sfd_target_P.Bits181_P6_Size
	  section.data(77).logicalSrcIdx = 76;
	  section.data(77).dtTransOffset = 172;
	
	  ;% sfd_target_P.Bits181_P6
	  section.data(78).logicalSrcIdx = 77;
	  section.data(78).dtTransOffset = 174;
	
	  ;% sfd_target_P.Bits181_P7_Size
	  section.data(79).logicalSrcIdx = 78;
	  section.data(79).dtTransOffset = 175;
	
	  ;% sfd_target_P.Bits181_P7
	  section.data(80).logicalSrcIdx = 79;
	  section.data(80).dtTransOffset = 177;
	
	  ;% sfd_target_P.Bits91_P1_Size
	  section.data(81).logicalSrcIdx = 80;
	  section.data(81).dtTransOffset = 178;
	
	  ;% sfd_target_P.Bits91_P1
	  section.data(82).logicalSrcIdx = 81;
	  section.data(82).dtTransOffset = 180;
	
	  ;% sfd_target_P.Bits91_P2_Size
	  section.data(83).logicalSrcIdx = 82;
	  section.data(83).dtTransOffset = 188;
	
	  ;% sfd_target_P.Bits91_P2
	  section.data(84).logicalSrcIdx = 83;
	  section.data(84).dtTransOffset = 190;
	
	  ;% sfd_target_P.Bits91_P3_Size
	  section.data(85).logicalSrcIdx = 84;
	  section.data(85).dtTransOffset = 191;
	
	  ;% sfd_target_P.Bits91_P3
	  section.data(86).logicalSrcIdx = 85;
	  section.data(86).dtTransOffset = 193;
	
	  ;% sfd_target_P.Bits91_P4_Size
	  section.data(87).logicalSrcIdx = 86;
	  section.data(87).dtTransOffset = 194;
	
	  ;% sfd_target_P.Bits91_P4
	  section.data(88).logicalSrcIdx = 87;
	  section.data(88).dtTransOffset = 196;
	
	  ;% sfd_target_P.Bits91_P5_Size
	  section.data(89).logicalSrcIdx = 88;
	  section.data(89).dtTransOffset = 197;
	
	  ;% sfd_target_P.Bits91_P5
	  section.data(90).logicalSrcIdx = 89;
	  section.data(90).dtTransOffset = 199;
	
	  ;% sfd_target_P.Bits91_P6_Size
	  section.data(91).logicalSrcIdx = 90;
	  section.data(91).dtTransOffset = 200;
	
	  ;% sfd_target_P.Bits91_P6
	  section.data(92).logicalSrcIdx = 91;
	  section.data(92).dtTransOffset = 202;
	
	  ;% sfd_target_P.Bits91_P7_Size
	  section.data(93).logicalSrcIdx = 92;
	  section.data(93).dtTransOffset = 203;
	
	  ;% sfd_target_P.Bits91_P7
	  section.data(94).logicalSrcIdx = 93;
	  section.data(94).dtTransOffset = 205;
	
	  ;% sfd_target_P.PulseGenerator1_Amp
	  section.data(95).logicalSrcIdx = 94;
	  section.data(95).dtTransOffset = 206;
	
	  ;% sfd_target_P.PulseGenerator1_Period
	  section.data(96).logicalSrcIdx = 95;
	  section.data(96).dtTransOffset = 207;
	
	  ;% sfd_target_P.PulseGenerator1_Duty
	  section.data(97).logicalSrcIdx = 96;
	  section.data(97).dtTransOffset = 208;
	
	  ;% sfd_target_P.Constant3_Value
	  section.data(98).logicalSrcIdx = 97;
	  section.data(98).dtTransOffset = 209;
	
	  ;% sfd_target_P.PCIDIO961_P1_Size
	  section.data(99).logicalSrcIdx = 98;
	  section.data(99).dtTransOffset = 210;
	
	  ;% sfd_target_P.PCIDIO961_P1
	  section.data(100).logicalSrcIdx = 99;
	  section.data(100).dtTransOffset = 212;
	
	  ;% sfd_target_P.PCIDIO961_P2_Size
	  section.data(101).logicalSrcIdx = 100;
	  section.data(101).dtTransOffset = 214;
	
	  ;% sfd_target_P.PCIDIO961_P2
	  section.data(102).logicalSrcIdx = 101;
	  section.data(102).dtTransOffset = 216;
	
	  ;% sfd_target_P.PCIDIO961_P3_Size
	  section.data(103).logicalSrcIdx = 102;
	  section.data(103).dtTransOffset = 217;
	
	  ;% sfd_target_P.PCIDIO961_P3
	  section.data(104).logicalSrcIdx = 103;
	  section.data(104).dtTransOffset = 219;
	
	  ;% sfd_target_P.PCIDIO961_P4_Size
	  section.data(105).logicalSrcIdx = 104;
	  section.data(105).dtTransOffset = 221;
	
	  ;% sfd_target_P.PCIDIO961_P4
	  section.data(106).logicalSrcIdx = 105;
	  section.data(106).dtTransOffset = 223;
	
	  ;% sfd_target_P.PCIDIO961_P5_Size
	  section.data(107).logicalSrcIdx = 106;
	  section.data(107).dtTransOffset = 225;
	
	  ;% sfd_target_P.PCIDIO961_P5
	  section.data(108).logicalSrcIdx = 107;
	  section.data(108).dtTransOffset = 227;
	
	  ;% sfd_target_P.PCIDIO961_P6_Size
	  section.data(109).logicalSrcIdx = 108;
	  section.data(109).dtTransOffset = 228;
	
	  ;% sfd_target_P.PCIDIO961_P6
	  section.data(110).logicalSrcIdx = 109;
	  section.data(110).dtTransOffset = 230;
	
	  ;% sfd_target_P.PCIDIO961_P7_Size
	  section.data(111).logicalSrcIdx = 110;
	  section.data(111).dtTransOffset = 231;
	
	  ;% sfd_target_P.PCIDIO961_P7
	  section.data(112).logicalSrcIdx = 111;
	  section.data(112).dtTransOffset = 233;
	
	  ;% sfd_target_P.PCIDIO961_P8_Size
	  section.data(113).logicalSrcIdx = 112;
	  section.data(113).dtTransOffset = 234;
	
	  ;% sfd_target_P.PCIDIO961_P8
	  section.data(114).logicalSrcIdx = 113;
	  section.data(114).dtTransOffset = 236;
	
	  ;% sfd_target_P.PCIDIO961_P9_Size
	  section.data(115).logicalSrcIdx = 114;
	  section.data(115).dtTransOffset = 237;
	
	  ;% sfd_target_P.PCIDIO961_P9
	  section.data(116).logicalSrcIdx = 115;
	  section.data(116).dtTransOffset = 239;
	
	  ;% sfd_target_P.Constant_Value
	  section.data(117).logicalSrcIdx = 116;
	  section.data(117).dtTransOffset = 240;
	
	  ;% sfd_target_P.PCI6031E_P1_Size_g
	  section.data(118).logicalSrcIdx = 117;
	  section.data(118).dtTransOffset = 241;
	
	  ;% sfd_target_P.PCI6031E_P1_d
	  section.data(119).logicalSrcIdx = 118;
	  section.data(119).dtTransOffset = 243;
	
	  ;% sfd_target_P.PCI6031E_P2_Size_d
	  section.data(120).logicalSrcIdx = 119;
	  section.data(120).dtTransOffset = 249;
	
	  ;% sfd_target_P.PCI6031E_P2_o
	  section.data(121).logicalSrcIdx = 120;
	  section.data(121).dtTransOffset = 251;
	
	  ;% sfd_target_P.PCI6031E_P3_Size_b
	  section.data(122).logicalSrcIdx = 121;
	  section.data(122).dtTransOffset = 257;
	
	  ;% sfd_target_P.PCI6031E_P3_c
	  section.data(123).logicalSrcIdx = 122;
	  section.data(123).dtTransOffset = 259;
	
	  ;% sfd_target_P.PCI6031E_P4_Size_h
	  section.data(124).logicalSrcIdx = 123;
	  section.data(124).dtTransOffset = 265;
	
	  ;% sfd_target_P.PCI6031E_P4_c
	  section.data(125).logicalSrcIdx = 124;
	  section.data(125).dtTransOffset = 267;
	
	  ;% sfd_target_P.PCI6031E_P5_Size_d
	  section.data(126).logicalSrcIdx = 125;
	  section.data(126).dtTransOffset = 268;
	
	  ;% sfd_target_P.PCI6031E_P5_e
	  section.data(127).logicalSrcIdx = 126;
	  section.data(127).dtTransOffset = 270;
	
	  ;% sfd_target_P.PCI6031E_P6_Size_f
	  section.data(128).logicalSrcIdx = 127;
	  section.data(128).dtTransOffset = 271;
	
	  ;% sfd_target_P.PCI6031E_P6_d
	  section.data(129).logicalSrcIdx = 128;
	  section.data(129).dtTransOffset = 273;
	
	  ;% sfd_target_P.Bit17_P1_Size
	  section.data(130).logicalSrcIdx = 129;
	  section.data(130).dtTransOffset = 274;
	
	  ;% sfd_target_P.Bit17_P1
	  section.data(131).logicalSrcIdx = 130;
	  section.data(131).dtTransOffset = 276;
	
	  ;% sfd_target_P.Bit17_P2_Size
	  section.data(132).logicalSrcIdx = 131;
	  section.data(132).dtTransOffset = 278;
	
	  ;% sfd_target_P.Bit17_P2
	  section.data(133).logicalSrcIdx = 132;
	  section.data(133).dtTransOffset = 280;
	
	  ;% sfd_target_P.Bit17_P3_Size
	  section.data(134).logicalSrcIdx = 133;
	  section.data(134).dtTransOffset = 281;
	
	  ;% sfd_target_P.Bit17_P3
	  section.data(135).logicalSrcIdx = 134;
	  section.data(135).dtTransOffset = 283;
	
	  ;% sfd_target_P.Bit17_P4_Size
	  section.data(136).logicalSrcIdx = 135;
	  section.data(136).dtTransOffset = 284;
	
	  ;% sfd_target_P.Bit17_P4
	  section.data(137).logicalSrcIdx = 136;
	  section.data(137).dtTransOffset = 286;
	
	  ;% sfd_target_P.Bit17_P5_Size
	  section.data(138).logicalSrcIdx = 137;
	  section.data(138).dtTransOffset = 287;
	
	  ;% sfd_target_P.Bit17_P5
	  section.data(139).logicalSrcIdx = 138;
	  section.data(139).dtTransOffset = 289;
	
	  ;% sfd_target_P.Bit17_P6_Size
	  section.data(140).logicalSrcIdx = 139;
	  section.data(140).dtTransOffset = 290;
	
	  ;% sfd_target_P.Bit17_P6
	  section.data(141).logicalSrcIdx = 140;
	  section.data(141).dtTransOffset = 292;
	
	  ;% sfd_target_P.Bit17_P7_Size
	  section.data(142).logicalSrcIdx = 141;
	  section.data(142).dtTransOffset = 293;
	
	  ;% sfd_target_P.Bit17_P7
	  section.data(143).logicalSrcIdx = 142;
	  section.data(143).dtTransOffset = 295;
	
	  ;% sfd_target_P.Bits18_P1_Size
	  section.data(144).logicalSrcIdx = 143;
	  section.data(144).dtTransOffset = 296;
	
	  ;% sfd_target_P.Bits18_P1
	  section.data(145).logicalSrcIdx = 144;
	  section.data(145).dtTransOffset = 298;
	
	  ;% sfd_target_P.Bits18_P2_Size
	  section.data(146).logicalSrcIdx = 145;
	  section.data(146).dtTransOffset = 306;
	
	  ;% sfd_target_P.Bits18_P2
	  section.data(147).logicalSrcIdx = 146;
	  section.data(147).dtTransOffset = 308;
	
	  ;% sfd_target_P.Bits18_P3_Size
	  section.data(148).logicalSrcIdx = 147;
	  section.data(148).dtTransOffset = 309;
	
	  ;% sfd_target_P.Bits18_P3
	  section.data(149).logicalSrcIdx = 148;
	  section.data(149).dtTransOffset = 311;
	
	  ;% sfd_target_P.Bits18_P4_Size
	  section.data(150).logicalSrcIdx = 149;
	  section.data(150).dtTransOffset = 312;
	
	  ;% sfd_target_P.Bits18_P4
	  section.data(151).logicalSrcIdx = 150;
	  section.data(151).dtTransOffset = 314;
	
	  ;% sfd_target_P.Bits18_P5_Size
	  section.data(152).logicalSrcIdx = 151;
	  section.data(152).dtTransOffset = 315;
	
	  ;% sfd_target_P.Bits18_P5
	  section.data(153).logicalSrcIdx = 152;
	  section.data(153).dtTransOffset = 317;
	
	  ;% sfd_target_P.Bits18_P6_Size
	  section.data(154).logicalSrcIdx = 153;
	  section.data(154).dtTransOffset = 318;
	
	  ;% sfd_target_P.Bits18_P6
	  section.data(155).logicalSrcIdx = 154;
	  section.data(155).dtTransOffset = 320;
	
	  ;% sfd_target_P.Bits18_P7_Size
	  section.data(156).logicalSrcIdx = 155;
	  section.data(156).dtTransOffset = 321;
	
	  ;% sfd_target_P.Bits18_P7
	  section.data(157).logicalSrcIdx = 156;
	  section.data(157).dtTransOffset = 323;
	
	  ;% sfd_target_P.Bits916_P1_Size
	  section.data(158).logicalSrcIdx = 157;
	  section.data(158).dtTransOffset = 324;
	
	  ;% sfd_target_P.Bits916_P1
	  section.data(159).logicalSrcIdx = 158;
	  section.data(159).dtTransOffset = 326;
	
	  ;% sfd_target_P.Bits916_P2_Size
	  section.data(160).logicalSrcIdx = 159;
	  section.data(160).dtTransOffset = 334;
	
	  ;% sfd_target_P.Bits916_P2
	  section.data(161).logicalSrcIdx = 160;
	  section.data(161).dtTransOffset = 336;
	
	  ;% sfd_target_P.Bits916_P3_Size
	  section.data(162).logicalSrcIdx = 161;
	  section.data(162).dtTransOffset = 337;
	
	  ;% sfd_target_P.Bits916_P3
	  section.data(163).logicalSrcIdx = 162;
	  section.data(163).dtTransOffset = 339;
	
	  ;% sfd_target_P.Bits916_P4_Size
	  section.data(164).logicalSrcIdx = 163;
	  section.data(164).dtTransOffset = 340;
	
	  ;% sfd_target_P.Bits916_P4
	  section.data(165).logicalSrcIdx = 164;
	  section.data(165).dtTransOffset = 342;
	
	  ;% sfd_target_P.Bits916_P5_Size
	  section.data(166).logicalSrcIdx = 165;
	  section.data(166).dtTransOffset = 343;
	
	  ;% sfd_target_P.Bits916_P5
	  section.data(167).logicalSrcIdx = 166;
	  section.data(167).dtTransOffset = 345;
	
	  ;% sfd_target_P.Bits916_P6_Size
	  section.data(168).logicalSrcIdx = 167;
	  section.data(168).dtTransOffset = 346;
	
	  ;% sfd_target_P.Bits916_P6
	  section.data(169).logicalSrcIdx = 168;
	  section.data(169).dtTransOffset = 348;
	
	  ;% sfd_target_P.Bits916_P7_Size
	  section.data(170).logicalSrcIdx = 169;
	  section.data(170).dtTransOffset = 349;
	
	  ;% sfd_target_P.Bits916_P7
	  section.data(171).logicalSrcIdx = 170;
	  section.data(171).dtTransOffset = 351;
	
	  ;% sfd_target_P.PulseGenerator_Amp
	  section.data(172).logicalSrcIdx = 171;
	  section.data(172).dtTransOffset = 352;
	
	  ;% sfd_target_P.PulseGenerator_Period
	  section.data(173).logicalSrcIdx = 172;
	  section.data(173).dtTransOffset = 353;
	
	  ;% sfd_target_P.PulseGenerator_Duty
	  section.data(174).logicalSrcIdx = 173;
	  section.data(174).dtTransOffset = 354;
	
	  ;% sfd_target_P.Constant1_Value
	  section.data(175).logicalSrcIdx = 174;
	  section.data(175).dtTransOffset = 355;
	
	  ;% sfd_target_P.IntOE_P1_Size
	  section.data(176).logicalSrcIdx = 175;
	  section.data(176).dtTransOffset = 356;
	
	  ;% sfd_target_P.IntOE_P1
	  section.data(177).logicalSrcIdx = 176;
	  section.data(177).dtTransOffset = 358;
	
	  ;% sfd_target_P.IntOE_P2_Size
	  section.data(178).logicalSrcIdx = 177;
	  section.data(178).dtTransOffset = 360;
	
	  ;% sfd_target_P.IntOE_P2
	  section.data(179).logicalSrcIdx = 178;
	  section.data(179).dtTransOffset = 362;
	
	  ;% sfd_target_P.IntOE_P3_Size
	  section.data(180).logicalSrcIdx = 179;
	  section.data(180).dtTransOffset = 363;
	
	  ;% sfd_target_P.IntOE_P3
	  section.data(181).logicalSrcIdx = 180;
	  section.data(181).dtTransOffset = 365;
	
	  ;% sfd_target_P.IntOE_P4_Size
	  section.data(182).logicalSrcIdx = 181;
	  section.data(182).dtTransOffset = 367;
	
	  ;% sfd_target_P.IntOE_P4
	  section.data(183).logicalSrcIdx = 182;
	  section.data(183).dtTransOffset = 369;
	
	  ;% sfd_target_P.IntOE_P5_Size
	  section.data(184).logicalSrcIdx = 183;
	  section.data(184).dtTransOffset = 371;
	
	  ;% sfd_target_P.IntOE_P5
	  section.data(185).logicalSrcIdx = 184;
	  section.data(185).dtTransOffset = 373;
	
	  ;% sfd_target_P.IntOE_P6_Size
	  section.data(186).logicalSrcIdx = 185;
	  section.data(186).dtTransOffset = 374;
	
	  ;% sfd_target_P.IntOE_P6
	  section.data(187).logicalSrcIdx = 186;
	  section.data(187).dtTransOffset = 376;
	
	  ;% sfd_target_P.IntOE_P7_Size
	  section.data(188).logicalSrcIdx = 187;
	  section.data(188).dtTransOffset = 377;
	
	  ;% sfd_target_P.IntOE_P7
	  section.data(189).logicalSrcIdx = 188;
	  section.data(189).dtTransOffset = 379;
	
	  ;% sfd_target_P.IntOE_P8_Size
	  section.data(190).logicalSrcIdx = 189;
	  section.data(190).dtTransOffset = 380;
	
	  ;% sfd_target_P.IntOE_P8
	  section.data(191).logicalSrcIdx = 190;
	  section.data(191).dtTransOffset = 382;
	
	  ;% sfd_target_P.IntOE_P9_Size
	  section.data(192).logicalSrcIdx = 191;
	  section.data(192).dtTransOffset = 383;
	
	  ;% sfd_target_P.IntOE_P9
	  section.data(193).logicalSrcIdx = 192;
	  section.data(193).dtTransOffset = 385;
	
	  ;% sfd_target_P.Constant_Value_m
	  section.data(194).logicalSrcIdx = 193;
	  section.data(194).dtTransOffset = 386;
	
	  ;% sfd_target_P.f_Gain
	  section.data(195).logicalSrcIdx = 194;
	  section.data(195).dtTransOffset = 387;
	
	  ;% sfd_target_P.Xy_Gain
	  section.data(196).logicalSrcIdx = 195;
	  section.data(196).dtTransOffset = 388;
	
	  ;% sfd_target_P.Xy2_Gain
	  section.data(197).logicalSrcIdx = 196;
	  section.data(197).dtTransOffset = 389;
	
	  ;% sfd_target_P.f1_Gain
	  section.data(198).logicalSrcIdx = 197;
	  section.data(198).dtTransOffset = 390;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 3;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (sfd_target_B)
    ;%
      section.nData     = 91;
      section.data(91)  = dumData; %prealloc
      
	  ;% sfd_target_B.OffsetinRadians
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_B.UnitDelay
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 1;
	
	  ;% sfd_target_B.DotProduct
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 18;
	
	  ;% sfd_target_B.Product1
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 19;
	
	  ;% sfd_target_B.Sum
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 20;
	
	  ;% sfd_target_B.J10
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 21;
	
	  ;% sfd_target_B.OffsetinRadians_h
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 22;
	
	  ;% sfd_target_B.UnitDelay_b
	  section.data(8).logicalSrcIdx = 7;
	  section.data(8).dtTransOffset = 23;
	
	  ;% sfd_target_B.EncoderDecimal
	  section.data(9).logicalSrcIdx = 8;
	  section.data(9).dtTransOffset = 40;
	
	  ;% sfd_target_B.EncoderRadians
	  section.data(10).logicalSrcIdx = 9;
	  section.data(10).dtTransOffset = 41;
	
	  ;% sfd_target_B.Sum_c
	  section.data(11).logicalSrcIdx = 10;
	  section.data(11).dtTransOffset = 42;
	
	  ;% sfd_target_B.J11
	  section.data(12).logicalSrcIdx = 11;
	  section.data(12).dtTransOffset = 43;
	
	  ;% sfd_target_B.Sum_a
	  section.data(13).logicalSrcIdx = 12;
	  section.data(13).dtTransOffset = 44;
	
	  ;% sfd_target_B.J00
	  section.data(14).logicalSrcIdx = 13;
	  section.data(14).dtTransOffset = 45;
	
	  ;% sfd_target_B.J01
	  section.data(15).logicalSrcIdx = 14;
	  section.data(15).dtTransOffset = 46;
	
	  ;% sfd_target_B.Sum1
	  section.data(16).logicalSrcIdx = 15;
	  section.data(16).dtTransOffset = 47;
	
	  ;% sfd_target_B.Receive_o2
	  section.data(17).logicalSrcIdx = 16;
	  section.data(17).dtTransOffset = 48;
	
	  ;% sfd_target_B.Unpack2_o1
	  section.data(18).logicalSrcIdx = 17;
	  section.data(18).dtTransOffset = 49;
	
	  ;% sfd_target_B.Unpack2_o2
	  section.data(19).logicalSrcIdx = 18;
	  section.data(19).dtTransOffset = 50;
	
	  ;% sfd_target_B.Unpack2_o3
	  section.data(20).logicalSrcIdx = 19;
	  section.data(20).dtTransOffset = 51;
	
	  ;% sfd_target_B.Unpack2_o4
	  section.data(21).logicalSrcIdx = 20;
	  section.data(21).dtTransOffset = 52;
	
	  ;% sfd_target_B.Unpack2_o5
	  section.data(22).logicalSrcIdx = 21;
	  section.data(22).dtTransOffset = 53;
	
	  ;% sfd_target_B.Unpack2_o6
	  section.data(23).logicalSrcIdx = 22;
	  section.data(23).dtTransOffset = 54;
	
	  ;% sfd_target_B.Unpack2_o7
	  section.data(24).logicalSrcIdx = 23;
	  section.data(24).dtTransOffset = 55;
	
	  ;% sfd_target_B.Unpack2_o8
	  section.data(25).logicalSrcIdx = 24;
	  section.data(25).dtTransOffset = 56;
	
	  ;% sfd_target_B.Unpack2_o9
	  section.data(26).logicalSrcIdx = 25;
	  section.data(26).dtTransOffset = 57;
	
	  ;% sfd_target_B.Stargetconditions
	  section.data(27).logicalSrcIdx = 26;
	  section.data(27).dtTransOffset = 58;
	
	  ;% sfd_target_B.SFunction1
	  section.data(28).logicalSrcIdx = 27;
	  section.data(28).dtTransOffset = 63;
	
	  ;% sfd_target_B.EGain
	  section.data(29).logicalSrcIdx = 28;
	  section.data(29).dtTransOffset = 67;
	
	  ;% sfd_target_B.SGain
	  section.data(30).logicalSrcIdx = 29;
	  section.data(30).dtTransOffset = 68;
	
	  ;% sfd_target_B.Bit1_o1
	  section.data(31).logicalSrcIdx = 30;
	  section.data(31).dtTransOffset = 69;
	
	  ;% sfd_target_B.Bit1_o2
	  section.data(32).logicalSrcIdx = 31;
	  section.data(32).dtTransOffset = 70;
	
	  ;% sfd_target_B.Bits181_o1
	  section.data(33).logicalSrcIdx = 32;
	  section.data(33).dtTransOffset = 71;
	
	  ;% sfd_target_B.Bits181_o2
	  section.data(34).logicalSrcIdx = 33;
	  section.data(34).dtTransOffset = 72;
	
	  ;% sfd_target_B.Bits181_o3
	  section.data(35).logicalSrcIdx = 34;
	  section.data(35).dtTransOffset = 73;
	
	  ;% sfd_target_B.Bits181_o4
	  section.data(36).logicalSrcIdx = 35;
	  section.data(36).dtTransOffset = 74;
	
	  ;% sfd_target_B.Bits181_o5
	  section.data(37).logicalSrcIdx = 36;
	  section.data(37).dtTransOffset = 75;
	
	  ;% sfd_target_B.Bits181_o6
	  section.data(38).logicalSrcIdx = 37;
	  section.data(38).dtTransOffset = 76;
	
	  ;% sfd_target_B.Bits181_o7
	  section.data(39).logicalSrcIdx = 38;
	  section.data(39).dtTransOffset = 77;
	
	  ;% sfd_target_B.Bits181_o8
	  section.data(40).logicalSrcIdx = 39;
	  section.data(40).dtTransOffset = 78;
	
	  ;% sfd_target_B.Bits91_o1
	  section.data(41).logicalSrcIdx = 40;
	  section.data(41).dtTransOffset = 79;
	
	  ;% sfd_target_B.Bits91_o2
	  section.data(42).logicalSrcIdx = 41;
	  section.data(42).dtTransOffset = 80;
	
	  ;% sfd_target_B.Bits91_o3
	  section.data(43).logicalSrcIdx = 42;
	  section.data(43).dtTransOffset = 81;
	
	  ;% sfd_target_B.Bits91_o4
	  section.data(44).logicalSrcIdx = 43;
	  section.data(44).dtTransOffset = 82;
	
	  ;% sfd_target_B.Bits91_o5
	  section.data(45).logicalSrcIdx = 44;
	  section.data(45).dtTransOffset = 83;
	
	  ;% sfd_target_B.Bits91_o6
	  section.data(46).logicalSrcIdx = 45;
	  section.data(46).dtTransOffset = 84;
	
	  ;% sfd_target_B.Bits91_o7
	  section.data(47).logicalSrcIdx = 46;
	  section.data(47).dtTransOffset = 85;
	
	  ;% sfd_target_B.Bits91_o8
	  section.data(48).logicalSrcIdx = 47;
	  section.data(48).dtTransOffset = 86;
	
	  ;% sfd_target_B.PulseGenerator1
	  section.data(49).logicalSrcIdx = 48;
	  section.data(49).dtTransOffset = 87;
	
	  ;% sfd_target_B.Sum_f
	  section.data(50).logicalSrcIdx = 49;
	  section.data(50).dtTransOffset = 88;
	
	  ;% sfd_target_B.MathFunction
	  section.data(51).logicalSrcIdx = 50;
	  section.data(51).dtTransOffset = 89;
	
	  ;% sfd_target_B.PCI6031E_o1
	  section.data(52).logicalSrcIdx = 51;
	  section.data(52).dtTransOffset = 90;
	
	  ;% sfd_target_B.PCI6031E_o2
	  section.data(53).logicalSrcIdx = 52;
	  section.data(53).dtTransOffset = 91;
	
	  ;% sfd_target_B.PCI6031E_o3
	  section.data(54).logicalSrcIdx = 53;
	  section.data(54).dtTransOffset = 92;
	
	  ;% sfd_target_B.PCI6031E_o4
	  section.data(55).logicalSrcIdx = 54;
	  section.data(55).dtTransOffset = 93;
	
	  ;% sfd_target_B.PCI6031E_o5
	  section.data(56).logicalSrcIdx = 55;
	  section.data(56).dtTransOffset = 94;
	
	  ;% sfd_target_B.PCI6031E_o6
	  section.data(57).logicalSrcIdx = 56;
	  section.data(57).dtTransOffset = 95;
	
	  ;% sfd_target_B.Bit17_o1
	  section.data(58).logicalSrcIdx = 57;
	  section.data(58).dtTransOffset = 96;
	
	  ;% sfd_target_B.Bit17_o2
	  section.data(59).logicalSrcIdx = 58;
	  section.data(59).dtTransOffset = 97;
	
	  ;% sfd_target_B.Bits18_o1
	  section.data(60).logicalSrcIdx = 59;
	  section.data(60).dtTransOffset = 98;
	
	  ;% sfd_target_B.Bits18_o2
	  section.data(61).logicalSrcIdx = 60;
	  section.data(61).dtTransOffset = 99;
	
	  ;% sfd_target_B.Bits18_o3
	  section.data(62).logicalSrcIdx = 61;
	  section.data(62).dtTransOffset = 100;
	
	  ;% sfd_target_B.Bits18_o4
	  section.data(63).logicalSrcIdx = 62;
	  section.data(63).dtTransOffset = 101;
	
	  ;% sfd_target_B.Bits18_o5
	  section.data(64).logicalSrcIdx = 63;
	  section.data(64).dtTransOffset = 102;
	
	  ;% sfd_target_B.Bits18_o6
	  section.data(65).logicalSrcIdx = 64;
	  section.data(65).dtTransOffset = 103;
	
	  ;% sfd_target_B.Bits18_o7
	  section.data(66).logicalSrcIdx = 65;
	  section.data(66).dtTransOffset = 104;
	
	  ;% sfd_target_B.Bits18_o8
	  section.data(67).logicalSrcIdx = 66;
	  section.data(67).dtTransOffset = 105;
	
	  ;% sfd_target_B.Bits916_o1
	  section.data(68).logicalSrcIdx = 67;
	  section.data(68).dtTransOffset = 106;
	
	  ;% sfd_target_B.Bits916_o2
	  section.data(69).logicalSrcIdx = 68;
	  section.data(69).dtTransOffset = 107;
	
	  ;% sfd_target_B.Bits916_o3
	  section.data(70).logicalSrcIdx = 69;
	  section.data(70).dtTransOffset = 108;
	
	  ;% sfd_target_B.Bits916_o4
	  section.data(71).logicalSrcIdx = 70;
	  section.data(71).dtTransOffset = 109;
	
	  ;% sfd_target_B.Bits916_o5
	  section.data(72).logicalSrcIdx = 71;
	  section.data(72).dtTransOffset = 110;
	
	  ;% sfd_target_B.Bits916_o6
	  section.data(73).logicalSrcIdx = 72;
	  section.data(73).dtTransOffset = 111;
	
	  ;% sfd_target_B.Bits916_o7
	  section.data(74).logicalSrcIdx = 73;
	  section.data(74).dtTransOffset = 112;
	
	  ;% sfd_target_B.Bits916_o8
	  section.data(75).logicalSrcIdx = 74;
	  section.data(75).dtTransOffset = 113;
	
	  ;% sfd_target_B.PulseGenerator
	  section.data(76).logicalSrcIdx = 75;
	  section.data(76).dtTransOffset = 114;
	
	  ;% sfd_target_B.Sum_k
	  section.data(77).logicalSrcIdx = 76;
	  section.data(77).dtTransOffset = 115;
	
	  ;% sfd_target_B.MathFunction_f
	  section.data(78).logicalSrcIdx = 77;
	  section.data(78).dtTransOffset = 116;
	
	  ;% sfd_target_B.cos_th2
	  section.data(79).logicalSrcIdx = 78;
	  section.data(79).dtTransOffset = 117;
	
	  ;% sfd_target_B.f
	  section.data(80).logicalSrcIdx = 79;
	  section.data(80).dtTransOffset = 118;
	
	  ;% sfd_target_B.Fxcosth2
	  section.data(81).logicalSrcIdx = 80;
	  section.data(81).dtTransOffset = 119;
	
	  ;% sfd_target_B.sin_th2
	  section.data(82).logicalSrcIdx = 81;
	  section.data(82).dtTransOffset = 120;
	
	  ;% sfd_target_B.Fxsinth2
	  section.data(83).logicalSrcIdx = 82;
	  section.data(83).dtTransOffset = 121;
	
	  ;% sfd_target_B.Xy
	  section.data(84).logicalSrcIdx = 83;
	  section.data(84).dtTransOffset = 122;
	
	  ;% sfd_target_B.Fycosth2
	  section.data(85).logicalSrcIdx = 84;
	  section.data(85).dtTransOffset = 123;
	
	  ;% sfd_target_B.Fysinth2
	  section.data(86).logicalSrcIdx = 85;
	  section.data(86).dtTransOffset = 124;
	
	  ;% sfd_target_B.Sum_e
	  section.data(87).logicalSrcIdx = 86;
	  section.data(87).dtTransOffset = 125;
	
	  ;% sfd_target_B.Sum1_h
	  section.data(88).logicalSrcIdx = 87;
	  section.data(88).dtTransOffset = 126;
	
	  ;% sfd_target_B.Xy2
	  section.data(89).logicalSrcIdx = 88;
	  section.data(89).dtTransOffset = 127;
	
	  ;% sfd_target_B.f1
	  section.data(90).logicalSrcIdx = 89;
	  section.data(90).dtTransOffset = 128;
	
	  ;% sfd_target_B.Clock
	  section.data(91).logicalSrcIdx = 90;
	  section.data(91).dtTransOffset = 129;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 36;
      section.data(36)  = dumData; %prealloc
      
	  ;% sfd_target_B.ComplexCheck_o1
	  section.data(1).logicalSrcIdx = 91;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_B.ComplexCheck_o2
	  section.data(2).logicalSrcIdx = 92;
	  section.data(2).dtTransOffset = 1;
	
	  ;% sfd_target_B.MatrixConcatenation
	  section.data(3).logicalSrcIdx = 93;
	  section.data(3).dtTransOffset = 2;
	
	  ;% sfd_target_B.DataTypeConversion
	  section.data(4).logicalSrcIdx = 94;
	  section.data(4).dtTransOffset = 4;
	
	  ;% sfd_target_B.ComplexCheck_o1_p
	  section.data(5).logicalSrcIdx = 95;
	  section.data(5).dtTransOffset = 6;
	
	  ;% sfd_target_B.ComplexCheck_o2_a
	  section.data(6).logicalSrcIdx = 96;
	  section.data(6).dtTransOffset = 7;
	
	  ;% sfd_target_B.MatrixConcatenation_b
	  section.data(7).logicalSrcIdx = 97;
	  section.data(7).dtTransOffset = 8;
	
	  ;% sfd_target_B.DataTypeConversion_e
	  section.data(8).logicalSrcIdx = 98;
	  section.data(8).dtTransOffset = 10;
	
	  ;% sfd_target_B.ComplexCheck_o1_pu
	  section.data(9).logicalSrcIdx = 99;
	  section.data(9).dtTransOffset = 12;
	
	  ;% sfd_target_B.ComplexCheck_o2_d
	  section.data(10).logicalSrcIdx = 100;
	  section.data(10).dtTransOffset = 13;
	
	  ;% sfd_target_B.MatrixConcatenation_o
	  section.data(11).logicalSrcIdx = 101;
	  section.data(11).dtTransOffset = 14;
	
	  ;% sfd_target_B.DataTypeConversion_g
	  section.data(12).logicalSrcIdx = 102;
	  section.data(12).dtTransOffset = 16;
	
	  ;% sfd_target_B.ComplexCheck_o1_b
	  section.data(13).logicalSrcIdx = 103;
	  section.data(13).dtTransOffset = 18;
	
	  ;% sfd_target_B.ComplexCheck_o2_p
	  section.data(14).logicalSrcIdx = 104;
	  section.data(14).dtTransOffset = 19;
	
	  ;% sfd_target_B.MatrixConcatenation_l
	  section.data(15).logicalSrcIdx = 105;
	  section.data(15).dtTransOffset = 20;
	
	  ;% sfd_target_B.DataTypeConversion_l
	  section.data(16).logicalSrcIdx = 106;
	  section.data(16).dtTransOffset = 22;
	
	  ;% sfd_target_B.ComplexCheck_o1_i
	  section.data(17).logicalSrcIdx = 107;
	  section.data(17).dtTransOffset = 24;
	
	  ;% sfd_target_B.ComplexCheck_o2_e
	  section.data(18).logicalSrcIdx = 108;
	  section.data(18).dtTransOffset = 25;
	
	  ;% sfd_target_B.MatrixConcatenation_lx
	  section.data(19).logicalSrcIdx = 109;
	  section.data(19).dtTransOffset = 26;
	
	  ;% sfd_target_B.DataTypeConversion_l2
	  section.data(20).logicalSrcIdx = 110;
	  section.data(20).dtTransOffset = 28;
	
	  ;% sfd_target_B.ComplexCheck_o1_m
	  section.data(21).logicalSrcIdx = 111;
	  section.data(21).dtTransOffset = 30;
	
	  ;% sfd_target_B.ComplexCheck_o2_h
	  section.data(22).logicalSrcIdx = 112;
	  section.data(22).dtTransOffset = 31;
	
	  ;% sfd_target_B.MatrixConcatenation_oh
	  section.data(23).logicalSrcIdx = 113;
	  section.data(23).dtTransOffset = 32;
	
	  ;% sfd_target_B.DataTypeConversion_h
	  section.data(24).logicalSrcIdx = 114;
	  section.data(24).dtTransOffset = 34;
	
	  ;% sfd_target_B.ComplexCheck_o1_k
	  section.data(25).logicalSrcIdx = 115;
	  section.data(25).dtTransOffset = 36;
	
	  ;% sfd_target_B.ComplexCheck_o2_dd
	  section.data(26).logicalSrcIdx = 116;
	  section.data(26).dtTransOffset = 37;
	
	  ;% sfd_target_B.MatrixConcatenation_ln
	  section.data(27).logicalSrcIdx = 117;
	  section.data(27).dtTransOffset = 38;
	
	  ;% sfd_target_B.DataTypeConversion_ev
	  section.data(28).logicalSrcIdx = 118;
	  section.data(28).dtTransOffset = 40;
	
	  ;% sfd_target_B.ComplexCheck_o1_h
	  section.data(29).logicalSrcIdx = 119;
	  section.data(29).dtTransOffset = 42;
	
	  ;% sfd_target_B.ComplexCheck_o2_c
	  section.data(30).logicalSrcIdx = 120;
	  section.data(30).dtTransOffset = 43;
	
	  ;% sfd_target_B.MatrixConcatenation_a
	  section.data(31).logicalSrcIdx = 121;
	  section.data(31).dtTransOffset = 44;
	
	  ;% sfd_target_B.DataTypeConversion_m
	  section.data(32).logicalSrcIdx = 122;
	  section.data(32).dtTransOffset = 46;
	
	  ;% sfd_target_B.ComplexCheck_o1_e
	  section.data(33).logicalSrcIdx = 123;
	  section.data(33).dtTransOffset = 48;
	
	  ;% sfd_target_B.ComplexCheck_o2_g
	  section.data(34).logicalSrcIdx = 124;
	  section.data(34).dtTransOffset = 49;
	
	  ;% sfd_target_B.MatrixConcatenation_j
	  section.data(35).logicalSrcIdx = 125;
	  section.data(35).dtTransOffset = 50;
	
	  ;% sfd_target_B.DataTypeConversion_c
	  section.data(36).logicalSrcIdx = 126;
	  section.data(36).dtTransOffset = 52;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% sfd_target_B.Receive_o1
	  section.data(1).logicalSrcIdx = 127;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_B.Pack1
	  section.data(2).logicalSrcIdx = 128;
	  section.data(2).dtTransOffset = 72;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 4;
    sectIdxOffset = 3;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (sfd_target_DWork)
    ;%
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% sfd_target_DWork.UnitDelay_DSTATE
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_DWork.UnitDelay_DSTATE_c
	  section.data(2).logicalSrcIdx = 1;
	  section.data(2).dtTransOffset = 17;
	
	  ;% sfd_target_DWork.Stargetconditions_DSTATE
	  section.data(3).logicalSrcIdx = 2;
	  section.data(3).dtTransOffset = 34;
	
	  ;% sfd_target_DWork.SFunction1_DSTATE
	  section.data(4).logicalSrcIdx = 3;
	  section.data(4).dtTransOffset = 37;
	
	  ;% sfd_target_DWork.SFunction1_RWORK
	  section.data(5).logicalSrcIdx = 4;
	  section.data(5).dtTransOffset = 41;
	
	  ;% sfd_target_DWork.PCI6031E_RWORK
	  section.data(6).logicalSrcIdx = 5;
	  section.data(6).dtTransOffset = 342;
	
	  ;% sfd_target_DWork.PCI6031E_RWORK_d
	  section.data(7).logicalSrcIdx = 6;
	  section.data(7).dtTransOffset = 348;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% sfd_target_DWork.Receive_PWORK
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_DWork.Send_PWORK
	  section.data(2).logicalSrcIdx = 8;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% sfd_target_DWork.clockTickCounter
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_DWork.clockTickCounter_g
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% sfd_target_DWork.Receive_IWORK
	  section.data(1).logicalSrcIdx = 11;
	  section.data(1).dtTransOffset = 0;
	
	  ;% sfd_target_DWork.PCI6031E_IWORK
	  section.data(2).logicalSrcIdx = 12;
	  section.data(2).dtTransOffset = 2;
	
	  ;% sfd_target_DWork.SFunction_IWORK.AcquireOK
	  section.data(3).logicalSrcIdx = 13;
	  section.data(3).dtTransOffset = 4;
	
	  ;% sfd_target_DWork.Send_IWORK
	  section.data(4).logicalSrcIdx = 14;
	  section.data(4).dtTransOffset = 5;
	
	  ;% sfd_target_DWork.Bit1_IWORK
	  section.data(5).logicalSrcIdx = 15;
	  section.data(5).dtTransOffset = 7;
	
	  ;% sfd_target_DWork.Bits181_IWORK
	  section.data(6).logicalSrcIdx = 16;
	  section.data(6).dtTransOffset = 9;
	
	  ;% sfd_target_DWork.Bits91_IWORK
	  section.data(7).logicalSrcIdx = 17;
	  section.data(7).dtTransOffset = 11;
	
	  ;% sfd_target_DWork.PCIDIO961_IWORK
	  section.data(8).logicalSrcIdx = 18;
	  section.data(8).dtTransOffset = 13;
	
	  ;% sfd_target_DWork.PCI6031E_IWORK_j
	  section.data(9).logicalSrcIdx = 19;
	  section.data(9).dtTransOffset = 15;
	
	  ;% sfd_target_DWork.Bit17_IWORK
	  section.data(10).logicalSrcIdx = 20;
	  section.data(10).dtTransOffset = 81;
	
	  ;% sfd_target_DWork.Bits18_IWORK
	  section.data(11).logicalSrcIdx = 21;
	  section.data(11).dtTransOffset = 83;
	
	  ;% sfd_target_DWork.Bits916_IWORK
	  section.data(12).logicalSrcIdx = 22;
	  section.data(12).dtTransOffset = 85;
	
	  ;% sfd_target_DWork.IntOE_IWORK
	  section.data(13).logicalSrcIdx = 23;
	  section.data(13).dtTransOffset = 87;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 2755381354;
  targMap.checksum1 = 3778275490;
  targMap.checksum2 = 427466465;
  targMap.checksum3 = 240757941;

