/*
 * sfd_target.c
 * 
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */

#include "sfd_target.h"
#include "sfd_target_private.h"

#include <stdio.h>
#include "sfd_target_dt.h"

#include "mdl_info.h"

#include "sfd_target_bio.c"

#include "sfd_target_pt.c"

/* user code (top of source file) */
static int (XPCCALLCONV * rl32eScopeExists)(int ScopeNo);
static int (XPCCALLCONV * rl32eDefScope)(int ScopeNo, int ScopeType);
static void (XPCCALLCONV * rl32eAddSignal)(int ScopeNo, int SignalNo);
static void (XPCCALLCONV * rl32eSetScope)(int ScopeNo, int action, double value);
static void (XPCCALLCONV *xpceFSScopeSet)(int ScopeNo, const char *filename,
 int mode, unsigned int writeSize);
static void (XPCCALLCONV * rl32eSetTargetScope)(int ScopeNo, int action, double
 value);
static void (XPCCALLCONV * rl32eRestartAcquisition)(int ScopeNo);
static void (XPCCALLCONV * xpceScopeAcqOK)(int ScopeNo, int *scopeAcqOK);

/* Block signals (auto storage) */
BlockIO_sfd_target sfd_target_B;

/* Block states (auto storage) */
D_Work_sfd_target sfd_target_DWork;

/* Real-time model */
rtModel_sfd_target sfd_target_M_;
rtModel_sfd_target *sfd_target_M = &sfd_target_M_;

/* Model output function */
void sfd_target_output(int_T tid)
{

  {
    int32_T i1;

    /* Product: '<S5>/Offset in Radians' incorporates:
     *  Constant: '<S5>/Bits//Radian (TRIG_SCALE)'
     *  Constant: '<S5>/Offset Shoulder (OFFSETSH) '
     */
    sfd_target_B.OffsetinRadians = sfd_target_P.OffsetShoulderOFFSETSH *
      sfd_target_P.BitsRadianTRIG_SCALE_Va;

    /* UnitDelay: '<S5>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sfd_target_B.UnitDelay[i1] = sfd_target_DWork.UnitDelay_DSTATE[i1];
    }
  }

  /* S-Function Block (sfix_dot): <S2>/Shoulder Angle */
  sfd_target_B.DotProduct = sfd_target_P.Vector217_Value[0] *
    sfd_target_B.UnitDelay[0];
  {
    int_T i1;

    const real_T *u0 = &sfd_target_P.Vector217_Value[1];
    const real_T *u1 = &sfd_target_B.UnitDelay[1];

    for (i1=0; i1 < 16; i1++) {
      sfd_target_B.DotProduct += u0[i1] * u1[i1];
    }
  }

  {
    int32_T i1;

    /* Product: '<S5>/Product1' incorporates:
     *  Constant: '<S5>/Bits//Radian (TRIG_SCALE)'
     */
    sfd_target_B.Product1 = sfd_target_P.BitsRadianTRIG_SCALE_Va *
      sfd_target_B.DotProduct;

    /* Sum: '<S5>/Sum' */
    sfd_target_B.Sum = sfd_target_B.Product1 - sfd_target_B.OffsetinRadians;

    /* Fcn: '<S4>/J10' */
    sfd_target_B.J10 = 462.50999999999999 * cos(sfd_target_B.Sum) * 0.001;

    /* Product: '<S3>/Offset in Radians' incorporates:
     *  Constant: '<S3>/Bits//Radian (TRIG_SCALE)'
     *  Constant: '<S3>/Offset Elbow (OFFSETEL) '
     */
    sfd_target_B.OffsetinRadians_h = sfd_target_P.OffsetElbowOFFSETEL_Va *
      sfd_target_P.BitsRadianTRIG_SCALE__d;

    /* UnitDelay: '<S3>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sfd_target_B.UnitDelay_b[i1] = sfd_target_DWork.UnitDelay_DSTATE_c[i1];
    }
  }

  /* S-Function Block (sfix_dot): <S2>/Elbow Angle */
  sfd_target_B.EncoderDecimal = sfd_target_P.Constant2_Value[0] *
    sfd_target_B.UnitDelay_b[0];
  {
    int_T i1;

    const real_T *u0 = &sfd_target_P.Constant2_Value[1];
    const real_T *u1 = &sfd_target_B.UnitDelay_b[1];

    for (i1=0; i1 < 16; i1++) {
      sfd_target_B.EncoderDecimal += u0[i1] * u1[i1];
    }
  }

  /* Product: '<S3>/Encoder Radians' incorporates:
   *  Constant: '<S3>/Bits//Radian (TRIG_SCALE)'
   */
  sfd_target_B.EncoderRadians = sfd_target_P.BitsRadianTRIG_SCALE__d *
    sfd_target_B.EncoderDecimal;

  /* Sum: '<S3>/Sum' incorporates:
   *  Constant: '<S3>/(ALPHA)'
   *  Constant: '<S3>/Const'
   */
  sfd_target_B.Sum_c = ((sfd_target_B.OffsetinRadians_h +
    sfd_target_P.Const_Value) - sfd_target_B.EncoderRadians) -
    sfd_target_P.ALPHA_Value;

  /* Fcn: '<S4>/J11' */
  sfd_target_B.J11 = 335.20999999999998 * cos(sfd_target_B.Sum_c) * 0.001;

  /* Sum: '<S4>/Sum' */
  sfd_target_B.Sum_a = sfd_target_B.J10 + sfd_target_B.J11;

  /* Fcn: '<S4>/J00' */
  sfd_target_B.J00 = -4.6251E+002 * sin(sfd_target_B.Sum) * 0.001;

  /* Fcn: '<S4>/J01' */
  sfd_target_B.J01 = -3.3521E+002 * sin(sfd_target_B.Sum_c) * 0.001;

  /* Sum: '<S4>/Sum1' */
  sfd_target_B.Sum1 = (0.0 - sfd_target_B.J00) - sfd_target_B.J01;

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[0];

    sfcnOutputs(rts, 1);
  }

  /* Unpack: <Root>/Unpack2 */
  memcpy(&sfd_target_B.Unpack2_o1, sfd_target_B.Receive_o1, 8);
  memcpy(&sfd_target_B.Unpack2_o2, &sfd_target_B.Receive_o1[8], 8);
  memcpy(&sfd_target_B.Unpack2_o3, &sfd_target_B.Receive_o1[16], 8);
  memcpy(&sfd_target_B.Unpack2_o4, &sfd_target_B.Receive_o1[24], 8);
  memcpy(&sfd_target_B.Unpack2_o5, &sfd_target_B.Receive_o1[32], 8);
  memcpy(&sfd_target_B.Unpack2_o6, &sfd_target_B.Receive_o1[40], 8);
  memcpy(&sfd_target_B.Unpack2_o7, &sfd_target_B.Receive_o1[48], 8);
  memcpy(&sfd_target_B.Unpack2_o8, &sfd_target_B.Receive_o1[56], 8);
  memcpy(&sfd_target_B.Unpack2_o9, &sfd_target_B.Receive_o1[64], 8);

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[1];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[2];

    sfcnOutputs(rts, 1);
  }

  /* Gain: '<Root>/E Gain' */
  sfd_target_B.EGain = sfd_target_B.SFunction1[0] * sfd_target_P.EGain_Gain;

  /* Gain: '<Root>/S Gain' */
  sfd_target_B.SGain = sfd_target_B.SFunction1[1] * sfd_target_P.SGain_Gain;

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[3];

    sfcnOutputs(rts, 1);
  }

  /* ok to acquire for <S1>/S-Function */
  sfd_target_DWork.SFunction_IWORK.AcquireOK = 1;

  /* Pack: <Root>/Pack1 */
  memcpy(sfd_target_B.Pack1, &sfd_target_B.Stargetconditions[0], 8);
  memcpy(&sfd_target_B.Pack1[8], &sfd_target_B.Stargetconditions[1], 16);
  memcpy(&sfd_target_B.Pack1[24], &sfd_target_B.Stargetconditions[3], 8);
  memcpy(&sfd_target_B.Pack1[32], &sfd_target_B.Stargetconditions[4], 8);

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[4];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[5];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[6];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[7];

    sfcnOutputs(rts, 1);
  }

  /* DiscretePulseGenerator: '<S3>/Pulse Generator1' */
  sfd_target_B.PulseGenerator1 = (sfd_target_DWork.clockTickCounter <
    (int32_T)sfd_target_P.PulseGenerator1_Duty) &&
    (sfd_target_DWork.clockTickCounter >= 0) ? sfd_target_P.PulseGenerator1_Amp
    : 0;
  if(sfd_target_DWork.clockTickCounter >=
   (int32_T)sfd_target_P.PulseGenerator1_Period - 1) {
    sfd_target_DWork.clockTickCounter = 0;
  } else {
    sfd_target_DWork.clockTickCounter++;
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[8];

    sfcnOutputs(rts, 1);
  }

  /* Sum: '<S6>/Sum' */
  sfd_target_B.Sum_f = ((((((((((((((((sfd_target_B.Bits181_o1 +
    sfd_target_B.Bits181_o2) + sfd_target_B.Bits181_o3) +
    sfd_target_B.Bits181_o4) + sfd_target_B.Bits181_o5) +
    sfd_target_B.Bits181_o6) + sfd_target_B.Bits181_o7) +
    sfd_target_B.Bits181_o8) + sfd_target_B.Bits91_o1) + sfd_target_B.Bits91_o2)
    + sfd_target_B.Bits91_o3) + sfd_target_B.Bits91_o4) +
    sfd_target_B.Bits91_o5) + sfd_target_B.Bits91_o6) + sfd_target_B.Bits91_o7)
    + sfd_target_B.Bits91_o8) + sfd_target_B.Bit1_o1) + sfd_target_B.Bit1_o2;

  /* Math Block: '<S6>/Math Function' */

  /* Operator : mod */
  if (sfd_target_P.Constant_Value == 0.0) {
    sfd_target_B.MathFunction = sfd_target_B.Sum_f;
  } else if (sfd_target_P.Constant_Value == floor(sfd_target_P.Constant_Value)) {
    /* Integer denominator.  Use conventional formula.*/
    sfd_target_B.MathFunction = (sfd_target_B.Sum_f -
      sfd_target_P.Constant_Value * floor((sfd_target_B.Sum_f) /
      (sfd_target_P.Constant_Value)));
  } else {
    /* Noninteger denominator. Check for nearly integer quotient.*/
    real_T uDivRound;
    real_T uDiv = sfd_target_B.Sum_f / sfd_target_P.Constant_Value;
    {
      real_T t;
      t = floor((fabs(uDiv) + 0.5));
      uDivRound = ((uDiv < 0.0) ? -t : t);
    }

    if (fabs((uDiv - uDivRound)) <= DBL_EPSILON * fabs(uDiv)) {
      sfd_target_B.MathFunction = 0.0;
    } else {
      sfd_target_B.MathFunction = (uDiv - floor(uDiv)) *
        sfd_target_P.Constant_Value;
    }
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[9];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[10];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[11];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[12];

    sfcnOutputs(rts, 1);
  }

  /* DiscretePulseGenerator: '<S5>/Pulse Generator' */
  sfd_target_B.PulseGenerator = (sfd_target_DWork.clockTickCounter_g <
    (int32_T)sfd_target_P.PulseGenerator_Duty) &&
    (sfd_target_DWork.clockTickCounter_g >= 0) ? sfd_target_P.PulseGenerator_Amp
    : 0;
  if(sfd_target_DWork.clockTickCounter_g >=
   (int32_T)sfd_target_P.PulseGenerator_Period - 1) {
    sfd_target_DWork.clockTickCounter_g = 0;
  } else {
    sfd_target_DWork.clockTickCounter_g++;
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[13];

    sfcnOutputs(rts, 1);
  }

  /* Sum: '<S7>/Sum' */
  sfd_target_B.Sum_k = ((((((((((((((((sfd_target_B.Bits18_o1 +
    sfd_target_B.Bits18_o2) + sfd_target_B.Bits18_o3) + sfd_target_B.Bits18_o4)
    + sfd_target_B.Bits18_o5) + sfd_target_B.Bits18_o6) +
    sfd_target_B.Bits18_o7) + sfd_target_B.Bits18_o8) + sfd_target_B.Bits916_o1)
    + sfd_target_B.Bits916_o2) + sfd_target_B.Bits916_o3) +
    sfd_target_B.Bits916_o4) + sfd_target_B.Bits916_o5) +
    sfd_target_B.Bits916_o6) + sfd_target_B.Bits916_o7) +
    sfd_target_B.Bits916_o8) + sfd_target_B.Bit17_o1) + sfd_target_B.Bit17_o2;

  /* Math Block: '<S7>/Math Function' */

  /* Operator : mod */
  if (sfd_target_P.Constant_Value_m == 0.0) {
    sfd_target_B.MathFunction_f = sfd_target_B.Sum_k;
  } else if (sfd_target_P.Constant_Value_m ==
   floor(sfd_target_P.Constant_Value_m)) {
    /* Integer denominator.  Use conventional formula.*/
    sfd_target_B.MathFunction_f = (sfd_target_B.Sum_k -
      sfd_target_P.Constant_Value_m * floor((sfd_target_B.Sum_k) /
      (sfd_target_P.Constant_Value_m)));
  } else {
    /* Noninteger denominator. Check for nearly integer quotient.*/
    real_T uDivRound;
    real_T uDiv = sfd_target_B.Sum_k / sfd_target_P.Constant_Value_m;
    {
      real_T t;
      t = floor((fabs(uDiv) + 0.5));
      uDivRound = ((uDiv < 0.0) ? -t : t);
    }

    if (fabs((uDiv - uDivRound)) <= DBL_EPSILON * fabs(uDiv)) {
      sfd_target_B.MathFunction_f = 0.0;
    } else {
      sfd_target_B.MathFunction_f = (uDiv - floor(uDiv)) *
        sfd_target_P.Constant_Value_m;
    }
  }

  /* Fcn: '<S2>/cos_th2' */
  sfd_target_B.cos_th2 = cos(sfd_target_B.Sum_c);

  /* Gain: '<S2>/f' */
  sfd_target_B.f = sfd_target_B.PCI6031E_o1 * sfd_target_P.f_Gain;

  /* Product: '<S2>/Fxcos(th2)' */
  sfd_target_B.Fxcosth2 = sfd_target_B.cos_th2 * sfd_target_B.f;

  /* Fcn: '<S2>/sin_th2' */
  sfd_target_B.sin_th2 = sin(sfd_target_B.Sum_c);

  /* Product: '<S2>/Fxsin(th2)' */
  sfd_target_B.Fxsinth2 = sfd_target_B.f * sfd_target_B.sin_th2;

  /* Gain: '<S2>/Xy' */
  sfd_target_B.Xy = sfd_target_B.PCI6031E_o2 * sfd_target_P.Xy_Gain;

  /* Product: '<S2>/Fycos(th2)' */
  sfd_target_B.Fycosth2 = sfd_target_B.Xy * sfd_target_B.cos_th2;

  /* Product: '<S2>/Fysin(th2)' */
  sfd_target_B.Fysinth2 = sfd_target_B.Xy * sfd_target_B.sin_th2;

  /* Sum: '<S2>/Sum' */
  sfd_target_B.Sum_e = sfd_target_B.Fxsinth2 - sfd_target_B.Fycosth2;

  /* Sum: '<S2>/Sum1' */
  sfd_target_B.Sum1_h = (0.0 - sfd_target_B.Fxcosth2) - sfd_target_B.Fysinth2;

  /* Gain: '<S2>/Xy2' */
  sfd_target_B.Xy2 = sfd_target_B.PCI6031E_o5 * sfd_target_P.Xy2_Gain;

  /* Gain: '<S2>/f1' */
  sfd_target_B.f1 = sfd_target_B.PCI6031E_o4 * sfd_target_P.f1_Gain;

  /* Clock Block: '<Root>/Clock' */

  sfd_target_B.Clock = sfd_target_M->Timing.t[0];
}

/* Model update function */
void sfd_target_update(int_T tid)
{

  /* Update for UnitDelay: '<S5>/Unit Delay' */
  sfd_target_DWork.UnitDelay_DSTATE[0] = sfd_target_B.Bits18_o1;
  sfd_target_DWork.UnitDelay_DSTATE[1] = sfd_target_B.Bits18_o2;
  sfd_target_DWork.UnitDelay_DSTATE[2] = sfd_target_B.Bits18_o3;
  sfd_target_DWork.UnitDelay_DSTATE[3] = sfd_target_B.Bits18_o4;
  sfd_target_DWork.UnitDelay_DSTATE[4] = sfd_target_B.Bits18_o5;
  sfd_target_DWork.UnitDelay_DSTATE[5] = sfd_target_B.Bits18_o6;
  sfd_target_DWork.UnitDelay_DSTATE[6] = sfd_target_B.Bits18_o7;
  sfd_target_DWork.UnitDelay_DSTATE[7] = sfd_target_B.Bits18_o8;
  sfd_target_DWork.UnitDelay_DSTATE[8] = sfd_target_B.Bits916_o1;
  sfd_target_DWork.UnitDelay_DSTATE[9] = sfd_target_B.Bits916_o2;
  sfd_target_DWork.UnitDelay_DSTATE[10] = sfd_target_B.Bits916_o3;
  sfd_target_DWork.UnitDelay_DSTATE[11] = sfd_target_B.Bits916_o4;
  sfd_target_DWork.UnitDelay_DSTATE[12] = sfd_target_B.Bits916_o5;
  sfd_target_DWork.UnitDelay_DSTATE[13] = sfd_target_B.Bits916_o6;
  sfd_target_DWork.UnitDelay_DSTATE[14] = sfd_target_B.Bits916_o7;
  sfd_target_DWork.UnitDelay_DSTATE[15] = sfd_target_B.Bits916_o8;
  sfd_target_DWork.UnitDelay_DSTATE[16] = sfd_target_B.Bit17_o1;

  /* Update for UnitDelay: '<S3>/Unit Delay' */
  sfd_target_DWork.UnitDelay_DSTATE_c[0] = sfd_target_B.Bits181_o1;
  sfd_target_DWork.UnitDelay_DSTATE_c[1] = sfd_target_B.Bits181_o2;
  sfd_target_DWork.UnitDelay_DSTATE_c[2] = sfd_target_B.Bits181_o3;
  sfd_target_DWork.UnitDelay_DSTATE_c[3] = sfd_target_B.Bits181_o4;
  sfd_target_DWork.UnitDelay_DSTATE_c[4] = sfd_target_B.Bits181_o5;
  sfd_target_DWork.UnitDelay_DSTATE_c[5] = sfd_target_B.Bits181_o6;
  sfd_target_DWork.UnitDelay_DSTATE_c[6] = sfd_target_B.Bits181_o7;
  sfd_target_DWork.UnitDelay_DSTATE_c[7] = sfd_target_B.Bits181_o8;
  sfd_target_DWork.UnitDelay_DSTATE_c[8] = sfd_target_B.Bits91_o1;
  sfd_target_DWork.UnitDelay_DSTATE_c[9] = sfd_target_B.Bits91_o2;
  sfd_target_DWork.UnitDelay_DSTATE_c[10] = sfd_target_B.Bits91_o3;
  sfd_target_DWork.UnitDelay_DSTATE_c[11] = sfd_target_B.Bits91_o4;
  sfd_target_DWork.UnitDelay_DSTATE_c[12] = sfd_target_B.Bits91_o5;
  sfd_target_DWork.UnitDelay_DSTATE_c[13] = sfd_target_B.Bits91_o6;
  sfd_target_DWork.UnitDelay_DSTATE_c[14] = sfd_target_B.Bits91_o7;
  sfd_target_DWork.UnitDelay_DSTATE_c[15] = sfd_target_B.Bits91_o8;
  sfd_target_DWork.UnitDelay_DSTATE_c[16] = sfd_target_B.Bit1_o1;

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[1];

    sfcnUpdate(rts, 1);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[2];

    sfcnUpdate(rts, 1);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Update absolute time for base rate */

  if(!(++sfd_target_M->Timing.clockTick0)) ++sfd_target_M->Timing.clockTickH0;
  sfd_target_M->Timing.t[0] = sfd_target_M->Timing.clockTick0 *
    sfd_target_M->Timing.stepSize0 + sfd_target_M->Timing.clockTickH0 *
    sfd_target_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */

    if(!(++sfd_target_M->Timing.clockTick1)) ++sfd_target_M->Timing.clockTickH1;
    sfd_target_M->Timing.t[1] = sfd_target_M->Timing.clockTick1 *
      sfd_target_M->Timing.stepSize1 + sfd_target_M->Timing.clockTickH1 *
      sfd_target_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Model initialize function */
void sfd_target_initialize(boolean_T firstTime)
{

  if (firstTime) {
    /* registration code */
    /* initialize real-time model */
    (void)memset((char_T *)sfd_target_M, 0, sizeof(rtModel_sfd_target));

    {
      /* Setup solver object */

      rtsiSetSimTimeStepPtr(&sfd_target_M->solverInfo,
       &sfd_target_M->Timing.simTimeStep);
      rtsiSetTPtr(&sfd_target_M->solverInfo, &rtmGetTPtr(sfd_target_M));
      rtsiSetStepSizePtr(&sfd_target_M->solverInfo,
       &sfd_target_M->Timing.stepSize0);
      rtsiSetErrorStatusPtr(&sfd_target_M->solverInfo,
       &rtmGetErrorStatus(sfd_target_M));

      rtsiSetRTModelPtr(&sfd_target_M->solverInfo, sfd_target_M);
    }
    rtsiSetSimTimeStep(&sfd_target_M->solverInfo, MAJOR_TIME_STEP);

    /* Initialize timing info */
    {
      int_T *mdlTsMap = sfd_target_M->Timing.sampleTimeTaskIDArray;
      mdlTsMap[0] = 0;
      mdlTsMap[1] = 1;
      sfd_target_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
      sfd_target_M->Timing.sampleTimes =
        (&sfd_target_M->Timing.sampleTimesArray[0]);
      sfd_target_M->Timing.offsetTimes =
        (&sfd_target_M->Timing.offsetTimesArray[0]);
      /* task periods */
      sfd_target_M->Timing.sampleTimes[0] = (0.0);
      sfd_target_M->Timing.sampleTimes[1] = (0.01);

      /* task offsets */
      sfd_target_M->Timing.offsetTimes[0] = (0.0);
      sfd_target_M->Timing.offsetTimes[1] = (0.0);
    }

    rtmSetTPtr(sfd_target_M, &sfd_target_M->Timing.tArray[0]);

    {
      int_T *mdlSampleHits = sfd_target_M->Timing.sampleHitArray;
      mdlSampleHits[0] = 1;
      mdlSampleHits[1] = 1;
      sfd_target_M->Timing.sampleHits = (&mdlSampleHits[0]);
    }

    rtmSetTFinal(sfd_target_M, 3.6E+005);
    sfd_target_M->Timing.stepSize0 = 0.01;
    sfd_target_M->Timing.stepSize1 = 0.01;

    /* Setup for data logging */
    {
      static RTWLogInfo rt_DataLoggingInfo;

      sfd_target_M->rtwLogInfo = &rt_DataLoggingInfo;

      rtliSetLogFormat(sfd_target_M->rtwLogInfo, 0);

      rtliSetLogMaxRows(sfd_target_M->rtwLogInfo, 0);

      rtliSetLogDecimation(sfd_target_M->rtwLogInfo, 1);

      rtliSetLogVarNameModifier(sfd_target_M->rtwLogInfo, "rt_");

      rtliSetLogT(sfd_target_M->rtwLogInfo, "tout");

      rtliSetLogX(sfd_target_M->rtwLogInfo, "");

      rtliSetLogXFinal(sfd_target_M->rtwLogInfo, "");

      rtliSetSigLog(sfd_target_M->rtwLogInfo, "");

      rtliSetLogXSignalInfo(sfd_target_M->rtwLogInfo, NULL);

      rtliSetLogXSignalPtrs(sfd_target_M->rtwLogInfo, NULL);

      rtliSetLogY(sfd_target_M->rtwLogInfo, "");

      rtliSetLogYSignalInfo(sfd_target_M->rtwLogInfo, NULL);

      rtliSetLogYSignalPtrs(sfd_target_M->rtwLogInfo, NULL);
    }

    /* external mode info */
    sfd_target_M->Sizes.checksums[0] = (2755381354U);
    sfd_target_M->Sizes.checksums[1] = (3778275490U);
    sfd_target_M->Sizes.checksums[2] = (427466465U);
    sfd_target_M->Sizes.checksums[3] = (240757941U);

    {
      static const int8_T rtAlwaysEnabled = EXTMODE_SUBSYS_ALWAYS_ENABLED;

      static RTWExtModeInfo rt_ExtModeInfo;
      static const void *sysActives[10];

      sfd_target_M->extModeInfo = (&rt_ExtModeInfo);
      rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, sysActives);

      sysActives[0] = &rtAlwaysEnabled;
      sysActives[1] = &rtAlwaysEnabled;
      sysActives[2] = &rtAlwaysEnabled;
      sysActives[3] = &rtAlwaysEnabled;
      sysActives[4] = &rtAlwaysEnabled;
      sysActives[5] = &rtAlwaysEnabled;
      sysActives[6] = &rtAlwaysEnabled;
      sysActives[7] = &rtAlwaysEnabled;
      sysActives[8] = &rtAlwaysEnabled;
      sysActives[9] = &rtAlwaysEnabled;

      rteiSetModelMappingInfoPtr(&rt_ExtModeInfo,
       &sfd_target_M->SpecialInfo.mappingInfo);

      rteiSetChecksumsPtr(&rt_ExtModeInfo, sfd_target_M->Sizes.checksums);

      rteiSetTPtr(&rt_ExtModeInfo, rtmGetTPtr(sfd_target_M));
    }

    sfd_target_M->solverInfoPtr = (&sfd_target_M->solverInfo);
    sfd_target_M->Timing.stepSize = (0.01);
    rtsiSetFixedStepSize(&sfd_target_M->solverInfo, 0.01);
    rtsiSetSolverMode(&sfd_target_M->solverInfo, SOLVER_MODE_SINGLETASKING);

    {
      /* block I/O */
      void *b = (void *) &sfd_target_B;
      sfd_target_M->ModelData.blockIO = (b);

      (void)memset(b, 0, sizeof(BlockIO_sfd_target));

      {

        int_T i;
        b = &sfd_target_B.OffsetinRadians;
        for (i = 0; i < 130; i++) {
          ((real_T*)b)[i] = 0.0;
        }

        b =&sfd_target_B.ComplexCheck_o1;
        for (i = 0; i < 54; i++) {
          ((real_T*)b)[i] = 0.0;
        }
      }
    }
    /* parameters */
    sfd_target_M->ModelData.defaultParam = ((real_T *) &sfd_target_P);

    /* data type work */
    sfd_target_M->Work.dwork = ((void *) &sfd_target_DWork);
    (void)memset((char_T *) &sfd_target_DWork, 0, sizeof(D_Work_sfd_target));
    {
      int_T i;
      real_T *dwork_ptr = (real_T *) &sfd_target_DWork.UnitDelay_DSTATE[0];

      for (i = 0; i < 412; i++) {
        dwork_ptr[i] = 0.0;
      }
    }

    /* data type transition information */
    {
      static DataTypeTransInfo dtInfo;

      (void)memset((char_T *) &dtInfo, 0, sizeof(dtInfo));
      sfd_target_M->SpecialInfo.mappingInfo = (&dtInfo);

      sfd_target_M->SpecialInfo.xpcData = ((void*) &dtInfo);
      dtInfo.numDataTypes = 14;
      dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
      dtInfo.dataTypeNames = &rtDataTypeNames[0];

      /* Block I/O transition table */
      dtInfo.B = &rtBTransTable;

      /* Parameters transition table */
      dtInfo.P = &rtPTransTable;
    }

    /* C API for Parameter Tuning and/or Signal Monitoring */
    {
      static ModelMappingInfo mapInfo;

      memset((char_T *) &mapInfo, 0, sizeof(mapInfo));

      /* block signal monitoring map */
      mapInfo.Signals.blockIOSignals = &rtBIOSignals[0];
      mapInfo.Signals.numBlockIOSignals = 93;

      /* parameter tuning maps */
      mapInfo.Parameters.blockTuning = &rtBlockTuning[0];
      mapInfo.Parameters.variableTuning = &rtVariableTuning[0];
      mapInfo.Parameters.parametersMap = rtParametersMap;
      mapInfo.Parameters.dimensionsMap = rtDimensionsMap;
      mapInfo.Parameters.numBlockTuning = 112;
      mapInfo.Parameters.numVariableTuning = 0;

      sfd_target_M->SpecialInfo.mappingInfo = (&mapInfo);
    }

    /* initialize non-finites */
    rt_InitInfAndNaN(sizeof(real_T));

    /* child S-Function registration */
    {
      RTWSfcnInfo *sfcnInfo = &sfd_target_M->NonInlinedSFcns.sfcnInfo;

      sfd_target_M->sfcnInfo = (sfcnInfo);

      rtssSetErrorStatusPtr(sfcnInfo, &rtmGetErrorStatus(sfd_target_M));
      rtssSetNumRootSampTimesPtr(sfcnInfo, &sfd_target_M->Sizes.numSampTimes);
      rtssSetTPtrPtr(sfcnInfo, &rtmGetTPtr(sfd_target_M));
      rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(sfd_target_M));
      rtssSetTimeOfLastOutputPtr(sfcnInfo,
       &rtmGetTimeOfLastOutput(sfd_target_M));
      rtssSetStepSizePtr(sfcnInfo, &sfd_target_M->Timing.stepSize);
      rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(sfd_target_M));
      rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
       &sfd_target_M->ModelData.derivCacheNeedsReset);
      rtssSetZCCacheNeedsResetPtr(sfcnInfo,
       &sfd_target_M->ModelData.zCCacheNeedsReset);
      rtssSetBlkStateChangePtr(sfcnInfo,
       &sfd_target_M->ModelData.blkStateChange);
      rtssSetSampleHitsPtr(sfcnInfo, &sfd_target_M->Timing.sampleHits);
      rtssSetPerTaskSampleHitsPtr(sfcnInfo,
       &sfd_target_M->Timing.perTaskSampleHits);
      rtssSetSimModePtr(sfcnInfo, &sfd_target_M->simMode);
      rtssSetSolverInfoPtr(sfcnInfo, &sfd_target_M->solverInfoPtr);
    }

    sfd_target_M->Sizes.numSFcns = (14);

    /* register each child */
    {
      (void)memset((void *)&sfd_target_M->NonInlinedSFcns.childSFunctions[0], 0,
       14*sizeof(SimStruct));
      sfd_target_M->childSfunctions =
        (&sfd_target_M->NonInlinedSFcns.childSFunctionPtrs[0]);
      {
        int_T i;

        for(i = 0; i < 14; i++) {
          sfd_target_M->childSfunctions[i] =
            (&sfd_target_M->NonInlinedSFcns.childSFunctions[i]);
        }
      }

      /* Level2 S-Function Block: sfd_target/<Root>/Receive (xpcudpbytereceive) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[0];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[0]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[0]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 72);
            ssSetOutputPortSignal(rts, 0, ((uint8_T *) sfd_target_B.Receive_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Receive_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Receive");
        ssSetPath(rts, "sfd_target/Receive");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn0.params;

          ssSetSFcnParamsCount(rts, 4);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Receive_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Receive_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Receive_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Receive_P4_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Receive_IWORK[0]);
        ssSetPWork(rts, (void **) &sfd_target_DWork.Receive_PWORK);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn0.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Receive_IWORK[0]);

          /* PWORK */
          ssSetDWorkWidth(rts, 1, 1);
          ssSetDWorkDataType(rts, 1,SS_POINTER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sfd_target_DWork.Receive_PWORK);
        }

        /* registration */
        xpcudpbytereceive(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<Root>/S-target conditions (intarget_trialtime1) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[1];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[1]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[1]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn1.UPtrs0;
            sfcnUPtrs[0] = &sfd_target_B.Sum_a;
            sfcnUPtrs[1] = &sfd_target_B.Sum1;
            sfcnUPtrs[2] = &sfd_target_B.Unpack2_o5;
            sfcnUPtrs[3] = &sfd_target_B.Unpack2_o6;
            sfcnUPtrs[4] = &sfd_target_B.Unpack2_o7;
            sfcnUPtrs[5] = &sfd_target_B.Unpack2_o3;
            sfcnUPtrs[6] = &sfd_target_B.Unpack2_o4;
            sfcnUPtrs[7] = &sfd_target_B.Unpack2_o8;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 8);
          }
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 1);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 5);
            ssSetOutputPortSignal(rts, 0, ((real_T *)
              sfd_target_B.Stargetconditions));
          }
        }

        /* states */
        ssSetDiscStates(rts, (real_T *)
         &sfd_target_DWork.Stargetconditions_DSTATE[0]);
        /* path info */
        ssSetModelName(rts, "S-target conditions");
        ssSetPath(rts, "sfd_target/S-target conditions");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn1.params;

          ssSetSFcnParamsCount(rts, 3);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0,
           (mxArray*)&sfd_target_P.Stargetconditions_P1_Size[0]);
          ssSetSFcnParam(rts, 1,
           (mxArray*)&sfd_target_P.Stargetconditions_P2_Size[0]);
          ssSetSFcnParam(rts, 2,
           (mxArray*)&sfd_target_P.Stargetconditions_P3_Size[0]);
        }

        /* work vectors */
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn1.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* DSTATE */
          ssSetDWorkWidth(rts, 0, 3);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWorkUsedAsDState(rts, 0, 1);
          ssSetDWork(rts, 0, &sfd_target_DWork.Stargetconditions_DSTATE[0]);
        }

        /* registration */
        intarget_trialtime1(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sfd_target/<Root>/S-Function1 (forces) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[2];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[2]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[2]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn2.UPtrs0;
            sfcnUPtrs[0] = &sfd_target_B.Sum_a;
            sfcnUPtrs[1] = &sfd_target_B.Sum1;
            sfcnUPtrs[2] = &sfd_target_B.Unpack2_o5;
            sfcnUPtrs[3] = &sfd_target_B.Unpack2_o6;
            sfcnUPtrs[4] = &sfd_target_B.J00;
            sfcnUPtrs[5] = &sfd_target_B.J10;
            sfcnUPtrs[6] = &sfd_target_B.J01;
            sfcnUPtrs[7] = &sfd_target_B.J11;
            sfcnUPtrs[8] = &sfd_target_B.Unpack2_o7;
            sfcnUPtrs[9] = &sfd_target_B.Unpack2_o3;
            sfcnUPtrs[10] = &sfd_target_B.Unpack2_o4;
            sfcnUPtrs[11] = &sfd_target_B.Stargetconditions[0];
            sfcnUPtrs[12] = &sfd_target_B.Unpack2_o8;
            sfcnUPtrs[13] = &sfd_target_B.Unpack2_o2;
            sfcnUPtrs[14] = &sfd_target_B.Sum;
            sfcnUPtrs[15] = &sfd_target_B.Sum_c;
            sfcnUPtrs[16] = &sfd_target_B.Unpack2_o9;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 17);
          }
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 1);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 4);
            ssSetOutputPortSignal(rts, 0, ((real_T *) sfd_target_B.SFunction1));
          }
        }

        /* states */
        ssSetDiscStates(rts, (real_T *) &sfd_target_DWork.SFunction1_DSTATE[0]);
        /* path info */
        ssSetModelName(rts, "S-Function1");
        ssSetPath(rts, "sfd_target/S-Function1");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn2.params;

          ssSetSFcnParamsCount(rts, 2);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.SFunction1_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.SFunction1_P2_Size[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sfd_target_DWork.SFunction1_RWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn2.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 301);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.SFunction1_RWORK[0]);

          /* DSTATE */
          ssSetDWorkWidth(rts, 1, 4);
          ssSetDWorkDataType(rts, 1,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWorkUsedAsDState(rts, 1, 1);
          ssSetDWork(rts, 1, &sfd_target_DWork.SFunction1_DSTATE[0]);
        }

        /* registration */
        forces(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sfd_target/<Root>/PCI-6031E  (danipcie) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[3];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[3]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[3]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn3.UPtrs0;
            sfcnUPtrs[0] = &sfd_target_B.EGain;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn3.UPtrs1;
            sfcnUPtrs[0] = &sfd_target_B.SGain;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "PCI-6031E ");
        ssSetPath(rts, "sfd_target/PCI-6031E ");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn3.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.PCI6031E_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.PCI6031E_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.PCI6031E_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.PCI6031E_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.PCI6031E_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.PCI6031E_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.PCI6031E_P7_Size[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sfd_target_DWork.PCI6031E_RWORK[0]);
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.PCI6031E_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn3.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 6);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.PCI6031E_RWORK[0]);

          /* IWORK */
          ssSetDWorkWidth(rts, 1, 2);
          ssSetDWorkDataType(rts, 1,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sfd_target_DWork.PCI6031E_IWORK[0]);
        }

        /* registration */
        danipcie(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }

      /* Level2 S-Function Block: sfd_target/<Root>/Send (xpcudpbytesend) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[4];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[4]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[4]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn4.inputPortInfo[0]);

          /* port 0 */
          {
            ssSetInputPortRequiredContiguous(rts, 0, 1);
            ssSetInputPortSignal(rts, 0, sfd_target_B.Pack1);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 40);
          }
        }

        /* path info */
        ssSetModelName(rts, "Send");
        ssSetPath(rts, "sfd_target/Send");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn4.params;

          ssSetSFcnParamsCount(rts, 4);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Send_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Send_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Send_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Send_P4_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Send_IWORK[0]);
        ssSetPWork(rts, (void **) &sfd_target_DWork.Send_PWORK);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn4.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Send_IWORK[0]);

          /* PWORK */
          ssSetDWorkWidth(rts, 1, 1);
          ssSetDWorkDataType(rts, 1,SS_POINTER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sfd_target_DWork.Send_PWORK);
        }

        /* registration */
        xpcudpbytesend(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetInputPortWidth(rts, 0, 40);
        ssSetInputPortDataType(rts, 0, SS_UINT8);
        ssSetInputPortComplexSignal(rts, 0, 0);
        ssSetInputPortFrameData(rts, 0, 0);

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sfd_target/<S3>/Bit1 (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[5];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[5]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[5]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn5.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bit1_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bit1_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bit1");
        ssSetPath(rts, "sfd_target/Subsystem/Elbow Angle/Bit1");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn5.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bit1_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bit1_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bit1_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bit1_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bit1_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bit1_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bit1_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bit1_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn5.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bit1_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S3>/Bits1-8 1 (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[6];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[6]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[6]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bits181_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bits181_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sfd_target_B.Bits181_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sfd_target_B.Bits181_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sfd_target_B.Bits181_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sfd_target_B.Bits181_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sfd_target_B.Bits181_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sfd_target_B.Bits181_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits1-8\n1");
        ssSetPath(rts, "sfd_target/Subsystem/Elbow Angle/Bits1-8 1");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn6.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bits181_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bits181_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bits181_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bits181_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bits181_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bits181_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bits181_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bits181_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn6.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bits181_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S3>/Bits9-1 (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[7];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[7]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[7]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn7.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bits91_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bits91_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sfd_target_B.Bits91_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sfd_target_B.Bits91_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sfd_target_B.Bits91_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sfd_target_B.Bits91_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sfd_target_B.Bits91_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sfd_target_B.Bits91_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits9-1");
        ssSetPath(rts, "sfd_target/Subsystem/Elbow Angle/Bits9-1");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn7.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bits91_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bits91_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bits91_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bits91_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bits91_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bits91_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bits91_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bits91_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn7.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bits91_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S3>/PCI-DIO-96 1 (dopci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[8];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[8]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[8]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn8.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn8.UPtrs0;
            sfcnUPtrs[0] = &sfd_target_B.PulseGenerator1;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn8.UPtrs1;
            sfcnUPtrs[0] = &sfd_target_P.Constant3_Value;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "PCI-DIO-96 1");
        ssSetPath(rts, "sfd_target/Subsystem/Elbow Angle/PCI-DIO-96 1");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn8.params;

          ssSetSFcnParamsCount(rts, 9);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.PCIDIO961_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.PCIDIO961_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.PCIDIO961_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.PCIDIO961_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.PCIDIO961_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.PCIDIO961_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.PCIDIO961_P7_Size[0]);
          ssSetSFcnParam(rts, 7, (mxArray*)&sfd_target_P.PCIDIO961_P8_Size[0]);
          ssSetSFcnParam(rts, 8, (mxArray*)&sfd_target_P.PCIDIO961_P9_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.PCIDIO961_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn8.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.PCIDIO961_IWORK[0]);
        }

        /* registration */
        dopci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }

      /* Level2 S-Function Block: sfd_target/<S2>/PCI-6031E  (adnipcie) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[9];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn9.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn9.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn9.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[9]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[9]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn9.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 6);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *)
              &sfd_target_B.PCI6031E_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *)
              &sfd_target_B.PCI6031E_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *)
              &sfd_target_B.PCI6031E_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *)
              &sfd_target_B.PCI6031E_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *)
              &sfd_target_B.PCI6031E_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *)
              &sfd_target_B.PCI6031E_o6));
          }
        }
        /* path info */
        ssSetModelName(rts, "PCI-6031E ");
        ssSetPath(rts, "sfd_target/Subsystem/PCI-6031E ");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn9.params;

          ssSetSFcnParamsCount(rts, 6);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.PCI6031E_P1_Size_g[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.PCI6031E_P2_Size_d[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.PCI6031E_P3_Size_b[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.PCI6031E_P4_Size_h[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.PCI6031E_P5_Size_d[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.PCI6031E_P6_Size_f[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sfd_target_DWork.PCI6031E_RWORK_d[0]);
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.PCI6031E_IWORK_j[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn9.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 64);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.PCI6031E_RWORK_d[0]);

          /* IWORK */
          ssSetDWorkWidth(rts, 1, 66);
          ssSetDWorkDataType(rts, 1,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sfd_target_DWork.PCI6031E_IWORK_j[0]);
        }

        /* registration */
        adnipcie(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 0);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S5>/Bit17 (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[10];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn10.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn10.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn10.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[10]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[10]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn10.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bit17_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bit17_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bit17");
        ssSetPath(rts, "sfd_target/Subsystem/Shoulder Angle/Bit17");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn10.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bit17_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bit17_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bit17_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bit17_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bit17_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bit17_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bit17_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bit17_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn10.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bit17_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S5>/Bits1-8  (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[11];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn11.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn11.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn11.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[11]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[11]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn11.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bits18_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bits18_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sfd_target_B.Bits18_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sfd_target_B.Bits18_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sfd_target_B.Bits18_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sfd_target_B.Bits18_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sfd_target_B.Bits18_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sfd_target_B.Bits18_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits1-8\n");
        ssSetPath(rts, "sfd_target/Subsystem/Shoulder Angle/Bits1-8 ");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn11.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bits18_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bits18_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bits18_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bits18_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bits18_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bits18_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bits18_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bits18_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn11.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bits18_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S5>/Bits9-16 (dipci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[12];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn12.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn12.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn12.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[12]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[12]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn12.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sfd_target_B.Bits916_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sfd_target_B.Bits916_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sfd_target_B.Bits916_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sfd_target_B.Bits916_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sfd_target_B.Bits916_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sfd_target_B.Bits916_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sfd_target_B.Bits916_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sfd_target_B.Bits916_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits9-16");
        ssSetPath(rts, "sfd_target/Subsystem/Shoulder Angle/Bits9-16");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn12.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.Bits916_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.Bits916_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.Bits916_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.Bits916_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.Bits916_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.Bits916_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.Bits916_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.Bits916_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn12.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.Bits916_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sfd_target/<S5>/Int & OE (dopci8255) */
      {
        SimStruct *rts = sfd_target_M->childSfunctions[13];
        /* timing info */
        time_T *sfcnPeriod = sfd_target_M->NonInlinedSFcns.Sfcn13.sfcnPeriod;
        time_T *sfcnOffset = sfd_target_M->NonInlinedSFcns.Sfcn13.sfcnOffset;
        int_T *sfcnTsMap = sfd_target_M->NonInlinedSFcns.Sfcn13.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sfd_target_M->NonInlinedSFcns.blkInfo2[13]);
          ssSetRTWSfcnInfo(rts, sfd_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sfd_target_M->NonInlinedSFcns.methods2[13]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sfd_target_M->NonInlinedSFcns.Sfcn13.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn13.UPtrs0;
            sfcnUPtrs[0] = &sfd_target_B.PulseGenerator;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sfd_target_M->NonInlinedSFcns.Sfcn13.UPtrs1;
            sfcnUPtrs[0] = &sfd_target_P.Constant1_Value;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "Int & OE");
        ssSetPath(rts, "sfd_target/Subsystem/Shoulder Angle/Int & OE");
        ssSetRTModel(rts,sfd_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sfd_target_M->NonInlinedSFcns.Sfcn13.params;

          ssSetSFcnParamsCount(rts, 9);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sfd_target_P.IntOE_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sfd_target_P.IntOE_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sfd_target_P.IntOE_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sfd_target_P.IntOE_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sfd_target_P.IntOE_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sfd_target_P.IntOE_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sfd_target_P.IntOE_P7_Size[0]);
          ssSetSFcnParam(rts, 7, (mxArray*)&sfd_target_P.IntOE_P8_Size[0]);
          ssSetSFcnParam(rts, 8, (mxArray*)&sfd_target_P.IntOE_P9_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sfd_target_DWork.IntOE_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sfd_target_M->NonInlinedSFcns.Sfcn13.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sfd_target_DWork.IntOE_IWORK[0]);
        }

        /* registration */
        dopci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }
    }
  }
}

/* Model terminate function */
void sfd_target_terminate(void)
{

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[8];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[9];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[10];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[11];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[12];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[13];
    sfcnTerminate(rts);
  }

  /* External mode */
  rtExtModeShutdown(2);
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/

void MdlOutputs(int_T tid) {

  sfd_target_output(tid);
}

void MdlUpdate(int_T tid) {

  sfd_target_update(tid);
}

void MdlInitializeSizes(void) {
  sfd_target_M->Sizes.numContStates = (0); /* Number of continuous states */
  sfd_target_M->Sizes.numY = (0);       /* Number of model outputs */
  sfd_target_M->Sizes.numU = (0);       /* Number of model inputs */
  sfd_target_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  sfd_target_M->Sizes.numSampTimes = (2); /* Number of sample times */
  sfd_target_M->Sizes.numBlocks = (111); /* Number of blocks */
  sfd_target_M->Sizes.numBlockIO = (129); /* Number of block outputs */
  sfd_target_M->Sizes.numBlockPrms = (391); /* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void) {
}

void MdlInitialize(void) {

  {
    int32_T i1;

    /* InitializeConditions for UnitDelay: '<S5>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sfd_target_DWork.UnitDelay_DSTATE[i1] = sfd_target_P.UnitDelay_X0;
    }
  }

  {
    int32_T i1;

    /* InitializeConditions for UnitDelay: '<S3>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sfd_target_DWork.UnitDelay_DSTATE_c[i1] = sfd_target_P.UnitDelay_X0_e;
    }
  }

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[1];
    sfcnInitializeConditions(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[2];
    sfcnInitializeConditions(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }
}

void MdlStart(void) {

  /* user code (Start function Body) */
  {
    void *module = GetModuleHandle(NULL);
    rl32eScopeExists = (int (XPCCALLCONV *)(int))GetProcAddress(module,
     "rl32eScopeExists");
    if (rl32eScopeExists == NULL) {
      printf("Error resolving function rl32eScopeExists\n");
      return;
    }

    rl32eDefScope = (int (XPCCALLCONV *)(int, int))GetProcAddress(module,
     "rl32eDefScope");
    if (rl32eDefScope == NULL) {
      printf("Error resolving function rl32eDefScope\n");
      return;
    }

    rl32eAddSignal = (void (XPCCALLCONV *)(int, int))GetProcAddress(module,
     "rl32eAddSignal");
    if (rl32eAddSignal == NULL) {
      printf("Error resolving function rl32eAddSignal\n");
      return;
    }

    rl32eSetScope = (void (XPCCALLCONV *)(int, int,
      double))GetProcAddress(module, "rl32eSetScope");
    if (rl32eSetScope == NULL) {
      printf("Error resolving function rl32eSetScope\n");
      return;
    }

    xpceFSScopeSet = (void (XPCCALLCONV *)(int, const char *, int, unsigned
      int))GetProcAddress(module, "xpceFSScopeSet");
    if (xpceFSScopeSet == NULL) {
      printf("Error resolving function xpceFSScopeSet\n");
      return;
    }

    rl32eSetTargetScope = (void (XPCCALLCONV *)(int, int,
      double))GetProcAddress(module, "rl32eSetTargetScope");
    if (rl32eSetTargetScope == NULL) {
      printf("Error resolving function rl32eSetTargetScope\n");
      return;
    }

    rl32eRestartAcquisition = (void (XPCCALLCONV *)(int))GetProcAddress(module,
     "rl32eRestartAcquisition");
    if (rl32eRestartAcquisition == NULL) {
      printf("Error resolving function rl32eRestartAcquisition\n");
      return;
    }

    xpceScopeAcqOK = (void (XPCCALLCONV *)(int, int *))GetProcAddress(module,
     "xpceScopeAcqOK");
    if (xpceScopeAcqOK == NULL) {
      printf("Error resolving function xpceScopeAcqOK\n");
      return;
    }
  }

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[0];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[3];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* S-Function Block: <S1>/S-Function (scblock) */

  {
    int i;
    if ((i = rl32eScopeExists(1)) == 0) {
      if ((i = rl32eDefScope(1,3)) != 0) {
        printf("Error creating scope 1\n");
      } else {

        rl32eAddSignal(1,
         0);
        rl32eAddSignal(1,
         125);
        rl32eAddSignal(1,
         120);
        rl32eAddSignal(1,
         196);
        rl32eAddSignal(1,
         197);
        rl32eAddSignal(1,
         150);
        rl32eAddSignal(1,
         151);
        rl32eAddSignal(1,
         118);
        rl32eAddSignal(1,
         119);
        rl32eSetScope(1, 4, 250);
        rl32eSetScope(1, 40, 0);
        rl32eSetScope(1, 7, 1);
        rl32eSetScope(1, 0, 0);
        xpceFSScopeSet(1, "sfddata.dat", 0, 512);
        rl32eSetScope (1, 11, 1);
        rl32eSetScope(1, 3,
         0);
        rl32eSetScope(1, 1, 0.0);
        rl32eSetScope(1, 2, 0);
        rl32eSetScope(1, 8, -1);
        rl32eSetScope(1, 10, 0);
        xpceScopeAcqOK(1, &sfd_target_DWork.SFunction_IWORK.AcquireOK);
      }
    }
    if (i) {
      rl32eRestartAcquisition(1);
    }
  }

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[4];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[5];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[6];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[7];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  {
    real_T tFirst_9_37;
    int32_T Ns_9_37;

    /* Start for DiscretePulseGenerator: '<S3>/Pulse Generator1' */
    tFirst_9_37 = 0.0;
    Ns_9_37 = (int32_T)floor(tFirst_9_37 / 0.01 + 0.5);
    if(Ns_9_37 <= 0) {
      sfd_target_DWork.clockTickCounter = Ns_9_37;
    } else {
      sfd_target_DWork.clockTickCounter = Ns_9_37 -
        (int32_T)(sfd_target_P.PulseGenerator1_Period * floor(Ns_9_37 /
        sfd_target_P.PulseGenerator1_Period));
    }
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[8];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[9];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[10];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[11];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[12];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  {
    real_T tFirst_9_47;
    int32_T Ns_9_47;

    /* Start for DiscretePulseGenerator: '<S5>/Pulse Generator' */
    tFirst_9_47 = 0.0;
    Ns_9_47 = (int32_T)floor(tFirst_9_47 / 0.01 + 0.5);
    if(Ns_9_47 <= 0) {
      sfd_target_DWork.clockTickCounter_g = Ns_9_47;
    } else {
      sfd_target_DWork.clockTickCounter_g = Ns_9_47 -
        (int32_T)(sfd_target_P.PulseGenerator_Period * floor(Ns_9_47 /
        sfd_target_P.PulseGenerator_Period));
    }
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sfd_target_M->childSfunctions[13];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  MdlInitialize();
}

rtModel_sfd_target *sfd_target(void) {
  sfd_target_initialize(1);
  return sfd_target_M;
}

void MdlTerminate(void) {
  sfd_target_terminate();
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/

