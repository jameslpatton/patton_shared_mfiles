#ifndef __sfd_target_XPCOPTIONS_H___
#define __sfd_target_XPCOPTIONS_H___

#include "simstruc_types.h"
#ifndef MT
#define MT                              0                        /* MT may be undefined by simstruc_types.h */
#endif

#include "sfd_target.h"

#define SIZEOF_PARAMS                   (-1 * (int)sizeof(Parameters_sfd_target))

#define SIMMODE                         0

#define LOGTET                          1

#define LOGBUFSIZE                      100000

#define IRQ_NO                          0
#define IO_IRQ                          0

#define WWW_ACCESS_LEVEL                0

#define CPUCLOCK                        0

/* Change all stepsize using the newBaseRateStepSize */
void sfd_target_ChangeStepSize(real_T newBaseRateStepSize, rtModel_sfd_target
 *sfd_target_M) {
  real_T ratio = newBaseRateStepSize / 0.01;

  /* update non-zore stepsize of periodic 
   * sample time. Stepsize of asynchronous
   * sample time is not changed in this function */
  sfd_target_M->Timing.stepSize0 = sfd_target_M->Timing.stepSize0 * ratio;
  sfd_target_M->Timing.stepSize1 = sfd_target_M->Timing.stepSize1 * ratio;
  sfd_target_M->Timing.stepSize = sfd_target_M->Timing.stepSize * ratio;
}

void XPCCALLCONV changeStepSize(real_T stepSize) {
  /* Change all stepsize using the newBaseRateStepSize */

  sfd_target_ChangeStepSize(stepSize, sfd_target_M);
}

#endif                                  /* __sfd_target_XPCOPTIONS_H___ */

