/*
 * sfd_target_types.h
 *
 * Real-Time Workshop code generation for Simulink model "sfd_target.mdl".
 *
 * Model Version              : 1.528
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Wed Jan 10 12:11:52 2007
 */
#ifndef _RTW_HEADER_sfd_target_types_h_
#define _RTW_HEADER_sfd_target_types_h_

/* Parameters (auto storage) */
typedef struct _Parameters_sfd_target Parameters_sfd_target;

/* Forward declaration for rtModel */
typedef struct _rtModel_sfd_target_Tag rtModel_sfd_target;

#endif                                  /* _RTW_HEADER_sfd_target_types_h_ */
