
function [sys,x0,str,ts] = target_2(t,x,u,flag,List_Exp,Snd_tooslow, Snd_toofast,Start_trial,zeroforce)
 
global index_t target aux_flag tg Ndata;
 
switch flag,

  %%%%%%%%%%%%%%%%%%
  % Initialization %
  %%%%%%%%%%%%%%%%%%
  case 0,
  [sys,x0,str,ts]=mdlInitializeSizes(List_Exp, Snd_tooslow, Snd_toofast,Start_trial,zeroforce);
    
  %%%%%%%%%%%%%%%
  % Derivatives %
  %%%%%%%%%%%%%%%
  %case 1,
  %  sys=mdlDerivatives(t,x,u);

  %%%%%%%%%%
  % Update %
  %%%%%%%%%%
  %case 2,
  %sys=mdlUpdate(t,x,u);

  %%%%%%%%%%%
  % Outputs %
  %%%%%%%%%%%
  case 3,
  sys=mdlOutputs(t,x,u,List_Exp, Snd_tooslow, Snd_toofast,Start_trial,zeroforce);

  %%%%%%%%%%%%%%%%%%%%%%%
  % GetTimeOfNextVarHit %
  %%%%%%%%%%%%%%%%%%%%%%%
  %case 4,
  %  sys=mdlGetTimeOfNextVarHit(t,x,u);

  %%%%%%%%%%%%%
  % Terminate %
  %%%%%%%%%%%%%
  case 9,
  sys=mdlTerminate(t,x,u);
  
  case {1, 2, 4, 9}
  sys=[]; %unused flags
  
  %%%%%%%%%%%%%%%%%%%%
  % Unexpected flags %
  %%%%%%%%%%%%%%%%%%%%
  otherwise
    error(['Unhandled flag = ',num2str(flag)]);

end

% end sfuntmpl

%
%=============================================================================
% mdlInitializeSizes
% Return the sizes, initial conditions, and sample times for the S-function.
%=============================================================================
%
function [sys,x0,str,ts]=mdlInitializeSizes(List_Exp, Snd_tooslow, Snd_toofast,Start_trial,zeroforce)
global index_t target aux_flag tg disaxes;
 
% call simsizes for a sizes structure, fill it in and convert it to a
% sizes array.
%
% Note that in this example, the values are hard coded.  This is not a
% recommended practice as the characteristics of the block are typically
% defined by the S-function parameters.
%

sizes = simsizes;

sizes.NumContStates  = 0;
sizes.NumDiscStates  = 0;
sizes.NumOutputs     = 14; %-1;          % attention: only the input could be dynamically sized (Santiago)
sizes.NumInputs      = -1;              % attention: only the input could be dynamically sized
sizes.DirFeedthrough = 1;
sizes.NumSampleTimes = 1;   % at least one sample time is needed

sys = simsizes(sizes);
 
%
% initialize the initial conditions
%
x0  = [];
 
% str is always an empty matrix
%
str = [];

data=zeros(0,2);
t=[];

%
% initialize the array of sample times
%
ts  = [-1 0];
aux_flag = 1;
index_t=Start_trial;
%index_t = 1;
x=List_Exp(index_t,:);
%Snd_Tg;
% end mdlInitializeSizes

%
%=============================================================================
% mdlDerivatives
% Return the derivatives for the continuous states.
%=============================================================================
%
%function sys=mdlDerivatives(t,x,u)

%sys = [];

% end mdlDerivatives

%
%=============================================================================
% mdlUpdate
% Handle discrete state updates, sample time hits, and major time step
% requirements.
%=============================================================================
%
%function sys=mdlUpdate(t,x,u)

%end if isempty
%sys = [];

% end mdlUpdate

%
%=============================================================================
% mdlOutputs
% Return the block outputs.
%=============================================================================
%
function sys=mdlOutputs(t,x,u,List_Exp, Snd_tooslow, Snd_toofast,Start_trial,zeroforce)
global index_t target aux_flag tg goodspeed;  
global hTt;
global trialTime;

goodspeed=0;
if (u(1)==1) & (aux_flag==1)   % if the flag is 1 (previous target has been reached) and the auxiliary target too (that is this is the first time we reah it)
                        % then skip to the next target, save everything (stopping and re-starting the target) and set up the auxiliary flag to 0 for some time
      index_t=index_t+1; 
      Start_trial=index_t;
      if index_t <= length(List_Exp)      
        x=List_Exp(index_t,:);
      end
   
       aux_flag = 0;               % we define an auxiliary flag to avoid that while we are in the target
       if u(2)>0.368              % if u(2)<0.45 %10 (use this if u(2) is time spent)
         wavplay(Snd_toofast,70000); goodspeed=0;    % the function continues to switch on new following targets
       elseif u(2)<0.208            % elseif u(2)>0.55 %20 (use this if u(2) is time spent)
         wavplay(Snd_toofast,2000); goodspeed=0;    
       else
          wavplay(Snd_toofast,14000); goodspeed=1;                         % we give a sound feedback when we are in the target
       end        
 
       if index_t > length(List_Exp)    % wrap things up after all trials collected  
            % the following is REMARKED OUT because it casues a crash:
            %set_param('test_host','SimulationCommand','stop'); % stop the host
            -tg;                        % stop the target 
           % ywei;
           txtHandle=textOnPlot('   All Finished! ',.5,.7);
           set(txtHandle, 'color','w',  ...
                          'fontsize', 30,  ...
                          'color','y',  ...
                          'fontSize',20,  ...
                          'HorizontalAlignment','center')
           for sndLoop=2000:1500:20000           
             wavplay(Snd_toofast,sndLoop)
           end

           disp (' ---------------------------' );
           disp ('Experiment is over, transferring data');
           retrieveAndConvertDataFromXpc('SFD'); 
           disp (' ---------------------------');
           
            
       end            

 elseif u(1)~=1 ;                 % in any case we plot the previous target
    if index_t <= length(List_Exp)
      x=List_Exp(index_t,:);   
     end    
 end  

if (u(1)==0)
    aux_flag = 1;
end

sys = x;

%sys = [x goodspeed];
%end mdlOutputs

 
 
%=============================================================================
% mdlTerminate
% Perform any end of simulation tasks.
%=============================================================================
%
function sys=mdlTerminate(t,x,u)

sys = [];

% end mdlTerminate
