%%%%%%%%%%%%%%%%%%%% This codes is called by main.m    %%%%%%%%%%%%%%%%%
% This file is to change Jim's style target file to a target file that XPC
% code can call from.  Baisc operation includes: 
% 1) Specify the movement type, either center-out or random-walk movement;
% 2) Transform the target to new visually seen target position if there is visual distortion;
% 3) Form a list of movement wbased on the order of the 
%      N_trial N_movement X_start Y_start X_target Y_target Field_Type
%      Direction Phase  Optional_e

%  Development by Yejun Wei, Auguet 14, 2003.

 
fid =fopen('listtarget.m','w');
protocolfile=input('Input Protocol File Name: [targ_sa.txd] ','s');
if isempty(protocolfile)
    protocolfile = 'targ_sa.txd';
end
%protocolfile = 'targ_sa.txd';
    
[h,d]=hdrload(protocolfile);
offset =0;
UGain=1;
[dRow,dCol]=size(d);

if dCol == 10
  
    % y=[d(1,1)   2   d(1,2)*UGain  d(1,3)*UGain-offset  d(1,2)*UGain    d(1,3)*UGain-offset d(1,6)  d(1,7)  d(1,9)   d(1,10) 0    0 0 0];
    % fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
    
         for i=1:length(d)-1
      y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)  d(i,10) 0    0 0 0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
  end
  
  i=length(d);
  y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)   d(i,10)    0 0 0 0];
  fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
  
%   y=[d(i,1)+1   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)   d(i,10)    0 0 0 0];
%   fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
end


if dCol == 9 
 for i=1:length(d)
    if d(i,6)>=4000 && d(i,6)<5000
        ntx = d(i,2) + cos((d(i,6)-4000)*pi/180)*(d(i,4)-d(i,2)) - sin((d(i,6)-4000)*pi/180)*(d(i,5)-d(i,3));
        nty = d(i,3) + sin((d(i,6)-4000)*pi/180)*(d(i,4)-d(i,2)) + cos((d(i,6)-4000)*pi/180)*(d(i,5)-d(i,3));
        d(i,4) = ntx;
        d(i,5) = nty;
    end    
  end

     y=[d(1,1)   2   d(1,2)*UGain  d(1,3)*UGain-offset  d(1,2)*UGain    d(1,3)*UGain-offset d(1,6)  d(1,7)  d(1,9)   0 0    0 0 0];
     fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
    
      for i=1:length(d)-1
      y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)  0 0    0 0 0];
      fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
  end
  
  i=length(d);
  y=[d(i,1)   1   d(i,2)*UGain  d(i,3)*UGain-offset  d(i,4)*UGain    d(i,5)*UGain-offset    d(i,6)  d(i,7)  d(i,9)   0    0 0 0 0];
  fprintf(fid,'%i\t%i\t %4.2f\t%4.2f\t%4.2f\t %4.2f\t %i\t%i\t  %i\t %i\t %4.2f\t %4.2f\t %4.2f\t %4.2f\n',y);
end

fclose(fid);

    