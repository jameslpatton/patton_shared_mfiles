/*
 * sa_target_bio.c
 *
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */

#ifndef BLOCK_IO_SIGNALS
#define BLOCK_IO_SIGNALS

#include "bio_sig.h"

/* Block output signal information */
const BlockIOSignals rtBIOSignals[] = {

  /* blockName,
   * signalName, portNumber, signalWidth, signalAddr, dtName, dtSize
   */

  {
    "Clock",
    "NULL", 0, 1, &sa_target_B.Clock, "double", sizeof(real_T)
  },

  {
    "E Gain",
    "NULL", 0, 1, &sa_target_B.EGain, "double", sizeof(real_T)
  },

  {
    "S Gain",
    "NULL", 0, 1, &sa_target_B.SGain, "double", sizeof(real_T)
  },

  {
    "Pack1",
    "NULL", 0, 40, &sa_target_B.Pack1[0], "unsigned char", sizeof(uint8_T)
  },

  {
    "Receive/p1",
    "NULL", 0, 72, &sa_target_B.Receive_o1[0], "unsigned char", sizeof(uint8_T)
  },

  {
    "Receive/p2",
    "NULL", 1, 1, &sa_target_B.Receive_o2, "double", sizeof(real_T)
  },

  {
    "S-Function1",
    "NULL", 0, 4, &sa_target_B.SFunction1[0], "double", sizeof(real_T)
  },

  {
    "S-target conditions",
    "NULL", 0, 5, &sa_target_B.Stargetconditions[0], "double", sizeof(real_T)
  },

  {
    "Unpack2/p1",
    "NULL", 0, 1, &sa_target_B.Unpack2_o1, "double", sizeof(real_T)
  },

  {
    "Unpack2/p2",
    "NULL", 1, 1, &sa_target_B.Unpack2_o2, "double", sizeof(real_T)
  },

  {
    "Unpack2/p3",
    "NULL", 2, 1, &sa_target_B.Unpack2_o3, "double", sizeof(real_T)
  },

  {
    "Unpack2/p4",
    "NULL", 3, 1, &sa_target_B.Unpack2_o4, "double", sizeof(real_T)
  },

  {
    "Unpack2/p5",
    "NULL", 4, 1, &sa_target_B.Unpack2_o5, "double", sizeof(real_T)
  },

  {
    "Unpack2/p6",
    "NULL", 5, 1, &sa_target_B.Unpack2_o6, "double", sizeof(real_T)
  },

  {
    "Unpack2/p7",
    "NULL", 6, 1, &sa_target_B.Unpack2_o7, "double", sizeof(real_T)
  },

  {
    "Unpack2/p8",
    "NULL", 7, 1, &sa_target_B.Unpack2_o8, "double", sizeof(real_T)
  },

  {
    "Unpack2/p9",
    "NULL", 8, 1, &sa_target_B.Unpack2_o9, "double", sizeof(real_T)
  },

  {
    "Subsystem/cos_th2",
    "NULL", 0, 1, &sa_target_B.cos_th2, "double", sizeof(real_T)
  },

  {
    "Subsystem/sin_th2",
    "NULL", 0, 1, &sa_target_B.sin_th2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Xy",
    "NULL", 0, 1, &sa_target_B.Xy, "double", sizeof(real_T)
  },

  {
    "Subsystem/Xy2",
    "NULL", 0, 1, &sa_target_B.Xy2, "double", sizeof(real_T)
  },

  {
    "Subsystem/f",
    "NULL", 0, 1, &sa_target_B.f, "double", sizeof(real_T)
  },

  {
    "Subsystem/f1",
    "NULL", 0, 1, &sa_target_B.f1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Fxcos(th2)",
    "NULL", 0, 1, &sa_target_B.Fxcosth2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Fxsin(th2)",
    "NULL", 0, 1, &sa_target_B.Fxsinth2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Fycos(th2)",
    "NULL", 0, 1, &sa_target_B.Fycosth2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Fysin(th2)",
    "NULL", 0, 1, &sa_target_B.Fysinth2, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p1",
    "NULL", 0, 1, &sa_target_B.PCI6031E_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p2",
    "NULL", 1, 1, &sa_target_B.PCI6031E_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p3",
    "NULL", 2, 1, &sa_target_B.PCI6031E_o3, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p4",
    "NULL", 3, 1, &sa_target_B.PCI6031E_o4, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p5",
    "NULL", 4, 1, &sa_target_B.PCI6031E_o5, "double", sizeof(real_T)
  },

  {
    "Subsystem/PCI-6031E/p6",
    "NULL", 5, 1, &sa_target_B.PCI6031E_o6, "double", sizeof(real_T)
  },

  {
    "Subsystem/Sum",
    "NULL", 0, 1, &sa_target_B.Sum_j, "double", sizeof(real_T)
  },

  {
    "Subsystem/Sum1",
    "NULL", 0, 1, &sa_target_B.Sum1_p, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Pulse Generator1",
    "NULL", 0, 1, &sa_target_B.PulseGenerator1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Encoder Radians",
    "NULL", 0, 1, &sa_target_B.EncoderRadians, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Offset in Radians",
    "NULL", 0, 1, &sa_target_B.OffsetinRadians_a, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bit1/p1",
    "NULL", 0, 1, &sa_target_B.Bit1_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bit1/p2",
    "NULL", 1, 1, &sa_target_B.Bit1_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p1",
    "NULL", 0, 1, &sa_target_B.Bits181_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p2",
    "NULL", 1, 1, &sa_target_B.Bits181_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p3",
    "NULL", 2, 1, &sa_target_B.Bits181_o3, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p4",
    "NULL", 3, 1, &sa_target_B.Bits181_o4, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p5",
    "NULL", 4, 1, &sa_target_B.Bits181_o5, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p6",
    "NULL", 5, 1, &sa_target_B.Bits181_o6, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p7",
    "NULL", 6, 1, &sa_target_B.Bits181_o7, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits1-8 1/p8",
    "NULL", 7, 1, &sa_target_B.Bits181_o8, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p1",
    "NULL", 0, 1, &sa_target_B.Bits91_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p2",
    "NULL", 1, 1, &sa_target_B.Bits91_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p3",
    "NULL", 2, 1, &sa_target_B.Bits91_o3, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p4",
    "NULL", 3, 1, &sa_target_B.Bits91_o4, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p5",
    "NULL", 4, 1, &sa_target_B.Bits91_o5, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p6",
    "NULL", 5, 1, &sa_target_B.Bits91_o6, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p7",
    "NULL", 6, 1, &sa_target_B.Bits91_o7, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Bits9-1/p8",
    "NULL", 7, 1, &sa_target_B.Bits91_o8, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Encoder Decimal",
    "NULL", 0, 1, &sa_target_B.EncoderDecimal, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Sum",
    "NULL", 0, 1, &sa_target_B.Sum_g, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Unit Delay",
    "NULL", 0, 17, &sa_target_B.UnitDelay_l[0], "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/J00",
    "NULL", 0, 1, &sa_target_B.J00, "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/J01",
    "NULL", 0, 1, &sa_target_B.J01, "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/J10",
    "NULL", 0, 1, &sa_target_B.J10, "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/J11",
    "NULL", 0, 1, &sa_target_B.J11, "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/Sum",
    "NULL", 0, 1, &sa_target_B.Sum_h, "double", sizeof(real_T)
  },

  {
    "Subsystem/Jacobian-Cartesian Transformation/Sum1",
    "NULL", 0, 1, &sa_target_B.Sum1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Pulse Generator",
    "NULL", 0, 1, &sa_target_B.PulseGenerator, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Offset in Radians",
    "NULL", 0, 1, &sa_target_B.OffsetinRadians, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Product1",
    "NULL", 0, 1, &sa_target_B.Product1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bit17/p1",
    "NULL", 0, 1, &sa_target_B.Bit17_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bit17/p2",
    "NULL", 1, 1, &sa_target_B.Bit17_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p1",
    "NULL", 0, 1, &sa_target_B.Bits18_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p2",
    "NULL", 1, 1, &sa_target_B.Bits18_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p3",
    "NULL", 2, 1, &sa_target_B.Bits18_o3, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p4",
    "NULL", 3, 1, &sa_target_B.Bits18_o4, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p5",
    "NULL", 4, 1, &sa_target_B.Bits18_o5, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p6",
    "NULL", 5, 1, &sa_target_B.Bits18_o6, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p7",
    "NULL", 6, 1, &sa_target_B.Bits18_o7, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits1-8/p8",
    "NULL", 7, 1, &sa_target_B.Bits18_o8, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p1",
    "NULL", 0, 1, &sa_target_B.Bits916_o1, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p2",
    "NULL", 1, 1, &sa_target_B.Bits916_o2, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p3",
    "NULL", 2, 1, &sa_target_B.Bits916_o3, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p4",
    "NULL", 3, 1, &sa_target_B.Bits916_o4, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p5",
    "NULL", 4, 1, &sa_target_B.Bits916_o5, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p6",
    "NULL", 5, 1, &sa_target_B.Bits916_o6, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p7",
    "NULL", 6, 1, &sa_target_B.Bits916_o7, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Bits9-16/p8",
    "NULL", 7, 1, &sa_target_B.Bits916_o8, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Dot Product",
    "NULL", 0, 1, &sa_target_B.DotProduct, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Sum",
    "NULL", 0, 1, &sa_target_B.Sum, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Unit Delay",
    "NULL", 0, 17, &sa_target_B.UnitDelay[0], "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Parity/Math Function",
    "NULL", 0, 1, &sa_target_B.MathFunction, "double", sizeof(real_T)
  },

  {
    "Subsystem/Elbow Angle/Parity/Sum",
    "NULL", 0, 1, &sa_target_B.Sum_o, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Parity/Math Function",
    "NULL", 0, 1, &sa_target_B.MathFunction_p, "double", sizeof(real_T)
  },

  {
    "Subsystem/Shoulder Angle/Parity/Sum",
    "NULL", 0, 1, &sa_target_B.Sum_i, "double", sizeof(real_T)
  },
  {
    NULL, NULL, 0, 0, 0, NULL, 0
  }
};

#endif                                  /* BLOCK_IO_SIGNALS */
