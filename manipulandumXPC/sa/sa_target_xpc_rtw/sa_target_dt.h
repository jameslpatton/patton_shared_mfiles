/*
 * sa_target_dt.h
 *
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */

#include "ext_types.h"

/* data type size table */
static uint_T rtDataTypeSizes[] = {
  sizeof(real_T),
  sizeof(real32_T),
  sizeof(int8_T),
  sizeof(uint8_T),
  sizeof(int16_T),
  sizeof(uint16_T),
  sizeof(int32_T),
  sizeof(uint32_T),
  sizeof(boolean_T),
  sizeof(fcn_call_T),
  sizeof(int_T),
  sizeof(pointer_T),
  sizeof(action_T),
  2*sizeof(uint32_T)
};

/* data type name table */
static const char_T * rtDataTypeNames[] = {
  "real_T",
  "real32_T",
  "int8_T",
  "uint8_T",
  "int16_T",
  "uint16_T",
  "int32_T",
  "uint32_T",
  "boolean_T",
  "fcn_call_T",
  "int_T",
  "pointer_T",
  "action_T",
  "timer_uint32_pair_T"
};

/* data type transitions for block I/O structure */
static DataTypeTransition rtBTransitions[] = {
  {(char_T *)(&sa_target_B.OffsetinRadians), 0, 0, 130} ,
  {(char_T *)(&sa_target_B.ComplexCheck_o1), 0, 0, 42} ,
  {(char_T *)(&sa_target_B.Receive_o1[0]), 3, 0, 112} ,

  {(char_T *)(&sa_target_DWork.UnitDelay_DSTATE[0]), 0, 0, 711} ,
  {(char_T *)(&sa_target_DWork.Receive_PWORK), 11, 0, 2} ,
  {(char_T *)(&sa_target_DWork.clockTickCounter), 6, 0, 2} ,
  {(char_T *)(&sa_target_DWork.Receive_IWORK[0]), 10, 0, 89}
};

/* data type transition table for block I/O structure */
static DataTypeTransitionTable rtBTransTable = {
  7U,
  rtBTransitions
};

/* data type transitions for Parameters structure */
static DataTypeTransition rtPTransitions[] = {
  {(char_T *)(&sa_target_P.OffsetShoulderOFFSETSH), 0, 0, 483}
};

/* data type transition table for Parameters structure */
static DataTypeTransitionTable rtPTransTable = {
  1U,
  rtPTransitions
};

/* [EOF] sa_target_dt.h */
