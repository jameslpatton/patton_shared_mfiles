/*
 * sa_target_private.h
 *
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */
#ifndef _RTW_HEADER_sa_target_private_h_
#define _RTW_HEADER_sa_target_private_h_

#include "rtwtypes.h"

#include <windows.h>

/* Private macros used by the generated code to access rtModel */

#ifndef __RTWTYPES_H__
#error This file requires rtwtypes.h to be included
#else
#ifdef TMWTYPES_PREVIOUSLY_INCLUDED
#error This file requires rtwtypes.h to be included before tmwtypes.h
#else
/* Check for inclusion of an incorrect version of rtwtypes.h */
#ifndef RTWTYPES_ID_C08S16I32L32N32F1
#error This code was generated with a different "rtwtypes.h" than the file included
#endif                                  /* RTWTYPES_ID_C08S16I32L32N32F1 */
#endif                                  /* TMWTYPES_PREVIOUSLY_INCLUDED */
#endif                                  /* __RTWTYPES_H__ */

extern void xpcudpbytereceive(SimStruct *rts);
extern void intarget_trialtime1(SimStruct *rts);
extern void forces(SimStruct *rts);
extern void danipcie(SimStruct *rts);
extern void xpcudpbytesend(SimStruct *rts);
extern void dipci8255(SimStruct *rts);
extern void dopci8255(SimStruct *rts);
extern void adnipcie(SimStruct *rts);

#endif                                  /* _RTW_HEADER_sa_target_private_h_ */
