set MATLAB=C:\MATLAB701
set MSVCDir=c:\program files\microsoft visual studio\vc98
set MSDevDir=c:\program files\microsoft visual studio\common\msdev98
"C:\MATLAB701\rtw\bin\win32\envcheck" INCLUDE "c:\program files\microsoft visual studio\vc98\include"
if errorlevel 1 goto vcvars32
"C:\MATLAB701\rtw\bin\win32\envcheck" PATH "c:\program files\microsoft visual studio\vc98\bin"
if errorlevel 1 goto vcvars32
goto make
:vcvars32
set VSCommonDir=\common
call "C:\MATLAB701\toolbox\rtw\rtw\private\vcvars32_600.bat"
:make
nmake -f sa_target.mk ADD_MDL_NAME_TO_GLOBALS=1 EXT_MODE=1 VISUAL_VER=6.0
@if not errorlevel 0 echo The make command returned an error of %errorlevel%
