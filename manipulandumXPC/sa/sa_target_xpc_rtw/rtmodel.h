/*
 *  rtmodel.h:
 *
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */
#ifndef _RTW_HEADER_rtmodel_h_
#define _RTW_HEADER_rtmodel_h_

/*
 *  Includes the appropriate headers when we are using rtModel
 */
#include "sa_target.h"

#endif                                  /* _RTW_HEADER_rtmodel_h_ */
