/*
 * sa_target_types.h
 *
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */
#ifndef _RTW_HEADER_sa_target_types_h_
#define _RTW_HEADER_sa_target_types_h_

/* Parameters (auto storage) */
typedef struct _Parameters_sa_target Parameters_sa_target;

/* Forward declaration for rtModel */
typedef struct _rtModel_sa_target_Tag rtModel_sa_target;

#endif                                  /* _RTW_HEADER_sa_target_types_h_ */
