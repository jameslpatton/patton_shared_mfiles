/*
 * sa_target.c
 * 
 * Real-Time Workshop code generation for Simulink model "sa_target.mdl".
 *
 * Model Version              : 1.521
 * Real-Time Workshop version : 6.1  (R14SP1)  05-Sep-2004
 * C source code generated on : Fri Nov 10 12:01:46 2006
 */

#include "sa_target.h"
#include "sa_target_private.h"

#include <stdio.h>
#include "sa_target_dt.h"

#include "mdl_info.h"

#include "sa_target_bio.c"

#include "sa_target_pt.c"

/* user code (top of source file) */
static int (XPCCALLCONV * rl32eScopeExists)(int ScopeNo);
static int (XPCCALLCONV * rl32eDefScope)(int ScopeNo, int ScopeType);
static void (XPCCALLCONV * rl32eAddSignal)(int ScopeNo, int SignalNo);
static void (XPCCALLCONV * rl32eSetScope)(int ScopeNo, int action, double value);
static void (XPCCALLCONV *xpceFSScopeSet)(int ScopeNo, const char *filename,
 int mode, unsigned int writeSize);
static void (XPCCALLCONV * rl32eSetTargetScope)(int ScopeNo, int action, double
 value);
static void (XPCCALLCONV * rl32eRestartAcquisition)(int ScopeNo);
static void (XPCCALLCONV * xpceScopeAcqOK)(int ScopeNo, int *scopeAcqOK);

/* Block signals (auto storage) */
BlockIO_sa_target sa_target_B;

/* Block states (auto storage) */
D_Work_sa_target sa_target_DWork;

/* Real-time model */
rtModel_sa_target sa_target_M_;
rtModel_sa_target *sa_target_M = &sa_target_M_;

/* Model output function */
void sa_target_output(int_T tid)
{

  {
    int32_T i1;

    /* Product: '<S5>/Offset in Radians' incorporates:
     *  Constant: '<S5>/Bits//Radian (TRIG_SCALE)'
     *  Constant: '<S5>/Offset Shoulder (OFFSETSH) '
     */
    sa_target_B.OffsetinRadians = sa_target_P.OffsetShoulderOFFSETSH *
      sa_target_P.BitsRadianTRIG_SCALE_Va;

    /* UnitDelay: '<S5>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sa_target_B.UnitDelay[i1] = sa_target_DWork.UnitDelay_DSTATE[i1];
    }
  }

  /* S-Function Block (sfix_dot): <S2>/Shoulder Angle */
  sa_target_B.DotProduct = sa_target_P.Vector217_Value[0] *
    sa_target_B.UnitDelay[0];
  {
    int_T i1;

    const real_T *u0 = &sa_target_P.Vector217_Value[1];
    const real_T *u1 = &sa_target_B.UnitDelay[1];

    for (i1=0; i1 < 16; i1++) {
      sa_target_B.DotProduct += u0[i1] * u1[i1];
    }
  }

  {
    int32_T i1;

    /* Product: '<S5>/Product1' incorporates:
     *  Constant: '<S5>/Bits//Radian (TRIG_SCALE)'
     */
    sa_target_B.Product1 = sa_target_P.BitsRadianTRIG_SCALE_Va *
      sa_target_B.DotProduct;

    /* Sum: '<S5>/Sum' */
    sa_target_B.Sum = sa_target_B.Product1 - sa_target_B.OffsetinRadians;

    /* Fcn: '<S4>/J10' */
    sa_target_B.J10 = 462.50999999999999 * cos(sa_target_B.Sum) * 0.001;

    /* Product: '<S3>/Offset in Radians' incorporates:
     *  Constant: '<S3>/Bits//Radian (TRIG_SCALE)'
     *  Constant: '<S3>/Offset Elbow (OFFSETEL) '
     */
    sa_target_B.OffsetinRadians_a = sa_target_P.OffsetElbowOFFSETEL_Va *
      sa_target_P.BitsRadianTRIG_SCALE__b;

    /* UnitDelay: '<S3>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sa_target_B.UnitDelay_l[i1] = sa_target_DWork.UnitDelay_DSTATE_m[i1];
    }
  }

  /* S-Function Block (sfix_dot): <S2>/Elbow Angle */
  sa_target_B.EncoderDecimal = sa_target_P.Constant2_Value[0] *
    sa_target_B.UnitDelay_l[0];
  {
    int_T i1;

    const real_T *u0 = &sa_target_P.Constant2_Value[1];
    const real_T *u1 = &sa_target_B.UnitDelay_l[1];

    for (i1=0; i1 < 16; i1++) {
      sa_target_B.EncoderDecimal += u0[i1] * u1[i1];
    }
  }

  /* Product: '<S3>/Encoder Radians' incorporates:
   *  Constant: '<S3>/Bits//Radian (TRIG_SCALE)'
   */
  sa_target_B.EncoderRadians = sa_target_P.BitsRadianTRIG_SCALE__b *
    sa_target_B.EncoderDecimal;

  /* Sum: '<S3>/Sum' incorporates:
   *  Constant: '<S3>/(ALPHA)'
   *  Constant: '<S3>/Const'
   */
  sa_target_B.Sum_g = ((sa_target_B.OffsetinRadians_a + sa_target_P.Const_Value)
    - sa_target_B.EncoderRadians) - sa_target_P.ALPHA_Value;

  /* Fcn: '<S4>/J11' */
  sa_target_B.J11 = 335.20999999999998 * cos(sa_target_B.Sum_g) * 0.001;

  /* Sum: '<S4>/Sum' */
  sa_target_B.Sum_h = sa_target_B.J10 + sa_target_B.J11;

  /* Fcn: '<S4>/J00' */
  sa_target_B.J00 = -4.6251E+002 * sin(sa_target_B.Sum) * 0.001;

  /* Fcn: '<S4>/J01' */
  sa_target_B.J01 = -3.3521E+002 * sin(sa_target_B.Sum_g) * 0.001;

  /* Sum: '<S4>/Sum1' */
  sa_target_B.Sum1 = (0.0 - sa_target_B.J00) - sa_target_B.J01;

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[0];

    sfcnOutputs(rts, 1);
  }

  /* Unpack: <Root>/Unpack2 */
  memcpy(&sa_target_B.Unpack2_o1, sa_target_B.Receive_o1, 8);
  memcpy(&sa_target_B.Unpack2_o2, &sa_target_B.Receive_o1[8], 8);
  memcpy(&sa_target_B.Unpack2_o3, &sa_target_B.Receive_o1[16], 8);
  memcpy(&sa_target_B.Unpack2_o4, &sa_target_B.Receive_o1[24], 8);
  memcpy(&sa_target_B.Unpack2_o5, &sa_target_B.Receive_o1[32], 8);
  memcpy(&sa_target_B.Unpack2_o6, &sa_target_B.Receive_o1[40], 8);
  memcpy(&sa_target_B.Unpack2_o7, &sa_target_B.Receive_o1[48], 8);
  memcpy(&sa_target_B.Unpack2_o8, &sa_target_B.Receive_o1[56], 8);
  memcpy(&sa_target_B.Unpack2_o9, &sa_target_B.Receive_o1[64], 8);

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[1];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[2];

    sfcnOutputs(rts, 1);
  }

  /* Gain: '<Root>/E Gain' */
  sa_target_B.EGain = sa_target_B.SFunction1[0] * sa_target_P.EGain_Gain;

  /* Gain: '<Root>/S Gain' */
  sa_target_B.SGain = sa_target_B.SFunction1[1] * sa_target_P.SGain_Gain;

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[3];

    sfcnOutputs(rts, 1);
  }

  /* ok to acquire for <S1>/S-Function */
  sa_target_DWork.SFunction_IWORK.AcquireOK = 1;

  /* Pack: <Root>/Pack1 */
  memcpy(sa_target_B.Pack1, &sa_target_B.Stargetconditions[0], 8);
  memcpy(&sa_target_B.Pack1[8], &sa_target_B.Stargetconditions[1], 16);
  memcpy(&sa_target_B.Pack1[24], &sa_target_B.Stargetconditions[3], 8);
  memcpy(&sa_target_B.Pack1[32], &sa_target_B.Stargetconditions[4], 8);

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[4];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[5];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[6];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[7];

    sfcnOutputs(rts, 1);
  }

  /* DiscretePulseGenerator: '<S3>/Pulse Generator1' */
  sa_target_B.PulseGenerator1 = (sa_target_DWork.clockTickCounter <
    (int32_T)sa_target_P.PulseGenerator1_Duty) &&
   (sa_target_DWork.clockTickCounter >= 0) ? sa_target_P.PulseGenerator1_Amp :
    0;
  if(sa_target_DWork.clockTickCounter >=
   (int32_T)sa_target_P.PulseGenerator1_Period - 1) {
    sa_target_DWork.clockTickCounter = 0;
  } else {
    sa_target_DWork.clockTickCounter++;
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[8];

    sfcnOutputs(rts, 1);
  }

  /* Sum: '<S6>/Sum' */
  sa_target_B.Sum_o = ((((((((((((((((sa_target_B.Bits181_o1 +
    sa_target_B.Bits181_o2) + sa_target_B.Bits181_o3) + sa_target_B.Bits181_o4)
    + sa_target_B.Bits181_o5) + sa_target_B.Bits181_o6) +
    sa_target_B.Bits181_o7) + sa_target_B.Bits181_o8) + sa_target_B.Bits91_o1) +
    sa_target_B.Bits91_o2) + sa_target_B.Bits91_o3) + sa_target_B.Bits91_o4) +
    sa_target_B.Bits91_o5) + sa_target_B.Bits91_o6) + sa_target_B.Bits91_o7) +
    sa_target_B.Bits91_o8) + sa_target_B.Bit1_o1) + sa_target_B.Bit1_o2;

  /* Math Block: '<S6>/Math Function' */

  /* Operator : mod */
  if (sa_target_P.Constant_Value == 0.0) {
    sa_target_B.MathFunction = sa_target_B.Sum_o;
  } else if (sa_target_P.Constant_Value == floor(sa_target_P.Constant_Value)) {
    /* Integer denominator.  Use conventional formula.*/
    sa_target_B.MathFunction = (sa_target_B.Sum_o - sa_target_P.Constant_Value *
      floor((sa_target_B.Sum_o) / (sa_target_P.Constant_Value)));
  } else {
    /* Noninteger denominator. Check for nearly integer quotient.*/
    real_T uDivRound;
    real_T uDiv = sa_target_B.Sum_o / sa_target_P.Constant_Value;
    {
      real_T t;
      t = floor((fabs(uDiv) + 0.5));
      uDivRound = ((uDiv < 0.0) ? -t : t);
    }

    if (fabs((uDiv - uDivRound)) <= DBL_EPSILON * fabs(uDiv)) {
      sa_target_B.MathFunction = 0.0;
    } else {
      sa_target_B.MathFunction = (uDiv - floor(uDiv)) *
        sa_target_P.Constant_Value;
    }
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[9];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[10];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[11];

    sfcnOutputs(rts, 1);
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[12];

    sfcnOutputs(rts, 1);
  }

  /* DiscretePulseGenerator: '<S5>/Pulse Generator' */
  sa_target_B.PulseGenerator = (sa_target_DWork.clockTickCounter_m <
    (int32_T)sa_target_P.PulseGenerator_Duty) &&
   (sa_target_DWork.clockTickCounter_m >= 0) ? sa_target_P.PulseGenerator_Amp :
    0;
  if(sa_target_DWork.clockTickCounter_m >=
   (int32_T)sa_target_P.PulseGenerator_Period - 1) {
    sa_target_DWork.clockTickCounter_m = 0;
  } else {
    sa_target_DWork.clockTickCounter_m++;
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[13];

    sfcnOutputs(rts, 1);
  }

  /* Sum: '<S7>/Sum' */
  sa_target_B.Sum_i = ((((((((((((((((sa_target_B.Bits18_o1 +
    sa_target_B.Bits18_o2) + sa_target_B.Bits18_o3) + sa_target_B.Bits18_o4) +
    sa_target_B.Bits18_o5) + sa_target_B.Bits18_o6) + sa_target_B.Bits18_o7) +
    sa_target_B.Bits18_o8) + sa_target_B.Bits916_o1) + sa_target_B.Bits916_o2) +
    sa_target_B.Bits916_o3) + sa_target_B.Bits916_o4) + sa_target_B.Bits916_o5)
    + sa_target_B.Bits916_o6) + sa_target_B.Bits916_o7) +
    sa_target_B.Bits916_o8) + sa_target_B.Bit17_o1) + sa_target_B.Bit17_o2;

  /* Math Block: '<S7>/Math Function' */

  /* Operator : mod */
  if (sa_target_P.Constant_Value_a == 0.0) {
    sa_target_B.MathFunction_p = sa_target_B.Sum_i;
  } else if (sa_target_P.Constant_Value_a ==
   floor(sa_target_P.Constant_Value_a)) {
    /* Integer denominator.  Use conventional formula.*/
    sa_target_B.MathFunction_p = (sa_target_B.Sum_i -
      sa_target_P.Constant_Value_a * floor((sa_target_B.Sum_i) /
      (sa_target_P.Constant_Value_a)));
  } else {
    /* Noninteger denominator. Check for nearly integer quotient.*/
    real_T uDivRound;
    real_T uDiv = sa_target_B.Sum_i / sa_target_P.Constant_Value_a;
    {
      real_T t;
      t = floor((fabs(uDiv) + 0.5));
      uDivRound = ((uDiv < 0.0) ? -t : t);
    }

    if (fabs((uDiv - uDivRound)) <= DBL_EPSILON * fabs(uDiv)) {
      sa_target_B.MathFunction_p = 0.0;
    } else {
      sa_target_B.MathFunction_p = (uDiv - floor(uDiv)) *
        sa_target_P.Constant_Value_a;
    }
  }

  /* Fcn: '<S2>/cos_th2' */
  sa_target_B.cos_th2 = cos(sa_target_B.Sum_g);

  /* Gain: '<S2>/f' */
  sa_target_B.f = sa_target_B.PCI6031E_o1 * sa_target_P.f_Gain;

  /* Product: '<S2>/Fxcos(th2)' */
  sa_target_B.Fxcosth2 = sa_target_B.cos_th2 * sa_target_B.f;

  /* Fcn: '<S2>/sin_th2' */
  sa_target_B.sin_th2 = sin(sa_target_B.Sum_g);

  /* Product: '<S2>/Fxsin(th2)' */
  sa_target_B.Fxsinth2 = sa_target_B.f * sa_target_B.sin_th2;

  /* Gain: '<S2>/Xy' */
  sa_target_B.Xy = sa_target_B.PCI6031E_o2 * sa_target_P.Xy_Gain;

  /* Product: '<S2>/Fycos(th2)' */
  sa_target_B.Fycosth2 = sa_target_B.Xy * sa_target_B.cos_th2;

  /* Product: '<S2>/Fysin(th2)' */
  sa_target_B.Fysinth2 = sa_target_B.Xy * sa_target_B.sin_th2;

  /* Sum: '<S2>/Sum' */
  sa_target_B.Sum_j = sa_target_B.Fxsinth2 - sa_target_B.Fycosth2;

  /* Sum: '<S2>/Sum1' */
  sa_target_B.Sum1_p = (0.0 - sa_target_B.Fxcosth2) - sa_target_B.Fysinth2;

  /* Gain: '<S2>/Xy2' */
  sa_target_B.Xy2 = sa_target_B.PCI6031E_o5 * sa_target_P.Xy2_Gain;

  /* Gain: '<S2>/f1' */
  sa_target_B.f1 = sa_target_B.PCI6031E_o4 * sa_target_P.f1_Gain;

  /* Clock Block: '<Root>/Clock' */

  sa_target_B.Clock = sa_target_M->Timing.t[0];
}

/* Model update function */
void sa_target_update(int_T tid)
{

  /* Update for UnitDelay: '<S5>/Unit Delay' */
  sa_target_DWork.UnitDelay_DSTATE[0] = sa_target_B.Bits18_o1;
  sa_target_DWork.UnitDelay_DSTATE[1] = sa_target_B.Bits18_o2;
  sa_target_DWork.UnitDelay_DSTATE[2] = sa_target_B.Bits18_o3;
  sa_target_DWork.UnitDelay_DSTATE[3] = sa_target_B.Bits18_o4;
  sa_target_DWork.UnitDelay_DSTATE[4] = sa_target_B.Bits18_o5;
  sa_target_DWork.UnitDelay_DSTATE[5] = sa_target_B.Bits18_o6;
  sa_target_DWork.UnitDelay_DSTATE[6] = sa_target_B.Bits18_o7;
  sa_target_DWork.UnitDelay_DSTATE[7] = sa_target_B.Bits18_o8;
  sa_target_DWork.UnitDelay_DSTATE[8] = sa_target_B.Bits916_o1;
  sa_target_DWork.UnitDelay_DSTATE[9] = sa_target_B.Bits916_o2;
  sa_target_DWork.UnitDelay_DSTATE[10] = sa_target_B.Bits916_o3;
  sa_target_DWork.UnitDelay_DSTATE[11] = sa_target_B.Bits916_o4;
  sa_target_DWork.UnitDelay_DSTATE[12] = sa_target_B.Bits916_o5;
  sa_target_DWork.UnitDelay_DSTATE[13] = sa_target_B.Bits916_o6;
  sa_target_DWork.UnitDelay_DSTATE[14] = sa_target_B.Bits916_o7;
  sa_target_DWork.UnitDelay_DSTATE[15] = sa_target_B.Bits916_o8;
  sa_target_DWork.UnitDelay_DSTATE[16] = sa_target_B.Bit17_o1;

  /* Update for UnitDelay: '<S3>/Unit Delay' */
  sa_target_DWork.UnitDelay_DSTATE_m[0] = sa_target_B.Bits181_o1;
  sa_target_DWork.UnitDelay_DSTATE_m[1] = sa_target_B.Bits181_o2;
  sa_target_DWork.UnitDelay_DSTATE_m[2] = sa_target_B.Bits181_o3;
  sa_target_DWork.UnitDelay_DSTATE_m[3] = sa_target_B.Bits181_o4;
  sa_target_DWork.UnitDelay_DSTATE_m[4] = sa_target_B.Bits181_o5;
  sa_target_DWork.UnitDelay_DSTATE_m[5] = sa_target_B.Bits181_o6;
  sa_target_DWork.UnitDelay_DSTATE_m[6] = sa_target_B.Bits181_o7;
  sa_target_DWork.UnitDelay_DSTATE_m[7] = sa_target_B.Bits181_o8;
  sa_target_DWork.UnitDelay_DSTATE_m[8] = sa_target_B.Bits91_o1;
  sa_target_DWork.UnitDelay_DSTATE_m[9] = sa_target_B.Bits91_o2;
  sa_target_DWork.UnitDelay_DSTATE_m[10] = sa_target_B.Bits91_o3;
  sa_target_DWork.UnitDelay_DSTATE_m[11] = sa_target_B.Bits91_o4;
  sa_target_DWork.UnitDelay_DSTATE_m[12] = sa_target_B.Bits91_o5;
  sa_target_DWork.UnitDelay_DSTATE_m[13] = sa_target_B.Bits91_o6;
  sa_target_DWork.UnitDelay_DSTATE_m[14] = sa_target_B.Bits91_o7;
  sa_target_DWork.UnitDelay_DSTATE_m[15] = sa_target_B.Bits91_o8;
  sa_target_DWork.UnitDelay_DSTATE_m[16] = sa_target_B.Bit1_o1;

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[1];

    sfcnUpdate(rts, 1);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[2];

    sfcnUpdate(rts, 1);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Update absolute time for base rate */

  if(!(++sa_target_M->Timing.clockTick0)) ++sa_target_M->Timing.clockTickH0;
  sa_target_M->Timing.t[0] = sa_target_M->Timing.clockTick0 *
    sa_target_M->Timing.stepSize0 + sa_target_M->Timing.clockTickH0 *
    sa_target_M->Timing.stepSize0 * 4294967296.0;

  {
    /* Update absolute timer for sample time: [0.01s, 0.0s] */

    if(!(++sa_target_M->Timing.clockTick1)) ++sa_target_M->Timing.clockTickH1;
    sa_target_M->Timing.t[1] = sa_target_M->Timing.clockTick1 *
      sa_target_M->Timing.stepSize1 + sa_target_M->Timing.clockTickH1 *
      sa_target_M->Timing.stepSize1 * 4294967296.0;
  }
}

/* Model initialize function */
void sa_target_initialize(boolean_T firstTime)
{

  if (firstTime) {
    /* registration code */
    /* initialize real-time model */
    (void)memset((char_T *)sa_target_M, 0, sizeof(rtModel_sa_target));

    {
      /* Setup solver object */

      rtsiSetSimTimeStepPtr(&sa_target_M->solverInfo,
       &sa_target_M->Timing.simTimeStep);
      rtsiSetTPtr(&sa_target_M->solverInfo, &rtmGetTPtr(sa_target_M));
      rtsiSetStepSizePtr(&sa_target_M->solverInfo,
       &sa_target_M->Timing.stepSize0);
      rtsiSetErrorStatusPtr(&sa_target_M->solverInfo,
       &rtmGetErrorStatus(sa_target_M));

      rtsiSetRTModelPtr(&sa_target_M->solverInfo, sa_target_M);
    }
    rtsiSetSimTimeStep(&sa_target_M->solverInfo, MAJOR_TIME_STEP);

    /* Initialize timing info */
    {
      int_T *mdlTsMap = sa_target_M->Timing.sampleTimeTaskIDArray;
      mdlTsMap[0] = 0;
      mdlTsMap[1] = 1;
      sa_target_M->Timing.sampleTimeTaskIDPtr = (&mdlTsMap[0]);
      sa_target_M->Timing.sampleTimes =
        (&sa_target_M->Timing.sampleTimesArray[0]);
      sa_target_M->Timing.offsetTimes =
        (&sa_target_M->Timing.offsetTimesArray[0]);
      /* task periods */
      sa_target_M->Timing.sampleTimes[0] = (0.0);
      sa_target_M->Timing.sampleTimes[1] = (0.01);

      /* task offsets */
      sa_target_M->Timing.offsetTimes[0] = (0.0);
      sa_target_M->Timing.offsetTimes[1] = (0.0);
    }

    rtmSetTPtr(sa_target_M, &sa_target_M->Timing.tArray[0]);

    {
      int_T *mdlSampleHits = sa_target_M->Timing.sampleHitArray;
      mdlSampleHits[0] = 1;
      mdlSampleHits[1] = 1;
      sa_target_M->Timing.sampleHits = (&mdlSampleHits[0]);
    }

    rtmSetTFinal(sa_target_M, 3.6E+005);
    sa_target_M->Timing.stepSize0 = 0.01;
    sa_target_M->Timing.stepSize1 = 0.01;

    /* Setup for data logging */
    {
      static RTWLogInfo rt_DataLoggingInfo;

      sa_target_M->rtwLogInfo = &rt_DataLoggingInfo;

      rtliSetLogFormat(sa_target_M->rtwLogInfo, 0);

      rtliSetLogMaxRows(sa_target_M->rtwLogInfo, 0);

      rtliSetLogDecimation(sa_target_M->rtwLogInfo, 1);

      rtliSetLogVarNameModifier(sa_target_M->rtwLogInfo, "rt_");

      rtliSetLogT(sa_target_M->rtwLogInfo, "tout");

      rtliSetLogX(sa_target_M->rtwLogInfo, "");

      rtliSetLogXFinal(sa_target_M->rtwLogInfo, "");

      rtliSetSigLog(sa_target_M->rtwLogInfo, "");

      rtliSetLogXSignalInfo(sa_target_M->rtwLogInfo, NULL);

      rtliSetLogXSignalPtrs(sa_target_M->rtwLogInfo, NULL);

      rtliSetLogY(sa_target_M->rtwLogInfo, "");

      rtliSetLogYSignalInfo(sa_target_M->rtwLogInfo, NULL);

      rtliSetLogYSignalPtrs(sa_target_M->rtwLogInfo, NULL);
    }

    /* external mode info */
    sa_target_M->Sizes.checksums[0] = (2867772741U);
    sa_target_M->Sizes.checksums[1] = (3943695809U);
    sa_target_M->Sizes.checksums[2] = (1207228451U);
    sa_target_M->Sizes.checksums[3] = (1677319470U);

    {
      static const int8_T rtAlwaysEnabled = EXTMODE_SUBSYS_ALWAYS_ENABLED;

      static RTWExtModeInfo rt_ExtModeInfo;
      static const void *sysActives[8];

      sa_target_M->extModeInfo = (&rt_ExtModeInfo);
      rteiSetSubSystemActiveVectorAddresses(&rt_ExtModeInfo, sysActives);

      sysActives[0] = &rtAlwaysEnabled;
      sysActives[1] = &rtAlwaysEnabled;
      sysActives[2] = &rtAlwaysEnabled;
      sysActives[3] = &rtAlwaysEnabled;
      sysActives[4] = &rtAlwaysEnabled;
      sysActives[5] = &rtAlwaysEnabled;
      sysActives[6] = &rtAlwaysEnabled;
      sysActives[7] = &rtAlwaysEnabled;

      rteiSetModelMappingInfoPtr(&rt_ExtModeInfo,
       &sa_target_M->SpecialInfo.mappingInfo);

      rteiSetChecksumsPtr(&rt_ExtModeInfo, sa_target_M->Sizes.checksums);

      rteiSetTPtr(&rt_ExtModeInfo, rtmGetTPtr(sa_target_M));
    }

    sa_target_M->solverInfoPtr = (&sa_target_M->solverInfo);
    sa_target_M->Timing.stepSize = (0.01);
    rtsiSetFixedStepSize(&sa_target_M->solverInfo, 0.01);
    rtsiSetSolverMode(&sa_target_M->solverInfo, SOLVER_MODE_SINGLETASKING);

    {
      /* block I/O */
      void *b = (void *) &sa_target_B;
      sa_target_M->ModelData.blockIO = (b);

      (void)memset(b, 0, sizeof(BlockIO_sa_target));

      {

        int_T i;
        b = &sa_target_B.OffsetinRadians;
        for (i = 0; i < 130; i++) {
          ((real_T*)b)[i] = 0.0;
        }

        b =&sa_target_B.ComplexCheck_o1;
        for (i = 0; i < 42; i++) {
          ((real_T*)b)[i] = 0.0;
        }
      }
    }
    /* parameters */
    sa_target_M->ModelData.defaultParam = ((real_T *) &sa_target_P);

    /* data type work */
    sa_target_M->Work.dwork = ((void *) &sa_target_DWork);
    (void)memset((char_T *) &sa_target_DWork, 0, sizeof(D_Work_sa_target));
    {
      int_T i;
      real_T *dwork_ptr = (real_T *) &sa_target_DWork.UnitDelay_DSTATE[0];

      for (i = 0; i < 711; i++) {
        dwork_ptr[i] = 0.0;
      }
    }

    /* data type transition information */
    {
      static DataTypeTransInfo dtInfo;

      (void)memset((char_T *) &dtInfo, 0, sizeof(dtInfo));
      sa_target_M->SpecialInfo.mappingInfo = (&dtInfo);

      sa_target_M->SpecialInfo.xpcData = ((void*) &dtInfo);
      dtInfo.numDataTypes = 14;
      dtInfo.dataTypeSizes = &rtDataTypeSizes[0];
      dtInfo.dataTypeNames = &rtDataTypeNames[0];

      /* Block I/O transition table */
      dtInfo.B = &rtBTransTable;

      /* Parameters transition table */
      dtInfo.P = &rtPTransTable;
    }

    /* C API for Parameter Tuning and/or Signal Monitoring */
    {
      static ModelMappingInfo mapInfo;

      memset((char_T *) &mapInfo, 0, sizeof(mapInfo));

      /* block signal monitoring map */
      mapInfo.Signals.blockIOSignals = &rtBIOSignals[0];
      mapInfo.Signals.numBlockIOSignals = 93;

      /* parameter tuning maps */
      mapInfo.Parameters.blockTuning = &rtBlockTuning[0];
      mapInfo.Parameters.variableTuning = &rtVariableTuning[0];
      mapInfo.Parameters.parametersMap = rtParametersMap;
      mapInfo.Parameters.dimensionsMap = rtDimensionsMap;
      mapInfo.Parameters.numBlockTuning = 112;
      mapInfo.Parameters.numVariableTuning = 0;

      sa_target_M->SpecialInfo.mappingInfo = (&mapInfo);
    }

    /* initialize non-finites */
    rt_InitInfAndNaN(sizeof(real_T));

    /* child S-Function registration */
    {
      RTWSfcnInfo *sfcnInfo = &sa_target_M->NonInlinedSFcns.sfcnInfo;

      sa_target_M->sfcnInfo = (sfcnInfo);

      rtssSetErrorStatusPtr(sfcnInfo, &rtmGetErrorStatus(sa_target_M));
      rtssSetNumRootSampTimesPtr(sfcnInfo, &sa_target_M->Sizes.numSampTimes);
      rtssSetTPtrPtr(sfcnInfo, &rtmGetTPtr(sa_target_M));
      rtssSetTStartPtr(sfcnInfo, &rtmGetTStart(sa_target_M));
      rtssSetTimeOfLastOutputPtr(sfcnInfo, &rtmGetTimeOfLastOutput(sa_target_M));
      rtssSetStepSizePtr(sfcnInfo, &sa_target_M->Timing.stepSize);
      rtssSetStopRequestedPtr(sfcnInfo, &rtmGetStopRequested(sa_target_M));
      rtssSetDerivCacheNeedsResetPtr(sfcnInfo,
       &sa_target_M->ModelData.derivCacheNeedsReset);
      rtssSetZCCacheNeedsResetPtr(sfcnInfo,
       &sa_target_M->ModelData.zCCacheNeedsReset);
      rtssSetBlkStateChangePtr(sfcnInfo, &sa_target_M->ModelData.blkStateChange);
      rtssSetSampleHitsPtr(sfcnInfo, &sa_target_M->Timing.sampleHits);
      rtssSetPerTaskSampleHitsPtr(sfcnInfo,
       &sa_target_M->Timing.perTaskSampleHits);
      rtssSetSimModePtr(sfcnInfo, &sa_target_M->simMode);
      rtssSetSolverInfoPtr(sfcnInfo, &sa_target_M->solverInfoPtr);
    }

    sa_target_M->Sizes.numSFcns = (14);

    /* register each child */
    {
      (void)memset((void *)&sa_target_M->NonInlinedSFcns.childSFunctions[0], 0,
       14*sizeof(SimStruct));
      sa_target_M->childSfunctions =
        (&sa_target_M->NonInlinedSFcns.childSFunctionPtrs[0]);
      {
        int_T i;

        for(i = 0; i < 14; i++) {
          sa_target_M->childSfunctions[i] =
            (&sa_target_M->NonInlinedSFcns.childSFunctions[i]);
        }
      }

      /* Level2 S-Function Block: sa_target/<Root>/Receive (xpcudpbytereceive) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[0];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn0.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn0.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn0.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[0]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[0]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn0.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 72);
            ssSetOutputPortSignal(rts, 0, ((uint8_T *) sa_target_B.Receive_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Receive_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Receive");
        ssSetPath(rts, "sa_target/Receive");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn0.params;

          ssSetSFcnParamsCount(rts, 4);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Receive_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Receive_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Receive_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Receive_P4_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Receive_IWORK[0]);
        ssSetPWork(rts, (void **) &sa_target_DWork.Receive_PWORK);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn0.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Receive_IWORK[0]);

          /* PWORK */
          ssSetDWorkWidth(rts, 1, 1);
          ssSetDWorkDataType(rts, 1,SS_POINTER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sa_target_DWork.Receive_PWORK);
        }

        /* registration */
        xpcudpbytereceive(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<Root>/S-target conditions (intarget_trialtime1) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[1];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn1.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn1.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn1.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[1]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[1]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn1.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn1.UPtrs0;
            sfcnUPtrs[0] = &sa_target_B.Sum_h;
            sfcnUPtrs[1] = &sa_target_B.Sum1;
            sfcnUPtrs[2] = &sa_target_B.Unpack2_o5;
            sfcnUPtrs[3] = &sa_target_B.Unpack2_o6;
            sfcnUPtrs[4] = &sa_target_B.Unpack2_o7;
            sfcnUPtrs[5] = &sa_target_B.Unpack2_o3;
            sfcnUPtrs[6] = &sa_target_B.Unpack2_o4;
            sfcnUPtrs[7] = &sa_target_B.Unpack2_o8;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 8);
          }
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn1.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 1);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 5);
            ssSetOutputPortSignal(rts, 0, ((real_T *)
              sa_target_B.Stargetconditions));
          }
        }

        /* states */
        ssSetDiscStates(rts, (real_T *)
         &sa_target_DWork.Stargetconditions_DSTATE[0]);
        /* path info */
        ssSetModelName(rts, "S-target conditions");
        ssSetPath(rts, "sa_target/S-target conditions");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn1.params;

          ssSetSFcnParamsCount(rts, 3);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0,
           (mxArray*)&sa_target_P.Stargetconditions_P1_Size[0]);
          ssSetSFcnParam(rts, 1,
           (mxArray*)&sa_target_P.Stargetconditions_P2_Size[0]);
          ssSetSFcnParam(rts, 2,
           (mxArray*)&sa_target_P.Stargetconditions_P3_Size[0]);
        }

        /* work vectors */
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn1.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* DSTATE */
          ssSetDWorkWidth(rts, 0, 3);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWorkUsedAsDState(rts, 0, 1);
          ssSetDWork(rts, 0, &sa_target_DWork.Stargetconditions_DSTATE[0]);
        }

        /* registration */
        intarget_trialtime1(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sa_target/<Root>/S-Function1 (forces) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[2];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn2.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn2.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn2.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[2]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[2]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn2.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn2.UPtrs0;
            sfcnUPtrs[0] = &sa_target_B.Sum_h;
            sfcnUPtrs[1] = &sa_target_B.Sum1;
            sfcnUPtrs[2] = &sa_target_B.Unpack2_o5;
            sfcnUPtrs[3] = &sa_target_B.Unpack2_o6;
            sfcnUPtrs[4] = &sa_target_B.J00;
            sfcnUPtrs[5] = &sa_target_B.J10;
            sfcnUPtrs[6] = &sa_target_B.J01;
            sfcnUPtrs[7] = &sa_target_B.J11;
            sfcnUPtrs[8] = &sa_target_B.Unpack2_o7;
            sfcnUPtrs[9] = &sa_target_B.Unpack2_o3;
            sfcnUPtrs[10] = &sa_target_B.Unpack2_o4;
            sfcnUPtrs[11] = &sa_target_B.Stargetconditions[0];
            sfcnUPtrs[12] = &sa_target_B.Unpack2_o8;
            sfcnUPtrs[13] = &sa_target_B.Unpack2_o2;
            sfcnUPtrs[14] = &sa_target_B.Sum;
            sfcnUPtrs[15] = &sa_target_B.Sum_g;
            sfcnUPtrs[16] = &sa_target_B.Unpack2_o9;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 17);
          }
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn2.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 1);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 4);
            ssSetOutputPortSignal(rts, 0, ((real_T *) sa_target_B.SFunction1));
          }
        }

        /* states */
        ssSetDiscStates(rts, (real_T *) &sa_target_DWork.SFunction1_DSTATE[0]);
        /* path info */
        ssSetModelName(rts, "S-Function1");
        ssSetPath(rts, "sa_target/S-Function1");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn2.params;

          ssSetSFcnParamsCount(rts, 2);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.SFunction1_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.SFunction1_P2_Size[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sa_target_DWork.SFunction1_RWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn2.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 600);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.SFunction1_RWORK[0]);

          /* DSTATE */
          ssSetDWorkWidth(rts, 1, 4);
          ssSetDWorkDataType(rts, 1,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWorkUsedAsDState(rts, 1, 1);
          ssSetDWork(rts, 1, &sa_target_DWork.SFunction1_DSTATE[0]);
        }

        /* registration */
        forces(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sa_target/<Root>/PCI-6031E  (danipcie) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[3];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn3.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn3.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn3.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[3]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[3]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn3.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn3.UPtrs0;
            sfcnUPtrs[0] = &sa_target_B.EGain;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn3.UPtrs1;
            sfcnUPtrs[0] = &sa_target_B.SGain;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "PCI-6031E ");
        ssSetPath(rts, "sa_target/PCI-6031E ");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn3.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.PCI6031E_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.PCI6031E_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.PCI6031E_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.PCI6031E_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.PCI6031E_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.PCI6031E_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.PCI6031E_P7_Size[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sa_target_DWork.PCI6031E_RWORK[0]);
        ssSetIWork(rts, (int_T *) &sa_target_DWork.PCI6031E_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn3.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 6);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.PCI6031E_RWORK[0]);

          /* IWORK */
          ssSetDWorkWidth(rts, 1, 2);
          ssSetDWorkDataType(rts, 1,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sa_target_DWork.PCI6031E_IWORK[0]);
        }

        /* registration */
        danipcie(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }

      /* Level2 S-Function Block: sa_target/<Root>/Send (xpcudpbytesend) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[4];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn4.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn4.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn4.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[4]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[4]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 1);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn4.inputPortInfo[0]);

          /* port 0 */
          {
            ssSetInputPortRequiredContiguous(rts, 0, 1);
            ssSetInputPortSignal(rts, 0, sa_target_B.Pack1);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 40);
          }
        }

        /* path info */
        ssSetModelName(rts, "Send");
        ssSetPath(rts, "sa_target/Send");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn4.params;

          ssSetSFcnParamsCount(rts, 4);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Send_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Send_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Send_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Send_P4_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Send_IWORK[0]);
        ssSetPWork(rts, (void **) &sa_target_DWork.Send_PWORK);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn4.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Send_IWORK[0]);

          /* PWORK */
          ssSetDWorkWidth(rts, 1, 1);
          ssSetDWorkDataType(rts, 1,SS_POINTER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sa_target_DWork.Send_PWORK);
        }

        /* registration */
        xpcudpbytesend(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetInputPortWidth(rts, 0, 40);
        ssSetInputPortDataType(rts, 0, SS_UINT8);
        ssSetInputPortComplexSignal(rts, 0, 0);
        ssSetInputPortFrameData(rts, 0, 0);

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
      }

      /* Level2 S-Function Block: sa_target/<S3>/Bit1 (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[5];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn5.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn5.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn5.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[5]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[5]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn5.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bit1_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bit1_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bit1");
        ssSetPath(rts, "sa_target/Subsystem/Elbow Angle/Bit1");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn5.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bit1_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bit1_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bit1_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bit1_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bit1_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bit1_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bit1_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bit1_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn5.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bit1_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S3>/Bits1-8 1 (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[6];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn6.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn6.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn6.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[6]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[6]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn6.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bits181_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bits181_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sa_target_B.Bits181_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sa_target_B.Bits181_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sa_target_B.Bits181_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sa_target_B.Bits181_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sa_target_B.Bits181_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sa_target_B.Bits181_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits1-8\n1");
        ssSetPath(rts, "sa_target/Subsystem/Elbow Angle/Bits1-8 1");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn6.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bits181_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bits181_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bits181_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bits181_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bits181_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bits181_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bits181_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bits181_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn6.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bits181_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S3>/Bits9-1 (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[7];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn7.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn7.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn7.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[7]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[7]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn7.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bits91_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bits91_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sa_target_B.Bits91_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sa_target_B.Bits91_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sa_target_B.Bits91_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sa_target_B.Bits91_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sa_target_B.Bits91_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sa_target_B.Bits91_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits9-1");
        ssSetPath(rts, "sa_target/Subsystem/Elbow Angle/Bits9-1");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn7.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bits91_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bits91_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bits91_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bits91_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bits91_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bits91_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bits91_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bits91_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn7.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bits91_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S3>/PCI-DIO-96 1 (dopci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[8];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn8.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn8.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn8.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[8]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[8]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn8.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn8.UPtrs0;
            sfcnUPtrs[0] = &sa_target_B.PulseGenerator1;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn8.UPtrs1;
            sfcnUPtrs[0] = &sa_target_P.Constant3_Value;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "PCI-DIO-96 1");
        ssSetPath(rts, "sa_target/Subsystem/Elbow Angle/PCI-DIO-96 1");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn8.params;

          ssSetSFcnParamsCount(rts, 9);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.PCIDIO961_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.PCIDIO961_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.PCIDIO961_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.PCIDIO961_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.PCIDIO961_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.PCIDIO961_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.PCIDIO961_P7_Size[0]);
          ssSetSFcnParam(rts, 7, (mxArray*)&sa_target_P.PCIDIO961_P8_Size[0]);
          ssSetSFcnParam(rts, 8, (mxArray*)&sa_target_P.PCIDIO961_P9_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.PCIDIO961_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn8.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.PCIDIO961_IWORK[0]);
        }

        /* registration */
        dopci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }

      /* Level2 S-Function Block: sa_target/<S2>/PCI-6031E  (adnipcie) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[9];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn9.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn9.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn9.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[9]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[9]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn9.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 6);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.PCI6031E_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.PCI6031E_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sa_target_B.PCI6031E_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sa_target_B.PCI6031E_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sa_target_B.PCI6031E_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sa_target_B.PCI6031E_o6));
          }
        }
        /* path info */
        ssSetModelName(rts, "PCI-6031E ");
        ssSetPath(rts, "sa_target/Subsystem/PCI-6031E ");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn9.params;

          ssSetSFcnParamsCount(rts, 6);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.PCI6031E_P1_Size_m[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.PCI6031E_P2_Size_b[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.PCI6031E_P3_Size_d[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.PCI6031E_P4_Size_c[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.PCI6031E_P5_Size_b[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.PCI6031E_P6_Size_b[0]);
        }

        /* work vectors */
        ssSetRWork(rts, (real_T *) &sa_target_DWork.PCI6031E_RWORK_m[0]);
        ssSetIWork(rts, (int_T *) &sa_target_DWork.PCI6031E_IWORK_n[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn9.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 2);

          /* RWORK */
          ssSetDWorkWidth(rts, 0, 64);
          ssSetDWorkDataType(rts, 0,SS_DOUBLE);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.PCI6031E_RWORK_m[0]);

          /* IWORK */
          ssSetDWorkWidth(rts, 1, 66);
          ssSetDWorkDataType(rts, 1,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 1, 0);
          ssSetDWork(rts, 1, &sa_target_DWork.PCI6031E_IWORK_n[0]);
        }

        /* registration */
        adnipcie(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 0);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S5>/Bit17 (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[10];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn10.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn10.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn10.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[10]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[10]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn10.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 2);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bit17_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bit17_o2));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bit17");
        ssSetPath(rts, "sa_target/Subsystem/Shoulder Angle/Bit17");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn10.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bit17_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bit17_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bit17_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bit17_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bit17_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bit17_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bit17_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bit17_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn10.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bit17_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S5>/Bits1-8  (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[11];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn11.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn11.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn11.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[11]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[11]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn11.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bits18_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bits18_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sa_target_B.Bits18_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sa_target_B.Bits18_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sa_target_B.Bits18_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sa_target_B.Bits18_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sa_target_B.Bits18_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sa_target_B.Bits18_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits1-8\n");
        ssSetPath(rts, "sa_target/Subsystem/Shoulder Angle/Bits1-8 ");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn11.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bits18_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bits18_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bits18_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bits18_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bits18_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bits18_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bits18_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bits18_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn11.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bits18_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S5>/Bits9-16 (dipci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[12];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn12.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn12.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn12.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[12]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[12]);
        }

        /* outputs */
        {
          ssSetPortInfoForOutputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn12.outputPortInfo[0]);
          _ssSetNumOutputPorts(rts, 8);
          /* port 0 */
          {
            _ssSetOutputPortNumDimensions(rts, 0, 1);
            ssSetOutputPortWidth(rts, 0, 1);
            ssSetOutputPortSignal(rts, 0, ((real_T *) &sa_target_B.Bits916_o1));
          }
          /* port 1 */
          {
            _ssSetOutputPortNumDimensions(rts, 1, 1);
            ssSetOutputPortWidth(rts, 1, 1);
            ssSetOutputPortSignal(rts, 1, ((real_T *) &sa_target_B.Bits916_o2));
          }
          /* port 2 */
          {
            _ssSetOutputPortNumDimensions(rts, 2, 1);
            ssSetOutputPortWidth(rts, 2, 1);
            ssSetOutputPortSignal(rts, 2, ((real_T *) &sa_target_B.Bits916_o3));
          }
          /* port 3 */
          {
            _ssSetOutputPortNumDimensions(rts, 3, 1);
            ssSetOutputPortWidth(rts, 3, 1);
            ssSetOutputPortSignal(rts, 3, ((real_T *) &sa_target_B.Bits916_o4));
          }
          /* port 4 */
          {
            _ssSetOutputPortNumDimensions(rts, 4, 1);
            ssSetOutputPortWidth(rts, 4, 1);
            ssSetOutputPortSignal(rts, 4, ((real_T *) &sa_target_B.Bits916_o5));
          }
          /* port 5 */
          {
            _ssSetOutputPortNumDimensions(rts, 5, 1);
            ssSetOutputPortWidth(rts, 5, 1);
            ssSetOutputPortSignal(rts, 5, ((real_T *) &sa_target_B.Bits916_o6));
          }
          /* port 6 */
          {
            _ssSetOutputPortNumDimensions(rts, 6, 1);
            ssSetOutputPortWidth(rts, 6, 1);
            ssSetOutputPortSignal(rts, 6, ((real_T *) &sa_target_B.Bits916_o7));
          }
          /* port 7 */
          {
            _ssSetOutputPortNumDimensions(rts, 7, 1);
            ssSetOutputPortWidth(rts, 7, 1);
            ssSetOutputPortSignal(rts, 7, ((real_T *) &sa_target_B.Bits916_o8));
          }
        }
        /* path info */
        ssSetModelName(rts, "Bits9-16");
        ssSetPath(rts, "sa_target/Subsystem/Shoulder Angle/Bits9-16");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn12.params;

          ssSetSFcnParamsCount(rts, 7);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.Bits916_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.Bits916_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.Bits916_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.Bits916_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.Bits916_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.Bits916_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.Bits916_P7_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.Bits916_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn12.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.Bits916_IWORK[0]);
        }

        /* registration */
        dipci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetOutputPortConnected(rts, 0, 1);
        _ssSetOutputPortConnected(rts, 1, 1);
        _ssSetOutputPortConnected(rts, 2, 1);
        _ssSetOutputPortConnected(rts, 3, 1);
        _ssSetOutputPortConnected(rts, 4, 1);
        _ssSetOutputPortConnected(rts, 5, 1);
        _ssSetOutputPortConnected(rts, 6, 1);
        _ssSetOutputPortConnected(rts, 7, 1);
        _ssSetOutputPortBeingMerged(rts, 0, 0);
        _ssSetOutputPortBeingMerged(rts, 1, 0);
        _ssSetOutputPortBeingMerged(rts, 2, 0);
        _ssSetOutputPortBeingMerged(rts, 3, 0);
        _ssSetOutputPortBeingMerged(rts, 4, 0);
        _ssSetOutputPortBeingMerged(rts, 5, 0);
        _ssSetOutputPortBeingMerged(rts, 6, 0);
        _ssSetOutputPortBeingMerged(rts, 7, 0);
        /* Update the BufferDstPort flags for each input port */
      }

      /* Level2 S-Function Block: sa_target/<S5>/Int & OE (dopci8255) */
      {
        SimStruct *rts = sa_target_M->childSfunctions[13];
        /* timing info */
        time_T *sfcnPeriod = sa_target_M->NonInlinedSFcns.Sfcn13.sfcnPeriod;
        time_T *sfcnOffset = sa_target_M->NonInlinedSFcns.Sfcn13.sfcnOffset;
        int_T *sfcnTsMap = sa_target_M->NonInlinedSFcns.Sfcn13.sfcnTsMap;

        (void)memset((void*)sfcnPeriod, 0, sizeof(time_T)*1);
        (void)memset((void*)sfcnOffset, 0, sizeof(time_T)*1);
        ssSetSampleTimePtr(rts, &sfcnPeriod[0]);
        ssSetOffsetTimePtr(rts, &sfcnOffset[0]);
        ssSetSampleTimeTaskIDPtr(rts, sfcnTsMap);

        /* Set up the mdlInfo pointer */
        {
          ssSetBlkInfo2Ptr(rts, &sa_target_M->NonInlinedSFcns.blkInfo2[13]);
          ssSetRTWSfcnInfo(rts, sa_target_M->sfcnInfo);
        }

        /* Allocate memory of model methods 2 */
        {
          ssSetModelMethods2(rts, &sa_target_M->NonInlinedSFcns.methods2[13]);
        }

        /* inputs */
        {
          _ssSetNumInputPorts(rts, 2);
          ssSetPortInfoForInputs(rts,
           &sa_target_M->NonInlinedSFcns.Sfcn13.inputPortInfo[0]);

          /* port 0 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn13.UPtrs0;
            sfcnUPtrs[0] = &sa_target_B.PulseGenerator;
            ssSetInputPortSignalPtrs(rts, 0, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 0, 1);
            ssSetInputPortWidth(rts, 0, 1);
          }

          /* port 1 */
          {
            real_T const **sfcnUPtrs = (real_T const **)
              &sa_target_M->NonInlinedSFcns.Sfcn13.UPtrs1;
            sfcnUPtrs[0] = &sa_target_P.Constant1_Value;
            ssSetInputPortSignalPtrs(rts, 1, (InputPtrsType)&sfcnUPtrs[0]);
            _ssSetInputPortNumDimensions(rts, 1, 1);
            ssSetInputPortWidth(rts, 1, 1);
          }
        }

        /* path info */
        ssSetModelName(rts, "Int & OE");
        ssSetPath(rts, "sa_target/Subsystem/Shoulder Angle/Int & OE");
        ssSetRTModel(rts,sa_target_M);
        ssSetParentSS(rts, NULL);
        ssSetRootSS(rts, rts);
        ssSetVersion(rts, SIMSTRUCT_VERSION_LEVEL2);

        /* parameters */
        {
          mxArray **sfcnParams = (mxArray **)
            &sa_target_M->NonInlinedSFcns.Sfcn13.params;

          ssSetSFcnParamsCount(rts, 9);
          ssSetSFcnParamsPtr(rts, &sfcnParams[0]);

          ssSetSFcnParam(rts, 0, (mxArray*)&sa_target_P.IntOE_P1_Size[0]);
          ssSetSFcnParam(rts, 1, (mxArray*)&sa_target_P.IntOE_P2_Size[0]);
          ssSetSFcnParam(rts, 2, (mxArray*)&sa_target_P.IntOE_P3_Size[0]);
          ssSetSFcnParam(rts, 3, (mxArray*)&sa_target_P.IntOE_P4_Size[0]);
          ssSetSFcnParam(rts, 4, (mxArray*)&sa_target_P.IntOE_P5_Size[0]);
          ssSetSFcnParam(rts, 5, (mxArray*)&sa_target_P.IntOE_P6_Size[0]);
          ssSetSFcnParam(rts, 6, (mxArray*)&sa_target_P.IntOE_P7_Size[0]);
          ssSetSFcnParam(rts, 7, (mxArray*)&sa_target_P.IntOE_P8_Size[0]);
          ssSetSFcnParam(rts, 8, (mxArray*)&sa_target_P.IntOE_P9_Size[0]);
        }

        /* work vectors */
        ssSetIWork(rts, (int_T *) &sa_target_DWork.IntOE_IWORK[0]);
        {
          struct _ssDWorkRecord *dWorkRecord = (struct _ssDWorkRecord *)
            &sa_target_M->NonInlinedSFcns.Sfcn13.dWork;

          ssSetSFcnDWork(rts, dWorkRecord);
          _ssSetNumDWork(rts, 1);

          /* IWORK */
          ssSetDWorkWidth(rts, 0, 2);
          ssSetDWorkDataType(rts, 0,SS_INTEGER);
          ssSetDWorkComplexSignal(rts, 0, 0);
          ssSetDWork(rts, 0, &sa_target_DWork.IntOE_IWORK[0]);
        }

        /* registration */
        dopci8255(rts);

        sfcnInitializeSizes(rts);
        sfcnInitializeSampleTimes(rts);

        /* adjust sample time */
        ssSetSampleTime(rts, 0, 0.01);
        ssSetOffsetTime(rts, 0, 0.0);
        sfcnTsMap[0] = 1;

        /* set compiled values of dynamic vector attributes */

        ssSetNumNonsampledZCs(rts, 0);
        /* Update connectivity flags for each port */
        _ssSetInputPortConnected(rts, 0, 1);
        _ssSetInputPortConnected(rts, 1, 1);
        /* Update the BufferDstPort flags for each input port */
        ssSetInputPortBufferDstPort(rts, 0, -1);
        ssSetInputPortBufferDstPort(rts, 1, -1);
      }
    }
  }
}

/* Model terminate function */
void sa_target_terminate(void)
{

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[0];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[1];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[2];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[3];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[4];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[5];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[6];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[7];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[8];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[9];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[10];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[11];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[12];
    sfcnTerminate(rts);
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[13];
    sfcnTerminate(rts);
  }

  /* External mode */
  rtExtModeShutdown(2);
}

/*========================================================================*
 * Start of GRT compatible call interface                                 *
 *========================================================================*/

void MdlOutputs(int_T tid) {

  sa_target_output(tid);
}

void MdlUpdate(int_T tid) {

  sa_target_update(tid);
}

void MdlInitializeSizes(void) {
  sa_target_M->Sizes.numContStates = (0); /* Number of continuous states */
  sa_target_M->Sizes.numY = (0);        /* Number of model outputs */
  sa_target_M->Sizes.numU = (0);        /* Number of model inputs */
  sa_target_M->Sizes.sysDirFeedThru = (0); /* The model is not direct feedthrough */
  sa_target_M->Sizes.numSampTimes = (2); /* Number of sample times */
  sa_target_M->Sizes.numBlocks = (101); /* Number of blocks */
  sa_target_M->Sizes.numBlockIO = (121); /* Number of block outputs */
  sa_target_M->Sizes.numBlockPrms = (483); /* Sum of parameter "widths" */
}

void MdlInitializeSampleTimes(void) {
}

void MdlInitialize(void) {

  {
    int32_T i1;

    /* InitializeConditions for UnitDelay: '<S5>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sa_target_DWork.UnitDelay_DSTATE[i1] = sa_target_P.UnitDelay_X0;
    }
  }

  {
    int32_T i1;

    /* InitializeConditions for UnitDelay: '<S3>/Unit Delay' */
    for(i1=0; i1<17; i1++) {
      sa_target_DWork.UnitDelay_DSTATE_m[i1] = sa_target_P.UnitDelay_X0_o;
    }
  }

  /* Level2 S-Function Block: <Root>/S-target conditions (intarget_trialtime1) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[1];
    sfcnInitializeConditions(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/S-Function1 (forces) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[2];
    sfcnInitializeConditions(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }
}

void MdlStart(void) {

  /* user code (Start function Body) */
  {
    void *module = GetModuleHandle(NULL);
    rl32eScopeExists = (int (XPCCALLCONV *)(int))GetProcAddress(module,
     "rl32eScopeExists");
    if (rl32eScopeExists == NULL) {
      printf("Error resolving function rl32eScopeExists\n");
      return;
    }

    rl32eDefScope = (int (XPCCALLCONV *)(int, int))GetProcAddress(module,
     "rl32eDefScope");
    if (rl32eDefScope == NULL) {
      printf("Error resolving function rl32eDefScope\n");
      return;
    }

    rl32eAddSignal = (void (XPCCALLCONV *)(int, int))GetProcAddress(module,
     "rl32eAddSignal");
    if (rl32eAddSignal == NULL) {
      printf("Error resolving function rl32eAddSignal\n");
      return;
    }

    rl32eSetScope = (void (XPCCALLCONV *)(int, int,
      double))GetProcAddress(module, "rl32eSetScope");
    if (rl32eSetScope == NULL) {
      printf("Error resolving function rl32eSetScope\n");
      return;
    }

    xpceFSScopeSet = (void (XPCCALLCONV *)(int, const char *, int, unsigned
      int))GetProcAddress(module, "xpceFSScopeSet");
    if (xpceFSScopeSet == NULL) {
      printf("Error resolving function xpceFSScopeSet\n");
      return;
    }

    rl32eSetTargetScope = (void (XPCCALLCONV *)(int, int,
      double))GetProcAddress(module, "rl32eSetTargetScope");
    if (rl32eSetTargetScope == NULL) {
      printf("Error resolving function rl32eSetTargetScope\n");
      return;
    }

    rl32eRestartAcquisition = (void (XPCCALLCONV *)(int))GetProcAddress(module,
     "rl32eRestartAcquisition");
    if (rl32eRestartAcquisition == NULL) {
      printf("Error resolving function rl32eRestartAcquisition\n");
      return;
    }

    xpceScopeAcqOK = (void (XPCCALLCONV *)(int, int *))GetProcAddress(module,
     "xpceScopeAcqOK");
    if (xpceScopeAcqOK == NULL) {
      printf("Error resolving function xpceScopeAcqOK\n");
      return;
    }
  }

  /* Level2 S-Function Block: <Root>/Receive (xpcudpbytereceive) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[0];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <Root>/PCI-6031E  (danipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[3];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* S-Function Block: <S1>/S-Function (scblock) */

  {
    int i;
    if ((i = rl32eScopeExists(1)) == 0) {
      if ((i = rl32eDefScope(1,3)) != 0) {
        printf("Error creating scope 1\n");
      } else {

        rl32eAddSignal(1,
         0);
        rl32eAddSignal(1,
         125);
        rl32eAddSignal(1,
         120);
        rl32eAddSignal(1,
         196);
        rl32eAddSignal(1,
         197);
        rl32eAddSignal(1,
         150);
        rl32eAddSignal(1,
         151);
        rl32eAddSignal(1,
         118);
        rl32eAddSignal(1,
         119);
        rl32eSetScope(1, 4, 250);
        rl32eSetScope(1, 40, 0);
        rl32eSetScope(1, 7, 1);
        rl32eSetScope(1, 0, 0);
        xpceFSScopeSet(1, "sadata.dat", 0, 512);
        rl32eSetScope (1, 11, 1);
        rl32eSetScope(1, 3,
         0);
        rl32eSetScope(1, 1, 0.0);
        rl32eSetScope(1, 2, 0);
        rl32eSetScope(1, 8, -1);
        rl32eSetScope(1, 10, 0);
        xpceScopeAcqOK(1, &sa_target_DWork.SFunction_IWORK.AcquireOK);
      }
    }
    if (i) {
      rl32eRestartAcquisition(1);
    }
  }

  /* Level2 S-Function Block: <Root>/Send (xpcudpbytesend) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[4];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bit1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[5];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bits1-8 1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[6];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S3>/Bits9-1 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[7];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  {
    real_T tFirst_7_37;
    int32_T Ns_7_37;

    /* Start for DiscretePulseGenerator: '<S3>/Pulse Generator1' */
    tFirst_7_37 = 0.0;
    Ns_7_37 = (int32_T)floor(tFirst_7_37 / 0.01 + 0.5);
    if(Ns_7_37 <= 0) {
      sa_target_DWork.clockTickCounter = Ns_7_37;
    } else {
      sa_target_DWork.clockTickCounter = Ns_7_37 -
        (int32_T)(sa_target_P.PulseGenerator1_Period * floor(Ns_7_37 /
        sa_target_P.PulseGenerator1_Period));
    }
  }

  /* Level2 S-Function Block: <S3>/PCI-DIO-96 1 (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[8];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S2>/PCI-6031E  (adnipcie) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[9];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bit17 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[10];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bits1-8  (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[11];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  /* Level2 S-Function Block: <S5>/Bits9-16 (dipci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[12];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  {
    real_T tFirst_7_47;
    int32_T Ns_7_47;

    /* Start for DiscretePulseGenerator: '<S5>/Pulse Generator' */
    tFirst_7_47 = 0.0;
    Ns_7_47 = (int32_T)floor(tFirst_7_47 / 0.01 + 0.5);
    if(Ns_7_47 <= 0) {
      sa_target_DWork.clockTickCounter_m = Ns_7_47;
    } else {
      sa_target_DWork.clockTickCounter_m = Ns_7_47 -
        (int32_T)(sa_target_P.PulseGenerator_Period * floor(Ns_7_47 /
        sa_target_P.PulseGenerator_Period));
    }
  }

  /* Level2 S-Function Block: <S5>/Int & OE (dopci8255) */
  {
    SimStruct *rts = sa_target_M->childSfunctions[13];
    sfcnStart(rts);
    if(ssGetErrorStatus(rts) != NULL) return;
  }

  MdlInitialize();
}

rtModel_sa_target *sa_target(void) {
  sa_target_initialize(1);
  return sa_target_M;
}

void MdlTerminate(void) {
  sa_target_terminate();
}

/*========================================================================*
 * End of GRT compatible call interface                                   *
 *========================================================================*/

