[h,d]=hdrload('DESIRED.TXD');

data=d(:,1);

plot(data,'.r');

for i=1:size(d,1)
    ex(i) = 0.1*d(i,1)*cos(315*pi/180);
    ey(i) = 0.1*d(i,1)*sin(315*pi/180) + 0.45;
end

plot(ex,ey,'.')
set(gca,'XDir','reverse');
set(gca,'YDir','reverse');
