function [f,h]=create_display       
f=figure;
 
 zoomt=1.2;
     scnsize = get(0,'ScreenSize');
        hAxes=axes;
        set(gcf,'Color','k')
        set(hAxes,'Color','k','Position', [0.00 0.00 .99 .99]);
        set(gca,'YDir','reverse');
        set(gcf,'Position',[scnsize(1) scnsize(2)-100 scnsize(3) scnsize(4)+20],'MenuBar','none');   
       
        set(hAxes,'XLim',[scnsize(3)/2-200,scnsize(3)/2+200],'YLim',[scnsize(4)/2-200,scnsize(4)/2+200])
        axis equal manual
        hLine=line('Parent',hAxes,'Xdata',0,'Ydata',0,'Marker','o','Color','g','LineWidth',2); % marker is bigger (to increase the attention of the patient
        hLine=line('Parent',hAxes,'Xdata',100,'Ydata',0,'Marker','o','Color','r','LineWidth',2);
        hLine=line('Parent',hAxes,'Xdata',0,'Ydata',100,'Marker','o','Color','y','LineWidth',2);
        
        set(gcf,'DoubleBuffer','On');
        set(gcf,'BackingStore','off')
                      
h=gca;

 