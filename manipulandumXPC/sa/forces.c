/* 
 *  File     : forces.c  'S-Function in C for XPC target'
 *
 *  Abstract : Moves the endpoint from current position to xo yo using an open loop pd control
 *			   *IMPORTANT* This function must reside inside an enable subsystem configured to reset states.  
 *      
 *      Input: Robot's endpoint position (X,Y) mm, initial position Xo,Yo mm, 
 *			   and the four elements of the jacobian
 *
 *
 *	Parameters:	Tt (sec) time to hold in initial position
 *				NV (mm/sec) nominal velocity. Velocity from current positon to initial position
 *			   
 *      
 *
 *    Output :  1 if Xo Yo  reached and spent Tt sec in that position 
 *              0 otherwise. 
 *      
  */


#define S_FUNCTION_NAME forces
#define S_FUNCTION_LEVEL 2

#include <math.h>
#include "simstruc.h"


#define U(element) (*uPtrs[element])  /* Pointer to Input Port0 */

#define PI 3.1415926
#define NDIRECTION 2
#define FGAIN 25

#define NINPUT 17
#define NOUTPUT 4
 
    /* PARAMETERS */
	/* 
	 *  Holding Time (time in target in sec) :'HI'  
	 */
#define HI_IDX 0
#define HI_PARAM(S) ssGetSFcnParam(S,HI_IDX)
 	
	/* 
	 *  Nominal Velocity (velocity to move from x,y to xo,yo mm/sec ) :'NV' 
	 */
#define NV_IDX 1
#define NV_PARAM(S) ssGetSFcnParam(S,NV_IDX)

#define NPARAMS 2 

 static	real_T x_hist[3]= { 0.0,0.0,0.0 };
 static real_T y_hist[3]= { 0.0,0.0,0.0 };
 static	real_T sh_hist[3]= { 0.0,0.0,0.0 };
 static real_T el_hist[3]= { 0.0,0.0,0.0 };
 static	real_T shv_hist[3]= { 0.0,0.0,0.0 };
 static real_T elv_hist[3]= { 0.0,0.0,0.0 };


 static real_T stime = 0.0; /* sampling time */
 static real_T samples_in_target=0.0; /* number of samples correspondig to parameter HI hold interval  */ 
 static	real_T vel_thresh=0.0;
 static	real_T nomvel;				/* speed to move to xo,yo correspondig to parameter  NV nominal velocity */
 static	real_T dxInc ;
 static real_T dyInc; 
 static real_T  Timesequence;
 
 static real_T numsteps=1;
 static	int_T count2=1;
 static int_T count=1;
 static int_T targetflag = 0;
 static real_T xo;
 static	 real_T yo;
 static int_T gohomeflag=1;
 static int_T TrialTime=0;

 /* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
// ***** Machine Learning force from subject 124 *****
 static F1[2][30] = {{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};                  
 static F2[2][30] = {{0.1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}, {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}};
 
/*====================*
 * S-function methods *
 *====================*/


#define MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
  /* Function: mdlCheckParameters =============================================
   * Abstract:
   *    Validate our parameters to verify they are okay.
   */
  static void mdlCheckParameters(SimStruct *S)
  {
      /* Check 1st parameter: Time in initial position */
	 {  
          if (!mxIsDouble(HI_PARAM(S)) ||
              mxGetNumberOfElements(HI_PARAM(S)) != 1) {
              ssSetErrorStatus(S,"1st parameter to S-function must be a "
                               "scalar :  time in target in sec ");
              return;
          }
      
	}

	     /* Check 2nd parameter: Nominal Velocity  */
	{  
          if (!mxIsDouble(NV_PARAM(S)) ||
              mxGetNumberOfElements(NV_PARAM(S)) != 1) {
              ssSetErrorStatus(S,"2nd parameter to S-function must be a "
                               "scalar: velocity to move from x,y to xo,yo mm/sec");
              return;
          }
	}
  }
#endif /* MDL_CHECK_PARAMETERS */

/* Function: mdlInitializeSizes ===============================================
 * Abstract:
 *    The sizes information is used by Simulink to determine the S-function
 *    block's characteristics (number of inputs, outputs, states, etc.).
 */
static void mdlInitializeSizes(SimStruct *S)
{  
    ssSetNumSFcnParams(S, NPARAMS);  /* Number of expected parameters */
    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 4 ); /* discrete states xdot, ydot */ 

    if (!ssSetNumInputPorts(S, 1)) return;
    ssSetInputPortWidth(S, 0, NINPUT);  /* 11 Inputs: X, Y,Xt,Yt,J00,J10,J01,J11, flag type of force */
    ssSetInputPortDirectFeedThrough(S,0,1);

    if (!ssSetNumOutputPorts(S, 1)) return;
    ssSetOutputPortWidth(S, 0, NOUTPUT);  /* 7 outputs: 2 torque values; 2 forces; 3 trapezoidal force params */

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, 101*NDIRECTION*2+196);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    /* Take care when specifying exception free code - see sfuntmpl_doc.c */
    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE);
}

/* Function: mdlInitializeSampleTimes =========================================
 * Abstract:
 *    Specifiy that we inherit our sample time from the driving block.
 */
static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, INHERITED_SAMPLE_TIME); 
    ssSetOffsetTime(S, 0, 0.0);
}

#define MDL_INITIALIZE_CONDITIONS
/* Function: mdlInitializeConditions ========================================
 * Abstract:
 *    Initialize discrete states .
 */
static void mdlInitializeConditions(SimStruct *S)
{
    real_T *x0 = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);
    int i;
    const mxArray *array_ptr ;
	
	real_T *rwork = ssGetRWork(S);
	
	 for(i=0;i<=599;i++)
		 rwork[i]=0;

      for (i=1; i<=30; i++) {
		 rwork[i+(2*1-2)*101] = F1[1][i]; 
         rwork[i+(2*1-1)*101] = F1[2][i]; 

         rwork[i+(2*2-2)*101] = F2[1][i]; 
         rwork[i+(2*2-1)*101] = F2[2][i]; 
	  }
	

        x0[0]=0;    /* xdot = 0 */
        x0[1]=0;    /* ydot = 0 */
     
	
	stime = ssGetSampleTime(S,0); /* Get the sampling period */
 
	array_ptr =  ssGetSFcnParam(S,0);
     samples_in_target= *(mxGetPr( ssGetSFcnParam(S,0) ))/stime;

	for(i=1;i<=92;i++)
    	rwork[500+i]= *(mxGetPr( ssGetSFcnParam(S,0) )+i);
	   

    nomvel=*mxGetPr(NV_PARAM(S));

	
	gohomeflag=1;

}



/* Function: mdlOutputs =======================================================
 * Abstract: This is what you change for modifying force fields!
 *      
 */
static void mdlOutputs(SimStruct *S, int_T tid)
{
    real_T            *y    = ssGetOutputPortRealSignal(S,0);
    real_T            *x    = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs = ssGetInputPortRealSignalPtrs(S,0);
	real_T            tempfx = 0.0;
	real_T			  tempfy = 0.0;
	real_T            tempf = 0.0;
	real_T            temptx = 0.0;
	real_T			  tempty = 0.0;	
	real_T	          tempx = 0.0;
	real_T            tempy = 0.0;
	int_T cnt=1;
	int_T forcetype;
	real_T fieldtype;
	int_T direction;
	int_T directlist;
    int_T i=1;
	real_T *rwork = ssGetRWork(S);
	real_T t1comp, t2comp;
    real_T ex=0.0;
	real_T ey=0.45;
	real_T errx=0, erry=0;
    int_T falsestart=0;

	UNUSED_ARG(tid); /* not used in single tasking mode */

	direction = (int)U(12);
    switch(direction)
	{ 
	  case 225:
        directlist = 1;
		break;
	  
	  case 315:
		  directlist = 2;
		  break;

      case 255:
		  directlist = 3;
		  break;

	  case 285:
		  directlist = 4;
		  break;

	   case 90:
		  directlist = 5;
		  break;

	  case 210:
		  directlist = 6;
		  break;

	   case 330:
		  directlist = 7;
		  break;
    }

    fieldtype=U(8); /* change to integer value */
    
    if(fieldtype==2) forcetype=2;
	if(fieldtype<5 && fieldtype!=2) forcetype=0;

    /* for any viscous field based on the velocity */
    if((fieldtype<=15) && (fieldtype>=5))
        forcetype = 15;

    /* for Usha's experiment */ 
	if ((fieldtype<=25) && (fieldtype>=16))
        forcetype = 20;
    
    if (fieldtype<4000 && fieldtype>=100)
		forcetype = (int)fieldtype;
 




    if(count>40)  /*DEBUG wait for derivatives to settle ?? */
   {

    switch(forcetype) 
	   {

		 case 0: /* no force field  */
		
			gohomeflag =1;
			count2=1;
			
			tempfx=0.0;
			tempfy=0.0;
		break;

		case 15: /* viscous field, constant gain of 20  */
			gohomeflag=1;
		    count2=1;
		
			tempfx=  20*x[1];   /* force calculation [N]: proportional to speed and perpendicular to the movement direction ydot=x[1]; xdot=x[0]; [0 20; -20 0] */    
			tempfy= -20*x[0];  
		break;
		
       case 20: /* viscous field, constant gain of 20  */
			gohomeflag=1;
		    count2=1;
		
			tempfx=  20*x[1];   /* force calculation [N]: proportional to speed and perpendicular to the movement direction ydot=x[1]; xdot=x[0]; [0 20; -20 0] */    
			tempfy= -20*x[0];  
 
		  	break;			
			 
		} /* end switch */

 
	//0.23277		-0.046586			0.041126
     /* calculate compensation torque */
     t1comp=0.23277*x[4]-0.046586*cos(U(15)-PI-U(14))*x[5]+0.046586*sin(U(15)-PI-U(14))*x[3]*x[3];
     t2comp=-0.046586*cos(U(15)-PI-U(14))*x[4]+0.041126*x[5]-0.046586*sin(U(15)-PI-U(14))*x[2]*x[2];

		/*
 *  Output Torques
 */
/* temporary security cut torques above 40 */

	if ( fabs(tempfx)>12 )
	{
		tempfx = 12 * tempfx/fabs(tempfx);
	}

	if ( fabs(tempfy)>12 )
	{
		tempfy = 12 * tempfy/fabs(tempfy);
	}
		
	y[0] = (U(4)*tempfx + U(5)*tempfy ) + t1comp;
	y[1] = (U(6)*tempfx + U(7)*tempfy ) + t2comp;
		
    y[2] = tempfx;
	y[3] = tempfy;	


 }
 	
 count++;

}  // end output calc


#define MDL_UPDATE
/* Function: mdlUpdate ======================================================
 * Abstract:
 *      xdot = 
 */
static void mdlUpdate(SimStruct *S, int_T tid)
{
   
    real_T            *x       = ssGetRealDiscStates(S);
    InputRealPtrsType uPtrs    = ssGetInputPortRealSignalPtrs(S,0);

	int_T lp;
    UNUSED_ARG(tid); /* not used in single tasking mode */

	/* start derivative vector */
   	if (count==1)
	{
	   for (lp=2;lp>=0;lp--) {  
 				x_hist[lp]=U(0);    /* = X */
				y_hist[lp]=U(1);    /* = Y */
				sh_hist[lp]=U(14);  /* shoulder angle */
				el_hist[lp]=U(15);  /* elbow angle */
				shv_hist[lp]=U(14); /* shoulder angular velocity */
				elv_hist[lp]=U(15); /* elbow angular velocity*/
		}
	}

	x_hist[0]=U(0);
	y_hist[0]=U(1);
    sh_hist[0]=U(14);
	el_hist[0]=U(15);

    shv_hist[0] = (sh_hist[0] - sh_hist[2])*0.5/stime;
    elv_hist[0] = (el_hist[0] - el_hist[2])*0.5/stime;


   /* Note on coeficient 0.0005 in following equations:
	* 0.001 to convert from mm to m, the time between positions (two samples appart is 2*stime [0.0005=0.001/(2*stime)] 
	*/
	/* x[0]-x[5]: xv, yv, shv, elv, sha, ela */
	x[0] = (x_hist[0] - x_hist[2])*0.5/stime;  /* m/s */
	x[1] = (y_hist[0] - y_hist[2])*0.5/stime;
	x[2] = shv_hist[0];
	x[3] = elv_hist[0];
	x[4] = (shv_hist[0] - shv_hist[2])*0.5/stime;
    x[5] = (elv_hist[0] - elv_hist[2])*0.5/stime;

	for (lp=2;lp>0;lp--) {  /* update derivative vector */
 		x_hist[lp]=x_hist[lp-1];
		y_hist[lp]=y_hist[lp-1];
	    sh_hist[lp]=sh_hist[lp-1];
		el_hist[lp]=el_hist[lp-1];
	    shv_hist[lp]=shv_hist[lp-1];
		elv_hist[lp]=elv_hist[lp-1];	
	} 
}



/* Function: mdlTerminate =====================================================
 * Abstract:
 *    No termination needed, but we are required to have this routine.
 */
static void mdlTerminate(SimStruct *S)
{
    
	count2=0;
	count=1;
	xo=0;
	yo=0;
	numsteps=1;
	gohomeflag=1;
     /*UNUSED_ARG(S); /* unused input argument */
    
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
