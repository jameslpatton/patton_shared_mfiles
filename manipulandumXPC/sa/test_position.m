 
U0 = 0.01;
U1 = 0.44;
U2 = 0.09;
U3 = 0.4;
U9 = 0;
U10 = 0.45;

 xper = ( (U1-U10)*(U2-U9)*(U3-U10) + (U3-U10)^2*U9 + (U2-U9)^2*U0 ) / ( (U3-U10)^2 + (U2-U9)^2 );
 yper = U10 + (U3-U10)*(xper-U9)/(U2-U9);
 
 x=[U2 U9 ];
 y=[U3 U10 ];
 plot(x,y,'r-o');
 hold on;
 x=[U0 xper];
 y =[U1 yper];
 plot(x,y,'b-o');
 xlim([0 0.1]);
 ylim([.4 0.5]);
 axis square
 
 k1 = (U3-U10)/(U2-U9);
 k2 = (U1-yper) /(U0-xper);
 k3 = (U3-yper)/(U2-xper);
 
 format long
 l1 = (xper-U2)^2+(yper-U3)^2;
 l2 = (U0-xper)^2+ (U1-yper)^2;
 l3 = (U0-U2)^2 + (U1-U3)^2;
 l1+l2;
 
 %xper=U2;
 %yper=U3;
 iPerDist = 1000*sqrt( (xper-U9)^2 + (yper-U10)^2 )
 
 fLpt = sqrt( (U3-yper)^2 + (U2-xper)^2 );
 fLpd = (-4*(iPerDist-50)^2/10000 + 1)*0.05/1.73;
 xd = (-U3+yper) * fLpd/fLpt + xper;
 yd = (U2-xper) * fLpd/fLpt + yper;
 fx=26*(xd - U0 );
  fy=26*(yd - U1 );