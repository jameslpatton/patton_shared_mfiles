% Main program for running an experiment.
% inputs:  target file called by "transformtarget" function
%          Movement type: Center out/random walk 
%          Start trial number
% output:  A lauched target xpc with realtime information from the robot 

clc                                 % Clear the Command Matlab Window
clear all                           % Clear all the variables
close all                           % Close all the figures

transformtarget;

%delete('date1.txd');
 
[figaxes,disaxes] =create_display;
hTarget=line('Parent',disaxes,'Xdata',0,'Ydata',400,'Marker','s','Color','y','LineWidth',4);
u(3)=0; u(4)=0;
pippo=u(3)*1.1486+(u(4)+400-400)*(-0.0001)+(+39.2420);      % the u(3) and u(4) targets coordinates are expressed on respect to the 
pluto=u(3)*0.0093+(u(4)+400-400)*(+1.1344)+(-35.5234);      % reference system on the midddle of the workspace, that is x, y+400 on respect to
x_delta=u(3)-pippo;                                         % the robot system on the motor axes which is the refernce system of u(1), u(2)
y_delta=u(4)+400-400-pluto;
set(hTarget,'Xdata',u(3)+x_delta,'Ydata',u(4)+400-400+y_delta,'Color','y')
 
clear all                           % Clear all the variables
close all                           % Close all the figures

%disp('Step. 1')
sp=0.01;
 
mex forces.c                                        % build the .dll file
mex intarget_trialtime1.c         % mex intarget.c  % build the .dll file

rtwbuild('sa_target');

global tg
tg=xpc;
% sc = getscope(tg,1)
global x            % get the signal ID of the endpoint coordinates
global y
x=getsignalid(tg,'Subsystem/Jacobian-Cartesian Transformation/Sum');        % ???
y=getsignalid(tg,'Subsystem/Jacobian-Cartesian Transformation/Sum1');       % ???
tg.stoptime=inf;
%+tg;

disp('Step. 5')
global disaxes;     %open the axes to display manipulandum information
global figaxes;
Snd_tooslow=wavread('too_slow.wav');  Snd_tooslow(1:2500,:)=[]; % Snd_Tg=wavread('good.wav');
Snd_toofast=wavread('too_fast.wav'); Snd_toofast(1:900,:)=[];
%tg.status

% Start_trial = input('Input start trial number [1]:  ');
% if isempty(Start_trial)
%     Start_trial = 1;
% end

Start_trial = 82;
input('Let go of the handle, and wait for 3 seconds');
+tg;
 pause(2);
-tg;
inidata=tg.outputlog;
zeroforce = [mean(inidata(:,1)) mean(inidata(:,2))];
fid = fopen('zeroforce.txd','w');
fprintf(fid,'fx - fy\n');
fprintf(fid,'%6.4f %6.4f\n',zeroforce);
fclose(fid);

if 1
 trialstosv = 20;
 

List_Exp=load('listtarget.m');     

[figaxes,disaxes] =create_display;
ywei;
+tg;
sa_host;
set_param('sa_host','SimulationCommand','start');
 
wavplay(Snd_toofast,37000);
wavplay(Snd_tooslow,37000); 
end