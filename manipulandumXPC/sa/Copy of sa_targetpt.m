function pt = sa_targetpt(flag)

if nargin == 0
  pt = [];
	    pt(1).blockname = 'E Gain';
	    pt(1).paramname = 'Gain';
	    pt(1).class     = 'rt_SCALAR';
	    pt(1).nrows     = 1;
	    pt(1).ncols     = 1;
	    pt(1).subsource = 'SS_DOUBLE';

	    pt(2).blockname = 'S Gain';
	    pt(2).paramname = 'Gain';
	    pt(2).class     = 'rt_SCALAR';
	    pt(2).nrows     = 1;
	    pt(2).ncols     = 1;
	    pt(2).subsource = 'SS_DOUBLE';

	    pt(3).blockname = 'PCI-6031E';
	    pt(3).paramname = 'P1';
	    pt(3).class     = 'rt_VECTOR';
	    pt(3).nrows     = 1;
	    pt(3).ncols     = 2;
	    pt(3).subsource = 'SS_DOUBLE';

	    pt(4).blockname = 'PCI-6031E';
	    pt(4).paramname = 'P2';
	    pt(4).class     = 'rt_VECTOR';
	    pt(4).nrows     = 1;
	    pt(4).ncols     = 2;
	    pt(4).subsource = 'SS_DOUBLE';

	    pt(5).blockname = 'PCI-6031E';
	    pt(5).paramname = 'P3';
	    pt(5).class     = 'rt_VECTOR';
	    pt(5).nrows     = 1;
	    pt(5).ncols     = 2;
	    pt(5).subsource = 'SS_DOUBLE';

	    pt(6).blockname = 'PCI-6031E';
	    pt(6).paramname = 'P4';
	    pt(6).class     = 'rt_VECTOR';
	    pt(6).nrows     = 1;
	    pt(6).ncols     = 2;
	    pt(6).subsource = 'SS_DOUBLE';

	    pt(7).blockname = 'PCI-6031E';
	    pt(7).paramname = 'P5';
	    pt(7).class     = 'rt_SCALAR';
	    pt(7).nrows     = 1;
	    pt(7).ncols     = 1;
	    pt(7).subsource = 'SS_DOUBLE';

	    pt(8).blockname = 'PCI-6031E';
	    pt(8).paramname = 'P6';
	    pt(8).class     = 'rt_SCALAR';
	    pt(8).nrows     = 1;
	    pt(8).ncols     = 1;
	    pt(8).subsource = 'SS_DOUBLE';

	    pt(9).blockname = 'PCI-6031E';
	    pt(9).paramname = 'P7';
	    pt(9).class     = 'rt_SCALAR';
	    pt(9).nrows     = 1;
	    pt(9).ncols     = 1;
	    pt(9).subsource = 'SS_DOUBLE';

	    pt(10).blockname = 'Receive';
	    pt(10).paramname = 'P1';
	    pt(10).class     = 'rt_VECTOR';
	    pt(10).nrows     = 1;
	    pt(10).ncols     = 11;
	    pt(10).subsource = 'SS_DOUBLE';

	    pt(11).blockname = 'Receive';
	    pt(11).paramname = 'P2';
	    pt(11).class     = 'rt_SCALAR';
	    pt(11).nrows     = 1;
	    pt(11).ncols     = 1;
	    pt(11).subsource = 'SS_DOUBLE';

	    pt(12).blockname = 'Receive';
	    pt(12).paramname = 'P3';
	    pt(12).class     = 'rt_SCALAR';
	    pt(12).nrows     = 1;
	    pt(12).ncols     = 1;
	    pt(12).subsource = 'SS_DOUBLE';

	    pt(13).blockname = 'Receive';
	    pt(13).paramname = 'P4';
	    pt(13).class     = 'rt_SCALAR';
	    pt(13).nrows     = 1;
	    pt(13).ncols     = 1;
	    pt(13).subsource = 'SS_DOUBLE';

	    pt(14).blockname = 'S-Function1';
	    pt(14).paramname = 'P1';
	    pt(14).class     = 'rt_VECTOR';
	    pt(14).nrows     = 1;
	    pt(14).ncols     = 93;
	    pt(14).subsource = 'SS_DOUBLE';

	    pt(15).blockname = 'S-Function1';
	    pt(15).paramname = 'P2';
	    pt(15).class     = 'rt_SCALAR';
	    pt(15).nrows     = 1;
	    pt(15).ncols     = 1;
	    pt(15).subsource = 'SS_DOUBLE';

	    pt(16).blockname = 'S-target conditions';
	    pt(16).paramname = 'P1';
	    pt(16).class     = 'rt_SCALAR';
	    pt(16).nrows     = 1;
	    pt(16).ncols     = 1;
	    pt(16).subsource = 'SS_DOUBLE';

	    pt(17).blockname = 'S-target conditions';
	    pt(17).paramname = 'P2';
	    pt(17).class     = 'rt_SCALAR';
	    pt(17).nrows     = 1;
	    pt(17).ncols     = 1;
	    pt(17).subsource = 'SS_DOUBLE';

	    pt(18).blockname = 'S-target conditions';
	    pt(18).paramname = 'P3';
	    pt(18).class     = 'rt_SCALAR';
	    pt(18).nrows     = 1;
	    pt(18).ncols     = 1;
	    pt(18).subsource = 'SS_DOUBLE';

	    pt(19).blockname = 'Send';
	    pt(19).paramname = 'P1';
	    pt(19).class     = 'rt_VECTOR';
	    pt(19).nrows     = 1;
	    pt(19).ncols     = 11;
	    pt(19).subsource = 'SS_DOUBLE';

	    pt(20).blockname = 'Send';
	    pt(20).paramname = 'P2';
	    pt(20).class     = 'rt_SCALAR';
	    pt(20).nrows     = 1;
	    pt(20).ncols     = 1;
	    pt(20).subsource = 'SS_DOUBLE';

	    pt(21).blockname = 'Send';
	    pt(21).paramname = 'P3';
	    pt(21).class     = 'rt_SCALAR';
	    pt(21).nrows     = 1;
	    pt(21).ncols     = 1;
	    pt(21).subsource = 'SS_DOUBLE';

	    pt(22).blockname = 'Send';
	    pt(22).paramname = 'P4';
	    pt(22).class     = 'rt_SCALAR';
	    pt(22).nrows     = 1;
	    pt(22).ncols     = 1;
	    pt(22).subsource = 'SS_DOUBLE';

	    pt(23).blockname = 'Subsystem/Xy';
	    pt(23).paramname = 'Gain';
	    pt(23).class     = 'rt_SCALAR';
	    pt(23).nrows     = 1;
	    pt(23).ncols     = 1;
	    pt(23).subsource = 'SS_DOUBLE';

	    pt(24).blockname = 'Subsystem/Xy2';
	    pt(24).paramname = 'Gain';
	    pt(24).class     = 'rt_SCALAR';
	    pt(24).nrows     = 1;
	    pt(24).ncols     = 1;
	    pt(24).subsource = 'SS_DOUBLE';

	    pt(25).blockname = 'Subsystem/f';
	    pt(25).paramname = 'Gain';
	    pt(25).class     = 'rt_SCALAR';
	    pt(25).nrows     = 1;
	    pt(25).ncols     = 1;
	    pt(25).subsource = 'SS_DOUBLE';

	    pt(26).blockname = 'Subsystem/f1';
	    pt(26).paramname = 'Gain';
	    pt(26).class     = 'rt_SCALAR';
	    pt(26).nrows     = 1;
	    pt(26).ncols     = 1;
	    pt(26).subsource = 'SS_DOUBLE';

	    pt(27).blockname = 'Subsystem/PCI-6031E';
	    pt(27).paramname = 'P1';
	    pt(27).class     = 'rt_VECTOR';
	    pt(27).nrows     = 1;
	    pt(27).ncols     = 6;
	    pt(27).subsource = 'SS_DOUBLE';

	    pt(28).blockname = 'Subsystem/PCI-6031E';
	    pt(28).paramname = 'P2';
	    pt(28).class     = 'rt_VECTOR';
	    pt(28).nrows     = 1;
	    pt(28).ncols     = 6;
	    pt(28).subsource = 'SS_DOUBLE';

	    pt(29).blockname = 'Subsystem/PCI-6031E';
	    pt(29).paramname = 'P3';
	    pt(29).class     = 'rt_VECTOR';
	    pt(29).nrows     = 1;
	    pt(29).ncols     = 6;
	    pt(29).subsource = 'SS_DOUBLE';

	    pt(30).blockname = 'Subsystem/PCI-6031E';
	    pt(30).paramname = 'P4';
	    pt(30).class     = 'rt_SCALAR';
	    pt(30).nrows     = 1;
	    pt(30).ncols     = 1;
	    pt(30).subsource = 'SS_DOUBLE';

	    pt(31).blockname = 'Subsystem/PCI-6031E';
	    pt(31).paramname = 'P5';
	    pt(31).class     = 'rt_SCALAR';
	    pt(31).nrows     = 1;
	    pt(31).ncols     = 1;
	    pt(31).subsource = 'SS_DOUBLE';

	    pt(32).blockname = 'Subsystem/PCI-6031E';
	    pt(32).paramname = 'P6';
	    pt(32).class     = 'rt_SCALAR';
	    pt(32).nrows     = 1;
	    pt(32).ncols     = 1;
	    pt(32).subsource = 'SS_DOUBLE';

	    pt(33).blockname = 'Subsystem/Elbow Angle/(ALPHA)';
	    pt(33).paramname = 'Value';
	    pt(33).class     = 'rt_SCALAR';
	    pt(33).nrows     = 1;
	    pt(33).ncols     = 1;
	    pt(33).subsource = 'SS_DOUBLE';

	    pt(34).blockname = 'Subsystem/Elbow Angle/Bits//Radian (TRIG_SCALE)';
	    pt(34).paramname = 'Value';
	    pt(34).class     = 'rt_SCALAR';
	    pt(34).nrows     = 1;
	    pt(34).ncols     = 1;
	    pt(34).subsource = 'SS_DOUBLE';

	    pt(35).blockname = 'Subsystem/Elbow Angle/Const';
	    pt(35).paramname = 'Value';
	    pt(35).class     = 'rt_SCALAR';
	    pt(35).nrows     = 1;
	    pt(35).ncols     = 1;
	    pt(35).subsource = 'SS_DOUBLE';

	    pt(36).blockname = 'Subsystem/Elbow Angle/Constant2';
	    pt(36).paramname = 'Value';
	    pt(36).class     = 'rt_VECTOR';
	    pt(36).nrows     = 1;
	    pt(36).ncols     = 17;
	    pt(36).subsource = 'SS_DOUBLE';

	    pt(37).blockname = 'Subsystem/Elbow Angle/Constant3';
	    pt(37).paramname = 'Value';
	    pt(37).class     = 'rt_SCALAR';
	    pt(37).nrows     = 1;
	    pt(37).ncols     = 1;
	    pt(37).subsource = 'SS_DOUBLE';

	    pt(38).blockname = 'Subsystem/Elbow Angle/Offset Elbow (OFFSETEL)';
	    pt(38).paramname = 'Value';
	    pt(38).class     = 'rt_SCALAR';
	    pt(38).nrows     = 1;
	    pt(38).ncols     = 1;
	    pt(38).subsource = 'SS_DOUBLE';

	    pt(39).blockname = 'Subsystem/Elbow Angle/Pulse Generator1';
	    pt(39).paramname = 'Amplitude';
	    pt(39).class     = 'rt_SCALAR';
	    pt(39).nrows     = 1;
	    pt(39).ncols     = 1;
	    pt(39).subsource = 'SS_DOUBLE';

	    pt(40).blockname = 'Subsystem/Elbow Angle/Pulse Generator1';
	    pt(40).paramname = 'Period';
	    pt(40).class     = 'rt_SCALAR';
	    pt(40).nrows     = 1;
	    pt(40).ncols     = 1;
	    pt(40).subsource = 'SS_DOUBLE';

	    pt(41).blockname = 'Subsystem/Elbow Angle/Pulse Generator1';
	    pt(41).paramname = 'PulseWidth';
	    pt(41).class     = 'rt_SCALAR';
	    pt(41).nrows     = 1;
	    pt(41).ncols     = 1;
	    pt(41).subsource = 'SS_DOUBLE';

	    pt(42).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(42).paramname = 'P1';
	    pt(42).class     = 'rt_VECTOR';
	    pt(42).nrows     = 1;
	    pt(42).ncols     = 2;
	    pt(42).subsource = 'SS_DOUBLE';

	    pt(43).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(43).paramname = 'P2';
	    pt(43).class     = 'rt_SCALAR';
	    pt(43).nrows     = 1;
	    pt(43).ncols     = 1;
	    pt(43).subsource = 'SS_DOUBLE';

	    pt(44).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(44).paramname = 'P3';
	    pt(44).class     = 'rt_SCALAR';
	    pt(44).nrows     = 1;
	    pt(44).ncols     = 1;
	    pt(44).subsource = 'SS_DOUBLE';

	    pt(45).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(45).paramname = 'P4';
	    pt(45).class     = 'rt_SCALAR';
	    pt(45).nrows     = 1;
	    pt(45).ncols     = 1;
	    pt(45).subsource = 'SS_DOUBLE';

	    pt(46).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(46).paramname = 'P5';
	    pt(46).class     = 'rt_SCALAR';
	    pt(46).nrows     = 1;
	    pt(46).ncols     = 1;
	    pt(46).subsource = 'SS_DOUBLE';

	    pt(47).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(47).paramname = 'P6';
	    pt(47).class     = 'rt_SCALAR';
	    pt(47).nrows     = 1;
	    pt(47).ncols     = 1;
	    pt(47).subsource = 'SS_DOUBLE';

	    pt(48).blockname = 'Subsystem/Elbow Angle/Bit1';
	    pt(48).paramname = 'P7';
	    pt(48).class     = 'rt_SCALAR';
	    pt(48).nrows     = 1;
	    pt(48).ncols     = 1;
	    pt(48).subsource = 'SS_DOUBLE';

	    pt(49).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(49).paramname = 'P1';
	    pt(49).class     = 'rt_VECTOR';
	    pt(49).nrows     = 1;
	    pt(49).ncols     = 8;
	    pt(49).subsource = 'SS_DOUBLE';

	    pt(50).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(50).paramname = 'P2';
	    pt(50).class     = 'rt_SCALAR';
	    pt(50).nrows     = 1;
	    pt(50).ncols     = 1;
	    pt(50).subsource = 'SS_DOUBLE';

	    pt(51).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(51).paramname = 'P3';
	    pt(51).class     = 'rt_SCALAR';
	    pt(51).nrows     = 1;
	    pt(51).ncols     = 1;
	    pt(51).subsource = 'SS_DOUBLE';

	    pt(52).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(52).paramname = 'P4';
	    pt(52).class     = 'rt_SCALAR';
	    pt(52).nrows     = 1;
	    pt(52).ncols     = 1;
	    pt(52).subsource = 'SS_DOUBLE';

	    pt(53).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(53).paramname = 'P5';
	    pt(53).class     = 'rt_SCALAR';
	    pt(53).nrows     = 1;
	    pt(53).ncols     = 1;
	    pt(53).subsource = 'SS_DOUBLE';

	    pt(54).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(54).paramname = 'P6';
	    pt(54).class     = 'rt_SCALAR';
	    pt(54).nrows     = 1;
	    pt(54).ncols     = 1;
	    pt(54).subsource = 'SS_DOUBLE';

	    pt(55).blockname = 'Subsystem/Elbow Angle/Bits1-8 1';
	    pt(55).paramname = 'P7';
	    pt(55).class     = 'rt_SCALAR';
	    pt(55).nrows     = 1;
	    pt(55).ncols     = 1;
	    pt(55).subsource = 'SS_DOUBLE';

	    pt(56).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(56).paramname = 'P1';
	    pt(56).class     = 'rt_VECTOR';
	    pt(56).nrows     = 1;
	    pt(56).ncols     = 8;
	    pt(56).subsource = 'SS_DOUBLE';

	    pt(57).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(57).paramname = 'P2';
	    pt(57).class     = 'rt_SCALAR';
	    pt(57).nrows     = 1;
	    pt(57).ncols     = 1;
	    pt(57).subsource = 'SS_DOUBLE';

	    pt(58).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(58).paramname = 'P3';
	    pt(58).class     = 'rt_SCALAR';
	    pt(58).nrows     = 1;
	    pt(58).ncols     = 1;
	    pt(58).subsource = 'SS_DOUBLE';

	    pt(59).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(59).paramname = 'P4';
	    pt(59).class     = 'rt_SCALAR';
	    pt(59).nrows     = 1;
	    pt(59).ncols     = 1;
	    pt(59).subsource = 'SS_DOUBLE';

	    pt(60).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(60).paramname = 'P5';
	    pt(60).class     = 'rt_SCALAR';
	    pt(60).nrows     = 1;
	    pt(60).ncols     = 1;
	    pt(60).subsource = 'SS_DOUBLE';

	    pt(61).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(61).paramname = 'P6';
	    pt(61).class     = 'rt_SCALAR';
	    pt(61).nrows     = 1;
	    pt(61).ncols     = 1;
	    pt(61).subsource = 'SS_DOUBLE';

	    pt(62).blockname = 'Subsystem/Elbow Angle/Bits9-1';
	    pt(62).paramname = 'P7';
	    pt(62).class     = 'rt_SCALAR';
	    pt(62).nrows     = 1;
	    pt(62).ncols     = 1;
	    pt(62).subsource = 'SS_DOUBLE';

	    pt(63).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(63).paramname = 'P1';
	    pt(63).class     = 'rt_VECTOR';
	    pt(63).nrows     = 1;
	    pt(63).ncols     = 2;
	    pt(63).subsource = 'SS_DOUBLE';

	    pt(64).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(64).paramname = 'P2';
	    pt(64).class     = 'rt_SCALAR';
	    pt(64).nrows     = 1;
	    pt(64).ncols     = 1;
	    pt(64).subsource = 'SS_DOUBLE';

	    pt(65).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(65).paramname = 'P3';
	    pt(65).class     = 'rt_VECTOR';
	    pt(65).nrows     = 1;
	    pt(65).ncols     = 2;
	    pt(65).subsource = 'SS_DOUBLE';

	    pt(66).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(66).paramname = 'P4';
	    pt(66).class     = 'rt_VECTOR';
	    pt(66).nrows     = 1;
	    pt(66).ncols     = 2;
	    pt(66).subsource = 'SS_DOUBLE';

	    pt(67).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(67).paramname = 'P5';
	    pt(67).class     = 'rt_SCALAR';
	    pt(67).nrows     = 1;
	    pt(67).ncols     = 1;
	    pt(67).subsource = 'SS_DOUBLE';

	    pt(68).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(68).paramname = 'P6';
	    pt(68).class     = 'rt_SCALAR';
	    pt(68).nrows     = 1;
	    pt(68).ncols     = 1;
	    pt(68).subsource = 'SS_DOUBLE';

	    pt(69).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(69).paramname = 'P7';
	    pt(69).class     = 'rt_SCALAR';
	    pt(69).nrows     = 1;
	    pt(69).ncols     = 1;
	    pt(69).subsource = 'SS_DOUBLE';

	    pt(70).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(70).paramname = 'P8';
	    pt(70).class     = 'rt_SCALAR';
	    pt(70).nrows     = 1;
	    pt(70).ncols     = 1;
	    pt(70).subsource = 'SS_DOUBLE';

	    pt(71).blockname = 'Subsystem/Elbow Angle/PCI-DIO-96 1';
	    pt(71).paramname = 'P9';
	    pt(71).class     = 'rt_SCALAR';
	    pt(71).nrows     = 1;
	    pt(71).ncols     = 1;
	    pt(71).subsource = 'SS_DOUBLE';

	    pt(72).blockname = 'Subsystem/Elbow Angle/Unit Delay';
	    pt(72).paramname = 'X0';
	    pt(72).class     = 'rt_SCALAR';
	    pt(72).nrows     = 1;
	    pt(72).ncols     = 1;
	    pt(72).subsource = 'SS_DOUBLE';

	    pt(73).blockname = 'Subsystem/Shoulder Angle/Bits//Radian (TRIG_SCALE)';
	    pt(73).paramname = 'Value';
	    pt(73).class     = 'rt_SCALAR';
	    pt(73).nrows     = 1;
	    pt(73).ncols     = 1;
	    pt(73).subsource = 'SS_DOUBLE';

	    pt(74).blockname = 'Subsystem/Shoulder Angle/Constant1';
	    pt(74).paramname = 'Value';
	    pt(74).class     = 'rt_SCALAR';
	    pt(74).nrows     = 1;
	    pt(74).ncols     = 1;
	    pt(74).subsource = 'SS_DOUBLE';

	    pt(75).blockname = 'Subsystem/Shoulder Angle/Offset Shoulder (OFFSETSH)';
	    pt(75).paramname = 'Value';
	    pt(75).class     = 'rt_SCALAR';
	    pt(75).nrows     = 1;
	    pt(75).ncols     = 1;
	    pt(75).subsource = 'SS_DOUBLE';

	    pt(76).blockname = 'Subsystem/Shoulder Angle/Vector 2^17';
	    pt(76).paramname = 'Value';
	    pt(76).class     = 'rt_VECTOR';
	    pt(76).nrows     = 1;
	    pt(76).ncols     = 17;
	    pt(76).subsource = 'SS_DOUBLE';

	    pt(77).blockname = 'Subsystem/Shoulder Angle/Pulse Generator';
	    pt(77).paramname = 'Amplitude';
	    pt(77).class     = 'rt_SCALAR';
	    pt(77).nrows     = 1;
	    pt(77).ncols     = 1;
	    pt(77).subsource = 'SS_DOUBLE';

	    pt(78).blockname = 'Subsystem/Shoulder Angle/Pulse Generator';
	    pt(78).paramname = 'Period';
	    pt(78).class     = 'rt_SCALAR';
	    pt(78).nrows     = 1;
	    pt(78).ncols     = 1;
	    pt(78).subsource = 'SS_DOUBLE';

	    pt(79).blockname = 'Subsystem/Shoulder Angle/Pulse Generator';
	    pt(79).paramname = 'PulseWidth';
	    pt(79).class     = 'rt_SCALAR';
	    pt(79).nrows     = 1;
	    pt(79).ncols     = 1;
	    pt(79).subsource = 'SS_DOUBLE';

	    pt(80).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(80).paramname = 'P1';
	    pt(80).class     = 'rt_VECTOR';
	    pt(80).nrows     = 1;
	    pt(80).ncols     = 2;
	    pt(80).subsource = 'SS_DOUBLE';

	    pt(81).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(81).paramname = 'P2';
	    pt(81).class     = 'rt_SCALAR';
	    pt(81).nrows     = 1;
	    pt(81).ncols     = 1;
	    pt(81).subsource = 'SS_DOUBLE';

	    pt(82).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(82).paramname = 'P3';
	    pt(82).class     = 'rt_SCALAR';
	    pt(82).nrows     = 1;
	    pt(82).ncols     = 1;
	    pt(82).subsource = 'SS_DOUBLE';

	    pt(83).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(83).paramname = 'P4';
	    pt(83).class     = 'rt_SCALAR';
	    pt(83).nrows     = 1;
	    pt(83).ncols     = 1;
	    pt(83).subsource = 'SS_DOUBLE';

	    pt(84).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(84).paramname = 'P5';
	    pt(84).class     = 'rt_SCALAR';
	    pt(84).nrows     = 1;
	    pt(84).ncols     = 1;
	    pt(84).subsource = 'SS_DOUBLE';

	    pt(85).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(85).paramname = 'P6';
	    pt(85).class     = 'rt_SCALAR';
	    pt(85).nrows     = 1;
	    pt(85).ncols     = 1;
	    pt(85).subsource = 'SS_DOUBLE';

	    pt(86).blockname = 'Subsystem/Shoulder Angle/Bit17';
	    pt(86).paramname = 'P7';
	    pt(86).class     = 'rt_SCALAR';
	    pt(86).nrows     = 1;
	    pt(86).ncols     = 1;
	    pt(86).subsource = 'SS_DOUBLE';

	    pt(87).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(87).paramname = 'P1';
	    pt(87).class     = 'rt_VECTOR';
	    pt(87).nrows     = 1;
	    pt(87).ncols     = 8;
	    pt(87).subsource = 'SS_DOUBLE';

	    pt(88).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(88).paramname = 'P2';
	    pt(88).class     = 'rt_SCALAR';
	    pt(88).nrows     = 1;
	    pt(88).ncols     = 1;
	    pt(88).subsource = 'SS_DOUBLE';

	    pt(89).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(89).paramname = 'P3';
	    pt(89).class     = 'rt_SCALAR';
	    pt(89).nrows     = 1;
	    pt(89).ncols     = 1;
	    pt(89).subsource = 'SS_DOUBLE';

	    pt(90).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(90).paramname = 'P4';
	    pt(90).class     = 'rt_SCALAR';
	    pt(90).nrows     = 1;
	    pt(90).ncols     = 1;
	    pt(90).subsource = 'SS_DOUBLE';

	    pt(91).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(91).paramname = 'P5';
	    pt(91).class     = 'rt_SCALAR';
	    pt(91).nrows     = 1;
	    pt(91).ncols     = 1;
	    pt(91).subsource = 'SS_DOUBLE';

	    pt(92).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(92).paramname = 'P6';
	    pt(92).class     = 'rt_SCALAR';
	    pt(92).nrows     = 1;
	    pt(92).ncols     = 1;
	    pt(92).subsource = 'SS_DOUBLE';

	    pt(93).blockname = 'Subsystem/Shoulder Angle/Bits1-8';
	    pt(93).paramname = 'P7';
	    pt(93).class     = 'rt_SCALAR';
	    pt(93).nrows     = 1;
	    pt(93).ncols     = 1;
	    pt(93).subsource = 'SS_DOUBLE';

	    pt(94).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(94).paramname = 'P1';
	    pt(94).class     = 'rt_VECTOR';
	    pt(94).nrows     = 1;
	    pt(94).ncols     = 8;
	    pt(94).subsource = 'SS_DOUBLE';

	    pt(95).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(95).paramname = 'P2';
	    pt(95).class     = 'rt_SCALAR';
	    pt(95).nrows     = 1;
	    pt(95).ncols     = 1;
	    pt(95).subsource = 'SS_DOUBLE';

	    pt(96).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(96).paramname = 'P3';
	    pt(96).class     = 'rt_SCALAR';
	    pt(96).nrows     = 1;
	    pt(96).ncols     = 1;
	    pt(96).subsource = 'SS_DOUBLE';

	    pt(97).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(97).paramname = 'P4';
	    pt(97).class     = 'rt_SCALAR';
	    pt(97).nrows     = 1;
	    pt(97).ncols     = 1;
	    pt(97).subsource = 'SS_DOUBLE';

	    pt(98).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(98).paramname = 'P5';
	    pt(98).class     = 'rt_SCALAR';
	    pt(98).nrows     = 1;
	    pt(98).ncols     = 1;
	    pt(98).subsource = 'SS_DOUBLE';

	    pt(99).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(99).paramname = 'P6';
	    pt(99).class     = 'rt_SCALAR';
	    pt(99).nrows     = 1;
	    pt(99).ncols     = 1;
	    pt(99).subsource = 'SS_DOUBLE';

	    pt(100).blockname = 'Subsystem/Shoulder Angle/Bits9-16';
	    pt(100).paramname = 'P7';
	    pt(100).class     = 'rt_SCALAR';
	    pt(100).nrows     = 1;
	    pt(100).ncols     = 1;
	    pt(100).subsource = 'SS_DOUBLE';

	    pt(101).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(101).paramname = 'P1';
	    pt(101).class     = 'rt_VECTOR';
	    pt(101).nrows     = 1;
	    pt(101).ncols     = 2;
	    pt(101).subsource = 'SS_DOUBLE';

	    pt(102).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(102).paramname = 'P2';
	    pt(102).class     = 'rt_SCALAR';
	    pt(102).nrows     = 1;
	    pt(102).ncols     = 1;
	    pt(102).subsource = 'SS_DOUBLE';

	    pt(103).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(103).paramname = 'P3';
	    pt(103).class     = 'rt_VECTOR';
	    pt(103).nrows     = 1;
	    pt(103).ncols     = 2;
	    pt(103).subsource = 'SS_DOUBLE';

	    pt(104).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(104).paramname = 'P4';
	    pt(104).class     = 'rt_VECTOR';
	    pt(104).nrows     = 1;
	    pt(104).ncols     = 2;
	    pt(104).subsource = 'SS_DOUBLE';

	    pt(105).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(105).paramname = 'P5';
	    pt(105).class     = 'rt_SCALAR';
	    pt(105).nrows     = 1;
	    pt(105).ncols     = 1;
	    pt(105).subsource = 'SS_DOUBLE';

	    pt(106).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(106).paramname = 'P6';
	    pt(106).class     = 'rt_SCALAR';
	    pt(106).nrows     = 1;
	    pt(106).ncols     = 1;
	    pt(106).subsource = 'SS_DOUBLE';

	    pt(107).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(107).paramname = 'P7';
	    pt(107).class     = 'rt_SCALAR';
	    pt(107).nrows     = 1;
	    pt(107).ncols     = 1;
	    pt(107).subsource = 'SS_DOUBLE';

	    pt(108).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(108).paramname = 'P8';
	    pt(108).class     = 'rt_SCALAR';
	    pt(108).nrows     = 1;
	    pt(108).ncols     = 1;
	    pt(108).subsource = 'SS_DOUBLE';

	    pt(109).blockname = 'Subsystem/Shoulder Angle/Int & OE';
	    pt(109).paramname = 'P9';
	    pt(109).class     = 'rt_SCALAR';
	    pt(109).nrows     = 1;
	    pt(109).ncols     = 1;
	    pt(109).subsource = 'SS_DOUBLE';

	    pt(110).blockname = 'Subsystem/Shoulder Angle/Unit Delay';
	    pt(110).paramname = 'X0';
	    pt(110).class     = 'rt_SCALAR';
	    pt(110).nrows     = 1;
	    pt(110).ncols     = 1;
	    pt(110).subsource = 'SS_DOUBLE';

	    pt(111).blockname = 'Subsystem/Elbow Angle/Parity/Constant';
	    pt(111).paramname = 'Value';
	    pt(111).class     = 'rt_SCALAR';
	    pt(111).nrows     = 1;
	    pt(111).ncols     = 1;
	    pt(111).subsource = 'SS_DOUBLE';

	    pt(112).blockname = 'Subsystem/Shoulder Angle/Parity/Constant';
	    pt(112).paramname = 'Value';
	    pt(112).class     = 'rt_SCALAR';
	    pt(112).nrows     = 1;
	    pt(112).ncols     = 1;
	    pt(112).subsource = 'SS_DOUBLE';

else
  pt = 112;
end
