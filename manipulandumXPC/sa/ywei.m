%______*** MATLAB "M" function (jim Patton) ***_______
% The sets up desirable environment for matlab.
% CALLS :      path.m	(matlab file)
% INITIATED:   10-12-98 by jim patton 
%______________________________________________________

  disp('____________________ startup.m ___________________________ ');
  which ywei
  load ('train');	sound(y,Fs*6); clear y; clear Fs
  matlabpath=([path, ...
		';C:\users\ywei\experiment\resources',...
        ';C:\users\ywei\experiment\resources\dataanalysis',...
         ';C:\users\ywei\experiment\resources\newdataanalysis',...
          ';C:\users\ywei\experiment\resources\usefulfiles',...
        ]);
  path(matlabpath);
  disp('Directories have been are added to the path. Type PATH to see'); 

  global DEBUGIT 
  DEBUGIT = 0;		% default is for no debugging to be done
  disp('DEBUGIT variable has been set to global.');
  
  
disp('________________ END startup.m __________________ ');

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ END ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

