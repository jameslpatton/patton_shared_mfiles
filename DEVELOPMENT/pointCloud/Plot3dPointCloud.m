% Plot3dPointCloud.m   % Plot a cloud of points in 3 d equal axis space

x=load('jimPointCloud2.txt');
L=size(x,1), 
s=100; 
plot3(x(1:s:L,1),x(1:s:L,2),x(1:s:L,3),'.'); 
axis equal;

