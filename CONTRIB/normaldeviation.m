X-Original-To: pattn@merle.it.northwestern.edu
Delivered-To: pattn@merle.it.northwestern.edu
DKIM-Signature: v=1; a=rsa-sha256; c=relaxed/relaxed;
        d=gmail.com; s=gamma;
        h=domainkey-signature:received:received:message-id:date:from:sender
         :to:subject:mime-version:content-type:content-transfer-encoding
         :content-disposition:x-google-sender-auth;
        bh=xGa/XpDtf/wImaLmzKlQIYUFY77MrsySTRauTo+rXbk=;
        b=PF9ROdcmVOj0KM0S9Jo37tEy0Lp77cPueMCnAfwCPFRVEimvp3xMHl61igR0XEspst
         dWNUK8gYZCPbm9TXF1bLR2ikabbrFCWfBqinGZLK/QgwlAENIy+LbnZXIv7q1yoOhQQn
         aR0Zhs1rEIerkRbRM1T5ic2MbrnWyZubeZG6E=
DomainKey-Signature: a=rsa-sha1; c=nofws;
        d=gmail.com; s=gamma;
        h=message-id:date:from:sender:to:subject:mime-version:content-type
         :content-transfer-encoding:content-disposition:x-google-sender-auth;
        b=sjCGxmIAv065hYXwDjoR0Le1SZeUj78FR3jEMg6U+Nsl4dntl97gdZda9y2prLR0R/
         ZkxFbd/Q/JtXt+AFDDVylbHM7yYij72PTg7ppRgWUGVVGHJu8vGnWX5YgI7Tk5cpe/Mr
         GXE+d+DuN/UwD3Mc16dqDuzQbpbnrweZMl27w=
Date: Mon, 23 Jun 2008 17:12:25 -0500
From: "Felix C Huang" <f-huang@northwestern.edu>
Sender: felix.charles.huang@gmail.com
To: "AMIT MEGHANI" <amitmeghani29@gmail.com>,
	"Jim Patton" <j-patton@northwestern.edu>, iansharp2@uic.edu
Subject: normal deviations calculation
X-Google-Sender-Auth: 3811c490b5af18c3

function y=normaldeviation(pos, pointA, pointB)

%Calculate vector of normal deviations from target straight line %PointA, PointB, and pos are class argugments with .x .y and .z elements

% For example
%
% pos.x=x; pos.y=y; pos.z=z;
%
% point_A.x=targetX(1);
% point_A.y=targetY(1);
% point_A.z=targetZ(1);
%
% point_B.x=targetX(end);
% point_B.y=targetY(end);
% point_B.z=targetZ(end);



tlength=length(pos.x);

%Vector of target direction
V_BA=[point_B.x point_B.y point_B.z]-[point_A.x point_A.y point_A.z];

%Normalized target direction
V_BAn=ones(tlength,1)*(V_BA/(V_BA(1)^2+V_BA(2)^2+V_BA(3)^2)^.5);

%Vector of home to data location
V_2A=[[pos.x]-[point_A.x],[pos.y]-[point_A.y],[pos.z]-[point_A.z]];

%Length of V_2A
LL=(V_2A(:,1).^2+V_2A(:,2).^2+V_2A(:,3).^2).^.5;

%dot product angle calc
projectionangle=acos(dot(V_2A', V_BAn')'./LL);


%Output is array of normal deviations away from target path to each %observed data point
y=LL.*sin(projectionangle);

