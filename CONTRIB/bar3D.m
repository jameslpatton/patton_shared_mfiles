function yyy=bar3d(xindep, yindep, zdata)


%In this example, there are 5 unique x and y variables, but there are
%two y-repetitions at each level of x
% 
% xindep =
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
%      1     2     3     4     5
% yindep =
%      1     1     1     1     1
%      2     2     2     2     2
%      3     3     3     3     3
%      4     4     4     4     4
%      5     5     5     5     5
%      1     1     1     1     1
%      2     2     2     2     2
%      3     3     3     3     3
%      4     4     4     4     4
%      5     5     5     5     5
% zindep =
%     0.3840    0.6085    0.0576    0.0841    0.6756
%     0.6831    0.0158    0.3676    0.4544    0.6992
%     0.0928    0.0164    0.6315    0.4418    0.7275
%     0.0353    0.1901    0.7176    0.3533    0.4784
%     0.6124    0.5869    0.6927    0.1536    0.5548
%     0.1210    0.2548    0.2319    0.1909    0.4398
%     0.4508    0.8656    0.2393    0.8439    0.3400
%     0.7159    0.2324    0.0498    0.1739    0.3142
%     0.8928    0.8049    0.0784    0.1708    0.3651
%     0.2731    0.9084    0.6408    0.9943    0.3932

    
    
%Default edgecolor
if nargin<4;     kk='k'; end

P = unique(xindep);
R = unique(yindep);


%Calculate basic size of bar prisms based on rough range of x and y
%independent variables
perc=.9;
xwidth=perc*( max(xindep(:))-min(xindep(:)))/(length(P)-1);
ywidth=perc*( max(yindep(:))-min(yindep(:)))/(length(R)-1);

%Color Scheme

bbggrr=[1 0 0;1 1 0;0 1 0; 0 1 1; 0 0 1; 1 0 0;1 1 0;0 1 0; 0 1 1; 0 0 1];

i=0;
COLS=length(P)+1;
hold on;
for pp=P'
    i=i+1;
    j=0;
    for rr=R'
        j=j+1;
        %Look for all the z-data at the right x an y coordinates
        [xi,xj]=find(xindep==pp);
        [yi,yj]=find(yindep==rr);
        xxx=intersect(xi,yi);
        yyy=intersect(xj,yj);


        %Calculate the mean of the is z-data
        mbb=mean(mean(zdata(xxx,yyy)));
        varlen=length(zdata(xxx,yyy));

        if varlen>1;
            stdbb=std(zdata(xxx,yyy));
        end

        %Make height matrix
        zzs=abs(mbb)*[1 1 1 1 1];
        zzsq=abs(mbb)*[0 0 1 1 0];

        %And the rest of the box
        xxs=[-1 -1 1 1 -1]*xwidth/2+(pp);
        yys=[-1 1 1 -1 -1]*ywidth/2+(rr);
        xxs1=[1 1 1 1 1]*xwidth/2;
        yys1=[-1 1 1 -1 -1]*ywidth/2+(rr);

        xxs2=[-1 1 1 -1 -1]*xwidth/2+(pp);
        yys2=[1 1 1 1 1]*ywidth/2;


        patch(xxs, yys, zzs*0,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)
        patch(+pp+xxs1, yys1, zzsq*1,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)
        patch(+pp-xxs1, yys1, zzsq*1,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)
        patch(xxs2, +yys2+rr, zzsq*1,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)
        patch(xxs2, -yys2+rr, zzsq*1,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)
        patch(xxs, yys, zzs*1,[1 1 1]*.5,'FaceColor',[bbggrr(j,:)]*(1-.65*i/COLS),'EdgeColor',kk)

        if varlen>1
            plot3([mean(xxs) mean(xxs)],[mean(yys) mean(yys)],[mbb, mbb+stdbb],'k-','linew',2)
            plot3([mean(xxs)-xwidth/3, mean(xxs)+xwidth/3],[mean(yys), mean(yys)],[mbb+stdbb, mbb+stdbb],'k-','linew',2)
        end

    end

end

view(3);
grid on
